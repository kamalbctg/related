package com.zuma.db.core.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.IndexColumn;

@Entity
@Table(name = "Company")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Company implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RequestDate", nullable = false, length = 16)
    private Date requestDate;
    @JoinColumn(name = "Franchise", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Franchise franchise;
    @Column(name = "EntityType", length = 20) // Ltd, Co, LLC
    private String entityType;
    // State Incorporated: Displays the state in which the company
    // was chartered as a corporation
    /* The first step in forming a corporation is to file an application of
    incorporation with the state. State incorporation laws differ and
    corporations often organize in those states with the more favorable laws.
    After the application of incorporation has been approved, the state
    grants a charter or articles of incorporation. The articles of
    incorporation formally create the corporation. */
    @JoinColumn(name = "IncorporatedState", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE) //todo:
    private USState incorporatedState;
    @JoinTable(name = "CompanyContact", joinColumns = {@JoinColumn(name = "Company", referencedColumnName = "ID", nullable = false)}, inverseJoinColumns = {@JoinColumn(name = "Contact", referencedColumnName = "ID")}) //,nullable = false
    @ManyToMany
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
        org.hibernate.annotations.CascadeType.ALL})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @IndexColumn(name = "SequenceNumber")
    private List<Contact> contactList = new LinkedList<Contact>();
    @OneToMany(mappedBy = "company")
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
        org.hibernate.annotations.CascadeType.ALL})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Collection<Owner> ownerList = new HashSet<Owner>();
    @OneToMany(mappedBy = "company")
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
        org.hibernate.annotations.CascadeType.ALL})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Collection<Store> storeList = new HashSet<Store>();
    @Basic(optional = false)
    @Column(name = "Caption", nullable = false, length = 100)
    private String caption;
    @Basic(optional = false)
    @Column(name = "ReferredBy", length = 50)
    private String referredBy;
    @Basic(optional = false)
    @Column(name = "Accepted", nullable = false)
    private boolean accepted = false;
    @Column(name = "Deleted", nullable = false)
    private boolean deleted = false;

    {
        contactList.add(new Contact()); //company contact
        contactList.add(new Contact()); //primary contact1
        contactList.add(new Contact()); //primary contact2
    }

    public Company() {
    }

    public Company(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        if (franchise != null && franchise.isNullValue()) {
            this.franchise = null;
        } else {
            this.franchise = franchise;
        }
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public USState getIncorporatedState() {
        return incorporatedState;
    }

    public void setIncorporatedState(USState incorporatedState) {
        if (incorporatedState != null && incorporatedState.isNullValue()) {
            this.incorporatedState = null;
        } else {
            this.incorporatedState = incorporatedState;
        }
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }

    public void normalizeContact(int count) {
        for (int i = count; i < 3; i++) {
            contactList.add(new Contact());
        }
    }

    public void autoNormalizeContact() {
        int cotactsSize = getContactList().size();
        if (cotactsSize < 3) {
            normalizeContact(cotactsSize);
        }
    }

    public Collection<Owner> getOwnerList() {
        return ownerList;
    }

    public void setOwnerList(Collection<Owner> ownerList) {
        this.ownerList = ownerList;
    }

    public Collection<Store> getStoreList() {
        return storeList;
    }

    public void setStoreList(Collection<Store> storeList) {
        this.storeList = storeList;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }
    
    public String getReferredBy() {
        return referredBy;
    }

    public void setReferredBy(String referredBy) {
        this.referredBy = referredBy;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = (getCaption() == null) ? 0 : getCaption().hashCode();
        result = prime * result + ((getFranchise() == null) ? 0 : getFranchise().hashCode());
        result = prime * result + ((getEntityType() == null) ? 0 : getEntityType().hashCode());
        result = prime * result + getRequestDate().hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Company)) {
            return false;
        }
        Company other = (Company) obj;
        if (getCaption() == null) {
            if (other.getCaption() != null) {
                return false;
            }
        } else if (!getCaption().equals(other.getCaption())) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.Company[id=" + getId() + "]";
    }
}
