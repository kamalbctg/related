/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import javax.persistence.Transient;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Contact")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Contact implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    @Column(name = "Caption", length = 50) // for Person/Store/Company name
    private String caption;
    @Column(name = "Address", length = 100)
    private String address;
    @Column(name = "City", length = 50)
    private String city;
    @Column(name = "Country", length = 50)
    private String country;
    @Column(name = "Email", length = 50)
    private String email;
    @Transient // Field only for UI - email confirm
    private String emailConfirm;
    @Column(name = "Phone1", length = 50)
    private String phone1;
    @Column(name = "Phone2", length = 50)
    private String phone2;
    @Column(name = "Fax", length = 50)
    private String fax;
    @JoinColumn(name = "State", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE) //todo:
    private USState state;
    @Column(name = "Zip", length = 50)
    private String zip;

    public Contact() {
    }

    public Contact(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailConfirm() {
        return emailConfirm;
    }

    public void setEmailConfirm(String emailConfirm) {
        this.emailConfirm = emailConfirm;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public USState getState() {
        return state;
    }

    public void setState(USState state) {
        if (state != null && state.isNullValue()) {
            this.state = null;
        } else {
            this.state = state;
        }
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public boolean confirmEmail() {
        return this.email.equals(this.emailConfirm);
    }

    @Override
    public int hashCode() {
        // TODO: Warning - this method won't work in the case the id fields are not set
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contact)) {
            return false;
        }
        Contact other = (Contact) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.Contact[id=" + getId() + "]";
    }
}
