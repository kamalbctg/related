/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.math.BigDecimal;

public class DirectPercentTrackerData extends TrackerData {

    @Override
    public BigDecimal getPercentMTD() {
        return getBaseMTD();
    }

    @Override
    public BigDecimal getPercentYTD() {
        return getBaseYTD();
    }

    @Override
    public BigDecimal getPercentCurrent() {
        return getBaseCurrent();
    }

    public void setPercentMTD(BigDecimal value) {
        setBaseMTD(value);
    }

    public void setPercentYTD(BigDecimal value) {
        setBaseYTD(value);
    }

    public void setPercentCurrent(BigDecimal value) {
        setBaseCurrent(value);
    }
}
