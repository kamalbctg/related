/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zuma.db.core.domain;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "EditableTargetTrackerData")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EditableTargetTrackerData extends TrackerData{
    private static final long serialVersionUID = 1L;
    private Integer id;

    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "Target", nullable = false, precision = 10, scale = 2)
    @Override
    public BigDecimal getTarget() {
        return super.getTarget();
    }

    @JoinColumn(name = "Store", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Override
    public Store getStore() {
        return super.getStore();
    }
    
    @Basic(optional = false)
    @Column(name = "Caption", nullable = false, length = 50)
    @Override
    public String getCaption() {
        return super.getCaption();
    }
    @Basic(optional = false)
    @Column(name = "Date", nullable = false)
    @Temporal(TemporalType.DATE)
    @Override
    public Calendar getDate() {
        return super.getDate();
    }

}
