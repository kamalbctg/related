/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "EditableTrackerData")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EditableTrackerData extends TrackerData {

    private static final long serialVersionUID = 1L;
    private Integer id;
    private boolean fixedCost = true;

    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "Target", nullable = false, precision = 10, scale = 2)
    @Override
    public BigDecimal getTarget() {
        return super.getTarget();
    }

    @Column(name = "CurrentValue", nullable = false, precision = 10, scale = 2)
    @Override
    public BigDecimal getActualCurrent() {
        return super.getActualCurrent();
    }

    @Column(name = "MTDValue", nullable = false, precision = 10, scale = 2)
    @Override
    public BigDecimal getActualMTD() {
        return super.getActualMTD();
    }

    @Column(name = "YTDValue", nullable = false, precision = 10, scale = 2)
    @Override
    public BigDecimal getActualYTD() {
        return super.getActualYTD();
    }

    @JoinColumn(name = "Store", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @Override
    public Store getStore() {
        return super.getStore();
    }

    @Basic(optional = false)
    @Column(name = "Caption", nullable = false, length = 50)
    @Override
    public String getCaption() {
        return super.getCaption();
    }
    @Basic(optional = false)
    @Column(name = "Date", nullable = false)
    @Temporal(TemporalType.DATE)
    @Override
    public Calendar getDate() {
        return super.getDate();
    }
    
    @Column(name = "FixedCost", nullable = false)
    public boolean isFixedCost() {
        return fixedCost;
    }

    public void setFixedCost(boolean fixedCost) {
        this.fixedCost = fixedCost;
    }

}
