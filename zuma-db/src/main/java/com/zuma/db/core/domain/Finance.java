package com.zuma.db.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Finance")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Finance implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
	@GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @JoinColumn(name = "Company", referencedColumnName = "ID", nullable = false)
    @OneToOne(optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
        org.hibernate.annotations.CascadeType.ALL})
    private Company company;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "FiscalDate", nullable = false, length = 16)
    private Date fiscalDate;

    @Column(name = "FederalTaxID", length = 20)
    private String federalTaxID;
    @Column(name = "StateTaxID", length = 20)
    private String stateTaxID;
    @Column(name = "CityTaxID", length = 20)
    private String cityTaxID;
    @Column(name = "CountyTaxID", length = 20)
    private String countryTaxID;

    @Column(name = "StateRate", precision = 5, scale = 2)
    private BigDecimal stateRate;
    @Column(name = "CityRate", precision = 5, scale = 2)
    private BigDecimal cityRate;
    @Column(name = "CountryRate", precision = 5, scale = 2)
    private BigDecimal countryRate;

    @Column(name = "RoyaltyRate", precision = 5, scale = 2)
    private BigDecimal royaltyRate;
    @Column(name = "AdvertisingRate", precision = 5, scale = 2)
    private BigDecimal advertisingRate;

    public Finance() {
    }

    public Finance(Integer id) {
        this.id = id;
    }

    public Finance(Company company) {
        this.company = company;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public Date getFiscalDate() {
        return fiscalDate;
    }

    public void setFiscalDate(Date fiscalDate) {
        this.fiscalDate = fiscalDate;
    }
    
    public String getFederalTaxID() {
        return federalTaxID;
    }

    public void setFederalTaxID(String federalTaxID) {
        this.federalTaxID = federalTaxID;
    }

    public String getStateTaxID() {
        return stateTaxID;
    }

    public void setStateTaxID(String stateTaxID) {
        this.stateTaxID = stateTaxID;
    }

    public String getCityTaxID() {
        return cityTaxID;
    }

    public void setCityTaxID(String cityTaxID) {
        this.cityTaxID = cityTaxID;
    }

    public String getCountryTaxID() {
        return countryTaxID;
    }

    public void setCountryTaxID(String countryTaxID) {
        this.countryTaxID = countryTaxID;
    }

    public BigDecimal getStateRate() {
        return stateRate;
    }

    public void setStateRate(BigDecimal stateRate) {
        this.stateRate = stateRate;
    }

    public BigDecimal getCityRate() {
        return cityRate;
    }

    public void setCityRate(BigDecimal cityRate) {
        this.cityRate = cityRate;
    }

    public BigDecimal getCountryRate() {
        return countryRate;
    }

    public void setCountryRate(BigDecimal countryRate) {
        this.countryRate = countryRate;
    }

    public BigDecimal getRoyaltyRate() {
        return royaltyRate;
    }

    public void setRoyaltyRate(BigDecimal royaltyRate) {
        this.royaltyRate = royaltyRate;
    }

    public BigDecimal getAdvertisingRate() {
        return advertisingRate;
    }

    public void setAdvertisingRate(BigDecimal advertisingRate) {
        this.advertisingRate = advertisingRate;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Finance)) {
            return false;
        }
        Owner other = (Owner) object;
        if ((this.getId() == null && other.getId() != null) ||
                (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.Finance[id=" + getId() + "]";
    }
}
