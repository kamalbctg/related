package com.zuma.db.core.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Folder")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Folder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    @Column(name = "Name", length = 255)
    private String name;
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "Parent", nullable = true)
    private Folder parent;
    @JoinColumn(name = "Store", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Store store;
    @Column(name = "Uploaded", nullable = false, columnDefinition = "bit default 0")
    private boolean uploaded = false;
   /* @OneToMany(mappedBy = "folder")
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
        org.hibernate.annotations.CascadeType.ALL})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Collection<PersonFolder> personFolderList = new HashSet<PersonFolder>();
*/
  /*  public Collection<PersonFolder> getPersonFolderList() {
        return personFolderList;
    }

    public void setPersonFolderList(Collection<PersonFolder> personFolderList) {
        this.personFolderList = personFolderList;
    }
*/
    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Folder getParent() {
        return parent;
    }

    public void setParent(Folder parent) {
        this.parent = parent;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public static List<Folder> getDefaultFolder() {
        List<Folder> folders = new ArrayList<Folder>();

        Folder fldr = new Folder();

        fldr.name = "Financial Statements";
        fldr.uploaded = true;
        folders.add(fldr);

        fldr = new Folder();
        fldr.name = "Daily Summaries";
        fldr.uploaded = false;
        folders.add(fldr);

        fldr = new Folder();
        fldr.name = "Purchase Journals";
        fldr.uploaded = false;
        folders.add(fldr);

        fldr = new Folder();
        fldr.name = "Inventory Summaries";
        fldr.uploaded = false;
        folders.add(fldr);

        fldr = new Folder();
        fldr.name = "Waste Tracking";
        fldr.uploaded = false;
        folders.add(fldr);

        fldr = new Folder();
        fldr.name = "Payroll Statements";
        fldr.uploaded = true;
        folders.add(fldr);

        fldr = new Folder();
        fldr.name = "Bank Statements";
        fldr.uploaded = true;
        folders.add(fldr);

        fldr = new Folder();
        fldr.name = "Other";
        fldr.uploaded = true;
        folders.add(fldr);
        return folders;
    }

    	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

    	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Folder)) {
			return false;
		}
		Folder other = (Folder) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.zuma.db.core.domain.Folder[id=" + id + "]";
	}
}
