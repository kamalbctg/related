/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Franchise")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Franchise implements Serializable, Comparable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Caption", nullable = false, length = 50)
    private String caption;

    public Franchise() {
    }

    public Franchise(Integer id) {
        this.id = id;
    }

    public Franchise(Integer id, String caption) {
        this.id = id;
        this.caption = caption;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public int hashCode() {
        int hash = (getId() != null) ? getId().hashCode() : 0;
        hash = hash*31 + getCaption().hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Franchise)) {
            return false;
        }
        Franchise second = (Franchise) obj;

        if ((this.getId() == null && second.getId() != null) ||
                (this.getId() != null && !this.getId().equals(second.getId()))) {
            return false;
        }
        // if id equals - need compare other fields
        if (getCaption() == null) {
            if (second.getCaption() != null) {
                return false;
            }
        } else if (!getCaption().equals(second.getCaption())) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.Franchise[id=" + getId() + "]";
    }

    @Override
    public int compareTo(Object obj) {
        if (obj == this) {
            return 0;
        }
        Franchise second = (Franchise) obj;
        int result = 0;
        if (this.getId() != null && second.getId() != null) {
            result = this.getId() - second.getId();
        }
        return result;
    }

    public boolean isNullValue()
    {
        return caption.equalsIgnoreCase("null");
    }
}
