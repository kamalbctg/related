package com.zuma.db.core.domain;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "InventoryMeasure")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class InventoryMeasure implements java.io.Serializable {
	@Id
	@GeneratedValue(generator = "inc")
	@GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;

	@Column(name = "Measure", nullable = false, length = 50)
	private String measureName;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "inventoryMeasure")
	@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
	private List<InventoryItem> inventoryItems = new LinkedList<InventoryItem>();

	public InventoryMeasure() {
	}

	public InventoryMeasure(String measure) {
		setMeasureName(measure);
	}

	public String getMeasureName() {
		return measureName;
	}

	public void setMeasureName(String measureName) {
		this.measureName = measureName;
	}

	public InventoryMeasure(String measure, List<InventoryItem> inventoryItems) {

		this.measureName = measure;
		this.inventoryItems = inventoryItems;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<InventoryItem> getInventoryItems() {
		return this.inventoryItems;
	}

	public void setInventoryItems(List<InventoryItem> inventoryItems) {
		this.inventoryItems = inventoryItems;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((getMeasureName() == null) ? 0 : getMeasureName().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof InventoryMeasure)) {
			return false;
		}
		InventoryMeasure other = (InventoryMeasure) obj;
		if (getMeasureName() == null) {
			if (other.getMeasureName() != null) {
				return false;
			}
		} else if (!getMeasureName().equals(other.getMeasureName())) {
			return false;
		}
		return true;
	}
}
