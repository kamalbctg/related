package com.zuma.db.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "InventoryStartData")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class InventoryStartData implements Serializable {

    private static final long serialVersionUID = 1L;
    private Integer id;

    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", nullable = false)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    private BigDecimal paperValue = BigDecimal.ZERO;
    private BigDecimal foodValue = BigDecimal.ZERO;
    private Store store;

    public InventoryStartData() {
    }

    public InventoryStartData(Store store) {
        this.store = store;
    }

    @Column(name = "PaperValue", nullable = false, precision = 10, scale = 2)
    public BigDecimal getPaperValue() {
        return paperValue;
    }

    public void setPaperValue(BigDecimal paperValue) {
        this.paperValue = paperValue;
    }

    @Column(name = "FoodValue", nullable = false, precision = 10, scale = 2)
    public BigDecimal getFoodValue() {
        return foodValue;
    }

    public void setFoodValue(BigDecimal foodValue) {
        this.foodValue = foodValue;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "Store", nullable = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
}
