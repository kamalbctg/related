/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zuma.db.core.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "InvoiceAccount")
public class InvoiceAccount implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Caption", nullable = false, length = 50)
    private String caption;
    @Basic(optional = false)
    @Column(name = "Type", nullable = false, length = 50)
    private String type;

    public InvoiceAccount() {
    }

    public InvoiceAccount(Integer id) {
        this.id = id;
    }

    public InvoiceAccount(Integer id, String caption, String type) {
        this.id = id;
        this.caption = caption;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvoiceAccount)) {
            return false;
        }
        InvoiceAccount other = (InvoiceAccount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.InvoiceAccount[id=" + id + "]";
    }

}
