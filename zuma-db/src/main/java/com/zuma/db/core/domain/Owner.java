/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zuma.db.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "Owner")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Owner implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
	@GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    @Column(name = "FirstName", length = 50)
    private String firstName;
    @Column(name = "LastName", length = 50)
    private String lastName;
    @Basic(optional = false)
    @Column(name = "Loss", nullable = false, precision = 5, scale = 2)
    private BigDecimal loss;
    @Basic(optional = false)
    @Column(name = "NumberOfShares", nullable = false)
    private int numberOfShares;
    @Basic(optional = false)
    @Column(name = "Profit", nullable = false, precision = 5, scale = 2)
    private BigDecimal profit;
    @Basic(optional = false)
    @Column(name = "SharePercent", nullable = false, precision = 5, scale = 2)
    private BigDecimal sharePercent;
    @JoinColumn(name = "Company", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Company company;
    @Column(name = "Deleted", nullable = false)
    private boolean deleted = false;

    public Owner() {
    }

    public Owner(Integer id) {
        this.id = id;
    }

    public Owner(Integer id, BigDecimal loss, int numberOfShares, BigDecimal profit, BigDecimal sharePercent) {
        this.id = id;
        this.loss = loss;
        this.numberOfShares = numberOfShares;
        this.profit = profit;
        this.sharePercent = sharePercent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BigDecimal getLoss() {
        return loss;
    }

    public void setLoss(BigDecimal loss) {
        this.loss = loss;
    }

    public int getNumberOfShares() {
        return numberOfShares;
    }

    public void setNumberOfShares(int numberOfShares) {
        this.numberOfShares = numberOfShares;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public void setProfit(BigDecimal profit) {
        this.profit = profit;
    }

    public BigDecimal getSharePercent() {
        return sharePercent;
    }

    public void setSharePercent(BigDecimal sharePercent) {
        this.sharePercent = sharePercent;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Owner)) {
            return false;
        }
        Owner other = (Owner) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.Owner[id=" + id + "]";
    }

}
