/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Page")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Page implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "inc")
	@GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
	@Column(name = "ID", unique = true, nullable = false)
	private Integer id;
	@Basic(optional = false)
	@Column(name = "Caption", length = 50)
	private String caption;
	@Basic(optional = false)
	@Column(name = "URL", nullable = false, length = 50)
	private String url;
	@Basic(optional = false)
	@Column(name = "Role", nullable = false)
	// 0 - disabled by default for users, but overloaded via PersonPage
	// 1 - enabled for all registered users
	// 2 - enabled only for admins
        // 3 - disabled for direct access - system page, include or other!
	private int role = 0;

	public Page() {
	}

	public Page(Integer id) {
		this.id = id;
	}

	public Page(Integer id, String url) {
		this.id = id;
		this.url = url;
	}

	public Page(Integer id, String url, String caption, int role) {
		this.id = id;
		this.url = url;
		this.caption = caption;
		this.role = role;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCaption() {
		return caption;
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Page)) {
			return false;
		}
		Page other = (Page) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.zuma.db.core.domain.Page[id=" + id + "]";
	}
}
