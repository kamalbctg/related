/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import com.zuma.db.service.util.MD5;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Person")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    @Column(name = "FirstName", length = 50)
    private String firstName;
    @Column(name = "LastName", length = 50)
    private String lastName;
    @Basic(optional = false)
    @Column(name = "Login",unique=true, nullable = false, length = 50)
    private String login;
    @Basic(optional = false)
    @Column(name = "Password", nullable = false, length = 50)
    private String password;
    @Basic(optional = false)
    @Column(name = "Role", nullable = false)
    // 0 - disabled
    // 1 - user
    // 2 - admin
    private int role = 0; // for new user = 0, after activation = 1
        /*
    @JoinTable(name = "PersonContact", joinColumns = { @JoinColumn(name = "Person", referencedColumnName = "ID", nullable = false) }, inverseJoinColumns = { @JoinColumn(name = "Contact", referencedColumnName = "ID", nullable = false) })
    @ManyToMany
    @org.hibernate.annotations.Cascade( {
    org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
    org.hibernate.annotations.CascadeType.ALL })
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Collection<Contact> contactList = new HashSet<Contact>();*
     */
    @JoinColumn(name = "Contact", referencedColumnName = "ID")
    @OneToOne(optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
        org.hibernate.annotations.CascadeType.ALL})
    private Contact contact;
    @JoinTable(name = "PersonStore", joinColumns = {@JoinColumn(name = "Person", referencedColumnName = "ID", nullable = false)}, inverseJoinColumns = {@JoinColumn(name = "Store", referencedColumnName = "ID", nullable = false)})
    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Collection<Store> storeList = new HashSet<Store>();
    @OneToMany(mappedBy = "person")
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
        org.hibernate.annotations.CascadeType.ALL})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Collection<PersonPage> personPages = new HashSet<PersonPage>();
    @JoinColumn(name = "Company", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Company company;
    @OneToMany(mappedBy = "person")
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
        org.hibernate.annotations.CascadeType.ALL})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Collection<PersonFolder> personFolders = new HashSet<PersonFolder>();
    @Column(name = "Deleted", nullable = false)
    private boolean deleted = false;

    public Person() {
        contact = new Contact();
    }

    public Person(Integer id) {
        this.id = id;
    }

    public Person(Integer id, String login, String password, int role) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = MD5.encode(password);
    }

    public void setPasswordDirect(String password) {
        this.password = password;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Collection<Store> getStoreList() {
        return storeList;
    }

    public void setStoreList(Collection<Store> storeList) {
        this.storeList = storeList;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are
        // not set
        if (!(object instanceof Person)) {
            return false;
        }
        Person other = (Person) object;
        if ((this.getId() == null && other.getId() != null) ||
            (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.Person[id=" + getId() + "]";
    }

    public Collection<PersonPage> getPersonPageList() {
        return personPages;
    }

    public void setPersonPageList(Collection<PersonPage> personPageList) {
        this.personPages = personPageList;
    }

    // access = 0 - only read
    // access = 1 - read/write
    public void setAccess(Page page, int access) {
        boolean exist = false;
        for (Iterator<PersonPage> it = personPages.iterator(); it.hasNext();) {
            PersonPage personPage = it.next();
            if (personPage.getPage().equals(page)) {
                personPage.setAccess(access);
                exist = true;
                break;
            }
        }
        if (!exist) {
            personPages.add(new PersonPage(page, access));
        }
    }

    public void setWriteAccess(Page page) {
        setAccess(page, 1);
    }

    public void setReadAccess(Page page) {
        setAccess(page, 0);
    }

    public void denyAccess(Page page) {
        for (Iterator<PersonPage> it = personPages.iterator(); it.hasNext();) {
            PersonPage personPage = it.next();
            if (personPage.getPage().equals(page)) {
                personPages.remove(personPage);
            }
        }
    }

    // -1 = no access
    // 0 - read access
    // 1 - write access
    // 2 - admin acccess
    public int getAccessLevel(Page page) {
        if (isAdmin()) {
            return 2;
        }
        if (role == 0) {
            return -1; // disabled account
        }
        int result = -1;
        for (Iterator<PersonPage> it = personPages.iterator(); it.hasNext();) {
            PersonPage personPage = it.next();
            if (personPage.getPage().equals(page)) {
                result = personPage.getAccess();
                break;
            }
        }
        return result;
    }

    public boolean isAdmin() {
        return role == 2;
    }

    public Collection<PersonFolder> getPersonFolders() {
        return personFolders;
    }

    public void setPersonFolders(Collection<PersonFolder> personFolders) {
        this.personFolders = personFolders;
    }
}
