/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zuma.db.core.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "PersonPage")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PersonPage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
	@GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Access", nullable = false)
    private int access=1;
    @JoinColumn(name = "Page", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Page page;
    @JoinColumn(name = "Person", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Person person;

    public PersonPage() {
    }

    public PersonPage(Integer id) {
        this.id = id;
    }

    public PersonPage(Integer id, int access) {
        this.id = id;
        this.access = access;
    }
    public PersonPage(Page page, int access) {
        this.page = page;
        this.access = access;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAccess() {
        return access;
    }

    public void setAccess(int access) {
        this.access = access;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonPage)) {
            return false;
        }
        PersonPage other = (PersonPage) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.PersonPage[id=" + id + "]";
    }

}
