/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Purchase")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Purchase implements Serializable, Comparable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "Vendor", length = 100)
    private String vendor;
    @Basic(optional = false)
    // TODO: After DEMO need to revert this changes for Invoice
    // @Column(name = "Invoice", nullable = false, length = 50)
    @Column(name = "Invoice", length = 50)
    private String invoice;
    @Column(name = "Description", length = 400)
    private String description;
    @Column(name = "Payment", length = 100)
    private String payment;
    // Invoice Total	= FoodAndBeverage+Paper+FAndBTaxes+OtherTaxes+Freight+Other;
    // All [nullable = false] was removed
    @Basic(optional = false)
    @Column(name = "FoodAndBeverage", nullable = false, precision = 10, scale = 2)
    private BigDecimal foodAndBeverage;
    @Basic(optional = false)
    @Column(name = "Paper", nullable = false, precision = 10, scale = 2)
    private BigDecimal paper;
    @Basic(optional = false)
    @Column(name = "FAndBTaxes", nullable = false, precision = 10, scale = 2)
    private BigDecimal fAndBTaxes;
    @Basic(optional = false)
    @Column(name = "OtherTaxes", nullable = false, precision = 10, scale = 2)
    private BigDecimal otherTaxes;
    @Basic(optional = false)
    @Column(name = "Freight", nullable = false, precision = 10, scale = 2)
    private BigDecimal freight;
    @Basic(optional = false)
    @Column(name = "Other", nullable = false, precision = 10, scale = 2)
    private BigDecimal other;
    @JoinColumn(name = "Store", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Store store;
    // TODO: Cascade delete fo all puchases for store

    // field only for UI
    @Transient
    private boolean visible = true;
    
    public Purchase() {
    }

    public Purchase(Integer id) {
        this.id = id;
    }

    public Purchase(Date date, 
                    String vendor, String description, String payment, String invoice,
                    BigDecimal foodAndBeverage, BigDecimal paper, BigDecimal fAndBTaxes,
                    BigDecimal otherTaxes, BigDecimal freight, BigDecimal other) {
        // this.id
        this.date = date;
        
        this.vendor = vendor;
        this.description = description;
        this.payment = payment;
        this.invoice = invoice;
        
        this.foodAndBeverage = foodAndBeverage;
        this.paper = paper;
        this.fAndBTaxes = fAndBTaxes;
        this.otherTaxes = otherTaxes;
        this.freight = freight;
        this.other = other;
        // this.store
    }

    public void zeroDecimal() {
        this.foodAndBeverage = BigDecimal.ZERO;
        this.paper = BigDecimal.ZERO;
        this.fAndBTaxes = BigDecimal.ZERO;
        this.otherTaxes = BigDecimal.ZERO;
        this.freight = BigDecimal.ZERO;
        this.other = BigDecimal.ZERO;
    }

    public void nullDecimal() {
        this.foodAndBeverage = null;
        this.paper = null;
        this.fAndBTaxes = null;
        this.otherTaxes = null;
        this.freight = null;
        this.other = null;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        Calendar cl = Calendar.getInstance();
        cl.set(1000, 0, 1, 0, 0, 0);
        if (date.after(cl.getTime() ))
            this.date = date;
        else
        {
            cl.setTime(date);
            cl.add(Calendar.YEAR,2000);
            this.date =cl.getTime();

        }
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public BigDecimal getFoodAndBeverage() {
        return foodAndBeverage;
    }

    public void setFoodAndBeverage(BigDecimal foodAndBeverage) {
        this.foodAndBeverage = foodAndBeverage;
    }

    public BigDecimal getPaper() {
        return paper;
    }

    public void setPaper(BigDecimal paper) {
        this.paper = paper;
    }

    public BigDecimal getFAndBTaxes() {
        return fAndBTaxes;
    }

    public void setFAndBTaxes(BigDecimal fAndBTaxes) {
        this.fAndBTaxes = fAndBTaxes;
    }

    public BigDecimal getOtherTaxes() {
        return otherTaxes;
    }

    public void setOtherTaxes(BigDecimal otherTaxes) {
        this.otherTaxes = otherTaxes;
    }

    public BigDecimal getFreight() {
        return freight;
    }

    public void setFreight(BigDecimal freight) {
        this.freight = freight;
    }

    public BigDecimal getOther() {
        return other;
    }

    public void setOther(BigDecimal other) {
        this.other = other;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public BigDecimal getInvoiceTotal() {
        BigDecimal total = BigDecimal.ZERO;
        if (foodAndBeverage!=null) total = total.add(foodAndBeverage);
        if (paper!=null) total = total.add(paper);
        if (fAndBTaxes!=null) total = total.add(fAndBTaxes);
        if (otherTaxes!=null) total = total.add(otherTaxes);
        if (freight!=null) total = total.add(freight);
        if (other!=null) total = total.add(other);
        return total;
    }

    //TODO: Need better approach to calculate hashCode - Look Store.java
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        hash += getDescription().hashCode();
        hash += getVendor().hashCode();
        hash += getInvoice().hashCode();
        hash += getPayment().hashCode();
        hash += getInvoiceTotal().hashCode();
        hash += (getDate() != null ? getDate().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Purchase)) {
			return false;
		}
		Purchase second = (Purchase) obj;

                if ((this.getId() == null && second.getId() != null) ||
                    (this.getId() != null && !this.getId().equals(second.getId()))) {
                    return false;
                }
                // if id equals - need compare other fields
                if (getVendor() == null) {
			if (second.getVendor() != null) {
				return false;
			}
		} else if (!getVendor().equals(second.getVendor())) {
			return false;
		}

		if (getDescription() == null) {
			if (second.getDescription() != null) {
				return false;
			}
		} else if (!getDescription().equals(second.getDescription())) {
			return false;
		}

                if (getInvoice() == null) {
			if (second.getInvoice() != null) {
				return false;
			}
		} else if (!getInvoice().equals(second.getInvoice())) {
			return false;
		}

                if (getPayment() == null) {
			if (second.getPayment() != null) {
				return false;
			}
		} else if (!getPayment().equals(second.getPayment())) {
			return false;
		}
                //TODO: Need to test equals for BigDecimal
                if (!this.getInvoiceTotal().equals(second.getInvoiceTotal()))
                        return false;

                return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.Purchase[id=" + getId() + "]";
    }

    @Override
    public int compareTo(Object obj) {
        if(obj == this)
            return 0;
        Purchase second = (Purchase)obj;
        int result = 0;
        if(this.getId()!=null && second.getId()!=null)
            result = this.getId() - second.getId();
        return result;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
