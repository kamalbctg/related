package com.zuma.db.core.domain;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;
import java.sql.SQLException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import javassist.bytecode.stackmap.BasicBlock.Catch;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "ReportFile")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ReportFile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    @Column(name = "Name", length = 40)
    private String name;
    @Column(name = "SizeFile")
    private Integer sizeFile;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DateReport")
    private Date dateReport; // date of begin ReportingWeek
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DateFile")
    private Date dateFile;  // date of File submitting
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "Folder", nullable = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Folder folder;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "Person", nullable = false, referencedColumnName = "ID")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Person person;

    public Date getDateReport() {
        return dateReport;
    }
    public Date getDateWeekReport() {
        return DateUtils.addDays(dateReport, 6);
    }

    public void setDateReport(Date dateReport) {
        this.dateReport = dateReport;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
  
    @Lob
    private byte[] report;
    @Column(name = "report")

    public Date getDateFile() {
        return dateFile;
    }


    public void setDateFile(Date dateFile) {
        this.dateFile = dateFile;
    }

    public int getSizeFile() {
        return sizeFile;
    }
    
    public int getSizeFileKb() {
        return sizeFile/1024;
    }

    public void setSizeFile(int sizeFile) {
        this.sizeFile = sizeFile;
    }

    public String getFileType() {
         String ext=(getName().substring(getName().lastIndexOf(".") + 1)).substring(0, 3);
        if (!(ext.equalsIgnoreCase("xls") || ext.equalsIgnoreCase("pdf") ||
              ext.equalsIgnoreCase("doc")|| ext.equalsIgnoreCase("ppt")))
             ext="txt";
        return  ext;       
    }

    public byte[] getReport() {
        return uncompressByteArray(report);
    }

    public void setReport(byte[] report) {
        this.report = compressByteArray(report);
    }

    // Don't invoke this. Used by Hibernate only.
    public void setReportBlob(Blob reportBlob) {
        this.report = this.toByteArray(reportBlob);
    }

    // Don't invoke this. Used by Hibernate only.
    public Blob getReportBlob() {
        return Hibernate.createBlob(this.report);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    private byte[] toByteArray(Blob fromBlob) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            return toByteArrayImpl(fromBlob, baos);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (IOException ex) {
                     Logger log = Logger.getRootLogger();
                    log.info("I/O Stream Exception", ex);
                }
            }
        }
    }

    private byte[] toByteArrayImpl(Blob fromBlob, ByteArrayOutputStream baos)
            throws SQLException, IOException {
        byte[] buf = new byte[1024];
        InputStream is = fromBlob.getBinaryStream();
        try {
            for (;;) {
                int dataSize = is.read(buf);

                if (dataSize == -1) {
                    break;
                }
                baos.write(buf, 0, dataSize);
            }
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ex) {
                    Logger log = Logger.getRootLogger();
                    log.info("I/O Stream Exception", ex);
                }
            }
        }
        return baos.toByteArray();
    }

        public static byte[] compressByteArray(byte[] input)  {
         // Create the compressor with highest level of compression
         Deflater compressor = new Deflater();
        compressor.setLevel(Deflater.BEST_COMPRESSION);
        // Give the compressor the data to compress

        compressor.setInput(input);
        compressor.finish();

        ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length);
        // Compress the data
        byte[] buf = new byte[1024];
        try {
        while (!compressor.finished()) {
            int count = compressor.deflate(buf);
            bos.write(buf, 0, count);
         }
        }finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException ex) {
                    Logger log = Logger.getRootLogger();
                    log.info("I/O Stream Exception", ex);
                }
            }
        }
        // Get the compressed data
        byte[] compressedData = bos.toByteArray();

        return compressedData;
    }

     public static byte[] uncompressByteArray(byte[] compressedData)  {
           // Create the decompressor and give it the data to compress
        Inflater decompressor = new Inflater();
        decompressor.setInput(compressedData);
        // Create an expandable byte array to hold the decompressed data
        ByteArrayOutputStream bos = new ByteArrayOutputStream(compressedData.length);
        // Decompress the data
        byte[] buf = new byte[1024];
        try{
        try{
        while (!decompressor.finished()) {

                int count = decompressor.inflate(buf);
                bos.write(buf, 0, count);
       }
        }
        catch(DataFormatException edf)
        {
            Logger log = Logger.getRootLogger();
                    log.info("Data Format Decompress Exception exception", edf);
        }
         
         }finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException ex) {
                     Logger log = Logger.getRootLogger();
                    log.info("I/O Stream Exception", ex);
                }
            }
        }
        // Get the decompressed data
        byte[] decompressedData = bos.toByteArray();

        return decompressedData;
    }

}
