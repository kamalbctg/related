/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "RetailWasteData")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RetailWasteData implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Calendar date = Calendar.getInstance();
    @Basic(optional = false)
    @Column(name = "Value", nullable = false, precision = 8, scale = 2)
    private BigDecimal value = new BigDecimal(0);
    @JoinColumn(name = "RetailWasteItem", referencedColumnName = "ID",  nullable = false)
    @ManyToOne(optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private RetailWasteItem item;

    public RetailWasteData() {
    }

    public RetailWasteData(Calendar tmp, RetailWasteItem item) {
        date = tmp;
        setItem(item);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public RetailWasteItem getItem() {
        return item;
    }

    public void setItem(RetailWasteItem item) {
        this.item = item;
     /*   if (!item.getDailyData().contains(this)) {
            item.getDailyData().add(this);
        }
      */
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RetailWasteData other = (RetailWasteData) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.date != other.date && (this.date == null || !this.date.equals(other.date))) {
            return false;
        }
        if (this.value != other.value && (this.value == null || !this.value.equals(other.value))) {
            return false;
        }
        if (this.item != other.item && (this.item == null || !this.item.equals(other.item))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = this.getClass().getName().hashCode();
        hash = 67 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
        hash = 67 * hash + (this.getDate() != null ? this.getDate().hashCode() : 0);
        hash = 67 * hash + (this.getValue() != null ? this.getValue().hashCode() : 0);
        hash = 67 * hash + (this.getItem() != null ? this.getItem().hashCode() : 0);
        return hash;
    }
}
