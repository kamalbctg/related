/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "RetailWasteItem")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class RetailWasteItem implements Serializable, Comparable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = true)
    @Column(name = "Caption", nullable=true, length = 200)
    private String caption;
    @Basic(optional = false)
    @Column(name = "UnitCost", nullable = false, precision = 8, scale = 2)
    private BigDecimal unitCost = new BigDecimal(0);
    @JoinColumn(name = "Store", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Store store;
    @Basic(optional = false)
    @Column(name = "Date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Calendar date = Calendar.getInstance();
    @Basic(optional = false)
    @Column(name = "SequenceNumber", nullable = false)
    private int sequenceNumber;
    @OneToMany(mappedBy = "item")
    
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Collection<RetailWasteData> dataList = new HashSet<RetailWasteData>();

    public RetailWasteItem() {
    }

    public RetailWasteItem(Calendar date) {
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public BigDecimal getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(BigDecimal unitCost) {
        this.unitCost = unitCost;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    @Override
    public int compareTo(Object o) {
        RetailWasteItem item = (RetailWasteItem) o;

        int result = this.getSequenceNumber() - item.getSequenceNumber();
        if (result == 0) {
            if (result == 0 && this.getId() != null && item.getId() != null) {
                result = this.getId() - item.getId();
            }
        }
        return result;
    }

    /**
     * @return the sequenceNumber
     */
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * @param sequenceNumber the sequenceNumber to set
     */
    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    @Override
    public int hashCode() {
        int hash = this.getClass().getName().hashCode();
        hash = 41 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
        //  hash = 41 * hash + (this.getStore() != null ? this.getStore().hashCode() : 0);
        hash = 41 * hash + (this.getDate() != null ? this.getDate().hashCode() : 0);
        hash = 41 * hash + this.sequenceNumber;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RetailWasteItem other = (RetailWasteItem) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.unitCost != other.unitCost && (this.unitCost == null || !this.unitCost.equals(other.unitCost))) {
            return false;
        }
        if (this.store != other.store && (this.store == null || !this.store.equals(other.store))) {
            return false;
        }
        if (this.date != other.date && (this.date == null || !this.date.equals(other.date))) {
            return false;
        }
        if (this.sequenceNumber != other.sequenceNumber) {
            return false;
        }
        return true;
    }

    /**
     * @return the dataList
     */
    public Collection<RetailWasteData> getDataList() {
        return dataList;
    }

    /**
     * @param dataList the dataList to set
     */
    public void setDataList(Collection<RetailWasteData> dataList) {
        this.dataList = dataList;
    }
}
