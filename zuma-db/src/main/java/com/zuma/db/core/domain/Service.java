package com.zuma.db.core.domain;

import java.io.Serializable;
import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "Service")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Service implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
	@GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;

    @JoinColumn(name = "Company", referencedColumnName = "ID", nullable = false)
    @OneToOne(optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
        org.hibernate.annotations.CascadeType.ALL})
    private Company company;

    @Column(name = "DeliveryType", nullable = false)
    private Integer deliveryType = 0;
    // 0 - Electronic delivery only
    // 1 - Company Address
    // 2 - Store Address
    // 3 - Other Address = Contact not null

    @JoinColumn(name = "OtherContact", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    private Contact otherContact;

    @Column(name = "PayByCheck", nullable = false, columnDefinition = "bit default 1")
    private boolean payByCheck = true;
    @Column(name = "PayByCreditCard", nullable = false, columnDefinition = "bit default 0")
    private boolean payByCreditCard = false;

    @Column(name = "NameOnAccount", length = 20)
    private String nameOnAccount;
    @Column(name = "RoutingNumber", length = 20)
    private String routingNumber;
    @Column(name = "AccountNumber", length = 20)
    private String accountNumber;

    public Service() {
    }

    public Service(Integer id) {
        this.id = id;
    }

    public Service(Company company) {
        this.company = company;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Integer getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(Integer deliveryType) {
        this.deliveryType = deliveryType;
    }

    public Contact getOtherContact() {
        return otherContact;
    }

    public void setOtherContact(Contact otherContact) {
        this.otherContact = otherContact;
    }

    public boolean isPayByCheck() {
        return payByCheck;
    }

    public void setPayByCheck(boolean payByCheck) {
        this.payByCheck = payByCheck;
    }

    public boolean isPayByCreditCard() {
        return payByCreditCard;
    }

    public void setPayByCreditCard(boolean payByCreditCard) {
        this.payByCreditCard = payByCreditCard;
    }

    public String getNameOnAccount() {
        return nameOnAccount;
    }

    public void setNameOnAccount(String nameOnAccount) {
        this.nameOnAccount = nameOnAccount;
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Service)) {
            return false;
        }
        Owner other = (Owner) object;
        if ((this.id == null && other.getId() != null) ||
                (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.Service[id=" + getId() + "]";
    }
}
