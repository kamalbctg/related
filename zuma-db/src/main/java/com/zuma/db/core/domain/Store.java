/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.IndexColumn;

@Entity
@Table(name = "Store")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Store implements Serializable, Comparable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", unique = true, nullable = false)
    private Integer id;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RequestDate", nullable = false, length = 16)
    private Date requestDate;
    @JoinColumn(name = "Franchise", referencedColumnName = "ID")
    @ManyToOne(optional = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Franchise franchise;
    @Column(name = "IfOther", length = 50)
    private String ifOther;
    @Column(name = "Number", length = 50)
    private String number;
    @JoinTable(name = "StoreContact", joinColumns = {
        @JoinColumn(name = "Store", referencedColumnName = "ID", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "Contact", referencedColumnName = "ID")}) //,nullable = false
    @ManyToMany
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
        org.hibernate.annotations.CascadeType.ALL})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @IndexColumn(name = "SequenceNumber")
    private List<Contact> contactList = new LinkedList<Contact>();
    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "store")
    @Cascade(value = {org.hibernate.annotations.CascadeType.ALL,
        org.hibernate.annotations.CascadeType.DELETE_ORPHAN})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Collection<InventoryCategory> inventoryItemList = new HashSet<InventoryCategory>();
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "store")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Collection<SummaryCategory> summaryItemList = new HashSet<SummaryCategory>();
    @JoinColumn(name = "Company", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Company company;
    /*
     * @OneToMany(cascade = CascadeType.ALL, mappedBy = "store") private
     * List<WasteRetailWaste> wasteRetailWasteList;
     */
    @Basic(optional = false)
    @Column(name = "Caption", nullable = false, length = 100)
    private String caption;
    @Basic(optional = false)
    @Column(name = "Accepted", nullable = false)
    private boolean accepted = false;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "store")
    private Collection<Purchase> purchaseList = new HashSet<Purchase>();
    @JoinTable(name = "PersonStore", joinColumns = {
        @JoinColumn(name = "Store", referencedColumnName = "ID", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "Person", referencedColumnName = "ID", nullable = false)})
    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Collection<Person> personList = new HashSet<Person>();
    @Column(name = "Deleted", nullable = false)
    private boolean deleted = false;

    {
        contactList.add(new Contact()); //store contact
        contactList.add(new Contact()); //primary contact1
        contactList.add(new Contact()); //primary contact2
    }

    public Store() {
    }

    public Store(Integer id) {
        this.id = id;
    }

    public Store(Integer id, Franchise franchise, String number) {
        this.id = id;
        this.franchise = franchise;
        this.number = number;
    }

    public Store(Integer id, Franchise franchise, String number, String caption) {
        this.id = id;
        this.franchise = franchise;
        this.number = number;
        this.caption = caption;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        if (franchise != null && franchise.isNullValue()) {
            this.franchise = null;
        } else {
            this.franchise = franchise;
        }
    }

    public Collection<InventoryCategory> getInventoryItemList() {
        return inventoryItemList;
    }

    public void setInventoryItemList(
            Collection<InventoryCategory> inventoryItemList) {
        this.inventoryItemList = inventoryItemList;
    }

    public String getIfOther() {
        return ifOther;
    }

    public void setIfOther(String ifOther) {
        this.ifOther = ifOther;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<Contact> getContactList() {
        return contactList;
    }

    public void setContactList(List<Contact> contactList) {
        this.contactList = contactList;
    }

    public void normalizeContact(int count) {
        for (int i = count; i < 3; i++) {
            contactList.add(new Contact());
        }
    }

    public void autoNormalizeContact() {
        int cotactsSize = getContactList().size();
        if (cotactsSize < 3) {
            normalizeContact(cotactsSize);
        }
    }

    public Collection<SummaryCategory> getSummaryItemList() {
        return summaryItemList;
    }

    public void setSummaryItemList(Collection<SummaryCategory> summaryItemList) {
        this.summaryItemList = summaryItemList;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public Collection<Purchase> getPurchaseList() {
        return purchaseList;
    }

    public void setPurchaseList(Collection<Purchase> purchaseList) {
        this.purchaseList = purchaseList;
    }

    public Collection<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(Collection<Person> personList) {
        this.personList = personList;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /*
     * public List<WasteRetailWaste> getWasteRetailWasteList() { return
     * wasteRetailWasteList; }
     *
     * public void setWasteRetailWasteList(List<WasteRetailWaste>
     * wasteRetailWasteList) { this.wasteRetailWasteList = wasteRetailWasteList;
     * }
     */
    @Override
    public String toString() {
        return "com.zuma.db.core.domain.Store[id=" + getId() + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = (getCaption() == null) ? 0 : getCaption().hashCode();
        result = prime * result + ((getCompany() == null) ? 0 : getCompany().hashCode());
        result = prime * result + ((getFranchise() == null) ? 0 : getFranchise().hashCode());
        result = prime * result + getRequestDate().hashCode();
        result = prime * result + ((getNumber() == null) ? 0 : getNumber().hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Store)) {
            return false;
        }
        Store other = (Store) obj;

        if ((this.getId() == null && other.getId() != null)
                || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        // if id equals - need compare other fields
        if (getCompany() == null) {
            if (other.getCompany() != null) {
                return false;
            }
        } else {
            Company comp1 = getCompany();
            Company comp2 = other.getCompany();
            if (!comp1.equals(comp2)) {
                return false;
            }
        }

        if (getNumber() == null) {
            if (other.getNumber() != null) {
                return false;
            }
        } else if (!getNumber().equals(other.getNumber())) {
            return false;
        }

        if (getCaption() == null) {
            if (other.getCaption() != null) {
                return false;
            }
        } else if (!getCaption().equals(other.getCaption())) {
            return false;
        }

        return true;
    }

    @Override
    public int compareTo(Object o) {
        if (o == this) {
            return 0;
        }
        Store second = (Store) o;
        int result = 0;
        if (this.getId() != null && second.getId() != null) {
            result = this.getId() - second.getId();
        }
        return result;
    }
}
