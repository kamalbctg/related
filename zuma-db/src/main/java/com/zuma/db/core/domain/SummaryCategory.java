/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.IndexColumn;

@Entity
@Table(name = "SummaryCategory")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SummaryCategory extends TreeNodeParent implements Serializable, Comparable, Cloneable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Caption", nullable = false, length = 50)
    private String caption;
    @Basic(optional = false)
    @Column(name = "SequenceNumber", nullable = false)
    private int sequenceNumber;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "summaryCategory")
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @IndexColumn(name = "SequenceNumber")
    private List<SummarySubCategory> summarySubCategoryList = new LinkedList<SummarySubCategory>();
    @Column(name = "Deleted", nullable = false)
    private boolean deleted = false;
    @JoinColumn(name = "Store", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Store store;

    public SummaryCategory() {
    }

    public SummaryCategory(Integer id) {
        this.id = id;
    }

    public SummaryCategory(Integer id, String caption, int sequenceNumber) {
        this.id = id;
        this.caption = caption;
        this.sequenceNumber = sequenceNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }
    /*
    public List<SummarySubCategory> getSummarySubCategoryList() {
    return summarySubCategoryList;
    }

    public void setSummarySubCategoryList(List<SummarySubCategory> summarySubCategoryList) {
    this.summarySubCategoryList = summarySubCategoryList;
    }
     */

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (this == object) {
            return true;
        }
        if (!(object instanceof SummaryCategory)) {
            return false;
        }
        SummaryCategory other = (SummaryCategory) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        if (this.getSequenceNumber() != other.getSequenceNumber()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.SummaryCategory[id=" + id + "]";
    }

    @Override
    public int compareTo(Object o) {
        if (o == this) {
            return 0;
        }
        SummaryCategory category = (SummaryCategory) o;
        int result = this.getSequenceNumber() - category.getSequenceNumber();
        if (result == 0 && (this.getId() != null && category.getId() != null)) {
            result = this.getId() - category.getId();
        }
        return result;
    }

    public List<SummarySubCategory> getSummarySubCategoryList() {
        return summarySubCategoryList;
    }

    public void setSummarySubCategoryList(List<SummarySubCategory> summarySubCategoryList) {
        this.summarySubCategoryList.clear();
        this.summarySubCategoryList.addAll(summarySubCategoryList);
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    @Override
    public SummaryCategory clone() {
        SummaryCategory result = null;
        try {
            result = (SummaryCategory) super.clone();

        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(SummaryItem.class.getName()).log(Level.ERROR, null, ex);
        }
        result.setId(null);
        result.summarySubCategoryList = new LinkedList<SummarySubCategory>();
        Collection<SummarySubCategory> subCategories = getSummarySubCategoryList();
        for (SummarySubCategory subCat : subCategories) {
            if (subCat!=null && !subCat.isDeleted()) {
                SummarySubCategory tmp = subCat.clone();
                tmp.setSummaryCategory(result);
                result.getSummarySubCategoryList().add(tmp);
            }
        }
        return result;
    }
}
