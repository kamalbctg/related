/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "SummaryData")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SummaryData implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Calendar date = Calendar.getInstance();
    @Basic(optional = false)
    @Column(name = "Value", nullable = false, precision = 10, scale = 4)
    private BigDecimal value;
    @JoinColumn(name = "SummaryItem", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private SummaryItem summaryItem;

    public SummaryData() {
        value = new BigDecimal(0);
        value = value.setScale(4,RoundingMode.HALF_UP);
    }

    public SummaryData(Integer id) {
        this();
        this.id = id;
    }

    public SummaryData(Calendar date) {
        this();
        this.date = date;
    }

    public SummaryData(Calendar date, SummaryItem summaryItem) {
        this();
        this.date = date;
        this.summaryItem = summaryItem;
    }

    public SummaryData(Integer id, Calendar date, BigDecimal value) {
        this();
        this.id = id;
        this.date = date;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
        if (this.value != null) {
            this.value = this.value.setScale(4,RoundingMode.HALF_UP);
        }
    }

    public SummaryItem getSummaryItem() {
        return summaryItem;
    }

    public void setSummaryItem(SummaryItem summaryItem) {
        this.summaryItem = summaryItem;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SummaryData)) {
            return false;
        }
        SummaryData other = (SummaryData) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.SummaryData[id=" + id + "]";
    }

    public String getValueText() {

        return getSummaryItem().convertValueToText(getValue());
    }

    public void setValueText(String value) throws Exception {
        BigDecimal tmp = getSummaryItem().convertTextToValue(value);
        setValue(tmp);
    }
}
