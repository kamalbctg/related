/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Collection;
import java.util.HashSet;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "SummaryItem")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SummaryItem extends TreeNodeParent implements Serializable, Comparable, Cloneable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Caption", nullable = false, length = 50)
    private String caption;
    @Column(name = "EditedCaption", length = 100)
    private String editedCaption;
    @Basic(optional = false)
    @Column(name = "SequenceNumber", nullable = false)
    private int sequenceNumber;
/*    @JoinColumn(name = "Store", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Store store;

 */
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "SummarySubCategory", nullable = false)
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.SAVE_UPDATE,
        org.hibernate.annotations.CascadeType.MERGE
    })
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private SummarySubCategory summarySubCategory;
    @OneToMany(mappedBy = "summaryItem")
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE_ORPHAN,
        org.hibernate.annotations.CascadeType.ALL})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Collection<SummaryData> summaryDataList = new HashSet<SummaryData>();
    @Column(name = "Formula", length = 500)
    private String formula;
    @Column(name = "PrevDaysDepended", nullable = false)
    private boolean prevDaysDepended = false;
    @Column(name = "Pattern", length = 20)
    private String pattern = "$######0.00";     //pattern for data representing
    @Column(name = "AllowsException", nullable = false)
    private boolean allowsException = false;    //sing to indicate that calculated field allows formula exceptions
    @Column(name = "Deleted", nullable = false)
    private boolean deleted = false;

    public SummaryItem() {
    }

    public SummaryItem(Integer id) {
        this.id = id;
    }

    public SummaryItem(Integer id, String caption, int sequenceNumber) {
        this.id = id;
        this.caption = caption;
        this.sequenceNumber = sequenceNumber;
    }

    @Override
    public SummaryItem clone() {
        SummaryItem item = null;
        try {
            item = (SummaryItem) super.clone();

        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(SummaryItem.class.getName()).log(Level.SEVERE, null, ex);
        }
        item.setId(null);
        item.setSummaryDataList(new HashSet<SummaryData>());
        return item;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getEditedCaption() {
        return editedCaption;
    }

    public void setEditedCaption(String editedCaption) {
        this.editedCaption = editedCaption;
    }

    @Override
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

/*    public Store getStore() {
        return store;
    }

*/
    public SummarySubCategory getSummarySubCategory() {
        return summarySubCategory;
    }

    public void setSummarySubCategory(SummarySubCategory summarySubCategory) {
        this.summarySubCategory = summarySubCategory;
    }

    public Collection<SummaryData> getSummaryDataList() {
        return summaryDataList;
    }

    public void setSummaryDataList(Collection<SummaryData> summaryDataList) {
        this.summaryDataList = summaryDataList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {

        if (this == object) {
            return true;
        }
        if (!(object instanceof SummaryItem)) {
            return false;
        }
        SummaryItem other = (SummaryItem) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        if (!this.getSummarySubCategory().equals(other.getSummarySubCategory())) {
            return false;
        }
      /*  if (!this.getStore().equals(other.getStore())) {
            return false;
        }
       */
        if (this.getSequenceNumber() != other.getSequenceNumber()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.SummaryItem[id=" + id + "]";
    }

    @Override
    public int compareTo(Object o) {
        SummaryItem item = (SummaryItem) o;

        int result = getSummarySubCategory().compareTo(
                item.getSummarySubCategory());
        if (result == 0) {
            result = this.getSequenceNumber() - item.getSequenceNumber();
        }
        if (result == 0 && this.getId() != null && item.getId() != null) {
            result = this.getId() - item.getId();
        }
        return result;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public boolean isPrevDaysDepended() {
        return prevDaysDepended;
    }

    public void setPrevDaysDepended(boolean prevDaysDepended) {
        this.prevDaysDepended = prevDaysDepended;
    }

    public boolean isCalculated() {
        return !(formula == null || formula.isEmpty());
    }

    /**
     * @return the pattern
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * @param pattern the pattern to set
     */
    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    /**
     * @return the allowsException
     */
    public boolean isAllowsException() {
        return allowsException;
    }

    /**
     * @param allowsException the allowsException to set
     */
    public void setAllowsException(boolean allowsException) {
        this.allowsException = allowsException;
    }

    public String convertValueToText(BigDecimal value) {
        if (value == null) {
            return "NA";
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat parser = new DecimalFormat(getPattern(), symbols);
        parser.setRoundingMode(RoundingMode.HALF_UP);
        return parser.format(value);

    }

    public BigDecimal convertTextToValue(String value) throws Exception {
        BigDecimal result = null;
        if (value == null || value.isEmpty()) {
            value = "0";
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        DecimalFormat parser = new DecimalFormat(getPattern(), symbols);
        parser.setParseIntegerOnly(false);
        Number tmp = null;
        try {
            tmp = parser.parse(value);
            result = BigDecimal.valueOf(tmp.doubleValue());
        } catch (ParseException ex) {
            try {
                String newPattern = getPattern().replace("$", "").replace("%", "");
                parser.applyPattern(newPattern);
                tmp = parser.parse(value);
                result = BigDecimal.valueOf(tmp.doubleValue());

            } catch (ParseException e) {
                throw new Exception("Invalid input for item ' " + getEditedCaption() + "'");
            }
        }
        return result;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
