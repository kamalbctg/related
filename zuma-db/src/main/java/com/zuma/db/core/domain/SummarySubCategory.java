/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "SummarySubCategory")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SummarySubCategory extends TreeNodeParent implements Serializable, Comparable, Cloneable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Caption", nullable = false, length = 50)
    private String caption;
    @Column(name = "EditedName", length = 100)
    private String editedName;
    @Basic(optional = false)
    @Column(name = "SequenceNumber", nullable = false)
    private int sequenceNumber;
    @Basic(optional = false)
    @Column(name = "ShowCaption", nullable = false)
    private boolean showCaption = false;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "summarySubCategory")
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.DELETE})
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    //@IndexColumn(name = "SequenceNumber")
    private Collection<SummaryItem> summaryItemList = new LinkedList<SummaryItem>();
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SummaryCategory", nullable = false)
    @org.hibernate.annotations.Cascade({
        org.hibernate.annotations.CascadeType.SAVE_UPDATE,
        org.hibernate.annotations.CascadeType.MERGE
    })
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private SummaryCategory summaryCategory;
    @Column(name = "Deleted", nullable = false)
    private boolean deleted = false;
    @Column(name = "BreakBefore", nullable = false)
    private boolean breakBefore = false;

    public SummarySubCategory() {
    }

    public SummarySubCategory(Integer id) {
        this.id = id;
    }

    public SummarySubCategory(Integer id, String caption, int number) {
        this.id = id;
        this.caption = caption;
        this.sequenceNumber = number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getEditedName() {
        return editedName;
    }

    public void setEditedName(String editedName) {
        this.editedName = editedName;
    }

    @Override
    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int number) {
        this.sequenceNumber = number;
    }

    public Collection<SummaryItem> getSummaryItemList() {
        return summaryItemList;
    }

    public void setSummaryItemList(Collection<SummaryItem> summaryItemList) {
        this.summaryItemList = summaryItemList;
    }

    public SummaryCategory getSummaryCategory() {
        return summaryCategory;
    }

    public void setSummaryCategory(SummaryCategory summaryCategory) {
        this.summaryCategory = summaryCategory;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof SummarySubCategory)) {
            return false;
        }
        SummarySubCategory other = (SummarySubCategory) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        if (!this.getSummaryCategory().equals(other.getSummaryCategory())) {
            return false;
        }
        if (this.getSequenceNumber() != other.getSequenceNumber()) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.SummarySubCategory[id=" + id + "]";
    }

    /**
     * @return the showCaption
     */
    public boolean isShowCaption() {
        return showCaption;
    }

    /**
     * @param showCaption
     *            the showCaption to set
     */
    public void setShowCaption(boolean showCaption) {
        this.showCaption = showCaption;
    }

    @Override
    public int compareTo(Object o) {
        SummarySubCategory subCategory = (SummarySubCategory) o;
        int result = getSummaryCategory().compareTo(
                subCategory.getSummaryCategory());
        if (result == 0) {
            result = this.getSequenceNumber() - subCategory.getSequenceNumber();
        }
        if (result == 0) {
            result = this.getSummaryCategory().getSequenceNumber() - subCategory.getSummaryCategory().getSequenceNumber();
        }
        if (result == 0 && this.getId() != null && subCategory.getId() != null) {
            result = this.getId() - subCategory.getId();
        }
        return result;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isBreakBefore() {
        return breakBefore;
    }

    public void setBreakBefore(boolean breakBefore) {
        this.breakBefore = breakBefore;
    }

    @Override
    public SummarySubCategory clone() {
        SummarySubCategory result = null;
        try {
            result = (SummarySubCategory) super.clone();

        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(SummaryItem.class.getName()).log(Level.ERROR, null, ex);
        }
        result.setId(null);
        result.setSummaryItemList(new LinkedList<SummaryItem>());
        for (SummaryItem item : summaryItemList) {
            SummaryItem tmp = item.clone();
            tmp.setSummarySubCategory(result);
            result.getSummaryItemList().add(tmp);
        }
        return result;
    }
}
