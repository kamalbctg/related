/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;

public class TrackerData implements Serializable {

    private Store store;
    private Calendar date = Calendar.getInstance();
    protected String caption;
    protected BigDecimal target = BigDecimal.ZERO;
    protected BigDecimal actualCurrent = BigDecimal.ZERO;
    private BigDecimal baseCurrent = BigDecimal.ZERO;
    protected BigDecimal actualMTD = BigDecimal.ZERO;
    private BigDecimal baseMTD = BigDecimal.ZERO;
    protected BigDecimal actualYTD = BigDecimal.ZERO;
    private BigDecimal baseYTD = BigDecimal.ZERO;
    private BigDecimal calculatedTarget = BigDecimal.ZERO;

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public BigDecimal getTarget() {
        return target;
    }

    public void setTarget(BigDecimal value) {
        if (value == null) {
            value = BigDecimal.ZERO;
        }
        this.target = value;
    }

    public BigDecimal getActualCurrent() {
        return actualCurrent;
    }

    public void setActualCurrent(BigDecimal value) {
        if (value == null) {
            value = BigDecimal.ZERO;
        }
        this.actualCurrent = value;
    }

    public BigDecimal getPercentCurrent() {
        BigDecimal tmp = getBaseCurrent();
        if (tmp.equals(BigDecimal.ZERO)) {
            return BigDecimal.ZERO;
        }
        return getActualCurrent().setScale(4,RoundingMode.HALF_UP).divide(tmp, 4, RoundingMode.HALF_UP);
    }

    public BigDecimal getActualMTD() {
        return actualMTD;
    }

    public void setActualMTD(BigDecimal value) {
        if (value == null) {
            value = BigDecimal.ZERO;
        }
        this.actualMTD = value;
    }

    public BigDecimal getPercentMTD() {
        BigDecimal tmp = getBaseMTD();
        if (tmp.equals(BigDecimal.ZERO)) {
            return BigDecimal.ZERO;
        }
        return getActualMTD().setScale(4,RoundingMode.HALF_UP).divide(tmp, 4, RoundingMode.HALF_UP);
    }

    public BigDecimal getActualYTD() {
        return actualYTD;
    }

    public void setActualYTD(BigDecimal value) {
        if (value == null) {
            value = BigDecimal.ZERO;
        }
        this.actualYTD = value;
    }

    public BigDecimal getPercentYTD() {
        BigDecimal tmp = getBaseYTD();
        if (tmp.equals(BigDecimal.ZERO)) {
            return BigDecimal.ZERO;
        }
        return getActualYTD().setScale(4,RoundingMode.HALF_UP).divide(tmp, 4, RoundingMode.HALF_UP);
    }

    public BigDecimal getBaseCurrent() {
        return baseCurrent;
    }

    public void setBaseCurrent(BigDecimal value) {
        if (value == null) {
            value = BigDecimal.ZERO;
        }
        this.baseCurrent = value;
    }

    public BigDecimal getBaseMTD() {
        return baseMTD;
    }

    public void setBaseMTD(BigDecimal value) {
        if (value == null) {
            value = BigDecimal.ZERO;
        }
        this.baseMTD = value;
    }

    public BigDecimal getBaseYTD() {
        return baseYTD;
    }

    public void setBaseYTD(BigDecimal value) {
        if (value == null) {
            value = BigDecimal.ZERO;
        }
        this.baseYTD = value;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public BigDecimal getActual(int index) throws Exception {
        if (index < 0 || index > 2) {
            throw new Exception("Incorrect index value.");
        }
        switch (index) {
            case 0:
                return getActualCurrent();
            case 1:
                return getActualMTD();
            case 2:
                return getActualYTD();
            default:
                return BigDecimal.ZERO;
        }
    }
    public BigDecimal getPercent(int index) throws Exception {
        if (index < 0 || index > 2) {
            throw new Exception("Incorrect index value.");
        }
        switch (index) {
            case 0:
                return getPercentCurrent();
            case 1:
                return getPercentMTD();
            case 2:
                return getPercentYTD();
            default:
                return BigDecimal.ZERO;
        }
    }

    public BigDecimal getCalculatedTarget() {
        return calculatedTarget;
    }

    public void setCalculatedTarget(BigDecimal calculatedTarget) {
        this.calculatedTarget = calculatedTarget;
    }

}
