package com.zuma.db.core.domain;


public abstract class TreeNodeParent {
    public abstract int getSequenceNumber();
    public abstract void setSequenceNumber(int val);
    public abstract String getCaption();
}
