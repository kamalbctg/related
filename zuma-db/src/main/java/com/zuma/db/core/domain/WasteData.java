/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "WasteData")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WasteData implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Basic(optional = false)
    @Column(name = "Date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Calendar date = Calendar.getInstance();
    @Basic(optional = false)
    @Column(name = "Value", nullable = false, precision = 8, scale = 2)
    private BigDecimal value = new BigDecimal(0);
    @JoinColumn(name = "InventoryItem", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private InventoryItem inventoryItem;

    public WasteData() {
    }

    public WasteData(Integer id) {
        this.id = id;
    }

    public WasteData(Calendar date) {
        this.date = date;
    }

    public WasteData(Calendar date, InventoryItem inventoryItem) {
        this.date = date;
        this.inventoryItem = inventoryItem;
    }

    public WasteData(Integer id, Calendar date, BigDecimal value) {
        this.id = id;
        this.date = date;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public InventoryItem getInventoryItem() {
        return inventoryItem;
    }

    public void setInventoryItem(InventoryItem inventoryItem) {
        this.inventoryItem = inventoryItem;
    }

    @Override
    public int hashCode() {
        int hash = this.getClass().getName().hashCode();
        hash = 53 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
        hash = 53 * hash + (this.getDate() != null ? this.getDate().hashCode() : 0);
        hash = 53 * hash + (this.getValue() != null ? this.getValue().hashCode() : 0);
        hash = 53 * hash + (this.getInventoryItem() != null ? this.getInventoryItem().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are
        // not set
        if (!(object instanceof WasteData)) {
            return false;
        }
        WasteData other = (WasteData) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.zuma.db.core.domain.WasteData[id=" + id + "]";
    }
}
