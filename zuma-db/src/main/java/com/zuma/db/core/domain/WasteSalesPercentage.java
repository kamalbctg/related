/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "WasteSalesPercentage")
@org.hibernate.annotations.Entity(selectBeforeUpdate = true)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class WasteSalesPercentage implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "inc")
    @GenericGenerator(name = "inc", strategy = "org.hibernate.id.IncrementGenerator")
    @Column(name = "ID", nullable = false)
    private Integer id;
    @JoinColumn(name = "Store", referencedColumnName = "ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Store store;
    @Basic(optional = false)
    @Column(name = "Date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Calendar date = Calendar.getInstance();
    @Basic(optional = false)
    @Column(name = "Value", nullable = false, precision = 8, scale = 2)
    private BigDecimal value = new BigDecimal(0);

    public WasteSalesPercentage()
    {
    }
    public WasteSalesPercentage(Store store)
    {
        this.store = store;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final WasteSalesPercentage other = (WasteSalesPercentage) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if (this.store != other.store && (this.store == null || !this.store.equals(other.store))) {
            return false;
        }
        if (this.date != other.date && (this.date == null || !this.date.equals(other.date))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = this.getClass().getName().hashCode();
        hash = 73 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
    //    hash = 73 * hash + ((this.getStore() != null && this.getStore().getId()!=null) ?
    //            this.getStore().getId().hashCode() : 0);
        hash = 73 * hash + (this.getDate() != null ? this.getDate().hashCode() : 0);
        return hash;
    }

}
