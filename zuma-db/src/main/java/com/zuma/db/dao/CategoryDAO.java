package com.zuma.db.dao;

import java.math.BigDecimal;
import java.util.List;

import com.zuma.db.core.domain.InventoryCategory;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;

public interface CategoryDAO extends GenericDAO<InventoryCategory, Integer> {
	List<InventoryCategory> getAll(Store store, ReportingWeek reportingWeek);

	List<InventoryCategory> getAll(Store store);
        List<InventoryCategory> getAllNotDeleted(Store store);
	public BigDecimal getTotalCost(InventoryCategory inventoryCategory,
			ReportingWeek reportingWeek);
}
