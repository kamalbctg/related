package com.zuma.db.dao;

import com.zuma.db.core.domain.Company;
import java.util.List;

public interface CompanyDAO extends GenericDAO<Company, Integer> {
        public List<Company> getAccepted(boolean mode); // pending/existing list
        public List<Company> getAll(int count, String like); // get only first count items
        public List<Company> getFull(); // get only first count items
}
