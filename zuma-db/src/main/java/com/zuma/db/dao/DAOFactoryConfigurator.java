package com.zuma.db.dao;

import com.zuma.db.core.domain.Company;
import com.zuma.db.core.domain.Franchise;
import com.zuma.db.core.domain.USState;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zuma.db.core.domain.Contact;
import com.zuma.db.core.domain.Dictionary;
import com.zuma.db.core.domain.InventoryMeasure;
import com.zuma.db.core.domain.InventorySubCategory;
import com.zuma.db.core.domain.Owner;
import com.zuma.db.core.domain.Page;
import com.zuma.db.core.domain.Person;
import com.zuma.db.core.domain.PersonPage;
import com.zuma.db.core.domain.Purchase;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import com.zuma.db.core.domain.SummaryCategory;
import com.zuma.db.core.domain.SummaryData;
import com.zuma.db.core.domain.SummaryItem;
import com.zuma.db.core.domain.SummarySubCategory;
import com.zuma.db.dao.impl.hibernate.GenericHibernateDAO;
import com.zuma.db.dao.impl.hibernate.UniversalHibernateDAO;

@Configuration
public class DAOFactoryConfigurator {
	@Bean
	public GenericHibernateDAO<Dictionary, Integer> dictionaryDAO() {
		return new GenericHibernateDAO<Dictionary, Integer>(Dictionary.class);
	}

        @Bean
	public GenericHibernateDAO<Franchise, Integer> franchiseDAO() {
		return new GenericHibernateDAO<Franchise, Integer>(Franchise.class);
	}

        @Bean
	public GenericHibernateDAO<USState, Integer> stateDAO() {
		return new GenericHibernateDAO<USState, Integer>(USState.class);
	}

	@Bean
	public GenericHibernateDAO<InventorySubCategory, Integer> subCategoryDAO() {
		return new GenericHibernateDAO<InventorySubCategory, Integer>(
				InventorySubCategory.class);
	}

	@Bean
	public GenericHibernateDAO<Contact, Integer> contactDAO() {
		return new GenericHibernateDAO<Contact, Integer>(Contact.class);
	}

	@Bean
	public GenericHibernateDAO<Owner, Integer> ownerDAO() {
		return new GenericHibernateDAO<Owner, Integer>(Owner.class);
	}

	@Bean
	public GenericHibernateDAO<Purchase, Integer> purchaseDAO() {
		return new GenericHibernateDAO<Purchase, Integer>(Purchase.class);
	}

	@Bean
	public GenericHibernateDAO<ReportingWeek, Integer> reportingWeekDAO() {
		return new GenericHibernateDAO<ReportingWeek, Integer>(
				ReportingWeek.class);
	}

	@Bean
	public GenericHibernateDAO<ReportingWeek, Integer> weekDAO() {
		return new GenericHibernateDAO<ReportingWeek, Integer>(
				ReportingWeek.class);
	}

	@Bean
	public GenericHibernateDAO<Store, Integer> storeDAO() {
		return new GenericHibernateDAO<Store, Integer>(Store.class);
	}
/*
        @Bean
	public GenericHibernateDAO<Company, Integer> companyDAO() {
		return new GenericHibernateDAO<Company, Integer>(Company.class);
	}
*/
	@Bean
	public GenericHibernateDAO<SummaryCategory, Integer> summaryCategoryDAO() {
		return new GenericHibernateDAO<SummaryCategory, Integer>(
				SummaryCategory.class);
	}

	@Bean
	public GenericHibernateDAO<SummaryData, Integer> summaryDataDAO() {
		return new GenericHibernateDAO<SummaryData, Integer>(SummaryData.class);
	}

	@Bean
	public GenericHibernateDAO<SummaryItem, Integer> summaryItemDAO() {
		return new GenericHibernateDAO<SummaryItem, Integer>(SummaryItem.class);
	}

	@Bean
	public GenericHibernateDAO<SummarySubCategory, Integer> summarySubCategoryDAO() {
		return new GenericHibernateDAO<SummarySubCategory, Integer>(
				SummarySubCategory.class);
	}

	@Bean
	public GenericHibernateDAO<Person, Integer> personDAO() {
		return new GenericHibernateDAO<Person, Integer>(Person.class);
	}

	@Bean
	public GenericHibernateDAO<PersonPage, Integer> personPageDAO() {
		return new GenericHibernateDAO<PersonPage, Integer>(PersonPage.class);
	}

	@Bean
	public GenericHibernateDAO<Page, Integer> pageDAO() {
		return new GenericHibernateDAO<Page, Integer>(Page.class);
	}

	@Bean
	public GenericHibernateDAO<InventoryMeasure, Integer> inventoryMeasureDAO() {
		return new GenericHibernateDAO<InventoryMeasure, Integer>(
				InventoryMeasure.class);
	}

	@Bean
	public UniversalDAO universalDAO() {
		return new UniversalHibernateDAO();
	}
}
