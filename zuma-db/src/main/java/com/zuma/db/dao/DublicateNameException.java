package com.zuma.db.dao;

public class DublicateNameException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DublicateNameException(String message, Throwable cause) {
		super(message, cause);
	}

	public DublicateNameException() {
	}
}
