package com.zuma.db.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDAO<T, PK extends Serializable> extends Serializable {

	/** Persist the newInstance object into database */
	PK save(T newInstance);

	/**
	 * Retrieve an object that was previously persisted to the database using
	 * the indicated id as primary key
	 */
	T findById(PK id);

	/** Save changes made to a persistent object. */
	void update(T transientObject);

	/** Remove an object from persistent storage in the database */
	void delete(T persistentObject);

	public void saveOrUpdate(T o);

	public int countAll();

	public List<T> getAll();

	public T merge(T o);

}
