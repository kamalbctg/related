package com.zuma.db.dao;

import java.util.List;

public interface GenericSQLProcessorDAO {
	List<String> execute(String sqlCommnd);
}
