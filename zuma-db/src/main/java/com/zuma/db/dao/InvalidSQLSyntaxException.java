package com.zuma.db.dao;

public class InvalidSQLSyntaxException extends RuntimeException {
	public InvalidSQLSyntaxException(String reason, Exception e) {
		super(reason, e);
	}
}
