package com.zuma.db.dao;

import java.util.List;

import com.zuma.db.core.domain.InventoryItem;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;

public interface InventoryItemDAO extends GenericDAO<InventoryItem, Integer> {
	public List<InventoryItem> getAll(Store store, ReportingWeek reportingWeek);

}
