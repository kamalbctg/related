package com.zuma.db.dao;

import java.util.List;

import com.zuma.db.core.domain.InventoryWeeklyRecord;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;

public interface InventoryWeeklyRecordDAO extends
		GenericDAO<InventoryWeeklyRecord, Integer> {
	public List<InventoryWeeklyRecord> getAll(Store store,
			ReportingWeek reportingWeek);

}
