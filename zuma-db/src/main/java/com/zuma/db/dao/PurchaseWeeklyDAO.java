package com.zuma.db.dao;

import java.util.Date;
import java.util.List;
import com.zuma.db.core.domain.Purchase;
import com.zuma.db.core.domain.Store;

public interface PurchaseWeeklyDAO extends GenericDAO<Purchase, Integer> {
        public List<Purchase> getAll(Store store, Date reportFrom, Date reportTo);
}
