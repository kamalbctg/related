package com.zuma.db.dao;


import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import java.util.List;

public interface ReportingWeekDAO extends GenericDAO<ReportingWeek, Integer> {
	ReportingWeek getFirstWeek();

	ReportingWeek getLastWeek();
        
        List<ReportingWeek> getAllUnlocked(Store store);
}
