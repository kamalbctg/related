/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

public interface UniversalDAO extends Serializable {

    public <T> T findById(Class<T> entityClass, int id);

    public <T> Integer save(T o);

    public <T> void update(T o);

    public <T> void saveOrUpdate(T o);

    public <T> void delete(T o);

    public <T> void evict(T o);

    public <T> void evictAll(Collection<T> collection);

    public <T> int countAll(Class<T> entityClass);

    public <T> List<T> getAll(Class<T> entityClass);

    public <T> List<T> get(String queryString);

    public <T> List<T> get(String queryString, boolean useCache);

    public int executeUpdate(String queryString);

    public BigDecimal getScalar(String queryString);

    public <T> T getObject(String queryString, Class<T> entityClass);

    public <T> T getObject(String queryString, Class<T> entityClass, boolean useCache);

    public <T> T merge(T o);

    public <T> void mergeAll(Collection<T> collection);

    public <T> void saveOrUpdateAll(Collection<T> collection);

    public <T> void deleteAll(Collection<T> collection);

    public void clear();
}
