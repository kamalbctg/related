package com.zuma.db.dao.impl.hibernate;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.zuma.db.core.domain.InventoryCategory;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.CategoryDAO;

@Repository("categoryDAO")
public class CategoryDAOImpl extends
		GenericHibernateDAO<InventoryCategory, Integer> implements CategoryDAO {

	@SuppressWarnings("unchecked")
	public List<InventoryCategory> getAll(Store store,
			ReportingWeek reportingWeek) {

		String queryString = "select distinct c from InventoryCategory as c left join c.store where c.store=:store and( c =any(select rec.inventoryItem.inventorySubCategory.inventoryCategory from InventoryWeeklyRecord rec where rec.reportingWeek=:week) or (c.inventorySubCategories is empty) or (c =any(select sub.inventoryCategory from InventorySubCategory as sub where sub.inventoryCategory=c and sub.inventoryItems is empty)) )) order by c.id";

		Query query = getSession().createQuery(queryString).setParameter(
				"store", store).setParameter("week", reportingWeek);
		query.setCacheable(true);
		return query.list();
	}

	public List<InventoryCategory> getAll(Store store) {

		String queryString = "select distinct c from InventoryCategory as c left join c.store left join fetch c.inventorySubCategories subc where c.store=:store order by c.id";
		Query query = getSession().createQuery(queryString).setParameter(
				"store", store);
		query.setCacheable(true);
		return query.list();
	}
        @Override
	public List<InventoryCategory> getAllNotDeleted(Store store) {

		String queryString = "select distinct c from InventoryCategory as c left join c.store left join fetch c.inventorySubCategories subc where c.store=:store and c.deleted=false order by c.id";
		Query query = getSession().createQuery(queryString).setParameter(
				"store", store);
		query.setCacheable(true);
		return query.list();
	}

	public BigDecimal getTotalCost(InventoryCategory inventoryCategory,
			ReportingWeek reportingWeek) {
		BigDecimal result;
		String queryString = "select sum(rec.unitCost*rec.quantity) from InventoryWeeklyRecord as rec where rec.inventoryItem.inventorySubCategory.inventoryCategory=:category and rec.reportingWeek=:week ";
		Query query = getSession().createQuery(queryString).setParameter(
				"category", inventoryCategory).setParameter("week",
				reportingWeek);
		query.setCacheable(true);
		Object resultObject = query.uniqueResult();
		if (resultObject == null) {
			result = BigDecimal.ZERO;
		} else {
			result = (BigDecimal) resultObject;
		}
		return result;
	}
}
