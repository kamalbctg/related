package com.zuma.db.dao.impl.hibernate;

import com.zuma.db.core.domain.Company;
import com.zuma.db.dao.CompanyDAO;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

@Repository("companyDAO")
public class CompanyDAOImpl extends GenericHibernateDAO<Company, Integer> implements CompanyDAO {

    @Override
    public List<Company> getAccepted(boolean mode) {
        String queryString = "from Company where accepted=:accept";

        Query query = getSession().createQuery(queryString);
        query.setBoolean("accept", mode);
        query.setCacheable(true);
        return query.list();
    }

    @Override
    public List<Company> getAll(int count, String like) {
        String queryString = "from Company where accepted=true";
        if (!like.isEmpty()) {
            queryString += " and caption like :pattern";
        }
        Query query = getSession().createQuery(queryString);
        if (!like.isEmpty()) {
            query.setString("pattern", "%" + like + "%");
        }
        return (count == 0) ? query.list() : query.list().subList(0, count);
    }

    @Override
    public List<Company> getFull() {
        String queryString = "select distinct company from Store s where s.accepted=true";
        Query query = getSession().createQuery(queryString);
        query.setCacheable(true);
        return query.list();
    }

}
