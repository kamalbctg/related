package com.zuma.db.dao.impl.hibernate;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.zuma.db.dao.GenericDAO;

public class GenericHibernateDAO<T, PK extends Serializable> implements
		GenericDAO<T, PK> {
	@Autowired
	protected SessionFactory sessionFactory;

	protected Class<T> domainClass;

	public final String getEntityName() {
		return domainClass.getSimpleName();
	}

	/** Child classes should call this constructor */
	public GenericHibernateDAO() {
		domainClass = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
	}

	/** Direct users should call this constructor */
	public GenericHibernateDAO(Class<T> clazz) {
		this.domainClass = clazz;
	}

	@SuppressWarnings("unchecked")
	public T findById(PK id) {
		return (T) getSession().get(domainClass, id);
	}

	@SuppressWarnings("unchecked")
	public PK save(T o) {

		return (PK) getSession().save(o);

	}

	public void update(T o) {
		getSession().merge(o);
	}

	public void saveOrUpdate(T o) {
		getSession().saveOrUpdate(o);
	}

	public void delete(T o) {
		getSession().delete(o);
	}

	public int countAll() {
		String queryString = "select count(e) from " + getEntityName() + " e";
		Query query = getSession().createQuery(queryString);
		query.setCacheable(true);
		Long size = (Long) query.uniqueResult();
		return size.intValue();
	}

	public List<T> getAll() {
		String queryString = "from " + getEntityName();
		Query query = getSession().createQuery(queryString);
		query.setCacheable(true);
		return query.list();
	}

	public Session getSession() {

		return sessionFactory.getCurrentSession();
	}

	protected void openSession() {
		sessionFactory.openSession();
	}

	protected void closeSession() {
		getSession().close();
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public T merge(T o) {
		return (T) getSession().merge(o);
	}

}
