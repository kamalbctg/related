package com.zuma.db.dao.impl.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.zuma.db.dao.GenericSQLProcessorDAO;
import com.zuma.db.dao.InvalidSQLSyntaxException;

@Repository
public class GenericSQLProcessorDAOImpl implements GenericSQLProcessorDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public List<String> execute(String sqlCommnd) {

		getSession().beginTransaction();
		Query query = getSession().createSQLQuery(sqlCommnd);
		ResultSet resultSet;
		Statement st;
		List<String> queryResults = null;
		try {
			st = getSession().connection().createStatement();
			resultSet = st.executeQuery(sqlCommnd);
			queryResults = createFormatedRows(resultSet);
		} catch (HibernateException e) {
			throw new InvalidSQLSyntaxException("hibernate exception", e);
		} catch (SQLException e) {
			throw new InvalidSQLSyntaxException("sql exception", e);
		}

		return queryResults;
	}

	private List<String> createFormatedRows(ResultSet resultSet)
			throws SQLException {
		List<String> queryResults = new LinkedList<String>();
		int columnCount = resultSet.getMetaData().getColumnCount();
		while (resultSet.next()) {
			String row = "";
			int currentColumnNo = 1;
			while (currentColumnNo <= columnCount) {
				String currentColumn = resultSet.getString(currentColumnNo++);

				row += currentColumn + " ";

			}
			queryResults.add(row);
		}
		return queryResults;
	}

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
}
