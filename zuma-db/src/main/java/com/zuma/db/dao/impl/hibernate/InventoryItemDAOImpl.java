package com.zuma.db.dao.impl.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.zuma.db.core.domain.InventoryItem;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.InventoryItemDAO;

@Repository("inventoryItemDAO")
public class InventoryItemDAOImpl extends
		GenericHibernateDAO<InventoryItem, Integer> implements InventoryItemDAO {

	@Override
	public List<InventoryItem> getAll(Store store, ReportingWeek reportingWeek) {
		List<InventoryItem> inventoryItems;
		String queryString = "select distinct rec.inventoryItem from InventoryWeeklyRecord as rec left join fetch rec.inventoryItem.inventorySubCategory where rec.reportingWeek=:week and rec.inventoryItem.inventorySubCategory.inventoryCategory.store=:store";
		Query query = getSession().createQuery(queryString).setParameter(
				"store", store).setParameter("week", reportingWeek);
		query.setCacheable(true);
		inventoryItems = query.list();

		return inventoryItems;

		// String queryString =
		// "select distinct c from InventoryCategory as c left join c.store where c.store=:store and :person member of c.store.personList and( c =any(select rec.inventoryItem.inventorySubCategory.inventoryCategory from InventoryWeeklyRecord rec where rec.reportingWeek=:week) or (c.inventorySubCategories is empty) or (c =any(select sub.inventoryCategory from InventorySubCategory as sub where sub.inventoryCategory=c and sub.inventoryItems is empty)) ))";
	}
}
