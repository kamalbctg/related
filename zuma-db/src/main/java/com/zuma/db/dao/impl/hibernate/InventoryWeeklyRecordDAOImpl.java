package com.zuma.db.dao.impl.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.zuma.db.core.domain.InventoryWeeklyRecord;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.InventoryWeeklyRecordDAO;

@Repository("inventoryWeeklyRecordDAO")
public class InventoryWeeklyRecordDAOImpl extends
		GenericHibernateDAO<InventoryWeeklyRecord, Integer> implements
		InventoryWeeklyRecordDAO {

	@Override
	public List<InventoryWeeklyRecord> getAll(Store store,
			ReportingWeek reportingWeek) {
		List<InventoryWeeklyRecord> inventoryWeeklyRecords;
		String queryString = "select rec from InventoryWeeklyRecord as rec left join fetch rec.inventoryItem where rec.inventoryItem.inventorySubCategory.inventoryCategory.store=:store and rec.reportingWeek=:week";
		Query query = getSession().createQuery(queryString).setParameter(
				"store", store).setParameter("week", reportingWeek);
		query.setCacheable(true);
		inventoryWeeklyRecords = query.list();

		return inventoryWeeklyRecords;
	}
}
