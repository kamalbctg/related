package com.zuma.db.dao.impl.hibernate;

import com.zuma.db.core.domain.Purchase;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.PurchaseWeeklyDAO;
import java.util.Date;

@Repository("purchaseWeeklyDAO")
public class PurchaseWeeklyDAOImpl extends
		GenericHibernateDAO<Purchase, Integer> implements
		PurchaseWeeklyDAO {

	@Override
	public List<Purchase> getAll(Store store, Date reportFrom, Date reportTo)
        {
		List<Purchase> purchaseRecords;

                String queryString = "select rec from Purchase as rec where rec.store=:store and rec.date>=:from_date and rec.date<=:to_date";
                // String queryString = "select rec from Purchase as rec where rec.store.id=:store.id and rec.date between :from_date and :to_date";
                Query query = getSession().createQuery(queryString);
                query.setParameter("store", store).setParameter("from_date", reportFrom).setParameter("to_date", reportTo);
		query.setCacheable(true);
		purchaseRecords = query.list();

		return purchaseRecords;
        }
}