package com.zuma.db.dao.impl.hibernate;

import com.zuma.db.core.domain.Store;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.dao.ReportingWeekDAO;
import com.zuma.db.service.util.Week;

@Repository
public class ReportingWeekDAOImpl extends GenericHibernateDAO<ReportingWeek, Integer> implements ReportingWeekDAO {

    public ReportingWeekDAOImpl() {
        super();
    }

    @Override
    public ReportingWeek getFirstWeek() {
        return getAll().get(0); // TODO: write a query that gets first and last,
        // instead of getting all list

    }

    @Override
    public ReportingWeek getLastWeek() {
        List<ReportingWeek> weeks = getAll();
        return getAll().get(weeks.size() - 1);// last TODO:fix
    }

    @Override
    public List<ReportingWeek> getAllUnlocked(Store store) {
        if (store == null) {
            return getAll();
        }
        Date fromDate = store.getRequestDate();
        if (fromDate == null) {
            return getAll();
        }
        Date beginWeekDate = Week.getWeekBegin(fromDate);
        // Checked only for Waste because all files are once submitted.
        String queryString = "select rec from ReportingWeek as rec where rec.weekDate>=:from_date "+
                "and rec.weekDate not in (select file.dateReport from ReportFile as file where "+
                "file.folder.name like 'Waste%' and file.folder.parent is null " +
                "and file.folder.store=:active_store) order by rec.weekDate";
        Query query = getSession().createQuery(queryString);
        query.setParameter("from_date", beginWeekDate).setParameter("active_store", store);
        query.setCacheable(true);
        // Add locking for locking weeks;
        return query.list();
    }
}