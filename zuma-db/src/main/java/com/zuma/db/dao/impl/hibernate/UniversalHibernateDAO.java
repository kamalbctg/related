/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.db.dao.impl.hibernate;

import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class UniversalHibernateDAO implements UniversalDAO {

    @Autowired
    protected SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public <T> T findById(Class<T> entityClass, int id) {
        return entityClass.cast(getSession().get(entityClass, id));
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> Integer save(T o) {
        Integer result;
        Session session = getSession();
        result = (Integer) session.save(o);
        return result;
    }

    @Override
    public <T> void update(T o) {
        Session session = getSession();
        session.merge(o);
        session.flush();
    }

    @Override
    public <T> void saveOrUpdate(T o) {
        Session session = getSession();
        session.saveOrUpdate(o);
        session.flush();
    }

    @Override
    public <T> void delete(T o) {
        Session session = getSession();
        session.delete(o);
        session.flush();
    }

    @Override
    public <T> void evict(T o) {
        Session session = getSession();
        session.evict(o);
        session.flush();
    }

    @Override
    public <T> void evictAll(Collection<T> collection) {
        Session session = getSession();
        for (T o : collection) {
            session.evict(o);
        }
        session.flush();

    }

    @Override
    public <T> int countAll(Class<T> entityClass) {
        String queryString = "select count(e) from " + entityClass + " e";
        Query query = getSession().createQuery(queryString);
        query.setCacheable(true);
        Long size = (Long) query.uniqueResult();
        return size.intValue();
    }

    @Override
    public <T> List<T> getAll(Class<T> entityClass) {

        String queryString = "from " + entityClass.getSimpleName();
        Query query = getSession().createQuery(queryString);
        query.setCacheable(true);
        return query.list();
    }

    @Override
    public <T> List<T> get(String queryString) {
        Query query = getSession().createQuery(queryString);
        query.setCacheable(true);
        return query.list();
    }

    @Override
    public <T> List<T> get(String queryString, boolean useCache) {
        Query query = getSession().createQuery(queryString);
        query.setCacheable(useCache);
        return query.list();
    }

    @Override
    public int executeUpdate(String queryString) {
        Session session = getSession();
        Query query = session.createQuery(queryString);
        int count = query.executeUpdate();
        session.flush();
        return count;
    }

    public Session getSession() {

        return sessionFactory.getCurrentSession();
    }

    protected void openSession() {
        sessionFactory.openSession();
    }

    protected void closeSession() {
        getSession().close();
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public <T> T merge(T o) {
        T result;
        Session session = getSession();
        result = (T) session.merge(o);
        session.flush();
        return result;

    }

    @Override
    public void clear() {
        Session session = getSession();
        session.flush();
        session.clear();

    }

    @Override
    public <T> void mergeAll(Collection<T> collection) {
        Session session = getSession();
        Collection<T> tmp = new ArrayList<T>();

        for (T object : collection) {
            tmp.add(object);
            //session.merge(object);
        }
        collection.clear();
        for (T object : tmp) {
            collection.add((T) session.merge(object));
        }
        session.flush();
    }

    @Override
    public <T> void saveOrUpdateAll(Collection<T> collection) {
        Session session = getSession();
        for (T object : collection) {
            session.saveOrUpdate(object);
        }
        session.flush();
    }

    @Override
    public <T> void deleteAll(Collection<T> collection) {
        Session session = getSession();
        for (T object : collection) {
            session.delete(object);
        }
        session.flush();
    }

    @Override
    public BigDecimal getScalar(String queryString) {
        List resSet = get(queryString);
        if (resSet != null && resSet.size() > 0) {
            return (BigDecimal) resSet.iterator().next();
        }
        return null;
    }

    @Override
    public <T> T getObject(String queryString, Class<T> entityClass) {
        List resSet = get(queryString);
        if (resSet != null && resSet.size() > 0) {
            return (T)resSet.iterator().next();
        }
        return null;
    }

    @Override
    public <T> T getObject(String queryString, Class<T> entityClass, boolean useCache) {
        List resSet = get(queryString,useCache);
        if (resSet != null && resSet.size() > 0) {
            return (T)resSet.iterator().next();
        }
        return null;
    }
}
