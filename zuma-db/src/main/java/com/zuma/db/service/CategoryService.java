package com.zuma.db.service;

import java.math.BigDecimal;
import java.util.List;

import com.zuma.db.core.domain.InventoryCategory;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;

public interface CategoryService extends Service<InventoryCategory, Integer> {
	public BigDecimal totalCategoryCost(List<InventoryCategory> categoryList,
			ReportingWeek reportingWeek);

	public BigDecimal totalCategoryCost(InventoryCategory inventoryCategory,
			ReportingWeek reportingWeek);

	public List<InventoryCategory> getAll(Store store,
			ReportingWeek reportingWeek);

	public List<InventoryCategory> getAll(Store store);

        public List<InventoryCategory> getAllNotDeleted(Store store);
}
