package com.zuma.db.service;


import com.zuma.db.core.domain.Company;
import java.util.List;


public interface CompanyService extends
        Service<Company, Integer> {

        public List<Company> getAccepted(boolean mode);      // pending/existing list
        public List<Company> getAll(int count, String like); // get only first count items
        public List<Company> getFull(); // get list with store

        public List<Company> search(String  like);  // search existing by filter
        public List<Company> getPending();          // all pending accounts
        public List<Company> getExisting();         // all not pending accounts
        public List<Company> getExisting(int count, String like);
}
