package com.zuma.db.service;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;

public interface GenericSQLProcessorService {
	List<String> execute(String sqlCommnd) throws HibernateException, SQLException;
}
