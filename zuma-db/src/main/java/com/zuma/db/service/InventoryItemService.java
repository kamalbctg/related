package com.zuma.db.service;

import java.util.List;

import com.zuma.db.core.domain.InventoryItem;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;

public interface InventoryItemService extends Service<InventoryItem, Integer> {
	public List<InventoryItem> getAll(Store store,
			ReportingWeek reportingWeek);
}
