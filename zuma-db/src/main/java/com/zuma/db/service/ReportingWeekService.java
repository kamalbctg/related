package com.zuma.db.service;

import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import java.util.List;

public interface ReportingWeekService extends Service<ReportingWeek, Integer> {
	public ReportingWeek getFirstWeek();

	public ReportingWeek getLastWeek();

        public List<ReportingWeek> getAllUnlocked(Store store);

	/**
	 * Returns NULL if week is not found in the database or week is the first
	 * week in BD
	 */
        // TODO: Fix for unlock only
	public ReportingWeek findPreviousWeek(ReportingWeek currentWeek);
}
