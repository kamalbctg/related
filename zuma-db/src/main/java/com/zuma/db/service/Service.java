package com.zuma.db.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface Service<T, PK extends Serializable> extends Serializable{
	public int countAll();

	public List<T> getAll();

	public T findById(PK id);

	public PK save(T category);

	public void update(T category);

	public void saveOrUpdate(T category);

	public void delete(T category);

	public void removeAll(Collection<T> itemList);

	public void mergeAll(Collection<T> categoryList);

	public T merge(T category);

	public void saveOrUpdateAll(Collection<T> categoryList);

}