package com.zuma.db.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zuma.db.core.domain.InventoryCategory;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.CategoryDAO;
import com.zuma.db.dao.GenericDAO;
import com.zuma.db.service.CategoryService;

@Service("categoryService")
@Transactional
public class CategoryServiceImpl extends GenericServiceImpl<InventoryCategory, Integer> implements
        CategoryService {

    @Autowired
    @Qualifier("categoryDAO")
    private CategoryDAO categoryDAO;

    @Override
    protected GenericDAO<InventoryCategory, Integer> getDao() {
        return categoryDAO;
    }

    @Override
    public BigDecimal totalCategoryCost(List<InventoryCategory> categoryList,
            ReportingWeek reportingWeek) {
        BigDecimal total = BigDecimal.ZERO;
        for (InventoryCategory inventoryCategory : categoryList) {
            total = total.add(totalCategoryCost(inventoryCategory,
                    reportingWeek));
        }
        return total;
    }

    @Override
    public BigDecimal totalCategoryCost(InventoryCategory inventoryCategory,
            ReportingWeek reportingWeek) {
        BigDecimal total = categoryDAO.getTotalCost(inventoryCategory,
                reportingWeek);
        return total;
    }

    @Override
    public List<InventoryCategory> getAll(Store store,
            ReportingWeek reportingWeek) {
        return categoryDAO.getAll(store, reportingWeek);
    }

    @Override
    public List<InventoryCategory> getAll(Store store) {
        return categoryDAO.getAll(store);
    }

    @Override
    public List<InventoryCategory> getAllNotDeleted(Store store) {
        return categoryDAO.getAllNotDeleted(store);
    }
}
