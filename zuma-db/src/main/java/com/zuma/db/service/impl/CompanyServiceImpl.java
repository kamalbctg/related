package com.zuma.db.service.impl;

import com.zuma.db.dao.CompanyDAO;
import com.zuma.db.service.CompanyService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zuma.db.core.domain.Company;
import com.zuma.db.dao.GenericDAO;


@Service("companyService")
@Transactional
public class CompanyServiceImpl extends GenericServiceImpl<Company, Integer> implements
        CompanyService {

    @Autowired
    private CompanyDAO companyDAO;

    @Override
    protected GenericDAO<Company, Integer> getDao() {
        return companyDAO;
    }

    @Override
    public List<Company> getAccepted(boolean mode) {
        return companyDAO.getAccepted(mode);
    }

    @Override
    public List<Company> getAll(int count, String like) {
        return companyDAO.getAll(count, like);
    }
    @Override
    public List<Company> getFull() {
        return companyDAO.getFull();
    }

    @Override
    public List<Company> search(String like) {
        return companyDAO.getAll(0, like);
    }

    @Override
    public List<Company> getPending() {
        return companyDAO.getAccepted(false);
    }

    @Override
    public List<Company> getExisting() {
        return companyDAO.getAccepted(true);
    }
    
    @Override
    public List<Company> getExisting(int count, String like) {
        return companyDAO.getAll(count, like);
    }
}
