package com.zuma.db.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zuma.db.dao.GenericSQLProcessorDAO;
import com.zuma.db.service.GenericSQLProcessorService;

@Transactional
@Service
public class GenericSQLProcessorServiceImpl implements
		GenericSQLProcessorService {
	@Autowired
	private GenericSQLProcessorDAO genericSQLProcessorDAO;

	public List<String> execute(String sqlCommnd) throws HibernateException,
			SQLException {
		return genericSQLProcessorDAO.execute(sqlCommnd);
	}
}
