package com.zuma.db.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.zuma.db.dao.GenericDAO;
import com.zuma.db.service.Service;

@org.springframework.stereotype.Service
@Transactional
public class GenericServiceImpl<T, PK extends Serializable> implements
		Service<T, PK> {

	private GenericDAO<T, PK> dao;

	public GenericServiceImpl(GenericDAO<T, PK> dao) {
		this.dao = dao;
	}

	public GenericServiceImpl() {
	}

	@Override
	public int countAll() {
		return getDao().countAll();
	}

	@Override
	public void delete(T item) {
		getDao().delete(item);

	}

	@Override
	public T findById(PK id) {
		return getDao().findById(id);
	}

	@Override
	public List<T> getAll() {
		return getDao().getAll();
	}

	@Override
	public PK save(T item) {
		return (PK) getDao().save(item);
	}

	@Override
	public void saveOrUpdate(T item) {
		getDao().saveOrUpdate(item);

	}

	@Override
	public T merge(T item) {
		return getDao().merge(item);
	}

	@Override
	public void mergeAll(Collection<T> items) {
		for (T item : items) {
			getDao().merge(item);
		}
	}

	@Override
	public void saveOrUpdateAll(Collection<T> items) {
		for (T item : items) {
			getDao().saveOrUpdate(item);
		}

	}

	@Override
	public void removeAll(Collection<T> items) {
		for (T item : items) {
			getDao().delete(item);
		}

	}

	@Override
	public void update(T item) {
		getDao().update(item);

	}

	protected void setDao(GenericDAO<T, PK> dao) {
		this.dao = dao;
	}

	/**
	 * Subclasses MUST ALWAYS override this method!!!, or users will get a NULL
	 * POINTER Exception
	 */
	protected GenericDAO<T, PK> getDao() {
		return dao;
	}

}
