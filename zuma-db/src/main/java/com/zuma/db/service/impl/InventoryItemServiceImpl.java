package com.zuma.db.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zuma.db.core.domain.InventoryItem;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.GenericDAO;
import com.zuma.db.dao.InventoryItemDAO;
import com.zuma.db.service.InventoryItemService;

@Service("inventoryItemService")
@Transactional
public class InventoryItemServiceImpl extends
		GenericServiceImpl<InventoryItem, Integer> implements
		InventoryItemService {

	@Autowired
	@Qualifier("inventoryItemDAO")
	private InventoryItemDAO inventoryItemDAO;

	@Override
	protected GenericDAO<InventoryItem, Integer> getDao() {
		return inventoryItemDAO;
	}

	@Override
	public List<InventoryItem> getAll( Store store,
			ReportingWeek reportingWeek) {
		return inventoryItemDAO.getAll( store, reportingWeek);
	}

}
