package com.zuma.db.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zuma.db.core.domain.InventoryWeeklyRecord;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.GenericDAO;
import com.zuma.db.dao.InventoryWeeklyRecordDAO;
import com.zuma.db.service.InventoryWeeklyRecordService;

@Service("inventoryWeeklyRecordService")
@Transactional
public class InventoryWeeklyRecordServiceImpl extends
		GenericServiceImpl<InventoryWeeklyRecord, Integer> implements
		InventoryWeeklyRecordService {

	@Autowired
	private InventoryWeeklyRecordDAO inventoryWeeklyRecordDAO;

	@Override
	protected GenericDAO<InventoryWeeklyRecord, Integer> getDao() {
		return inventoryWeeklyRecordDAO;
	}

	@Override
	public List<InventoryWeeklyRecord> getAll(Store store,
			ReportingWeek reportingWeek) {
		return inventoryWeeklyRecordDAO.getAll(store, reportingWeek);
	}

}
