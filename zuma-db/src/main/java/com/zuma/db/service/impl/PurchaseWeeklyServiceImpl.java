package com.zuma.db.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zuma.db.core.domain.Purchase;
import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.GenericDAO;
import com.zuma.db.dao.PurchaseWeeklyDAO;
import com.zuma.db.service.PurchaseWeeklyService;

@Service("purchaseWeeklyService")
@Transactional
public class PurchaseWeeklyServiceImpl extends GenericServiceImpl<Purchase, Integer> implements
        PurchaseWeeklyService {

    @Autowired
    private PurchaseWeeklyDAO purchaseWeeklyDAO;

    @Override
    protected GenericDAO<Purchase, Integer> getDao() {
        return purchaseWeeklyDAO;
    }

    @Override
    public List<Purchase> getAll(Store store,
            Date reportFrom, Date reportTo) {
        return purchaseWeeklyDAO.getAll(store,
                reportFrom, reportTo);
    }
}
