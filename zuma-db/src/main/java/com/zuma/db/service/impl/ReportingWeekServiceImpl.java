package com.zuma.db.service.impl;

import com.zuma.db.core.domain.Store;
import java.util.List;
import java.util.ListIterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.dao.GenericDAO;
import com.zuma.db.dao.ReportingWeekDAO;
import com.zuma.db.service.ReportingWeekService;

@Service
public class ReportingWeekServiceImpl extends
		GenericServiceImpl<ReportingWeek, Integer> implements
		ReportingWeekService {

	@Autowired
	private ReportingWeekDAO reportingWeekDAO;

	@Override
	public ReportingWeek getFirstWeek() {
		return reportingWeekDAO.getFirstWeek();
	}

	@Override
	public ReportingWeek getLastWeek() {
		return reportingWeekDAO.getLastWeek();
	}

        @Override
        public List<ReportingWeek> getAllUnlocked(Store store) {
            return reportingWeekDAO.getAllUnlocked(store);
        }

	@Override
	protected GenericDAO<ReportingWeek, Integer> getDao() {
		return reportingWeekDAO;
	}

	@Override
	public ReportingWeek findPreviousWeek(ReportingWeek currentWeek) {
		ReportingWeek previousWeek = null;
		ListIterator<ReportingWeek> weekIterator = getAll().listIterator();
		while (weekIterator.hasNext()) {
			if (weekIterator.next().getWeekDate().equals(
					currentWeek.getWeekDate())) {
				weekIterator.previous();// here it points to current week
				previousWeek = weekIterator.previous();
				// now we shifted to the real previous week
				break;
			}
		}
		return previousWeek;
	}
}
