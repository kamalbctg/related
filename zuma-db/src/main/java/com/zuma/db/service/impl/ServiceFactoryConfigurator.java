package com.zuma.db.service.impl;

import com.zuma.db.core.domain.Company;
import com.zuma.db.core.domain.Franchise;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.USState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zuma.db.core.domain.Contact;
import com.zuma.db.core.domain.Dictionary;
import com.zuma.db.core.domain.InventoryMeasure;
import com.zuma.db.core.domain.InventorySubCategory;
import com.zuma.db.core.domain.Owner;
import com.zuma.db.core.domain.Page;
import com.zuma.db.core.domain.Person;
import com.zuma.db.core.domain.PersonPage;
import com.zuma.db.core.domain.Purchase;
import com.zuma.db.core.domain.Store;
import com.zuma.db.core.domain.SummaryCategory;
import com.zuma.db.core.domain.SummaryData;
import com.zuma.db.core.domain.SummaryItem;
import com.zuma.db.core.domain.SummarySubCategory;
import com.zuma.db.dao.DAOFactoryConfigurator;

@Configuration
public class ServiceFactoryConfigurator {
	@Autowired
	private DAOFactoryConfigurator daoConfigurator;

	@Bean
	protected GenericServiceImpl<Dictionary, Integer> dictionaryService() {
		return new GenericServiceImpl<Dictionary, Integer>(daoConfigurator
				.dictionaryDAO());
	}

        @Bean
	protected GenericServiceImpl<Franchise, Integer> franchiseService() {
		return new GenericServiceImpl<Franchise, Integer>(daoConfigurator
				.franchiseDAO());
	}
        
        @Bean
	protected GenericServiceImpl<USState, Integer> stateService() {
		return new GenericServiceImpl<USState, Integer>(daoConfigurator
				.stateDAO());
	}

	@Bean
	protected GenericServiceImpl<SummaryCategory, Integer> summaryCategoryService() {
		return new GenericServiceImpl<SummaryCategory, Integer>(daoConfigurator
				.summaryCategoryDAO());
	}

	@Bean
	protected GenericServiceImpl<SummaryItem, Integer> summaryItemService() {
		return new GenericServiceImpl<SummaryItem, Integer>(daoConfigurator
				.summaryItemDAO());
	}

	@Bean
	protected GenericServiceImpl<SummarySubCategory, Integer> summarySubCategoryService() {
		return new GenericServiceImpl<SummarySubCategory, Integer>(
				daoConfigurator.summarySubCategoryDAO());
	}

	@Bean
	protected GenericServiceImpl<Contact, Integer> contactService() {
		return new GenericServiceImpl<Contact, Integer>(daoConfigurator
				.contactDAO());
	}

	@Bean
	protected GenericServiceImpl<SummaryData, Integer> summaryDataService() {
		return new GenericServiceImpl<SummaryData, Integer>(daoConfigurator
				.summaryDataDAO());
	}

	@Bean
	protected GenericServiceImpl<Store, Integer> storeService() {
		return new GenericServiceImpl<Store, Integer>(daoConfigurator
				.storeDAO());
	}
/*
	@Bean
	protected GenericServiceImpl<Company, Integer> companyService() {
		return new GenericServiceImpl<Company, Integer>(daoConfigurator
				.companyDAO());
	}
*/
	@Bean
	protected GenericServiceImpl<InventorySubCategory, Integer> subCategoryService() {
		return new GenericServiceImpl<InventorySubCategory, Integer>(
				daoConfigurator.subCategoryDAO());
	}

	@Bean
	protected GenericServiceImpl<Person, Integer> personService() {
		return new GenericServiceImpl<Person, Integer>(daoConfigurator
				.personDAO());
	}

	@Bean
	protected GenericServiceImpl<PersonPage, Integer> personPageService() {
		return new GenericServiceImpl<PersonPage, Integer>(daoConfigurator
				.personPageDAO());
	}

	@Bean
	protected GenericServiceImpl<Page, Integer> pageService() {
		return new GenericServiceImpl<Page, Integer>(daoConfigurator.pageDAO());
	}

	@Bean
	protected GenericServiceImpl<Purchase, Integer> purchaseService() {
		return new GenericServiceImpl<Purchase, Integer>(daoConfigurator
				.purchaseDAO());
	}

	@Bean
	protected GenericServiceImpl<ReportingWeek, Integer> weekService() {
		return new GenericServiceImpl<ReportingWeek, Integer>(daoConfigurator
				.weekDAO());
	}

	@Bean
	protected GenericServiceImpl<Owner, Integer> ownerService() {
		return new GenericServiceImpl<Owner, Integer>(daoConfigurator
				.ownerDAO());
	}

	@Bean
	protected GenericServiceImpl<InventoryMeasure, Integer> inventoryMeasureService() {
		return new GenericServiceImpl<InventoryMeasure, Integer>(
				daoConfigurator.inventoryMeasureDAO());
	}

}
