package com.zuma.db.service.util;

import java.util.Arrays;
import java.util.List;

public class ListUtil {

	public static <T> List<T> insertNewItemInList(List<T> oldList, T item) {
		List<T> newList;
		if (oldList != null) {
			oldList.add(item);
			newList = oldList;
		} else {
			newList = (List<T>) Arrays.asList(item);
		}

		return newList;
	}
}
