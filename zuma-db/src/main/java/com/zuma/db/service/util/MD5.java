package com.zuma.db.service.util;

import java.security.MessageDigest;

public class MD5 {
    public static String byteToHex(byte b) {
        char hex[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a',
            'b', 'c', 'd', 'e', 'f'};
        return String.valueOf(hex[(b >> 4) & 0x0f]) + String.valueOf(hex[b & 0x0f]);
    }

    public static String encode(String password) {
        String md5String = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes()); // getBytes("ISO-8859-1")
            byte[] md5 = md.digest();

            if (md5 != null) {
                for (int i = 0; i < md5.length; i++) {
                    md5String += byteToHex(md5[i]);
                }
            }

        } catch (Exception e) {
            System.err.print(e.toString());
        }
        return md5String;
    }
}
