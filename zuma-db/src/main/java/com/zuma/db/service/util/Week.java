package com.zuma.db.service.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Week {

    public static Date getCurrentDate() {
        Calendar calendar = nullTime(Calendar.getInstance());
        return calendar.getTime();
    }

    public static Date getCurrentDateTime() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static Date getWeekBegin(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setMinimalDaysInFirstWeek(1);

        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int daysToIncrement = dayOfWeek - Calendar.MONDAY;
        if (daysToIncrement < 0) // if sunday of previus week
        {
            daysToIncrement = 6;
        }
        calendar.add(Calendar.DATE, -daysToIncrement);
        return nullTime(calendar).getTime();
    }

    public static Date getWeekEnd(Date date) {
        Calendar weekEnd = Calendar.getInstance();
        weekEnd.setTime(getWeekBegin(date));
        weekEnd.add(Calendar.DATE, 6);
        return nullTime(weekEnd).getTime();
    }

    public static Date getWeekEndTime(Date date) {
        Calendar weekEnd = Calendar.getInstance();
        weekEnd.setTime(getWeekBegin(date));
        weekEnd.add(Calendar.DATE, 6);
        return finalTime(weekEnd).getTime();
    }

    public static Date getYearBegin() {
        Calendar yearEnd = nullTime(Calendar.getInstance());
        yearEnd.set(Calendar.MONTH,Calendar.JANUARY);
        yearEnd.set(Calendar.DAY_OF_MONTH, 1);
        return yearEnd.getTime();
    }

    public static Date getYearEnd() {
        Calendar yearEnd = nullTime(Calendar.getInstance());
        yearEnd.set(Calendar.MONTH,Calendar.DECEMBER);
        yearEnd.set(Calendar.DAY_OF_MONTH, 31);
        return yearEnd.getTime();
    }

    public static Calendar nullTime(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    public static Calendar finalTime(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar;
    }

    public static Date getCurrentWeekBegin() {
        return getWeekBegin(getCurrentDate());
    }

    public static Date getCurrentWeekEnd() {
        return getWeekEnd(getCurrentDate());
    }

    public static boolean isEqualDate(Date first, Date second) {
        if (first == null || second == null) {
            return false;
        }
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();

        cal1.setTime(first);
        cal2.setTime(second);

        cal1 = nullTime(cal1);
        cal2 = nullTime(cal2);

        return cal1.equals(cal2);
    }

    public static String toString(Date weekDate) {
        if (weekDate == null) {
            return "";
        }

        String pattern = "MM/dd/yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);

        Date weekBegin = Week.getWeekBegin(weekDate);
        Date weekEnd = Week.getWeekEnd(weekDate);

        String weekString = formatter.format(weekBegin) + " - " + formatter.format(weekEnd);

        return weekString;
    }
}
