package com.zuma.db.service.impl;

import static com.zuma.db.core.domain.Dictionary.newRandomInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.zuma.db.core.domain.Dictionary;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext-test.xml")
@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional
public class DictionaryDAOImplTest {

	private GenericServiceImpl<Dictionary, Integer> dictionaryService;

	@Autowired
	public void setDictionaryService(
			@Qualifier("dictionaryService") GenericServiceImpl<Dictionary, Integer> dictionaryService) {
		this.dictionaryService = dictionaryService;
	}

	@Autowired
	private SessionFactory sessionFactory;
	@Test
	@Rollback(true)
	public void testCountAll() {
		/*Dictionary dictionary;
		saveNewDictionary();
		sessionFactory.getCurrentSession().flush();
		assertEquals(1, dictionaryService.countAll());
		dictionary = saveNewDictionary();
		sessionFactory.getCurrentSession().flush();
		assertEquals(2, dictionaryService.countAll());
		deleteUnExistingDictionary();
		sessionFactory.getCurrentSession().flush();
		assertEquals(2, dictionaryService.countAll());
		delete(dictionary);
		sessionFactory.getCurrentSession().flush();
		assertEquals(1, dictionaryService.countAll());
*/
	}
/*
	@Test
	@Rollback(true)
	public void testFindById() {
		Dictionary dictionary = saveNewDictionary();
		sessionFactory.getCurrentSession().flush();
		Dictionary sameDictionary = dictionaryService.findById(dictionary
				.getId());
		assertTrue(sameDictionary.getId() == dictionary.getId());
	}

	@Test
	@Rollback(true)
	public void testSave() {
		Dictionary dictionary = saveNewDictionary();
		sessionFactory.getCurrentSession().flush();
		assertTrue(dictionaryService.countAll() == 1);
	}

	@Test
	@Rollback(true)
	public void testUpdate() {
		String oldDescription = "oldDescriptiton", updatedDescription = "updatedDescription";
		int dictionaryId;
		Dictionary dictionary;
		dictionary = newRandomInstance();
		dictionary.setDescription(oldDescription);
		dictionaryService.save(dictionary);
		sessionFactory.getCurrentSession().flush();
		dictionaryId = dictionary.getId();
		dictionary = newRandomInstance();
		dictionary.setId(dictionaryId);
		dictionary.setDescription(updatedDescription);
		dictionaryService.update(dictionary);
		sessionFactory.getCurrentSession().flush();
		dictionary = dictionaryService.findById(dictionaryId);
		assertEquals(updatedDescription, dictionary.getDescription());

	}

	@Test
	@Rollback(true)
	public void testDelete() {
		int dictionaryId;
		Dictionary dictionary = newRandomInstance();
		dictionaryService.save(dictionary);
		sessionFactory.getCurrentSession().flush();
		dictionaryId = dictionary.getId();
		dictionaryService.delete(dictionary);
		sessionFactory.getCurrentSession().flush();
		assertNull(dictionaryService.findById(dictionaryId));
	}

	public void delete(Dictionary dictionary) {
		dictionaryService.delete(dictionary);
	}

	public void deleteUnExistingDictionary() {
		dictionaryService.delete(newRandomInstance());
	}

	public Dictionary saveNewDictionary() {
		Dictionary dictionary = newRandomInstance();
		dictionaryService.save(dictionary);
		return dictionary;
	}
*/
}
