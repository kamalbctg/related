use zumablacks;
drop table DICTIONARY;
drop table SubCategory_Item;
drop table Category_SubCategory;
drop table Item;
drop table SubCategory;
drop table Category;

drop table PersonStore;
drop table PersonContact;
drop table StoreContact;
drop table CompanyContact;
drop table Contact;



drop table SummaryData;
drop table SummaryItem;
drop table SummarySubcategory;
drop table SummaryCategory;

drop table Owner;
drop table Waste;
drop table InventoryWeeklyRecord;
drop table InventoryItem;
drop table InventoryMeasure;
drop table InventorySubCategory;
drop table InventoryCategory;


drop table PersonPage;
drop table Page;

drop table Purchase;

drop table WasteRetailWaste;


drop table Person;
drop table ReportingWeek;
drop table Store;
drop table Company;


