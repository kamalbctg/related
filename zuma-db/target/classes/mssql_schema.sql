USE [ZUMABLACKS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Contact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Contact](
	[ID] [int] NOT NULL,
	[Address] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Phone1] [varchar](50) NULL,
	[Phone2] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Zip] [varchar](50) NULL,
 CONSTRAINT [PK__Contact__6E2152BE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SummaryCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SummaryCategory](
	[ID] [int] NOT NULL,
	[Caption] [varchar](50) NOT NULL,
	[SequenceNumber] [int] NOT NULL,
 CONSTRAINT [PK__SummaryCategory__73DA2C14] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InventoryMeasure]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[InventoryMeasure](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Measure] [varchar](50) NOT NULL,
 CONSTRAINT [PK_InventoryMeasure] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReportingWeek]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ReportingWeek](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WeekDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_ReportingWeek] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Page]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Page](
	[ID] [int] NOT NULL,
	[Caption] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InventoryCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[InventoryCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Caption] [varchar](50) NOT NULL,
	[SequenceNumber] [int] NOT NULL,
 CONSTRAINT [PK_InventoryCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Company]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Company](
	[ID] [int] NOT NULL,
	[EntityType] [varchar](50) NULL,
	[IncorporatedState] [int] NOT NULL,
 CONSTRAINT [PK__Company__6C390A4C] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CompanyContact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CompanyContact](
	[Company] [int] NOT NULL,
	[Contact] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StoreContact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StoreContact](
	[Contact] [int] NOT NULL,
	[Store] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonContact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PersonContact](
	[Contact] [int] NOT NULL,
	[Person] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SummarySubCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SummarySubCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SummaryCategory] [int] NOT NULL,
	[Caption] [varchar](50) NOT NULL,
	[EditedName] [varchar](100) NULL,
	[Number] [int] NOT NULL,
 CONSTRAINT [PK_SummarySubcategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InventoryItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[InventoryItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Caption] [varchar](50) NOT NULL,
	[InventorySubCategory] [int] NOT NULL,
	[InventoryMeasure] [int] NOT NULL,
	[Store] [int] NOT NULL,
	[SequenceNumber] [int] NOT NULL,
 CONSTRAINT [PK_InventoryItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonStore]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PersonStore](
	[Store] [int] NOT NULL,
	[Person] [int] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WasteRetailWaste]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[WasteRetailWaste](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WasteDate] [smalldatetime] NOT NULL,
	[WasteDescription] [varchar](100) NOT NULL,
	[UnitCost] [smallmoney] NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[Store] [int] NOT NULL,
 CONSTRAINT [PK_WasteRetailWaste] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Purchase]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Purchase](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Store] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Vendor] [varchar](100) NULL,
	[Invoice] [varchar](50) NOT NULL,
	[Description] [varchar](400) NULL,
	[Payment] [varchar](100) NULL,
	[InvoiceTotal] [numeric](10, 2) NOT NULL CONSTRAINT [DF_Purchase_PurchaseInvoiceTotal]  DEFAULT ((0)),
	[FoodAndBeverage] [numeric](10, 2) NOT NULL CONSTRAINT [DF_Purchase_PurchaseFoodAndBeverage]  DEFAULT ((0)),
	[Paper] [numeric](10, 2) NOT NULL CONSTRAINT [DF_Purchase_PurchasePaper]  DEFAULT ((0)),
	[FAndBTaxes] [numeric](10, 2) NOT NULL CONSTRAINT [DF_Purchase_PurchaseFAndBTaxes]  DEFAULT ((0)),
	[OtherTaxes] [numeric](10, 2) NOT NULL CONSTRAINT [DF_Purchase_PurchaseOtherTaxes]  DEFAULT ((0)),
	[Freight] [numeric](10, 2) NOT NULL CONSTRAINT [DF_Purchase_Freight]  DEFAULT ((0)),
	[Other] [numeric](10, 2) NOT NULL CONSTRAINT [DF_Purchase_PurchaseOther]  DEFAULT ((0)),
 CONSTRAINT [PK_Purchase] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SummaryItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SummaryItem](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SummarySubCategory] [int] NOT NULL,
	[Store] [int] NOT NULL,
	[Caption] [varchar](50) NOT NULL,
	[EditedCaption] [varchar](100) NULL,
	[SequenceNumber] [int] NOT NULL,
 CONSTRAINT [PK_SummaryItem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PersonPage]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PersonPage](
	[ID] [int] NOT NULL,
	[Person] [int] NOT NULL,
	[Page] [int] NOT NULL,
	[Access] [int] NOT NULL CONSTRAINT [DF_UserPage_Access]  DEFAULT ((0)),
 CONSTRAINT [PK_UserPage_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'PersonPage', N'COLUMN',N'Access'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - read access; 1- write access' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PersonPage', @level2type=N'COLUMN',@level2name=N'Access'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InventoryWeeklyRecord]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[InventoryWeeklyRecord](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReportingWeek] [int] NOT NULL,
	[InventoryItem] [int] NOT NULL,
	[UnitCost] [smallmoney] NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_InventoryWeeklyRecord] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InventorySubCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[InventorySubCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Caption] [varchar](50) NOT NULL,
	[SequenceNumber] [int] NOT NULL,
	[InventoryCategory] [int] NOT NULL,
 CONSTRAINT [PK_InventorySubCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Waste]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Waste](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[InventoryItem] [int] NOT NULL,
	[Date] [smalldatetime] NOT NULL,
	[Value] [decimal](18, 2) NULL,
 CONSTRAINT [PK_Waste] PRIMARY KEY NONCLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SummaryData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SummaryData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SummaryItem] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[Value] [numeric](8, 2) NOT NULL CONSTRAINT [DF_SummaryData_DataValue]  DEFAULT ((0)),
 CONSTRAINT [PK_SummaryData] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Owner]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Owner](
	[ID] [int] NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Loss] [numeric](5, 2) NOT NULL,
	[NumberOfShares] [int] NOT NULL,
	[Profit] [numeric](5, 2) NOT NULL,
	[SharePercent] [numeric](5, 2) NOT NULL,
	[Company] [int] NOT NULL,
 CONSTRAINT [PK__Owner__70099B30] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Person]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Person](
	[ID] [int] NOT NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[Login] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Company] [int] NOT NULL,
	[Role] [int] NOT NULL CONSTRAINT [DF_User_Role]  DEFAULT ((0)),
 CONSTRAINT [PK__User__7993056A] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Person', N'COLUMN',N'Role'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - simple user, 1 - admin' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Person', @level2type=N'COLUMN',@level2name=N'Role'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Store]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Store](
	[ID] [int] NOT NULL,
	[FranchiseAffilation] [int] NOT NULL,
	[IfOther] [varchar](50) NULL,
	[Number] [int] NOT NULL,
	[Company] [int] NOT NULL,
 CONSTRAINT [PK__Store__71F1E3A2] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKED26B503F70ADC2]') AND parent_object_id = OBJECT_ID(N'[dbo].[CompanyContact]'))
ALTER TABLE [dbo].[CompanyContact]  WITH CHECK ADD  CONSTRAINT [FKED26B503F70ADC2] FOREIGN KEY([Company])
REFERENCES [dbo].[Company] ([ID])
GO
ALTER TABLE [dbo].[CompanyContact] CHECK CONSTRAINT [FKED26B503F70ADC2]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKED26B503F826587]') AND parent_object_id = OBJECT_ID(N'[dbo].[CompanyContact]'))
ALTER TABLE [dbo].[CompanyContact]  WITH CHECK ADD  CONSTRAINT [FKED26B503F826587] FOREIGN KEY([Contact])
REFERENCES [dbo].[Contact] ([ID])
GO
ALTER TABLE [dbo].[CompanyContact] CHECK CONSTRAINT [FKED26B503F826587]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK8051881F164EB4C9]') AND parent_object_id = OBJECT_ID(N'[dbo].[StoreContact]'))
ALTER TABLE [dbo].[StoreContact]  WITH CHECK ADD  CONSTRAINT [FK8051881F164EB4C9] FOREIGN KEY([Store])
REFERENCES [dbo].[Store] ([ID])
GO
ALTER TABLE [dbo].[StoreContact] CHECK CONSTRAINT [FK8051881F164EB4C9]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK8051881FF826587]') AND parent_object_id = OBJECT_ID(N'[dbo].[StoreContact]'))
ALTER TABLE [dbo].[StoreContact]  WITH CHECK ADD  CONSTRAINT [FK8051881FF826587] FOREIGN KEY([Contact])
REFERENCES [dbo].[Contact] ([ID])
GO
ALTER TABLE [dbo].[StoreContact] CHECK CONSTRAINT [FK8051881FF826587]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK8A22ADB55B115E0A]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonContact]'))
ALTER TABLE [dbo].[PersonContact]  WITH CHECK ADD  CONSTRAINT [FK8A22ADB55B115E0A] FOREIGN KEY([Person])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PersonContact] CHECK CONSTRAINT [FK8A22ADB55B115E0A]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK8A22ADB5F826587]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonContact]'))
ALTER TABLE [dbo].[PersonContact]  WITH CHECK ADD  CONSTRAINT [FK8A22ADB5F826587] FOREIGN KEY([Contact])
REFERENCES [dbo].[Contact] ([ID])
GO
ALTER TABLE [dbo].[PersonContact] CHECK CONSTRAINT [FK8A22ADB5F826587]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SummarySubCategory_SummaryCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[SummarySubCategory]'))
ALTER TABLE [dbo].[SummarySubCategory]  WITH CHECK ADD  CONSTRAINT [FK_SummarySubCategory_SummaryCategory] FOREIGN KEY([SummaryCategory])
REFERENCES [dbo].[SummaryCategory] ([ID])
GO
ALTER TABLE [dbo].[SummarySubCategory] CHECK CONSTRAINT [FK_SummarySubCategory_SummaryCategory]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InventoryItem_InventoryMeasure]') AND parent_object_id = OBJECT_ID(N'[dbo].[InventoryItem]'))
ALTER TABLE [dbo].[InventoryItem]  WITH CHECK ADD  CONSTRAINT [FK_InventoryItem_InventoryMeasure] FOREIGN KEY([InventoryMeasure])
REFERENCES [dbo].[InventoryMeasure] ([ID])
GO
ALTER TABLE [dbo].[InventoryItem] CHECK CONSTRAINT [FK_InventoryItem_InventoryMeasure]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InventoryItem_InventorySubCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[InventoryItem]'))
ALTER TABLE [dbo].[InventoryItem]  WITH CHECK ADD  CONSTRAINT [FK_InventoryItem_InventorySubCategory] FOREIGN KEY([InventorySubCategory])
REFERENCES [dbo].[InventorySubCategory] ([ID])
GO
ALTER TABLE [dbo].[InventoryItem] CHECK CONSTRAINT [FK_InventoryItem_InventorySubCategory]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InventoryItem_Store]') AND parent_object_id = OBJECT_ID(N'[dbo].[InventoryItem]'))
ALTER TABLE [dbo].[InventoryItem]  WITH CHECK ADD  CONSTRAINT [FK_InventoryItem_Store] FOREIGN KEY([Store])
REFERENCES [dbo].[Store] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[InventoryItem] CHECK CONSTRAINT [FK_InventoryItem_Store]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK8B05E3B6164EB4C9]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonStore]'))
ALTER TABLE [dbo].[PersonStore]  WITH CHECK ADD  CONSTRAINT [FK8B05E3B6164EB4C9] FOREIGN KEY([Store])
REFERENCES [dbo].[Store] ([ID])
GO
ALTER TABLE [dbo].[PersonStore] CHECK CONSTRAINT [FK8B05E3B6164EB4C9]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK8B05E3B65B115E0A]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonStore]'))
ALTER TABLE [dbo].[PersonStore]  WITH CHECK ADD  CONSTRAINT [FK8B05E3B65B115E0A] FOREIGN KEY([Person])
REFERENCES [dbo].[Person] ([ID])
GO
ALTER TABLE [dbo].[PersonStore] CHECK CONSTRAINT [FK8B05E3B65B115E0A]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_WasteRetailWaste_Store]') AND parent_object_id = OBJECT_ID(N'[dbo].[WasteRetailWaste]'))
ALTER TABLE [dbo].[WasteRetailWaste]  WITH CHECK ADD  CONSTRAINT [FK_WasteRetailWaste_Store] FOREIGN KEY([Store])
REFERENCES [dbo].[Store] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[WasteRetailWaste] CHECK CONSTRAINT [FK_WasteRetailWaste_Store]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Purchase_Store]') AND parent_object_id = OBJECT_ID(N'[dbo].[Purchase]'))
ALTER TABLE [dbo].[Purchase]  WITH CHECK ADD  CONSTRAINT [FK_Purchase_Store] FOREIGN KEY([Store])
REFERENCES [dbo].[Store] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Purchase] CHECK CONSTRAINT [FK_Purchase_Store]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SummaryItem_Store]') AND parent_object_id = OBJECT_ID(N'[dbo].[SummaryItem]'))
ALTER TABLE [dbo].[SummaryItem]  WITH CHECK ADD  CONSTRAINT [FK_SummaryItem_Store] FOREIGN KEY([Store])
REFERENCES [dbo].[Store] ([ID])
GO
ALTER TABLE [dbo].[SummaryItem] CHECK CONSTRAINT [FK_SummaryItem_Store]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SummaryItem_SummarySubcategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[SummaryItem]'))
ALTER TABLE [dbo].[SummaryItem]  WITH CHECK ADD  CONSTRAINT [FK_SummaryItem_SummarySubcategory] FOREIGN KEY([SummarySubCategory])
REFERENCES [dbo].[SummarySubCategory] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SummaryItem] CHECK CONSTRAINT [FK_SummaryItem_SummarySubcategory]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserPage_Page]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonPage]'))
ALTER TABLE [dbo].[PersonPage]  WITH CHECK ADD  CONSTRAINT [FK_UserPage_Page] FOREIGN KEY([Page])
REFERENCES [dbo].[Page] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PersonPage] CHECK CONSTRAINT [FK_UserPage_Page]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserPage_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[PersonPage]'))
ALTER TABLE [dbo].[PersonPage]  WITH CHECK ADD  CONSTRAINT [FK_UserPage_User] FOREIGN KEY([Person])
REFERENCES [dbo].[Person] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PersonPage] CHECK CONSTRAINT [FK_UserPage_User]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InventoryWeeklyRecord_InventoryItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[InventoryWeeklyRecord]'))
ALTER TABLE [dbo].[InventoryWeeklyRecord]  WITH CHECK ADD  CONSTRAINT [FK_InventoryWeeklyRecord_InventoryItem] FOREIGN KEY([InventoryItem])
REFERENCES [dbo].[InventoryItem] ([ID])
GO
ALTER TABLE [dbo].[InventoryWeeklyRecord] CHECK CONSTRAINT [FK_InventoryWeeklyRecord_InventoryItem]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InventoryWeeklyRecord_ReportingWeek]') AND parent_object_id = OBJECT_ID(N'[dbo].[InventoryWeeklyRecord]'))
ALTER TABLE [dbo].[InventoryWeeklyRecord]  WITH CHECK ADD  CONSTRAINT [FK_InventoryWeeklyRecord_ReportingWeek] FOREIGN KEY([ReportingWeek])
REFERENCES [dbo].[ReportingWeek] ([ID])
GO
ALTER TABLE [dbo].[InventoryWeeklyRecord] CHECK CONSTRAINT [FK_InventoryWeeklyRecord_ReportingWeek]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InventorySubCategory_InventoryCategory]') AND parent_object_id = OBJECT_ID(N'[dbo].[InventorySubCategory]'))
ALTER TABLE [dbo].[InventorySubCategory]  WITH CHECK ADD  CONSTRAINT [FK_InventorySubCategory_InventoryCategory] FOREIGN KEY([InventoryCategory])
REFERENCES [dbo].[InventoryCategory] ([ID])
GO
ALTER TABLE [dbo].[InventorySubCategory] CHECK CONSTRAINT [FK_InventorySubCategory_InventoryCategory]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Waste_InventoryItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[Waste]'))
ALTER TABLE [dbo].[Waste]  WITH CHECK ADD  CONSTRAINT [FK_Waste_InventoryItem] FOREIGN KEY([InventoryItem])
REFERENCES [dbo].[InventoryItem] ([ID])
GO
ALTER TABLE [dbo].[Waste] CHECK CONSTRAINT [FK_Waste_InventoryItem]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SummaryData_SummaryItem]') AND parent_object_id = OBJECT_ID(N'[dbo].[SummaryData]'))
ALTER TABLE [dbo].[SummaryData]  WITH CHECK ADD  CONSTRAINT [FK_SummaryData_SummaryItem] FOREIGN KEY([SummaryItem])
REFERENCES [dbo].[SummaryItem] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SummaryData] CHECK CONSTRAINT [FK_SummaryData_SummaryItem]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK4910293F629641]') AND parent_object_id = OBJECT_ID(N'[dbo].[Owner]'))
ALTER TABLE [dbo].[Owner]  WITH CHECK ADD  CONSTRAINT [FK4910293F629641] FOREIGN KEY([Company])
REFERENCES [dbo].[Company] ([ID])
GO
ALTER TABLE [dbo].[Owner] CHECK CONSTRAINT [FK4910293F629641]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FKA02C9E57F629641]') AND parent_object_id = OBJECT_ID(N'[dbo].[Person]'))
ALTER TABLE [dbo].[Person]  WITH CHECK ADD  CONSTRAINT [FKA02C9E57F629641] FOREIGN KEY([Company])
REFERENCES [dbo].[Company] ([ID])
GO
ALTER TABLE [dbo].[Person] CHECK CONSTRAINT [FKA02C9E57F629641]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK4C808C1F629641]') AND parent_object_id = OBJECT_ID(N'[dbo].[Store]'))
ALTER TABLE [dbo].[Store]  WITH CHECK ADD  CONSTRAINT [FK4C808C1F629641] FOREIGN KEY([Company])
REFERENCES [dbo].[Company] ([ID])
GO
ALTER TABLE [dbo].[Store] CHECK CONSTRAINT [FK4C808C1F629641]
