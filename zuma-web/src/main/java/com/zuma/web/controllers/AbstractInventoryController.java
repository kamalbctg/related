package com.zuma.web.controllers;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.zuma.db.core.domain.InventoryCategory;
import com.zuma.db.core.domain.InventoryItem;
import com.zuma.db.core.domain.InventoryMeasure;
import com.zuma.db.core.domain.InventorySubCategory;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import com.zuma.db.service.CategoryService;
import com.zuma.db.service.InventoryItemService;
import com.zuma.db.service.Service;

public abstract class AbstractInventoryController implements Serializable {
	@Autowired
	protected UserManagedBean userManagedBean;
	@Autowired
	protected InventoryItemService inventoryItemService;
	@Autowired
	@Qualifier("inventoryMeasureService")
	protected Service<InventoryMeasure, Integer> inventoryMeasureService;
	@Autowired
	protected CategoryService categoryService;

	protected Store currentStore;
	protected ReportingWeek currentWeek;
	protected InventorySubCategory selectedSubCategory;
	protected InventoryCategory selectedCategory;
	protected InventoryItem selectedItem;

	private Integer updateRow = 0;
	protected HashSet<Integer> updateRows = new HashSet<Integer>();
	protected String targetPage;

	protected List<InventoryCategory> categoryList;

	@PostConstruct
	public void reInitControllerData() {
		categoryList = categoryService.getAll();
	}

	public Set<Integer> getUpdateRows() {
		return new HashSet<Integer>(updateRows);
	}

	public String refreshPage() {
		categoryList = null;
		return targetPage;
	}

	public abstract String save();

	protected void addFacesMessageForUI(String uiMessage) {
		FacesMessage facesMessage = new FacesMessage(uiMessage);
		FacesContext facesContext = FacesContext.getCurrentInstance();

		// Passing null for the client ID argument to the FacesContext
		// addMessage() method specifies the message as not belonging
		// to any particular UI component. This will cause the message
		// to be displayed as a general message on the UI

		facesContext.addMessage(null, facesMessage);
	}

	public List<InventoryCategory> getCategoryList() {

		if ((categoryList == null)) {
			reInitControllerData();
		}

		return categoryList;
	}

	protected void reloadSelections() {
		currentStore = userManagedBean.getActiveStore();
		currentWeek = userManagedBean.getActiveWeek();
	}

	public void setCategoryList(List<InventoryCategory> categoryList) {
		this.categoryList = categoryList;
	}

	public InventoryItem getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(InventoryItem selectedItem) {
		this.selectedItem = selectedItem;
	}

	public InventoryCategory getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(InventoryCategory selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	public InventorySubCategory getSelectedSubCategory() {
		return selectedSubCategory;
	}

	public void setSelectedSubCategory(InventorySubCategory selectedSubCategory) {
		this.selectedSubCategory = selectedSubCategory;
	}

	public String getTargetPage() {
		return targetPage;
	}

	public void setTargetPage(String targetPage) {
		this.targetPage = targetPage;
	}

	public void setUpdateRow(Integer updateRow) {
		this.updateRow = updateRow;
	}

	public Integer getUpdateRow() {
		return updateRow;
	}
}
