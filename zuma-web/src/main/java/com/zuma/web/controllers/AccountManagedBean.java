package com.zuma.web.controllers;

import com.zuma.db.core.domain.Company;
import com.zuma.db.core.domain.Contact;
import com.zuma.db.core.domain.Finance;
import com.zuma.db.core.domain.Folder;
import com.zuma.db.core.domain.Owner;
import com.zuma.db.core.domain.Person;
// import com.zuma.db.core.domain.Service;
import com.zuma.db.core.domain.Store;
import com.zuma.db.service.CompanyService;
import com.zuma.db.service.impl.GenericServiceImpl;
import com.zuma.db.service.util.Week;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.apache.commons.collections.list.TreeList;

@Component
@Scope("session")
public class AccountManagedBean extends ParentController {

    @Autowired
    private CompanyService companyService;
    @Autowired
    protected StateManagedBean stateManagedBean;
    @Autowired
    protected FranchiseManagedBean franchiseManagedBean;
    @Autowired
    @Qualifier("storeService")
    protected GenericServiceImpl<Store, Integer> storeService;
    @Autowired
    @Qualifier("ownerService")
    protected GenericServiceImpl<Owner, Integer> ownerService;
    private Company company;
    private Store store = new Store();
    private Finance finance;
    /* temporary remuoved from ver1, it is enchancement for ver2
    private Service service;
    private Contact otherContact = new Contact();
    private Integer paymentType;
     */
    private Company selectedAccount;
    private List<Owner> ownerList = new TreeList();
    private final String currentPage = null;
    private Store selectedStore;
    private Person selectedUser;
    private List<Store> storeList;
    private List<Person> userList;
    private List<Store> userStoreList;

    public void loadData() {
        // load all companies
        company = companyService.findById(getSelectedAccount().getId());
        //company = getSelectedAccount();
        finance = dao.findById(Finance.class, company.getId());
        if (finance == null) {
            finance = new Finance(company);
        }
        /*
        service = dao.findById(Service.class, company.getId());
        if (service == null) {
        service = new Service(company);
        }
        if (service.isPayByCheck()) {
        setPaymentType(0);
        } else {
        setPaymentType(1);
        }
        Contact deliveryContact = service.getOtherContact();
        if (deliveryContact != null) {
        copyContact(deliveryContact, otherContact);
        otherContact.setCaption(deliveryContact.getCaption());
        }
         */
        company.autoNormalizeContact();
        
        loadStores();
        loadUsers();
        int storesSize = storeList.size();
        for (Store storeItem : storeList) {
            List<Contact> lstContacts = storeItem.getContactList();
            int cntctsSize = lstContacts.size();
        }
        ownerList = dao.get("from Owner where company=" +
                String.valueOf(getSelectedAccount().getId()) +
                "order by numberOfShares");
        if (finance.getFiscalDate() == null) {
            Calendar clndr = Calendar.getInstance();
            clndr.set(clndr.get(Calendar.YEAR), 11, 31, 0, 0);
            finance.setFiscalDate(clndr.getTime());
        }
        // load seletedStoreInfo
        //copyStore(getSelectedStore(), store);
        store = getSelectedStore();
        store.autoNormalizeContact();
    }

    public String refresh() {
        loadData();
        String page = getTargetPage();
        return page;
    }

    public void setMyAccount() {
        setSelectedAccount(userManagedBean.getCompanyOfActiveUser());
    }

    public void initMyAccount(PhaseEvent ev) {
        if (ev.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            setMyAccount();
        }
    }

    public Company getCompany() {
        if (company == null) {
            loadData();
        }
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Finance getFinance() {
        return finance;
    }

    public void setFinance(Finance finance) {
        this.finance = finance;
    }
    /* temporary remuoved from ver1, it is enchancement for ver2
    public Service getService() {
    return service;
    }

    public void setService(Service service) {
    this.service = service;
    }

    public Contact getOtherContact() {
    return otherContact;
    }

    public void setOtherContact(Contact otherContact) {
    this.otherContact = otherContact;
    }

    public Integer getPaymentType() {
    return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
    if (paymentType == 0) {
    service.setPayByCheck(true);
    service.setPayByCreditCard(false);
    } else {
    service.setPayByCheck(false);
    service.setPayByCreditCard(true);
    }

    this.paymentType = paymentType;
    }
     */

    public Company getSelectedAccount() {
        if (selectedAccount == null) {
            setMyAccount();
        }

        return selectedAccount;
    }

    public void setSelectedAccount(Company selectedAccount) {
        if ((selectedAccount != null &&
                !selectedAccount.equals(this.selectedAccount)) ||
                selectedAccount == null) {
            this.selectedAccount = selectedAccount;
            setSelectedStore(null);

            loadData();
        }
    }

    public Map getStateItems() {
        return stateManagedBean.getStateItems();
    }

    public Map getFranchiseItems() {
        return franchiseManagedBean.getFranchiseItems();
    }

// COPY methods
//==============================================================================
    private void copyStore(Store source, Store target) {
        target.setCaption(source.getCaption());
        target.setCompany(source.getCompany());
        target.setFranchise(source.getFranchise());
        target.setNumber(source.getNumber());
        target.setAccepted(source.isAccepted());
        target.setIfOther(source.getIfOther());
        source.autoNormalizeContact();
        target.autoNormalizeContact();

        for (int i = 0; i < 3; i++) {
            copyContact(source.getContactList().get(i), target.getContactList().get(i));
        }
    }

    // generic copy address
    private void copyContact(Contact source, Contact target) {
        target.setAddress(source.getAddress());
        target.setCity(source.getCity());
        target.setCountry(source.getCountry());
        target.setState(source.getState());
        target.setZip(source.getZip());
        target.setPhone1(source.getPhone1());
        target.setFax(source.getFax());
    }

    public String companyToPrimary1() {
        copyContact(company.getContactList().get(0), company.getContactList().get(1));
        return currentPage;
    }

    public String companyToPrimary2() {
        copyContact(company.getContactList().get(0), company.getContactList().get(2));
        return currentPage;
    }

    public String companyToStore() {
        copyContact(company.getContactList().get(0), store.getContactList().get(0));
        return currentPage;
    }

    public String storeToContact1() {
        copyContact(store.getContactList().get(0), store.getContactList().get(1));
        return currentPage;
    }

    public String storeToContact2() {
        copyContact(store.getContactList().get(0), store.getContactList().get(2));
        return currentPage;
    }

    public void updateCompaniesContact() {
        company.getContactList().get(0).setCaption(company.getCaption());
        //TODO: update Captions for StoreList
    }

    public void updateStoresCaption(Store item) {
        item.getContactList().get(0).setCaption(item.getCaption());
    }

    public String save() {
        removeEmptyRows(); // remove empty Owners
        if (!validateOwnerList())
            return null;

        updateCompaniesContact();
        activateCompany();
        dao.saveOrUpdate(company);
        
        /* temporary remuoved from ver1, it is enchancement for ver2
        otherContact.setId(null);
        Contact delContact = null;
        Contact prevContact = service.getOtherContact();
        if (service.getDeliveryType() != 3) {
        // if contact don't need - only remove it
        if (prevContact != null) {
        service.setOtherContact(null);
        delContact = prevContact;
        }
        } else {
        // cotact is needed
        if (prevContact != null) { // updare contact
        copyContact(otherContact, prevContact);
        prevContact.setCaption(otherContact.getCaption());
        dao.saveOrUpdate(prevContact);
        } else { // create new
        dao.saveOrUpdate(otherContact);
        service.setOtherContact(otherContact);
        }
        }

        dao.saveOrUpdate(service);
        if (delContact != null) {
        dao.delete(delContact);
        }
         */

        saveOwnerList();
        saveStoreList();

        loadStores();
        loadUsers();
        return getTargetPage();
    }
// OWNER
//==============================================================================
    private List<Owner> changedOwnerRecords = new LinkedList<Owner>();
    private List<Owner> removeOwnerRecords = new LinkedList<Owner>();
    private Owner selectedOwner;
    private int emptyRowsCount = 3;

    public void reInitOwnerList() {
        ownerList = dao.get("from Owner where company=" +
                String.valueOf(getSelectedAccount().getId()) +
                "order by numberOfShares");
        changedOwnerRecords.clear();
        removeOwnerRecords.clear();
    }

    public void addEmptyRows() {
        int additionalRows = emptyRowsCount - ownerList.size();
        for (int i = 0; i < additionalRows; i++) {
            Owner newOwner = createNewOwner();
            ownerList.add(newOwner);
        }
    }

    public void removeEmptyRows() {
        Collection<Owner> removeList = new LinkedList<Owner>();

        for (Owner owner : ownerList) {
            if (!isForSave(owner)) {
                removeList.add(owner);
            }
        }
        ownerList.removeAll(removeList);
    }

    private boolean isForSave(Owner owner) {
        if (owner.getId() != null) {
            return true;
        }

        if (!owner.getFirstName().trim().isEmpty() ||
            !owner.getLastName().trim().isEmpty() ||
            owner.getNumberOfShares() != 0) {
            return true;
        }

        if (!owner.getSharePercent().equals(BigDecimal.ZERO) ||
            !owner.getProfit().equals(BigDecimal.ZERO) ||
            !owner.getLoss().equals(BigDecimal.ZERO)) {
            return true;
        }

        return false;
    }

    public List<Owner> getOwnerList() {
        // reInitOwnerList();
        addEmptyRows();
        return ownerList;
    }

    public void setOwnerList(List<Owner> ownerList) {
        this.ownerList = ownerList;
    }

    public void saveOwnerList() {
        //removeEmptyRows();
        ownerService.removeAll(removeOwnerRecords);
        ownerService.mergeAll(ownerList); // saveOrUpdate
    }

    public String deleteOwner() {
        ownerList.remove(selectedOwner);
        removeOwnerRecords.add(selectedOwner);

        return currentPage;
    }

    public Owner getSelectedOwner() {
        return selectedOwner;
    }

    public void setSelectedOwner(Owner selectedOwner) {
        this.selectedOwner = selectedOwner;
    }

    private Owner createNewOwner() {
        Owner newOwner = new Owner();
        newOwner.setCompany(company);
        return newOwner;
    }

    public String addOwner() {
        ownerList.add(createNewOwner());
        return currentPage;
    }
// STORE
//==============================================================================
    private List<Store> removeStoreRecords = new LinkedList<Store>();

    private void loadStores() {
        storeList = dao.get("from Store where company=" +
                String.valueOf(getSelectedAccount().getId()) +
                "order by caption");
        if (storeList == null) {
            storeList = new LinkedList<Store>();
        }
    }

    public List<Store> getStoreList() {
        return storeList;
    }

    public List<Store> getUserStoreList() {
        // filter stores for user
        // if user add store it is available for it
        // bug - user can view
        return storeList;
    }
    
    public Store getSelectedStore() {
        if (selectedStore == null) {
            if (storeList != null && !storeList.isEmpty()) {
                //get first unaccepted store or first store from list
                boolean allIsAccepted = true;
                for (Store item : storeList) {
                    if (item.isAccepted() == false) {
                        selectedStore = item;
                        allIsAccepted = false;
                        break;
                    }
                }
                if (allIsAccepted) {
                    selectedStore = storeList.get(0);
                }
            } else {
                addStore(); //TODO: TEMPRORARY STORE! IF not saved - Change selected!
            }
        }

        return selectedStore;
    }

    public void setSelectedStore(Store selectedStore) {
        this.selectedStore = selectedStore;
    }

    public void saveStoreList() {
//      storeService.removeAll(removeStoreRecords);
        for (Store item : storeList) {
            item.autoNormalizeContact();
        }
        for (Store item : storeList) {
            storeService.saveOrUpdate(item);
            if (dao.get("from Folder where store="+item.getId()).isEmpty())
            {
                List<Folder> lst=Folder.getDefaultFolder();
                for (Folder selfolder : lst) {
                    selfolder.setStore(item);
                    dao.saveOrUpdate(selfolder);
                }
            }
        }
    }
    /*
    public String deleteStore() {
    storeList.remove(selectedStore);
    removeStoreRecords.add(selectedStore);

    return currentPage;
    }
     */

    private Store createNewStore() {
        Store newStore = new Store();
        newStore.setCompany(company);
        newStore.setCaption(company.getCaption());
        newStore.setRequestDate(new Date());
        newStore.setAccepted(true);
        newStore.autoNormalizeContact();

        return newStore;
    }

    public String addStore() {

        Store item = createNewStore();
        storeList.add(item);
        //copyStore(item, store);
        store = item;
        setSelectedStore(item);

        return currentPage;
    }

// Current edited store
    @Override
    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public void activateCompany() {
        boolean allAccepted = true;
        for (Store item : storeList) {
            if (!item.isAccepted()) {
                allAccepted = false;
                break;
            }
        }
        if (allAccepted) {
            company.setAccepted(true);
            company.setRequestDate(new Date()); // Accepted Date
        }
    }

    public String activateStore() {
        getSelectedStore().setAccepted(true);
        getSelectedStore().setRequestDate(new Date());
        return currentPage;
    }

    public String accessStore() {
        store = getSelectedStore();
        store.autoNormalizeContact();
        // copyStore(getSelectedStore(), store);
        return currentPage;
    }

// USER
//==============================================================================
    private void loadUsers() {
        userList = dao.get("from Person where company=" +
                String.valueOf(getSelectedAccount().getId()) +
                "order by lastName");
        if (userList == null) {
            userList = new LinkedList<Person>();
        }

    }

    public List<Person> getUserList() {
        return userList;
    }

    public Person getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(Person selectedUser) {
        this.selectedUser = selectedUser;
    }
    /*
    public String viewUser() {
    return "createuser";
    }
     */
    private Date beginOfYear = Week.getYearBegin();
    private Date endOfYear = Week.getYearEnd();

    public Date getBeginOfYear() {
        return beginOfYear;
    }

    public Date getEndOfYear() {
        return endOfYear;
    }

    private boolean validateOwnerList() {
        boolean firstFailed = false;
        boolean lastFailed = false;
        for(Owner item : ownerList) {
            if (item.getFirstName().trim().isEmpty()) {
                firstFailed = true;
            }
            if (item.getLastName().trim().isEmpty()) {
                lastFailed = true;
            }
        }

        if (!firstFailed && !lastFailed)
            return true;

        FacesContext ctx = FacesContext.getCurrentInstance();
        String errMessage = "";

        if (firstFailed) {
            errMessage = "Error: Owner's First Name is required";
        }
        if (lastFailed) {
            errMessage = "Error: Owner's Last Name is required";
        }
        ctx.addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                       errMessage, null));
        return false;
    }
}
