package com.zuma.web.controllers;

import com.zuma.db.core.domain.Company;
import com.zuma.db.core.domain.Person;
import com.zuma.db.service.CompanyService;
import java.util.List;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class AccountManagementController extends ParentController {
    @Autowired
    protected CompanyService companyService;
    private List<Company> accountPendingList;
    private List<Company> accountExistingList;
    private String pattern = "";
    private final String currentPage = null;

    public void loadData() {
        accountPendingList = companyService.getPending();
        accountExistingList = companyService.getExisting(0,pattern);
    }

    public List<Company> getAccountPendingList() {
        return accountPendingList;
    }

    public List<Company> getAccountExistingList() {
        return accountExistingList;
    }

    public List<Person> getPersonList() {       
        return dao.get("from Person where role=0"); //TOD0: verify null
    }
    
    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String search() {
        accountExistingList = companyService.getExisting(0,getPattern());
        return currentPage;
    }

    public void prepareContent(PhaseEvent ev) {
        if (ev.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            loadData();
        }
    }

    public String refreshPage() {
        // loadData();
        return getTargetPage();
    }
}
