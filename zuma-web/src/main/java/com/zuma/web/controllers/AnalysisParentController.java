/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers;

import com.zuma.db.core.domain.DirectPercentTrackerData;
import com.zuma.db.core.domain.EditableTargetTrackerData;
import com.zuma.db.core.domain.Store;
import com.zuma.db.core.domain.TrackerData;
import com.zuma.web.controllers.helper.analysis.AdvertizingFeesLoader;
import com.zuma.web.controllers.helper.analysis.DataLoader;
import com.zuma.web.controllers.helper.analysis.DiscountsLoader;
import com.zuma.web.controllers.helper.analysis.FBLoader;
import com.zuma.web.controllers.helper.analysis.LaborLoader;
import com.zuma.web.controllers.helper.analysis.Net2Loader;
import com.zuma.web.controllers.helper.analysis.Net3Loader;
import com.zuma.web.controllers.helper.analysis.PaperLoader;
import com.zuma.web.controllers.helper.analysis.RoyaltyFeesLoader;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;

public class AnalysisParentController extends ParentController {

    public static final String net2Caption = "Net 2 (Net 1- Manual Voids)";
    public static final String net3Caption = "Net 3 (Net 2 - Discounts)";
    public static final String discountsStr = "Discounts";
    protected Store oldStore;
    protected Calendar oldDate;
    private Map<String, TrackerData> dataMap = new HashMap<String, TrackerData>();
    private Map<String, EditableTargetTrackerData> dataTargetMap = new HashMap<String, EditableTargetTrackerData>();
    private List<String> names = new ArrayList<String>();

    {
        names.add("Food and Beverage");
//        names.add("Waste");
        names.add("Paper");
        names.add("Labor");
        names.add("Royalty Fees");
        names.add("Advertising Fees");

    }

    public List<String> getNames() {
        return names;
    }

    public Map<String, TrackerData> getDataMap() {
        return dataMap;
    }

    public Map<String, EditableTargetTrackerData> getDataTargetMap() {
        return dataTargetMap;
    }

    protected boolean isReloadingNeeded() {
        boolean flag = isNeedRefresh();
        if (flag) {
            setNeedRefresh(false);
        }
        return flag || !getStore().equals(oldStore) || !getStartDate().equals(oldDate);
    }

    protected void clear() {
        getDataMap().clear();
        dataTargetMap.clear();
    }

    protected void loadData() throws ParseException, Exception {
        oldStore = getStore();
        oldDate = (Calendar) getStartDate().clone();
        clear();
        loadEditableTargetData();

        DirectPercentTrackerData net3 = new DirectPercentTrackerData();
        net3.setCaption(net3Caption);
        DataLoader loader = new Net3Loader(dao, getStore(), getStartDate(), getFinishDate());
        prepareData(net3, loader);
        net3.setPercentCurrent(new BigDecimal(1));
        net3.setPercentMTD(new BigDecimal(1));
        net3.setPercentYTD(new BigDecimal(1));

        //TrackerData net2 = new DirectPercentTrackerData();
        //net2.setCaption(net2Caption);
        loader = new Net2Loader(dao, getStore(), getStartDate(), getFinishDate());
        TrackerData net2 = prepareEditableTargetData(net2Caption, null, loader);
        //prepareData(net2, loader);

        loader = new DiscountsLoader(dao, getStore(), getStartDate(), getFinishDate());
        TrackerData discounts = prepareEditableTargetData(discountsStr, net2, loader);
        discounts.setCalculatedTarget(discounts.getTarget().multiply(net2.getTarget()).divide(new BigDecimal(100)));
        net3.setCalculatedTarget(net2.getTarget().subtract(discounts.getCalculatedTarget()));
        //     loader = new WasteLoader(dao, getStore(), getStartDate(), getFinishDate());
        //     prepareEditableTargetData("Waste", net2, loader);

        loader = new LaborLoader(dao, getStore(), getStartDate(), getFinishDate());
        prepareEditableTargetData("Labor", net2, loader);

        loader = new FBLoader(dao, getStore(), getStartDate(), getFinishDate());
        prepareEditableTargetData("Food and Beverage", net2, loader);

        loader = new PaperLoader(dao, getStore(), getStartDate(), getFinishDate());
        prepareEditableTargetData("Paper", net2, loader);

        loader = new RoyaltyFeesLoader(dao, getStore(), getStartDate(), getFinishDate());
        prepareEditableTargetData("Royalty Fees", net3, loader);

        loader = new AdvertizingFeesLoader(dao, getStore(), getStartDate(), getFinishDate());
        prepareEditableTargetData("Advertising Fees", net3, loader);

        //net2.setPercentCurrent(net3.getPercentCurrent().add(discounts.getPercentCurrent()));
        //net2.setPercentMTD(net3.getPercentMTD().add(discounts.getPercentMTD()));
        //net2.setPercentYTD(net3.getPercentYTD().add(discounts.getPercentYTD()));
        net2.setBaseCurrent(net3.getPercentCurrent().add(discounts.getPercentCurrent()));
        dataMap.put(net2.getCaption(), net2);
        dataMap.put(net3.getCaption(), net3);

        for (String name : getNames()) {
            EditableTargetTrackerData data = getDataTargetMap().get(name);
            data.setCalculatedTarget(data.getTarget().multiply(net2.getTarget()).divide(new BigDecimal(100)));
        }
        EditableTargetTrackerData data = getDataTargetMap().get("Royalty Fees");
        data.setCalculatedTarget(data.getTarget().multiply(net3.getCalculatedTarget()).divide(new BigDecimal(100)));
        data = getDataTargetMap().get("Advertising Fees");
        data.setCalculatedTarget(data.getTarget().multiply(net3.getCalculatedTarget()).divide(new BigDecimal(100)));
    }

    public void recalculate(PhaseEvent ev) throws ParseException, Exception {
        if (ev.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            if (isReloadingNeeded()) {
                loadData();
            }
        }
    }

    protected TrackerData prepareData(TrackerData data, DataLoader loader) throws ParseException {
        data.setActualCurrent(loader.getCurrent());
        data.setActualMTD(loader.getMTD());
        data.setActualYTD(loader.getYTD());
        return data;
    }

    protected EditableTargetTrackerData prepareEditableTargetData(String name, TrackerData base, DataLoader loader) throws ParseException {
        EditableTargetTrackerData data = getDataTargetMap().get(name);
        prepareData(data, loader);
        if (base != null) {
            data.setBaseCurrent(base.getActualCurrent());
            data.setBaseMTD(base.getActualMTD());
            data.setBaseYTD(base.getActualYTD());
        }
        return data;
    }

    protected EditableTargetTrackerData addIfNotExist(String name, Collection<EditableTargetTrackerData> col) {
        EditableTargetTrackerData result = null;
        boolean exist = false;
        for (EditableTargetTrackerData data : col) {
            if (data.getCaption().equals(name)) {
                exist = true;
            }
        }
        if (!exist) {
            result = new EditableTargetTrackerData();
            result.setCaption(name);
            result.setStore(getStore());
            col.add(result);
        }
        return result;
    }

    protected Collection<EditableTargetTrackerData> initEditableTargetData(Collection<EditableTargetTrackerData> col) {
        Collection<EditableTargetTrackerData> result = col;
        addIfNotExist(discountsStr, col);
        addIfNotExist(net2Caption, col);

        for (String name : getNames()) {
            addIfNotExist(name, col);
        }
        return result;
    }

    protected void loadEditableTargetData() {
        String sql = "FROM EditableTargetTrackerData WHERE"
                + " store.id=" + getStore().getId().toString();
        Collection<EditableTargetTrackerData> col = dao.get(sql);
        if (col == null || col.size() < 6) {
            col = initEditableTargetData(col);
        }
        for (EditableTargetTrackerData tmp : col) {
            getDataTargetMap().put(tmp.getCaption(), tmp);
        }
    }
}

