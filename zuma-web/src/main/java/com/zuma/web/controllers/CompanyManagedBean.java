package com.zuma.web.controllers;

import java.io.Serializable;

public class CompanyManagedBean implements Serializable {
    private String companyName;
    private String franchiseAffilation;

    // store address
    private String storeAddress;
    private String storeCity;
    private String storeState;
    private String storeZip;

    // primary contact
    private String primaryFirstName;
    private String primaryLastName;
    private String primaryPhone;
    private String primaryEmail;
    private String primaryConfirmEmail;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getFranchiseAffilation() {
        return franchiseAffilation;
    }

    public void setFranchiseAffilation(String franchiseAffilation) {
        this.franchiseAffilation = franchiseAffilation;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getStoreCity() {
        return storeCity;
    }

    public void setStoreCity(String storeCity) {
        this.storeCity = storeCity;
    }

    public String getStoreState() {
        return storeState;
    }

    public void setStoreState(String storeState) {
        this.storeState = storeState;
    }

    public String getStoreZip() {
        return storeZip;
    }

    public void setStoreZip(String storeZip) {
        this.storeZip = storeZip;
    }

    public String getPrimaryFirstName() {
        return primaryFirstName;
    }

    public void setPrimaryFirstName(String primaryFirstName) {
        this.primaryFirstName = primaryFirstName;
    }

    public String getPrimaryLastName() {
        return primaryLastName;
    }

    public void setPrimaryLastName(String primarySecondName) {
        this.primaryLastName = primarySecondName;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    public String getPrimaryEmail() {
        return primaryEmail;
    }

    public void setPrimaryEmail(String primaryEmail) {
        this.primaryEmail = primaryEmail;
    }

    public String getPrimaryConfirmEmail() {
        return primaryConfirmEmail;
    }

    public void setPrimaryConfirmEmail(String primaryConfirmEmail) {
        this.primaryConfirmEmail = primaryConfirmEmail;
    }

    public String openAccount()
    {
        //TODO: Verify and Save to Data Base
        return "success";
    }
}
