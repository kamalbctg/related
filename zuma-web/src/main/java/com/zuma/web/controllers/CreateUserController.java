package com.zuma.web.controllers;

import com.zuma.db.core.domain.Company;
import com.zuma.db.core.domain.Folder;
import com.zuma.db.core.domain.Page;
import com.zuma.db.core.domain.Person;
import com.zuma.db.core.domain.PersonFolder;
import com.zuma.db.core.domain.PersonPage;
import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.UniversalDAO;
import com.zuma.db.service.CompanyService;
import com.zuma.db.service.impl.GenericServiceImpl;
import com.zuma.web.controllers.helper.TreeSelectMap;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@Component
@Scope("session")
public class CreateUserController extends EmailSenderController {

    @Autowired
    protected AccountManagedBean account;
    @Autowired
    protected UniversalDAO dao;
    @Autowired
    private CompanyService companyService;
    @Autowired
    @Qualifier("personService")
    protected GenericServiceImpl<Person, Integer> personService;
    private List<Company> companyList;
    private Company activeCompany, originCompany;
    private ArrayList<Store> storeList = new ArrayList<Store>();
    private ArrayList<Page> pageList = new ArrayList<Page>();
    private ArrayList<Folder> folderList = new ArrayList<Folder>();
    @Autowired
    protected GenericServiceImpl<Store, Integer> storeService;
    @Autowired
    protected StateManagedBean stateManagedBean;
    private Person person = new Person();
    private String password;
    private String passwordConfirm;
    private Map<String, Page> mapPage = new TreeMap<String, Page>();
    private Map<String, Folder> mapFolder = new TreeMap<String, Folder>();
    private Map<String, Store> mapStore = new TreeSelectMap<Store>();
    private Person selectedUser;
    private boolean showSuccessPage = false;
    private boolean autoPassword = true;

    public boolean isAutoPassword() {
        return autoPassword;
    }

    public void setAutoPassword(boolean autoPassword) {
        this.autoPassword = autoPassword;
    }

    public boolean isShowSuccessPage() {
        return showSuccessPage;
    }

    private void refreshPage() {
        person = new Person();
        person.setPassword("");
        getFolderList().clear();
        getPageList().clear();
        getStoreList().clear();
        setAutoPassword(true);
        originCompany = null;
        password = "";
        passwordConfirm = "";
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public ArrayList<Store> getStoreList() {
        return storeList;
    }

    public void setStoreList(ArrayList<Store> storeList) {
        this.storeList = storeList;
    }

    public ArrayList<Folder> getFolderList() {
        return folderList;
    }

    public void setFolderList(ArrayList<Folder> folderList) {
        this.folderList = folderList;
    }

    public ArrayList<Page> getPageList() {
        return pageList;
    }

    public void setPageList(ArrayList<Page> pageList) {
        this.pageList = pageList;
    }

    @PostConstruct
    public void loadData() {
        companyList = companyService.getAll();
        activeCompany = companyService.findById(getActiveCompany().getId());
        changeCompany();
        List<Page> lst = dao.get("from Page where role=0");
        for (Page page : lst) {
            mapPage.put(page.getCaption(), page);
        }
    }

    public void setCompanyService(CompanyService companyService) {
        this.companyService = companyService;
    }

    public Company getActiveCompany() {
        if (activeCompany == null) {
            if (!companyList.isEmpty()) {
                activeCompany = companyList.get(0);
            }
        }
        return activeCompany;
    }

    public void setActiveCompany(Company activeCompany) {
        if (this.activeCompany == activeCompany) // nothing to do
        {
            return;
        } else {
            this.activeCompany = activeCompany;
        }

        // Collection<Store> storeList = activeCompany.getStoreList(); // TODO!!!!!
    }

    public List<Company> getCompanyList() {
        return companyList;
    }

    public void setCompanyList(List<Company> companyList) {
        this.companyList = companyList;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Map getAccountItems() {
        Map<String, Company> choices = new HashMap<String, Company>();
        for (Company company : companyList) {
            //company = companyService.merge(company); // refresh Id
            choices.put(company.getCaption(), company);
        }
        return choices;
    }

    public Map getStoreItems() { //TODO: Optimize loading
        return mapStore;
    }

    public boolean isVisibleStore() {
        return (getStoreItems().size() > 1);
    }

    public boolean isVisibleFolder() {
        return (getFolderItems().size() > 1);
    }

    public List<Page> setListPage(Page page) {
        return new ArrayList<Page>(getPageItems().values());
    }

    public Map getPageItems() {
        return mapPage;
    }

    public Map getFolderItems() {

        return mapFolder;
    }

    public Map getStateItems() {
        return stateManagedBean.getStateItems();
    }

    public String changeCompany() {
        mapStore.clear();
        activeCompany = dao.findById(Company.class, getActiveCompany().getId());
        Collection<Store> stList = activeCompany.getStoreList();

        for (Store store : stList) {
            if (store.getContactList().size() > 0 && store.getContactList().get(0) != null && !store.getContactList().get(0).getAddress().isEmpty()) {
                mapStore.put(store.getContactList().get(0).getAddress(), store);
            }
        }

        mapFolder.clear();
        List<Store> lst = dao.get("from Store s where company=" + String.valueOf(activeCompany.getId()) + " order by Id ");
        int minStore = lst.iterator().next().getId();

        //   Store selStore = stList.iterator().next();
        List<Folder> lstFldr = dao.get("from Folder where store=" + String.valueOf(minStore));
        for (Folder folder : lstFldr) {
            mapFolder.put(folder.getName(), folder);
        }
        storeList.clear();
        pageList.clear();
        folderList.clear();
        if (getSelectedUser() != null && originCompany.equals(activeCompany)) {
            Collection<Store> cs = getPerson().getStoreList();
            for (Store store : cs) {
                storeList.add(store);
            }

            Collection<PersonPage> cpp = person.getPersonPageList();
            for (PersonPage pp : cpp) {
                pageList.add(pp.getPage());

            }
            Collection<PersonFolder> cpf = getPerson().getPersonFolders();
            for (Iterator<PersonFolder> it = cpf.iterator(); it.hasNext();) {
                PersonFolder pf = it.next();
                if (mapFolder.containsKey(pf.getFolder().getName())) {
                    folderList.add(mapFolder.get(pf.getFolder().getName()));

                }
            }
        }

        return null;
    }

    private boolean saveToDB(String pageStr, String folderStr) {

        if (!validateData()) {
            return false;
        }
        try {

            person.setCompany(activeCompany);
            getPerson().setStoreList(storeList);
            boolean updated = false;
            if (getPerson().getId() != null) {
                updated = true;
            }
            if (updated) {
                personService.merge(getPerson());
            } else {
                personService.saveOrUpdate(getPerson());
            }
            //save personpage
            getPerson().getPersonPageList().clear();
            getPerson().getPersonFolders().clear();
            for (Page page : pageList) {
                pageStr += " " + page.getCaption() + ",";
                PersonPage pp = new PersonPage(page, 1);
                pp.setPerson(getPerson());
                getPerson().getPersonPageList().add(pp);

            }
            //save personfolder
            for (Store store : storeList) {
                List<Folder> lstFldr = dao.get("from Folder where parent=null and store=" + String.valueOf(store.getId()) + "order by id");
                for (Folder selfolder : getFolderList()) {
                    for (Folder folder : lstFldr) {
                        if (selfolder.getName().equalsIgnoreCase(folder.getName())) {
                            if (!folderStr.contains(folder.getName())) {
                                folderStr += " " + folder.getName() + ",";
                            }
                            PersonFolder pf = new PersonFolder(folder, 2);
                            pf.setPerson(getPerson());
                            getPerson().getPersonFolders().add(pf);
                            dao.saveOrUpdate(pf);
                            break;
                        }
                    }

                }
            }
            if (updated) {
                dao.merge(getPerson());
            } else {
                dao.saveOrUpdate(getPerson());
            }

        } catch (Exception ex) {

            FacesContext ctx = FacesContext.getCurrentInstance();

            ctx.addMessage("NewUser",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error CreateUser", null));
            return false;
        }
        return true;
    }

    public String saveData() {

        String toReturn = null;
        String pageStr = "", folderStr = "";
        if (saveToDB(pageStr, folderStr)) {
            toReturn = "success";
            account.refresh();
        }
        return toReturn;
    }

    public String sendData() {
        String toReturn = null;
        try {

            FacesContext ctx = FacesContext.getCurrentInstance();

            // check mail
            String mail = getPerson().getContact().getEmail();

            if (mail.isEmpty() || mail.trim().length() < 4) {
                ctx.addMessage("NewUser:Email",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error: Field E-Mail in this case is required field", null));
                return toReturn;
            }
            if (!validateData()) {
                return toReturn;
            }

            SecureRandom random = new SecureRandom();
            String passwordMail = new BigInteger(130, random).toString(8).substring(1, 10);
            if (!isAutoPassword()) { // rewrite password
                if (!getPassword().equalsIgnoreCase(getPasswordConfirm())) {
                    ctx.addMessage("NewUser:passwordConfirm",
                            new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Error: Please make sure Password and Confirm Password fields are matching.", null));
                    return toReturn;
                }
                passwordMail = getPasswordConfirm();
                person.setPassword(passwordMail);
            } else { // auto password
                // if user was not previously activated and password != ": auto :" than not set password for him
                if (person.getPassword().equalsIgnoreCase(": auto :")) {
                    person.setPassword(passwordMail); // set auto-generated password
                } else {
                    String pass = person.getPassword();
                    if (pass.substring(0, 2).equals("??")) { // encode user-defined password
                        passwordMail = pass.substring(2);
                        person.setPassword(passwordMail);
                    } else { // nothing to do
                        passwordMail = "user  defined"; // after locking user ad re-activating
                    }
                }

            }
            person.setRole(1); // activate Person

            String pageStr = "", folderStr = "";
            saveToDB(pageStr, folderStr);

            sendMail(pageStr, folderStr, passwordMail);

        } catch (FileNotFoundException efnf) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage("NewUser",
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "User was succesfully created, but Error occured when generate and send Mail", null));
            refreshPage();

        } catch (IOException eio) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage("NewUser",
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "User was succesfully created, but Error occured when generate and send Mail", null));
            refreshPage();

        }
        showSuccessPage = true;

        return toReturn;
    }

    public String redirectPage() {
        showSuccessPage = false;
        account.refresh();
        return "success";
    }

    private boolean sendMail(String pageStr, String folderStr, String password)
            throws FileNotFoundException, IOException {
        FacesContext fctx = FacesContext.getCurrentInstance();
        ServletContext sctx = (ServletContext) fctx.getExternalContext().getContext();
        String path = sctx.getRealPath(File.separator);
        FileInputStream fis = new FileInputStream(path + "prop" + File.separator + "mail.properties");
        Properties prop = new Properties();
        prop.load(fis);

        String storeStr = "";
        for (Store store : storeList) {
            storeStr += store.getCaption() + ", ";
        }

        if (storeStr.length() > 5) {
            storeStr = storeStr.substring(0, storeStr.length() - 2);
        }

        if (pageStr.length() > 5) {
            pageStr = pageStr.substring(0, pageStr.length() - 1);
        }

        if (folderStr.length() > 5) {
            folderStr = folderStr.substring(0, folderStr.length() - 1);
        }

        String body = String.format(prop.getProperty("userBody"), getPerson().getFirstName(), getPerson().getLastName(), getPerson().getLogin(), password,
                activeCompany.getCaption(), storeStr, pageStr, folderStr);
        return sendEmail(getPerson().getContact().getEmail(),
                prop.getProperty("userBcc"),
                new Date(),
                prop.getProperty("userSubject"),
                body,
                prop.getProperty("userFrom"), prop.getProperty("userFromName"),
                prop.getProperty("userReply"));
    }

    private boolean validateData() {
        boolean toReturn = true;

        FacesContext ctx = FacesContext.getCurrentInstance();

        // check login is exist
        String login = person.getLogin();
        List<Person> lstFldr = dao.get((String) "from Person where login='" + login + "'");
        if (!lstFldr.isEmpty() && person.getId() == null) {
            ctx.addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error: Login is exist in the database", null));
            toReturn = false;
        }
        return toReturn;
    }

    public Person getSelectedUser() {
        return selectedUser;
    }

    public void refreshUser() {
        //companyList = companyService.getAll();

        if (getSelectedUser() != null) {

            setPerson(personService.findById(selectedUser.getId()));
            originCompany = companyService.findById(getPerson().getCompany().getId());
            setActiveCompany(originCompany);
            changeCompany();


        } else {
            refreshPage();
        }
    }

    public void setSelectedUser(Person selectedUser) {

        this.selectedUser = selectedUser;
        refreshUser();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
