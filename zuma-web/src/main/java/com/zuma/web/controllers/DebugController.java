package com.zuma.web.controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.collections.iterators.EnumerationIterator;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.zuma.db.dao.InvalidSQLSyntaxException;
import com.zuma.db.service.GenericSQLProcessorService;

@Controller
@Scope("session")
public class DebugController {
	private static final long MEGABYTE = 1024L * 1024L;
	private static final String LINE_SEPERATOR = System
			.getProperty("line.separator");
	@Autowired
	private GenericSQLProcessorService genericSQLProcessorService;

	private String userEnteredSql = " select x.a, y.a, z.a from ( select 0 a union all select 2 union all select 3 union all select 4 union all select 5 ) x cross join  ( select 0 a union all select 2 union all select 3 union all select 4 union all select 5 ) y cross join ( select 0 a union all select 2 union all select 3 union all select 4 union all select 5 ) z order by x.a, y.a, z.a";

	private Map<String, String> debugInfoMap;

	private List<String> sqlResultStrings;

	private int sqlExecutionCount = 0;

	public Map<String, String> getDebugInfoMap() {
		if (debugInfoMap == null) {
			Map<String, String> evnKeyValueMap = new HashMap<String, String>();
			Map<String, String> variables = System.getenv();
			Properties properties = System.getProperties();

			for (Map.Entry<String, String> entry : variables.entrySet()) {
				String name = entry.getKey();
				String value = entry.getValue();
				evnKeyValueMap.put(name, value);

			}

			Enumeration<Object> propertyKeys = properties.keys();
			Iterator<Object> propertyKeyIterator = new EnumerationIterator(
					propertyKeys);
			while (propertyKeyIterator.hasNext()) {
				Object key = propertyKeyIterator.next();
				evnKeyValueMap.put(key.toString(), properties.get(key)
						.toString());
			}
			evnKeyValueMap.put(
					"total amount of memory in the Java virtual machine",
					Runtime.getRuntime().totalMemory() / MEGABYTE + "MB");
			evnKeyValueMap.put("free memory in the Java virtual machine",
					Runtime.getRuntime().freeMemory() / MEGABYTE + "MB");
			evnKeyValueMap
					.put(
							"maximum amount of memory that the Java virtual machine will attempt to use",
							Runtime.getRuntime().maxMemory() / MEGABYTE + "MB");
			debugInfoMap = evnKeyValueMap;
		}
		return debugInfoMap;
	}

	public String executeSQLAndMarkTime() throws HibernateException,
			SQLException {
		long start = System.currentTimeMillis();
		try {
			for (int i = 0; i < sqlExecutionCount; i++) {
				sqlResultStrings = genericSQLProcessorService
						.execute(getUserEnteredSql());
			}
			double totalSeconds = (System.currentTimeMillis() - start) / 1000.0;
			addFacesMessageForUI("Total time = " + totalSeconds + " sec");
		} catch (InvalidSQLSyntaxException e) {
			addFacesMessageForUI("Invalid sql");
		}
		return null;
	}

	protected void addFacesMessageForUI(String uiMessage) {
		FacesMessage facesMessage = new FacesMessage(uiMessage);
		FacesContext facesContext = FacesContext.getCurrentInstance();

		facesContext.addMessage(null, facesMessage);
	}

	public List<String> getDebugInfKeyList() {
		return new ArrayList<String>(getDebugInfoMap().keySet());
	}

	public void setDebugInfKeyList(List<String> list) {
		// not used
	}

	public String getUserEnteredSql() {
		return userEnteredSql;
	}

	public void setUserEnteredSql(String userEnteredSql) {
		this.userEnteredSql = userEnteredSql;
	}

	public List<String> getSqlResultStrings() {
		return sqlResultStrings;
	}

	public void setSqlResultStrings(List<String> sqlResultStrings) {
		this.sqlResultStrings = sqlResultStrings;
	}

	public int getSqlExecutionCount() {
		return sqlExecutionCount;
	}

	public void setSqlExecutionCount(int sqlExecutionCount) {
		this.sqlExecutionCount = sqlExecutionCount;
	}

	
}