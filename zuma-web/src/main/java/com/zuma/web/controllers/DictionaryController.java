package com.zuma.web.controllers;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.zuma.db.core.domain.Dictionary;
import com.zuma.db.service.impl.GenericServiceImpl;

@Component
@Scope("session")
public class DictionaryController {

	@Autowired
	@Qualifier("dictionaryService")
	private GenericServiceImpl<Dictionary, Integer> dictionaryService;
	private List<Dictionary> dataModel;
	private Dictionary selectedDictionary;
	private int amountToAdd;

	public int getAmountToAdd() {
		return amountToAdd;
	}

	public void setAmountToAdd(int amountToAdd) {
		this.amountToAdd = amountToAdd;
	}

	@PostConstruct
	public void readCurrentData() {
		dataModel = new LinkedList<Dictionary>(dictionaryService.getAll());
	}

	public Dictionary getSelectedDictionary() {
		return selectedDictionary;
	}

	public void setSelectedDictionary(Dictionary selectedDictionary) {
		this.selectedDictionary = selectedDictionary;
	}

	public String addNewDictionary() {
		dataModel.add((Dictionary.newRandomInstance()));
		return null; // null means the same page
	}

	public List<Dictionary> getDataModel() {
		return dataModel;
	}

	public String performUpdate() {
		dictionaryService.saveOrUpdateAll(dataModel);
		return null; // null means the same page
	}

	public void setDataModel(List<Dictionary> dataModel) {
		this.dataModel = dataModel;
	}

	public String deleteDictionary() {
		if (selectedDictionary != null) {
			dataModel.remove(selectedDictionary);
			dictionaryService.delete(selectedDictionary);
		}
		return null;
	}

	public String addDictionarys() {
		for (int i = 0; i < amountToAdd; i++) {
			addNewDictionary();
		}
		return null;
	}

}