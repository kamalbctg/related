package com.zuma.web.controllers;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import javax.mail.internet.InternetAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.apache.log4j.Logger;

public class EmailSenderController implements Serializable {
    static private Logger log = Logger.getLogger(EmailSenderController.class.getName());

    @Autowired
    @Qualifier("simpleEmailBean")
    private SimpleEmailBean mailSender;

    public SimpleEmailBean getMailSender() {
        return mailSender;
    }

    public void setMailSender(SimpleEmailBean mailSender) {
        this.mailSender = mailSender;
    }

  public boolean sendEmail(String rcpnt,String bcc,Date date,String subject,String body,String from,String fromName,String replyTo) {
        boolean res = true;
        try {
        Collection toList = new LinkedList<InternetAddress>();
        InternetAddress iaTo = new InternetAddress(rcpnt);
        toList.add(iaTo);
        mailSender.setTo(toList);

        Collection replyList = new LinkedList<InternetAddress>();
        String replyToAddr = (replyTo!=null && replyTo.trim().length()>0)?replyTo:from;
        InternetAddress iaReply = new InternetAddress(replyToAddr);
        replyList.add(iaReply);
        mailSender.setReplyTo(replyList);

        if (bcc!=null) {
        Collection bccList = new LinkedList<InternetAddress>();
        InternetAddress iaBcc = new InternetAddress(bcc);
        bccList.add(iaBcc);
        mailSender.setBcc(bccList);
        }

        mailSender.setFrom(from, fromName);
        mailSender.setSentDate(date);
        mailSender.setSubject(subject);
        mailSender.setBody(body);

        mailSender.send();
        }
        catch (Exception e) {
            res = false;
            log.debug(e.getMessage());
        }
        return res;
    }
}
