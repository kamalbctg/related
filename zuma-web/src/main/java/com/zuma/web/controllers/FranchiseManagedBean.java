package com.zuma.web.controllers;

import com.zuma.db.core.domain.Franchise;
import com.zuma.db.service.impl.GenericServiceImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

@Component
@Scope("application")
public class FranchiseManagedBean {

    @Autowired
    @Qualifier("franchiseService")
    private GenericServiceImpl<Franchise, Integer> franchiseService;
    private Collection<Franchise> franchiseList;
    private Map<String, Franchise> franchiseMap = new TreeMap<String, Franchise>();
    private String selectedFranchise;

    @PostConstruct
    private void loadData() {
        franchiseList = franchiseService.getAll();
        for (Franchise franchise : franchiseList) {
            franchiseMap.put(franchise.getCaption(), franchise);
        }
        franchiseMap.put("- Select One -", new Franchise(null, "null"));
    }

    public String getSelectedFranchise() {
        return selectedFranchise;
    }

    public void setSelectedFranchise(String selectedFranchise) {
        this.selectedFranchise = selectedFranchise;
    }

    public Collection<Franchise> getFranchiseList() {
        return franchiseList;
    }

    public Map getFranchiseItems() {
        return franchiseMap;
    }

    public void refresh() {
        loadData();
    }

    public Franchise getByValue(String value) {
        Franchise selFranchise = null;
        for (Franchise franchise : franchiseList) {
            if (franchise.toString().equals(value)) {
                selFranchise = franchise;
                break;
            }
        }
        return selFranchise;
    }
    public Franchise getByCaption(String value) {
        Franchise selFranchise = null;
        for (Franchise franchise : franchiseList) {
            if (franchise.getCaption().equalsIgnoreCase(value)) {
                selFranchise = franchise;
                break;
            }
        }
        return selFranchise;
    }

    public List<Franchise> autocomplete(Object suggest) {
        String pref = (String) suggest;
        if (pref==null)
            return (List) getFranchiseList();
        ArrayList<Franchise> result = new ArrayList<Franchise>();

        Iterator<Franchise> iterator = getFranchiseList().iterator();
        while (iterator.hasNext()) {
            Franchise elem = ((Franchise) iterator.next());
            if ((elem!=null && elem.getCaption() != null && elem.getCaption().toLowerCase().indexOf(pref.toLowerCase()) == 0) || "".equals(pref)) {
                result.add(elem);
            }
        }
        return result;
    }
}
