package com.zuma.web.controllers;

import com.zuma.web.controllers.helper.SelectMap;
import com.zuma.db.core.domain.Company;
import com.zuma.db.core.domain.Contact;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import com.zuma.db.core.domain.InventoryCategory;
import com.zuma.db.core.domain.InventoryItem;
import com.zuma.db.core.domain.InventoryMeasure;
import com.zuma.db.core.domain.InventorySubCategory;
import com.zuma.db.core.domain.Store;
import com.zuma.db.core.domain.TreeNodeParent;
import com.zuma.db.dao.UniversalDAO;
import java.util.Collection;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;

@Component
@Scope("session")
public class InventoryEditController extends AbstractInventoryController {

    private Map<String, String> inventoryMeasureSelects = new TreeMap<String, String>();
    private Set<InventoryCategory> categoriesToDelete = new HashSet<InventoryCategory>();
    @Autowired
    protected UniversalDAO dao;
    private Store storeToCopyFrom;
    private Company companyToCopyFrom;

    public String insertOneCategory() {
        categoryList.add(randomCategory());
        return null;
    }

    protected boolean canBeDeleted(InventoryItem item) {
        if (item.getId() == null) {
            return true;
        }

        String sql = "SELECT count(*) FROM WasteData WHERE inventoryItem.id=" + item.getId().toString();

        int count = ((Long) dao.get(sql).iterator().next()).intValue();
        if (count > 0) {
            return false;
        }
        sql = "SELECT count(*) FROM InventoryWeeklyRecord WHERE inventoryItem.id=" + item.getId().toString();
        count = ((Long) dao.get(sql).iterator().next()).intValue();
        if (count > 0) {
            return false;
        }
        return true;
    }

    protected boolean canBeDeleted(InventorySubCategory subCat) {
        if (subCat.getId() == null) {
            return true;
        }

        String sql = "SELECT count(*) FROM WasteData WHERE inventoryItem.inventorySubCategory.id=" + subCat.getId().toString();

        int count = ((Long) dao.get(sql).iterator().next()).intValue();
        if (count > 0) {
            return false;
        }
        sql = "SELECT count(*) FROM InventoryWeeklyRecord WHERE inventoryItem.inventorySubCategory.id=" + subCat.getId().toString();
        count = ((Long) dao.get(sql).iterator().next()).intValue();
        if (count > 0) {
            return false;
        }
        return true;
    }

    protected boolean canBeDeleted(InventoryCategory cat) {
        if (cat.getId() == null) {
            return true;
        }
        String sql = "SELECT count(*) FROM WasteData WHERE inventoryItem.inventorySubCategory.inventoryCategory.id=" + cat.getId().toString();

        int count = ((Long) dao.get(sql).iterator().next()).intValue();
        if (count > 0) {
            return false;
        }
        sql = "SELECT count(*) FROM InventoryWeeklyRecord WHERE inventoryItem.inventorySubCategory.inventoryCategory.id=" + cat.getId().toString();
        count = ((Long) dao.get(sql).iterator().next()).intValue();
        if (count > 0) {
            return false;
        }
        return true;
    }

    public String deleteItem() {
        if (selectedItem != null) {
            if (canBeDeleted(selectedItem)) {
                selectedSubCategory.getInventoryItems().remove(selectedItem);

            } else {
                selectedItem.setDeleted(true);

            }
        }
        return null;
    }

    @Override
    public void reInitControllerData() {
        dao.clear();
        clearDerivedData();
        reloadSelections();
        reloadMeasureTypes();
        categoryList = new LinkedList<InventoryCategory>(categoryService.getAll(currentStore));
        companyToCopyFrom = getStore().getCompany();
        storeToCopyFrom = getStore();

    }

    private void clearDerivedData() {
        categoriesToDelete.clear();

    }

    private void reloadMeasureTypes() {

        List<InventoryMeasure> inventoryMeasures = inventoryMeasureService.getAll();
        for (InventoryMeasure inventoryMeasure : inventoryMeasures) {
            inventoryMeasureSelects.put(inventoryMeasure.getMeasureName(),
                    inventoryMeasure.getMeasureName());
        }
    }

    public String deleteCategory() {
        if (selectedCategory != null) {
            if (canBeDeleted(selectedCategory)) {
                if (selectedCategory.getId() != null) {
                    postponePersistedCategoryRemoval(selectedCategory);
                }
                categoryList.remove(selectedCategory);
            } else {
                selectedCategory.setDeleted(true);
            }
        }

        return null;
    }

    private void postponePersistedCategoryRemoval(
            InventoryCategory persistedCategory) {
        categoriesToDelete.add(persistedCategory);
    }

    public String deleteSubCategory() {
        if (selectedSubCategory != null) {
            if (canBeDeleted(selectedSubCategory)) {
                selectedCategory.getInventorySubCategories().remove(selectedSubCategory);

            } else {
                selectedSubCategory.setDeleted(true);

            }
        }
        return null;
    }

    public String insertOneItem() {

        if (selectedSubCategory != null) {

            InventoryItem newItem = new InventoryItem(selectedSubCategory,
                    new InventoryMeasure("LB"), "Item-" + RandomStringUtils.randomAlphabetic(6) + RandomUtils.nextInt());
            selectedSubCategory.getInventoryItems().add(newItem);
            selectedSubCategory = null;
        }
        return null;
    }

    public String insertOneSubCategory() {
        if (selectedCategory != null) {
            InventorySubCategory newCategory = new InventorySubCategory(
                    selectedCategory, "SubCategory" + RandomStringUtils.randomNumeric(6));
            int sequenceNumber = 0;
            if (!selectedCategory.getInventorySubCategories().isEmpty()) {
                InventorySubCategory subCat = selectedCategory.getInventorySubCategories().get(selectedCategory.getInventorySubCategories().size() - 1);
                if (subCat != null) {
                    sequenceNumber = subCat.getSequenceNumber() + 1;
                }
            }
            newCategory.setSequenceNumber(sequenceNumber);
            selectedCategory.getInventorySubCategories().add(newCategory);
            selectedCategory = null;
        }
        return null;
    }

    public InventoryCategory randomCategory() {
        InventoryCategory inventoryCategory = new InventoryCategory(
                "Category" + RandomStringUtils.randomNumeric(6),
                userManagedBean.getActiveStore());
        InventorySubCategory inventorySubCategory = new InventorySubCategory(
                inventoryCategory, "SubCategory" + RandomStringUtils.randomNumeric(6));

        inventorySubCategory.setSequenceNumber(0);
        inventoryCategory.getInventorySubCategories().add(inventorySubCategory);

        return inventoryCategory;
    }

    public String putItemUp() {
        int indexOfItemInList = selectedSubCategory.getInventoryItems().indexOf(selectedItem);
        if ((indexOfItemInList - 1) >= 0) {
            Collections.swap(selectedSubCategory.getInventoryItems(),
                    indexOfItemInList, indexOfItemInList - 1);
            reSetRowsToUpdate(indexOfItemInList, indexOfItemInList - 1);
        }

        return null;
    }

    public String putItemDown() {
        int indexOfItemInList = selectedSubCategory.getInventoryItems().indexOf(selectedItem);
        if ((indexOfItemInList + 1) < selectedSubCategory.getInventoryItems().size()) {
            Collections.swap(selectedSubCategory.getInventoryItems(),
                    indexOfItemInList, indexOfItemInList + 1);
            reSetRowsToUpdate(indexOfItemInList, indexOfItemInList + 1);
        }
        return null;
    }

    public String putSubCategoryUp() {
        if (selectedSubCategory != null) {

            InventoryCategory cat = selectedSubCategory.getInventoryCategory();
            int index = cat.getInventorySubCategories().indexOf(selectedSubCategory);
            if (index > 0) {
                InventorySubCategory prev = cat.getInventorySubCategories().get(index - 1);
                index = prev.getSequenceNumber();
                prev.setSequenceNumber(selectedSubCategory.getSequenceNumber());
                selectedSubCategory.setSequenceNumber(index);
                Collections.sort(cat.getInventorySubCategories());
            }
        }
        return null;
    }

    public String putSubCategoryDown() {
        if (selectedSubCategory != null) {

            InventoryCategory cat = selectedSubCategory.getInventoryCategory();
            int index = cat.getInventorySubCategories().indexOf(selectedSubCategory);
            if (index < cat.getInventorySubCategories().size() - 1) {
                InventorySubCategory next = cat.getInventorySubCategories().get(index + 1);
                index = next.getSequenceNumber();
                next.setSequenceNumber(selectedSubCategory.getSequenceNumber());
                selectedSubCategory.setSequenceNumber(index);
                Collections.sort(cat.getInventorySubCategories());
            }
        }
        return null;
    }

    private void reSetRowsToUpdate(int... indexOfItemInList) {
        updateRows.clear();
        for (Integer row : indexOfItemInList) {
            updateRows.add(row);
        }

    }

    public List<InventoryMeasure> getMeasureTypeChoices() {
        return inventoryMeasureService.getAll();
    }

    @Override
    public List<InventoryCategory> getCategoryList() {

        boolean reloadCondition = !currentStore.equals(userManagedBean.getActiveStore());
        if ((categoryList == null) || reloadCondition) {
            reInitControllerData();
        }

        return categoryList;
    }

    @Override
    public String save() {
        try {
            categoryService.mergeAll(categoryList);
        } catch (DataIntegrityViolationException e) {
            addFacesMessageForUI("Non Unique Name entered");
        }
        try {
            categoryService.removeAll(categoriesToDelete);
        } finally {
            categoriesToDelete.clear();
        }
        reInitControllerData();

        return null;
    }

    public Map<String, String> getInventoryMeasureSelects() {
        return inventoryMeasureSelects;
    }

    public void setInventoryMeasureSelects(
            Map<String, String> inventoryMeasureSelects) {
        this.inventoryMeasureSelects = inventoryMeasureSelects;
    }

    Store getStore() {
        return userManagedBean.getActiveStore();
    }

    protected void copyInventory(Store from) {
        /*
        String sql = "FROM InventoryCategory WHERE store.id=" + String.valueOf(getStore().getId());
        List<InventoryCategory> items = dao.get(sql);

        sql = "FROM InventoryCategory WHERE store.id=" + String.valueOf(getStoreToCopyFrom().getId());
        items = dao.get(sql);
        List<InventoryCategory> newItems = new ArrayList<InventoryCategory>();
        for (InventoryCategory item : items) {
            InventoryCategory newItem = item.clone();
            newItem.setStore(getStore());
            newItems.add(newItem);
        }
        dao.saveOrUpdateAll(newItems);
        */
        String sql = "FROM InventoryCategory WHERE store.id=" + String.valueOf(getStore().getId());
        List<InventoryCategory> oldCategories = dao.get(sql);

        sql = "FROM InventoryCategory WHERE store.id=" + String.valueOf(getStoreToCopyFrom().getId());
        List<InventoryCategory> categories = dao.get(sql);

        for (InventoryCategory cat : categories) {
            InventoryCategory oldCat = (InventoryCategory) findByName((Collection) oldCategories, cat);
            if (oldCat == null) {
                oldCat = cat.clone();
                oldCat.setStore(getStore());
                oldCategories.add(oldCat);
            } else {
                Collection<InventorySubCategory> oldSubCats = oldCat.getInventorySubCategories();
                Collection<InventorySubCategory> subCats = cat.getInventorySubCategories();
                for (InventorySubCategory subCat : subCats) {
                    if (subCat == null) {
                        continue;
                    }
                    InventorySubCategory oldSubCat = (InventorySubCategory) findByName((Collection) oldSubCats, subCat);
                    if (oldSubCat == null) {
                        oldSubCat = subCat.clone();
                        oldSubCat.setInventoryCategory(oldCat);
                        oldSubCats.add(oldSubCat);

                    } else {
                        Collection<InventoryItem> oldItems = oldSubCat.getInventoryItems();
                        Collection<InventoryItem> items = subCat.getInventoryItems();
                        for (InventoryItem item : items) {
                            if (item == null) {
                                continue;
                            }
                            InventoryItem oldItem = (InventoryItem) findByName((Collection) oldItems, item);
                            if (oldItem == null) {
                                oldItem = item.clone();
                                oldItem.setInventorySubCategory(oldSubCat);
                                oldItems.add(oldItem);

                            }
                        }
                    }
                }

            }
        }
        
        dao.saveOrUpdateAll(oldCategories);
        reInitControllerData();
    }
    protected TreeNodeParent findByName(Collection<TreeNodeParent> categories, TreeNodeParent obj) {
        for (TreeNodeParent tmp : categories) {
            if (tmp == null) {
                continue;
            }
            if ((tmp.getCaption() == null && obj.getCaption() == null)
                    || tmp.getCaption().equals(obj.getCaption())) {
                return tmp;
            }
        }
        return null;
    }

    public String processDataCopying() {
        if (getStoreToCopyFrom() != null && !getStore().equals(getStoreToCopyFrom())) {
            copyInventory(getStoreToCopyFrom());
        }
        return null;
    }

    public Company getCompanyToCopyFrom() {
        return companyToCopyFrom;
    }

    public void setCompanyToCopyFrom(Company companyToCopyFrom) {

        this.companyToCopyFrom = dao.findById(Company.class, companyToCopyFrom.getId());
        this.storeToCopyFrom = this.companyToCopyFrom.getStoreList().iterator().next();
    }

    public Store getStoreToCopyFrom() {
        return storeToCopyFrom;
    }

    public void setStoreToCopyFrom(Store storeToCopyFrom) {
        this.storeToCopyFrom = storeToCopyFrom;
    }

    public Map getStoresToCopyFrom() {
        Map<String, Store> choices = new SelectMap<Store>();
        if (companyToCopyFrom != null) {
            companyToCopyFrom = dao.findById(Company.class, companyToCopyFrom.getId());
            for (Store store : companyToCopyFrom.getStoreList()) {
                int size = store.getContactList().size();
                Contact storeContact = store.getContactList().get(0);
                String storeName = storeContact.getAddress();
                if (storeName == null || storeName.isEmpty()) {
                    storeName = store.getCaption();
                }
                choices.put(storeName, store);
            }
        }
        return choices;
    }
}
