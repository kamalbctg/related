package com.zuma.web.controllers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.component.UIParameter;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.zuma.db.core.domain.InventoryCategory;
import com.zuma.db.core.domain.InventoryItem;
import com.zuma.db.core.domain.InventorySubCategory;
import com.zuma.db.core.domain.InventoryWeeklyRecord;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.dao.UniversalDAO;
import com.zuma.db.service.InventoryWeeklyRecordService;
import com.zuma.db.service.ReportingWeekService;
import java.text.SimpleDateFormat;
import javax.swing.text.DateFormatter;

@Component
@Scope("session")
public class InventoryViewController extends AbstractInventoryController {

    public static Logger logger = Logger.getLogger(InventoryViewController.class);
    public static final String CATEGORY_NAMES_DELIMETER = " & ";

    @Autowired
    protected UniversalDAO dao;
    @Autowired
    private InventoryWeeklyRecordService inventoryWeeklyRecordService;
    @Autowired
    private ReportingWeekService reportingWeekService;
    private List<InventoryWeeklyRecord> inventoryWeeklyRecords;
    private Set<InventoryWeeklyRecord> changedRecords = new HashSet<InventoryWeeklyRecord>();
    private Map<InventoryItem, InventoryWeeklyRecord> itemRecordMap = new HashMap<InventoryItem, InventoryWeeklyRecord>();
    private Map<InventoryCategory, BigDecimal> categoryTotalCostMap = new HashMap<InventoryCategory, BigDecimal>();
    private Boolean copyFromPreviousWeekLinkEnabled = null;
    private Map<InventoryItem, Boolean> itemShowFlags = new HashMap<InventoryItem, Boolean>();
    private Map<InventorySubCategory, Boolean> subCatShowFlags = new HashMap<InventorySubCategory, Boolean>();
    private Map<InventoryCategory, Boolean> catShowFlags = new HashMap<InventoryCategory, Boolean>();
    boolean wasDataCopied = false;

    public Set<Integer> getUpdateRows() {
        return Collections.singleton(getUpdateRow());
    }

    public BigDecimal getTotalCategoryCost() {
        BigDecimal totalForAllCategories = BigDecimal.ZERO;
        for (BigDecimal currentCategoryTotal : categoryTotalCostMap.values()) {

            totalForAllCategories = totalForAllCategories.add(currentCategoryTotal);
        }
        return totalForAllCategories;
    }

    public void reInitControllerData() {
        clearDerivedData();
        reloadSelections();
        loadCategoriesAndRecords();
        accociateItemsAndRecords();
        countCategoryTotalCosts(null);
        initShowFlags();
        insertDummyRecords();

    }

    private void clearDerivedData() {
        itemRecordMap.clear();
        changedRecords.clear();
        categoryTotalCostMap.clear();
        copyFromPreviousWeekLinkEnabled = null;
    }

    private void loadCategoriesAndRecords() {
        if (currentStore != null && currentWeek != null) {
            inventoryWeeklyRecords = inventoryWeeklyRecordService.getAll(
                    currentStore, currentWeek);
            categoryList = categoryService.getAll(currentStore);
        } else {
            loadEmptyList();
        }
    }

    private void loadEmptyList() {
        inventoryWeeklyRecords = Collections.emptyList();
        categoryList = Collections.emptyList();
    }

    public void countCategoryTotalCosts(ActionEvent event) {
        putZeroAmountToEachCategory();
        for (InventoryItem inventoryItem : itemRecordMap.keySet()) {
            addItemTotalToCategoryTotal(inventoryItem);
        }

    }

    private void addItemTotalToCategoryTotal(InventoryItem inventoryItem) {
        InventoryCategory itemsCategory = inventoryItem.getInventorySubCategory().getInventoryCategory();
        BigDecimal oldValue = categoryTotalCostMap.get(itemsCategory);

        InventoryWeeklyRecord itemsWeeklyRecord = itemRecordMap.get(inventoryItem);
        BigDecimal newValue = oldValue.add(itemsWeeklyRecord.getTotalCost());
        categoryTotalCostMap.put(itemsCategory, newValue);
    }

    private void insertDummyRecords() {
        boolean haveSomeRecordsForCurrentWeek = false;
        for (InventoryCategory category : categoryList) {
            for (InventorySubCategory subCategory : category.getInventorySubCategories()) {
                for (InventoryItem inventoryItem : subCategory.getInventoryItems()) {
                    if (noRecordForCurrentWeek(inventoryItem)) {
                        createDummyRecordForCurrentWeek(inventoryItem);
                    } else {
                        haveSomeRecordsForCurrentWeek = true;
                    }
                }
            }
        }

        logger.debug("Dummy record have been insered");
        if (isReloadNeeded()) {
            wasDataCopied = false;
        }
        if (!haveSomeRecordsForCurrentWeek && !wasDataCopied && isCopyFromPreviousWeekEnabled()) {
            wasDataCopied = true;
            copyFromPreviousWeek();
            save();
        }
    }

    private boolean noRecordForCurrentWeek(InventoryItem inventoryItem) {
        return itemRecordMap.get(inventoryItem) == null;
    }

    private void createDummyRecordForCurrentWeek(InventoryItem inventoryItem) {
        InventoryWeeklyRecord newRecord = new InventoryWeeklyRecord(
                inventoryItem, currentWeek, BigDecimal.ZERO, BigDecimal.ZERO);
        itemRecordMap.put(inventoryItem, newRecord);
    }

    private void accociateItemsAndRecords() {
        for (InventoryWeeklyRecord inventoryWeeklyRecord : inventoryWeeklyRecords) {
            itemRecordMap.put(inventoryWeeklyRecord.getInventoryItem(),
                    inventoryWeeklyRecord);
        }
    }

    private void putZeroAmountToEachCategory() {
        for (InventoryCategory inventoryCategory : categoryList) {
            categoryTotalCostMap.put(inventoryCategory, BigDecimal.ZERO);
        }
    }

    public String getCategoryNames() {
        String categoryNames = new String();
        if (categoryList.size() > 0) {
            categoryNames = categoryList.get(0).getCategoryName();
            for (int i = 1; i < categoryList.size(); i++) {
                categoryNames += CATEGORY_NAMES_DELIMETER + categoryList.get(i).getCategoryName();
            }
        }
        return categoryNames;
    }

    public void processValueChange(ValueChangeEvent event)
            throws AbortProcessingException {
        setUpdateRow(((Integer) (((UIParameter) (event.getComponent()).getChildren().get(1)).getValue())));
        // see <param> tag in jsf page

        BigDecimal newValue = new BigDecimal(event.getOldValue().toString()).setScale(2, RoundingMode.HALF_UP);
        BigDecimal oldValue = new BigDecimal(event.getNewValue().toString()).setScale(2, RoundingMode.HALF_UP);

        if (oldValue.compareTo((newValue)) != 0) {
            changedRecords.add((InventoryWeeklyRecord) (((UIParameter) (event.getComponent()).getChildren().get(0)).getValue()));
            // see <param> tag in jsf page
        }
    }

    @Override
    public String save() {

        if (!changedRecords.isEmpty()) {
            inventoryWeeklyRecordService.mergeAll(new ArrayList<InventoryWeeklyRecord>(
                    changedRecords));
            changedRecords.clear();
        }
        reInitControllerData(); // check if concurrent modifications were made
        return null;

    }

    public String copyFromPreviousWeek() {
        logger.debug("Copying from previous week...");

        ReportingWeek previousWeek = reportingWeekService.findPreviousWeek(currentWeek);
        List<InventoryWeeklyRecord> inventoryWeeklyRecords = inventoryWeeklyRecordService.getAll(currentStore, previousWeek);

        for (InventoryWeeklyRecord previousWeeklyRecord : inventoryWeeklyRecords) {

            InventoryWeeklyRecord dummyRecord = itemRecordMap.get(previousWeeklyRecord.getInventoryItem());

            dummyRecord = itemRecordMap.get(previousWeeklyRecord.getInventoryItem());
            //dummyRecord.setQuantity(previousWeeklyRecord.getQuantity());
            dummyRecord.setUnitCost(previousWeeklyRecord.getUnitCost());
            changedRecords.add(dummyRecord);
            countCategoryTotalCosts(null);// recount totals
        }

        logger.info("Copying from previous week completed");
        return null;
    }

    public boolean isCopyFromPreviousWeekEnabled() {
        if (isReloadNeeded()) {
            reInitControllerData();
        }
        if (copyFromPreviousWeekLinkEnabled == null) {
            if (reportingWeekService.countAll() > 1) {
                ReportingWeek lastWeek = reportingWeekService.getLastWeek();
                copyFromPreviousWeekLinkEnabled = lastWeek.equals(currentWeek);

            }
        }
        return copyFromPreviousWeekLinkEnabled;
    }

    @Override
    public List<InventoryCategory> getCategoryList() {

        if ((categoryList == null)) {
            reInitControllerData();
        }
        return categoryList;
    }

    private boolean isReloadNeeded() {
        return !currentStore.equals(userManagedBean.getActiveStore()) || !currentWeek.getWeekDate().equals(
                userManagedBean.getActiveWeek().getWeekDate());
    }

    @Override
    public String refreshPage() {
        reInitControllerData();
        return targetPage;
    }

    public Map<InventoryCategory, BigDecimal> getCategoryTotalCostMap() {
        return categoryTotalCostMap;
    }

    public void setCategoryTotalCostMap(
            Map<InventoryCategory, BigDecimal> categoryTotalCostMap) {
        this.categoryTotalCostMap = categoryTotalCostMap;
    }

    public Map<InventoryItem, InventoryWeeklyRecord> getItemRecordMap() {
        return itemRecordMap;
    }

    public void setItemRecordMap(
            Map<InventoryItem, InventoryWeeklyRecord> itemRecordMap) {
        this.itemRecordMap = itemRecordMap;
    }

    public Map<InventoryItem, Boolean> getItemShowFlags() {
        return itemShowFlags;
    }

    public Map<InventorySubCategory, Boolean> getSubCatShowFlags() {
        return subCatShowFlags;
    }

    public Map<InventoryCategory, Boolean> getCatShowFlags() {
        return catShowFlags;
    }

    protected void initShowFlags() {
        for (InventoryCategory category : categoryList) {
            boolean catShowFlag = !category.isDeleted();
            for (InventorySubCategory subCategory : category.getInventorySubCategories()) {
                boolean subCatShowFlag = !subCategory.isDeleted();
                for (InventoryItem inventoryItem : subCategory.getInventoryItems()) {
                    boolean itemShowFlag = !inventoryItem.isDeleted();
                    if (!noRecordForCurrentWeek(inventoryItem)) {
                        itemShowFlag = true;
                        subCatShowFlag = true;
                        catShowFlag = true;
                    }
                    itemShowFlags.put(inventoryItem, itemShowFlag);
                }
                subCatShowFlags.put(subCategory, subCatShowFlag);
            }
            catShowFlags.put(category, catShowFlag);
        }
    }

    public String copyCounts() throws ParseException {
        ReportingWeek week = dao.findById(ReportingWeek.class, currentWeek.getId());
        
        DateFormatter formatter = new DateFormatter(new SimpleDateFormat(ParentController.DATE_FORMAT));
        String sql = "FROM InventoryWeeklyRecord WHERE reportingWeek.weekDate='"
                + formatter.valueToString(userManagedBean.getActiveWeek().getWeekDate())
                + "' AND inventoryItem.inventorySubCategory.inventoryCategory.store.id="
                + String.valueOf(currentStore.getId())
                + " AND inventoryItem.deleted=0 AND inventoryItem.inventorySubCategory.deleted=0 AND inventoryItem.inventorySubCategory.inventoryCategory.deleted = 0";
        List<InventoryWeeklyRecord> current = dao.get(sql);

        sql = "FROM InventoryWeeklyRecord WHERE reportingWeek.weekDate=(SELECT max(weekDate) FROM ReportingWeek WHERE weekDate<'"
                + formatter.valueToString(userManagedBean.getActiveWeek().getWeekDate())
                + "') AND inventoryItem.inventorySubCategory.inventoryCategory.store.id="
                + String.valueOf(currentStore.getId())
                + " AND inventoryItem.deleted=0 AND inventoryItem.inventorySubCategory.deleted=0 AND inventoryItem.inventorySubCategory.inventoryCategory.deleted = 0";
        List<InventoryWeeklyRecord> previous = dao.get(sql);
        List<InventoryWeeklyRecord> forSave = new ArrayList<InventoryWeeklyRecord>();
        for(InventoryWeeklyRecord prev:previous)
        {
            boolean exists = false;
            for(InventoryWeeklyRecord curr:current)
            {
                if(curr.getInventoryItem().equals(prev.getInventoryItem()))
                {
                    curr.setQuantity(prev.getQuantity());
                    exists = true;
                    forSave.add(curr);
                }
            }
            if(!exists)
            {
               InventoryWeeklyRecord tmp = prev.clone();
               tmp.setReportingWeek(week);
               forSave.add(tmp);
            }
        }
        dao.saveOrUpdateAll(forSave);
        reInitControllerData();
        return null;
    }
}
