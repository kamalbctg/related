package com.zuma.web.controllers;

import com.zuma.db.core.domain.Invoice;
import com.zuma.db.core.domain.InvoiceAccount;
import com.zuma.db.core.domain.InvoiceDetail;
import com.zuma.db.core.domain.Store;
import com.zuma.db.service.util.Week;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.swing.text.DateFormatter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class InvoiceController extends ParentController {

    private Collection<Invoice> invoices;
    private Map<String, InvoiceAccount> accounts = new TreeMap<String, InvoiceAccount>();
    private Store currentStore;
    private Date currentWeekDate;
    private Invoice selectedInvoice;
    Map<String, String> types = new TreeMap<String, String>();

    {
        types.put("Check", "Check");
        types.put("ACH", "ACH");
        types.put("Cash", "Cash");
        types.put("A/P", "A/P");
    }

    @PostConstruct
    protected void init() throws ParseException {
        List<InvoiceAccount> listIA = dao.getAll(InvoiceAccount.class);
        for (InvoiceAccount invoiceAccount : listIA) {
            accounts.put(invoiceAccount.getCaption(), invoiceAccount);
        }

        currentStore = userManagedBean.getActiveStore();
        currentWeekDate = userManagedBean.getActiveWeekDay();
        setNeedRefresh(true);
    }

    public void recalculate(PhaseEvent ev) throws ParseException {
        if (ev.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            if (isReloadingNeeded()) {
                currentStore = getStore();
                currentWeekDate = getStartDate().getTime();
                loadData();
            }
        }
    }

    protected boolean isReloadingNeeded() {
        boolean flag = isNeedRefresh();
        if (flag) {
            setNeedRefresh(false);
        }
        return flag || !getStore().equals(currentStore) || !(getStartDate().getTime().equals(currentWeekDate));
    }

    protected void loadData() throws ParseException {
        if (getStore() == null) {
            return;
        }

        DateFormatter formatter = new DateFormatter(new SimpleDateFormat(DATE_FORMAT));

        String sql = "FROM Invoice WHERE date BETWEEN '" + formatter.valueToString(getStartDate().getTime()) + "' AND '" + formatter.valueToString(getFinishDate().getTime()) + "' AND store.id=" + String.valueOf(getStore().getId());

        invoices = dao.get(sql);

        if (getInvoices() == null) {
            invoices = new ArrayList<Invoice>();
        }
        if (getInvoices().size() < 5) {
            int size = getInvoices().size();
            for (int i = 0; i < 5 - size; i++) {
                Invoice tmpInv = new Invoice();
                tmpInv.setDate(getStartDate().getTime());
                tmpInv.setStore(getStore());
                Collection<InvoiceDetail> collDetail = new ArrayList<InvoiceDetail>();
                InvoiceDetail tmpDet = new InvoiceDetail();
                tmpDet.setPrice(BigDecimal.ZERO);
                tmpDet.setInvoice(tmpInv);
                collDetail.add(tmpDet);
                tmpInv.setInvoiceDetails(collDetail);
                invoices.add(tmpInv);
            }
        }

    }

    public Collection<Invoice> getInvoices() {
        return invoices;
    }

    public String save() {
        return null;
    }

    public Map<String, InvoiceAccount> getAccounts() {
        return accounts;
    }

    public String addInvoice() {
        Invoice tmpInv = new Invoice();
        tmpInv.setDate(getStartDate().getTime());
        tmpInv.setStore(getStore());
        Collection<InvoiceDetail> collDetail = new ArrayList<InvoiceDetail>();
        InvoiceDetail tmpDet = new InvoiceDetail();
        tmpDet.setPrice(BigDecimal.ZERO);
        tmpDet.setInvoice(tmpInv);
        collDetail.add(tmpDet);
        tmpInv.setInvoiceDetails(collDetail);
        invoices.add(tmpInv);

        return null;
    }

    public String addDetail() {
        InvoiceDetail tmpDet = new InvoiceDetail();
        tmpDet.setPrice(BigDecimal.ZERO);
        tmpDet.setInvoice(selectedInvoice);
        selectedInvoice.getInvoiceDetails().add(tmpDet);
        return null;
    }

    public String deleteInvoice() {
        getInvoices().remove(selectedInvoice);
        return null;
    }

    public String saveInvoice() {
        Collection<Invoice> is = getInvoices();

        for (Invoice invoice : is) {
            dao.saveOrUpdate(invoice);
        }
        return "purchase";
    }

    public BigDecimal getTotalSumm() {

        BigDecimal result = BigDecimal.ZERO;
        for (Invoice it : getInvoices()) {
            result = result.add(it.getTotalPrice());
        }
        return result;
    }

    public Invoice getSelectedInvoice() {
        return selectedInvoice;
    }

    public void setSelectedInvoice(Invoice selectedInvoice) {
        this.selectedInvoice = selectedInvoice;
    }

    public void reInitDataList() throws ParseException {
        Store store = userManagedBean.getActiveStore();
        Date date = userManagedBean.getActiveWeekDay();

        if (!currentStore.equals(store) || !currentWeekDate.equals(date) || getInvoices() == null) {
            currentStore = store;
            currentWeekDate = date;
            loadData();
        }
    }

    public Collection<InvoiceDetail> getInvoiceDetails() throws ParseException {
        Collection<InvoiceDetail> ids = new ArrayList<InvoiceDetail>();

        if (getInvoices() != null) {
            for (Invoice invoice : getInvoices()) {
                if (invoice.getId() != null) {
                    ids.addAll(invoice.getInvoiceDetails());
                }
            }
        }
        return ids;
    }

    public Date getMinValidDate() {
        return Week.getWeekBegin(currentWeekDate);
    }

    public Date getMaxValidDate() {
        return Week.getWeekEnd(currentWeekDate);
    }

    public Map<String, String> getPaymentTypes() {
        return types;
    }
}
