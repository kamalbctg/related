package com.zuma.web.controllers;

import com.zuma.web.menu.MenuProcessor;
import com.zuma.web.menu.MenuMap;
import java.io.Serializable;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class MenuManagedBean implements Serializable {
    private MenuProcessor styleProc = new MenuProcessor("selected","");
    private MenuProcessor pageProc  = new MenuProcessor();
    
    public  MenuMap  style;
    public  MenuMap  page;

    private String activePage;
    static private String extention = ".jsp";
    static private String renderTrueString = "";//"true";
    static private String renderFalseString = "display:none;";//"false";
    @Autowired
    private UserManagedBean userManagedBean;

    private void PrepareData() {
        activePage = getName(FacesContext.getCurrentInstance().getViewRoot().getViewId());
        setStyle(new MenuMap(activePage, styleProc));
        setPage(new MenuMap(activePage, pageProc));
    }
    
    public MenuManagedBean() {
        PrepareData();
    }

    private String getName(String pageUrl) {
        String result = pageUrl;
        int prefixPos = pageUrl.lastIndexOf("/");
        int suffixPos = pageUrl.lastIndexOf(extention);
        result = pageUrl.substring(prefixPos + 1, suffixPos);
        return result;
    }

    public String getActivePage() {
        return activePage;
    }

    // ===================== CHECK ACTIVE PAGE =================================
    public boolean isWelcomePage() {
        return activePage.equals("welcome");
    }

    public boolean isLinksPage() {
        return activePage.equals("qlinks");
    }

    public boolean isServicesPage() {
        return activePage.equals("services");
    }

    public boolean isOpenAccountPage() {
        return activePage.equals("openaccount");
    }

    public boolean isMyAccountPage() {
        return activePage.equals("myaccount");
    }

    public boolean isPurchasePage() {
        return activePage.equals("purchase");
    }

    public boolean isInventoryEditPage() {
        return activePage.equals("inventoryedit");
    }

    public boolean isInventoryViewPage() {
        return activePage.equals("inventoryview");
    }

    public boolean isSummaryPage() {
        return activePage.equals("summary");
    }

    public boolean isSummaryConstructPage() {
        return activePage.equals("summaryconstruct");
    }

    public boolean isWastePage() {
        return activePage.equals("waste");
    }

    public boolean isPerformancePage() {
        return activePage.equals("performance");
    }

    public boolean isProfitabilityPage() {
        return activePage.equals("profitability");
    }

    public boolean isFileManagerPage() {
        return activePage.equals("filemanager");
    }
    // for admins ==

    public boolean isAccountManagementPage() {
        return activePage.equals("management");
    }

    public boolean isAccountDetailsPage() {
        return activePage.equals("accountdetails");
    }

    public boolean isCreateUserPage() {
        return activePage.equals("createuser");
    }
    // general pages ==

    public boolean isFAQPage() {
        return activePage.equals("faq");
    }

    public boolean isAboutPage() {
        return activePage.equals("about");
    }

    public boolean isContactPage() {
        return activePage.equals("contact");
    }

    public boolean isAdminGroup() {
        boolean result = (activePage.equals("createuser") ||
                activePage.equals("accountdetails") ||
                activePage.equals("management") ||
                activePage.equals("myaccount"));
        return result;
    }

    public boolean isUserGroup() {
        boolean result = isAboutPage() || isFAQPage() ||
                         isContactPage() || isServicesPage() ||
                         isOpenAccountPage() ||  isWelcomePage();
        return result;
    }

    public boolean isMenuAccountRendered() {
        boolean res = isAdminGroup() || isUserGroup();
        return !res;
    }
    
    public boolean isMenuStoreRendered() {
        boolean res = isAdminGroup() || isUserGroup();

        return !res;
    }

    public boolean isMenuWeekRendered() {
        boolean res = isInventoryEditPage() || isSummaryConstructPage() ||
                      isAdminGroup() || isUserGroup() || isLinksPage() ;
        return !res;
    }
    // ===================== ACTIVE GROUP STYLE ================================
    // Selected grou
    public String getHomeStyle() {
        boolean result = (activePage.equals("welcome") ||
                activePage.equals("qlinks") ||
                activePage.equals("openaccount") ||
                activePage.equals("services") ||
                activePage.equals("about"));

        return (result) ? "selected" : "";
    }

    public String getAdminStyle() {
        boolean result = (activePage.equals("createuser") ||
                activePage.equals("accountdetails") ||
                activePage.equals("management"));

        return (result) ? "selected" : "";
    }

    public String getReportsStyle() {
        boolean result = (activePage.equals("performance") ||
                activePage.equals("profitability") ||
                activePage.equals("filemanager"));

        return (result) ? "selected" : "";
    }

    public String getReportsClass() {
        if (!userManagedBean.isLogon())
           return "class='disabled'";
        if (!isReports())
           return "class='locked'";

        return"";
    }

    public String getDataStyle() {
        boolean result = (activePage.equals("inventoryview") ||
                activePage.equals("inventoryedit") ||
                activePage.equals("summary") ||
                activePage.equals("summaryconstruct") ||
                activePage.equals("waste") ||
                activePage.equals("purchase"));

        return (result) ? "selected" : "";
    }

    public String getDataClass() {
        if (!userManagedBean.isLogon())
           return "class='disabled'";
        if (!isData())
           return "class='locked'";

        return"";
    }

    public String getMyAccountClass() {
        if (!userManagedBean.isLogon())
           return "class='disabled'";
        if (!isMyAccount())
           return "class='locked'";

        return"";
    }

    // ===================== Is Page Rendered ==================================
    // new  13/04/2010
    public String getBeforeSignIn() {
        // show only before sign in
        return  (!userManagedBean.isLogon()) ? renderTrueString : renderFalseString;
    }

    public String getAfterSignIn() {
        // show only after sign in
        return  (userManagedBean.isLogon()) ? renderTrueString : renderFalseString;
    }
    // =========================================================================
    public String getWelcomeRendered() {
         // boolean result = !userManagedBean.isLogon();
         // return  (result) ? renderTrueString : renderFalseString;
         return renderTrueString;
    }

    public String getServicesRendered() {
        return renderTrueString;
    }

    public String getOpenAccountRendered() {
        boolean result = !userManagedBean.isLogon();
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getMyAccountRendered() {
        boolean result = userManagedBean.isLogon();
        return  (result) ? renderTrueString : renderFalseString;
    }
    // for personal pages
    public String getPurchaseRendered() {
        boolean result = isPurchase();
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getInventoryEditRendered() {
        boolean result = isInventoryEdit();
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getInventoryViewRendered() {
        boolean result = isInventoryView();
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getSummaryRendered() {
        boolean result = isSummary();
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getSummaryConstructRendered() {
        boolean result = isSummaryConstruct();
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getWasteRendered() {
        boolean result = isWaste();
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getPerformanceRendered() {
        boolean result = isPerformance();
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getProfitabilityRendered() {
        boolean result = isProfitability();
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getFileManagerRendered() {
        boolean result = isFileManager();
        return  (result) ? renderTrueString : renderFalseString;
    }
    // for admins ==
    public String getAccountManagementRendered() {
        boolean result = userManagedBean.isAdmin();
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getAccountDetailsRendered() {
        boolean result = userManagedBean.isAdmin();
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getCreateUserRendered() {
        boolean result = userManagedBean.isAdmin();
        return  (result) ? renderTrueString : renderFalseString;
    }
    // general pages ==
    public String getFAQRendered() {
        boolean result = true;
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getAboutRendered() {
        boolean result = !userManagedBean.isLogon();
        return  (result) ? renderTrueString : renderFalseString;
    }

    public String getContactRendered() {
        boolean result = true;
        return  (result) ? renderTrueString : renderFalseString;
    }
    // for groups ==============================================================
    public boolean isHome() {
        return true;
    }

    public boolean isMyAccount() {
        return checkAccess("myaccount");
    }

    public boolean isAdmin() {
        return userManagedBean.isAdmin();
    }

    public boolean isReports() {
        return (isPerformance() || isProfitability() || isFileManager());
    }

    public boolean isData() {
        return (isInventoryView() || isInventoryEdit() ||
                isSummary() || isSummaryConstruct() ||
                isWaste() || isPurchase());
    }

    public String getHomeRendered() {
        // boolean result = userManagedBean.isLogon();
        // return  (result) ? renderTrueString : renderFalseString;
        return renderTrueString;
    }

    public String getAdminRendered() {
        return  (isAdmin()) ? renderTrueString : renderFalseString;
    }

    public String getReportsRendered() {
        return  (isReports()) ? renderTrueString : renderFalseString;
    }

    public String getDataRendered() {
        return  (isData()) ? renderTrueString : renderFalseString;
    }

    // ===================== ACCESS ROLE =======================================
    public boolean checkAccess(String pageName) {
        return userManagedBean.isAccessToPage(pageName, false);
    }

    public boolean isSummary() {
        return checkAccess("summary");
    }

    public boolean isSummaryConstruct() {
        return checkAccess("summaryconstruct");
    }

    public boolean isPurchase() {
        return checkAccess("purchase");
    }

    public boolean isInventoryEdit() {
        return checkAccess("inventoryedit");
    }

    public boolean isInventoryView() {
        return checkAccess("inventoryview");
    }

    public boolean isWaste() {
        return checkAccess("waste");
    }

    public boolean isPerformance() {
        return checkAccess("performance");
    }
    
    public boolean isReportCenter() {
        return checkAccess("reportcenter");
    }

    public boolean isProfitability() {
        return checkAccess("profitability");
    }

    public boolean isFileManager() {
        return checkAccess("filemanager");
    }

    public MenuMap getStyle() {
        return style;
    }

    public void setStyle(MenuMap style) {
        this.style = style;
    }

    public MenuMap getPage() {
        return page;
    }

    public void setPage(MenuMap page) {
        this.page = page;
    }
    // ===================== ACCESS ROLE RENDERED===============================
}
