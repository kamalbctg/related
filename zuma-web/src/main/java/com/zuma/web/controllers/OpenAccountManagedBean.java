package com.zuma.web.controllers;

import com.zuma.db.core.domain.Company;
import com.zuma.db.core.domain.Contact;
import com.zuma.db.core.domain.Folder;
import com.zuma.db.core.domain.Person;
import com.zuma.db.core.domain.Store;
import com.zuma.db.core.domain.StoreDictionary;
import com.zuma.db.dao.UniversalDAO;
import com.zuma.db.service.CompanyService;
import com.zuma.db.service.impl.GenericServiceImpl;
import com.zuma.web.util.Email;
import com.zuma.db.service.util.Week;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class OpenAccountManagedBean extends EmailSenderController {

    @Autowired
    protected UserManagedBean userBean;
    @Autowired
    protected UniversalDAO dao;
    @Autowired
    protected CompanyService companyService;
    @Autowired
    @Qualifier("personService")
    protected GenericServiceImpl<Person, Integer> personService;
    @Autowired
    @Qualifier("contactService")
    protected GenericServiceImpl<Contact, Integer> contactService;
    private Company company;
    private Person person;
    private Store store;
    private boolean autoPassword = false;
    private String password;
    private String passwordConfirm;
    @Autowired
    protected StateManagedBean stateManagedBean;
    @Autowired
    protected FranchiseManagedBean franchiseManagedBean;
    private boolean editMode = false;
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    public boolean isEditMode() {
        return editMode;
    }

    public OpenAccountManagedBean() {
        renew();
    }

    public void renew() {
       setSuccess(false);
        editMode = false;
        company = new Company();
        person = new Person();
        store = new Store();
        password = "";
        passwordConfirm = "";
        
    }

    public void edit(StoreDictionary storeEdit) {
        String companyFullName = storeEdit.getCompanyName().trim();
        if (!(companyFullName != null && !companyFullName.isEmpty())) {
            companyFullName = "Company " + storeEdit.getAddress().trim();
        }

        company.setCaption(companyFullName); //storeEdit.getCompanyFullName()
        company.setFranchise(storeEdit.getFranchise());
        company.autoNormalizeContact();
        String fullName = storeEdit.getFirstName().trim() + " " + storeEdit.getLastName().trim();
        Contact companyPrimaryContact = company.getContactList().get(1);
        if (!(companyPrimaryContact.getCaption() != null &&
                !companyPrimaryContact.getCaption().isEmpty())) {
            companyPrimaryContact.setCaption(fullName.trim());
        }
        if (!(companyPrimaryContact.getPhone1() != null &&
                !companyPrimaryContact.getPhone1().isEmpty())) {
            companyPrimaryContact.setPhone1(storeEdit.getPhone());
        }

        store.setCompany(company);
        store.setNumber(storeEdit.getNumber());
        store.setFranchise(storeEdit.getFranchise());

        store.autoNormalizeContact();
        Contact storeContact = store.getContactList().get(0);
        storeContact.setCaption(companyFullName);
        storeContact.setAddress(storeEdit.getAddress());
        storeContact.setCity(storeEdit.getCity());
        storeContact.setState(storeEdit.getState());
        storeContact.setZip(storeEdit.getZip());
        storeContact.setPhone1(storeEdit.getPhone());

        Contact personContact = person.getContact();
        personContact.setPhone1(storeEdit.getPhone());
        person.setFirstName(storeEdit.getFirstName());
        person.setLastName(storeEdit.getLastName());
    }

    public boolean verify() {
        boolean toReturn = true;
        FacesContext ctx = FacesContext.getCurrentInstance();

        if (company.getCaption().isEmpty()) {
            return false;
        }
        if (!isAutoPassword()) {
            if (!getPassword().equalsIgnoreCase(getPasswordConfirm())) {
                ctx.addMessage("OpenAccount:passwordConfirm",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error: Please make sure Password and Confirm Password fields are matching", null));
                toReturn = false;
            }
            // user can't use default password ': auto :'
            if (getPassword().equalsIgnoreCase(": auto :")) {
                ctx.addMessage("OpenAccount:password",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error: You can't use such password", null));
                toReturn = false;
            }
        }
        if (!userBean.isAdmin()) {
            if (person.getFirstName().isEmpty()) {
                return false;
            }
            if (person.getLastName().isEmpty()) {
                return false;
            }
            if (company.getFranchise() == null || company.getFranchise().isNullValue()) {
                ctx.addMessage("OpenAccount:franchiseCombo",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error: Please select franchise", null));
                toReturn = false;
            }
            if (  store.getContactList().size()==0 || store.getContactList().get(0).getState() == null ||  store.getContactList().get(0).getState().isNullValue()) {
                ctx.addMessage("OpenAccount:stateCombo",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error: Please select state", null));
                toReturn = false;
            }
            if (person.getContact().getEmail().isEmpty() ||
                    !Email.validate(person.getContact().getEmail()) ||
                    !person.getContact().confirmEmail()) {
                ctx.addMessage("OpenAccount:Email",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error: Fields Email and Email Confirm must be identical", null));
                toReturn = false;
            }
            List<Person> lstPerson = dao.get("from Person p where p.login= '" + person.getContact().getEmail().trim() + "'");
            if (!lstPerson.isEmpty()) {
                ctx.addMessage("OpenAccount:Email", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "The email address you have entered is already in use. \r\n" +
                        "If you already have an account, please go to the Sign In page  \r\n" +
                        "or contact us if you are still having trouble.", null));
                toReturn = false;
            }

        }

        return toReturn;
    }

    String validateLogin(String personName) {
        if (personName.isEmpty()) {
            personName = "empty";
        }
        String login = personName;
        if (personName.length() > 49) {
            personName = personName.substring(personName.length() - (personName.length() - 49));
        }
        List<Person> lstFldr = dao.get((String) "from Person where login='" + login + "'");
        if (!lstFldr.isEmpty()) {
            SecureRandom random = new SecureRandom();
            String ident = new BigInteger(130, random).toString(8).substring(1, 2);
            login = personName + ident;
            validateLogin(login);
        }
        return login;
    }

    public void save() {
        company.setAccepted(false);
        store.setAccepted(false);
        //company.setIncorporatedState(null);
        company.setRequestDate(new Date());

        company.autoNormalizeContact();
        company.getContactList().get(0).setCountry("USA"); // default country
        Contact companyPrimary = company.getContactList().get(1);

        String personName = person.getFirstName() + " " + person.getLastName();

        String firstLetter = (person.getFirstName().isEmpty()) ? "" : person.getFirstName().substring(0, 1);
        person.setLogin(validateLogin(person.getContact().getEmail()));
        person.setRole(0); // locked account
        String pass;
        if (isAutoPassword()) {
            pass = ": auto :";
        } else {
            if (getPasswordConfirm().isEmpty()) {
                pass = ": auto :";
            } else {
                pass = "??" + getPasswordConfirm();
            }
        }
        person.setPasswordDirect(pass);

        Contact personContact = person.getContact();
        personContact.setCaption(personName.trim());

        // Add Primary Contact
        companyPrimary.setCaption(personName);
        companyPrimary.setPhone1(personContact.getPhone1());
        companyPrimary.setEmail(personContact.getEmail());
        companyPrimary.setCountry("USA");  // default country
        companyService.saveOrUpdate(company);

        person.setLogin(personContact.getEmail());
        person.setCompany(company);
        //contactService.saveOrUpdate(personContact);
        personService.saveOrUpdate(person); // TODO: merge
        //person.getContact().
        store.setRequestDate(Week.getCurrentDateTime());
        store.setFranchise(company.getFranchise());
        store.setCaption(company.getCaption());

        store.autoNormalizeContact();
        store.getContactList().get(0).setCaption(store.getCaption());
        store.getContactList().get(0).setCountry("USA"); // default country
        store.getContactList().get(0).setPhone1(personContact.getPhone1());

        // person.getStoreList().add(store); check
        store.getPersonList().add(person); //TODO: Check on Lazy
        store.setCompany(company);
        company.getStoreList().add(store);
        companyService.saveOrUpdate(company);
        for (Folder folder : Folder.getDefaultFolder()) {
            folder.setStore(store);
            dao.saveOrUpdate(folder);
        }
    }

    /*  private void edit() {
    company.autoNormalizeContact();
    // company.getContactList().get(0).setCountry("USA"); // default country
    Contact companyPrimary = company.getContactList().get(1);

    String personName = person.getFirstName() + " " + person.getLastName();

    person.setLogin(person.getContact().getEmail());
    //person.setRole(0); // locked account
    person.setPasswordDirect(getPasswordConfirm());

    Contact personContact = person.getContact();
    personContact.setCaption(personName.trim());

    // Add Primary Contact
    //companyPrimary.setCaption(personName);
    companyPrimary.setPhone1(personContact.getPhone1());
    companyPrimary.setEmail(personContact.getEmail());
    companyPrimary.setCountry("USA");  // default country
    companyService.saveOrUpdate(company);

    person.setCompany(company);
    //contactService.saveOrUpdate(personContact);
    personService.saveOrUpdate(person); // TODO: merge
    //person.getContact().
    store.setRequestDate(Week.getCurrentDateTime());
    //store.setFranchise(company.getFranchise());
    //store.setCaption(company.getCaption());

    store.autoNormalizeContact();
    store.getContactList().get(0).setCaption(store.getCaption());
    store.getContactList().get(0).setCountry("USA"); // default country

    // person.getStoreList().add(store); check

    store.getPersonList().add(person); //TODO: Check on Lazy
    //store.setCompany(company);
    // company.getStoreList().add(store);
    companyService.saveOrUpdate(company);
    for (Folder folder : Folder.getDefaultFolder()) {
    folder.setStore(store);
    dao.saveOrUpdate(folder);
    }

    }*/
    public String createAccount() {
        String result = "success";
        if (!verify()) {
            return null;
        }

        save();

        FacesContext fctx = FacesContext.getCurrentInstance();
        ServletContext sctx = (ServletContext) fctx.getExternalContext().getContext();
        String path = sctx.getRealPath(File.separator);
        FileInputStream fis;
        try {
            fis = new FileInputStream(path + "prop" + File.separator + "mail.properties");
            Properties prop = new Properties();
            prop.load(fis);
            /*
            String body = String.format(prop.getProperty("accountMailBody"),
            getCompany().getCaption(), getCompany().getFranchise().getCaption(),
            getPerson().getFirstName(), getPerson().getLastName());
            String passwordMsg = "";
            if (!isAutoPassword()) {
            passwordMsg = "\nUser Name is: "+person.getLogin()+" with defined Password: " + getPassword();
            }
             */
            if (!getPersonContact().getEmail().isEmpty()) {
                sendEmail(getPersonContact().getEmail(),
                        prop.getProperty("accountMailBcc"),
                        new Date(),
                        prop.getProperty("accountMailSubject"),
                        prop.getProperty("accountMailBody"),
                        prop.getProperty("accountMailFrom"),
                        prop.getProperty("accountMailFromName"),
                        getPersonContact().getEmail());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OpenAccountManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OpenAccountManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }

       // renew();
        success=true;
        return result;
    }
    //========================================================

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Contact getPersonContact() {
        return person.getContact();
    }

    public void setPersonContact(Contact personContact) {
        this.person.setContact(personContact);
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Contact getStoreContact() {
        return store.getContactList().get(0);
    }

    public void setStoreContact(Contact storeContact) {
        this.store.getContactList().set(0, storeContact);
    }

    public Map getStateItems() {
        return stateManagedBean.getStateItems();
    }

    public Map getFranchiseItems() {
        return franchiseManagedBean.getFranchiseItems();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAutoPassword() {
        return autoPassword;
    }

    public void setAutoPassword(boolean autoPassword) {
        this.autoPassword = autoPassword;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
