/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers;

import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.UniversalDAO;
import java.io.Serializable;
import java.util.Calendar;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;

public class ParentController implements Serializable {

    public final static String DATE_FORMAT = "yyyy-MM-dd";
    @Autowired
    protected UniversalDAO dao;
    @Autowired
    protected UserManagedBean userManagedBean;
    protected Calendar periodBegin = Calendar.getInstance();
    private boolean needRefresh = true;
    private String targetPage;

    public UserManagedBean getUserManagedBean() {
        return userManagedBean;
    }

    public void setUserManagedBean(UserManagedBean userManagedBean) {
        this.userManagedBean = userManagedBean;
    }

    public Store getStore() {
        return userManagedBean.getActiveStore();
    }

    protected Calendar getStartDate() {
        ReportingWeek week = userManagedBean.getActiveWeek();
        if (week != null) {
            periodBegin.setTime(week.getWeekDate());
        }
        return periodBegin;
    }

    protected Calendar getFinishDate() {
        Calendar result = (Calendar) periodBegin.clone();
        result.add(Calendar.DATE, 6);
        return result;
    }

    public boolean isNeedRefresh() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        String referer = request.getHeader("referer");
        boolean doRefresh = false;
        if (referer == null || (referer != null && !referer.contains(request.getRequestURI()))) {
            doRefresh = true;
        }
        return needRefresh || doRefresh;
    }

    public void setNeedRefresh(boolean needRefresh) {
        this.needRefresh = needRefresh;
    }

    public String setNeedRefreshFlag() {
        this.needRefresh = true;
        return targetPage;
    }

    public String getTargetPage() {
        return targetPage;
    }

    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }

    public static void addFacesMessageForUI(String uiMessage) {
        addFacesMessageForUI(uiMessage, null);
    }

    public static void addFacesMessageForUI(String uiMessage, UIComponent comp) {
        FacesMessage facesMessage = new FacesMessage(uiMessage);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        // Passing null for the client ID argument to the FacesContext
        // addMessage() method specifies the message as not belonging
        // to any particular UI component. This will cause the message
        // to be displayed as a general message on the UI
        String id = null;
        if (comp != null) {
            id = comp.getClientId(facesContext);
        }
        facesContext.addMessage(id, facesMessage);
        int count = 0;
        String countStr = (String) facesContext.getExternalContext().getRequestMap().get("ErrorCount");
        if (countStr != null) {
            count = Integer.parseInt(countStr);
        }
        count += 1;
        facesContext.getExternalContext().getRequestMap().put("ErrorCount", String.valueOf(count));
    }
}
