package com.zuma.web.controllers;

import com.zuma.db.core.domain.Person;
import com.zuma.db.dao.UniversalDAO;
import com.zuma.db.service.util.MD5;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class PasswordManagerController extends EmailSenderController { 
    @Autowired
    protected UniversalDAO dao;
    @Autowired
    protected UserManagedBean userManagedBean;
    private String newPassword;
    private String oldPassword;
    private String confrimPassword;
    private String email;
    private boolean active=false;
    private String curPage="myaccount";
    private boolean showSuccessPage = false;

    public void setShowSuccessPage(boolean showSuccessPage) {
        this.showSuccessPage = showSuccessPage;
    }

    public boolean isShowSuccessPage() {
        return showSuccessPage;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public String getConfrimPassword() {
        return confrimPassword;
    }

    public void setConfrimPassword(String confrimPassword) {
        this.confrimPassword = confrimPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String createPassword() {
        String toReturn =  curPage;
        if (!validatePasswords()) {
            return null;
        }
        active = false;
        return toReturn;
    }
    public String cancelPassword() {
       
        active = false;
        return curPage;
    }
    public String changePassword() {

        active = true;
        return curPage;
    }
    public String renewPassword() {
        FacesContext ctx= FacesContext.getCurrentInstance();

        List<Person>lstPerson= dao.get("from Person p where p.login= '"+getEmail().trim()+"'");
        if (lstPerson.isEmpty()) {
            ctx.addMessage("frm:email", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error: The email address you entered does not match our records.\r\n" +
                    "Please try again or contact us if you are still having trouble.",null));
             return null;
        }
        SecureRandom random = new SecureRandom();
        String newPsswrd = new BigInteger(130, random).toString(8).substring(1, 10);
        ServletContext sctx = (ServletContext) ctx.getExternalContext().getContext();
        String path = sctx.getRealPath(File.separator);
        FileInputStream fis;
        Properties prop = new Properties();
        try {
            fis = new FileInputStream(path + "prop" + File.separator + "mail.properties");
            prop.load(fis);
        }catch (FileNotFoundException ex) {
            Logger.getLogger(PasswordManagerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex) {
            Logger.getLogger(PasswordManagerController.class.getName()).log(Level.SEVERE, null, ex);
        }

        String body= String.format(prop.getProperty("passwordBody"),newPsswrd);
        sendEmail(getEmail(),
                prop.getProperty("userBcc"),
                new Date(),
                prop.getProperty("passwordSubject"),
                body,
                prop.getProperty("userFrom"),prop.getProperty("userFromName"),
                null);
        Person prsn=lstPerson.iterator().next();
        prsn.setPassword(newPsswrd);
        dao.saveOrUpdate(prsn);
        setEmail("");
       showSuccessPage = true;
        return null;
    }

    public boolean validatePasswords() {
        boolean toReturn = true;
        FacesContext ctx = FacesContext.getCurrentInstance();

        if(getOldPassword()==null ||getNewPassword()==null||getConfrimPassword()==null){
            return false;
        }


        if (!userManagedBean.getActiveUser().getPassword().equalsIgnoreCase(MD5.encode(getOldPassword()))) {
            ctx.addMessage("svCP:f_Password:itOld", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error: The old password is not correct. Please Try again", null));
            toReturn=false;

        }
        if ( !getNewPassword().equalsIgnoreCase(getConfrimPassword()) )  {
            ctx.addMessage("svCP:f_Password:itNew", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error:Please make sure Password and Confirm Password fields are matching.", null));
            toReturn=false;

        }
        userManagedBean.getActiveUser().setPassword(newPassword);
        dao.saveOrUpdate(userManagedBean.getActiveUser());
        return toReturn;

    }
        public String redirectPage() {
        showSuccessPage = false;       
         return userManagedBean.logout();

    }


}
