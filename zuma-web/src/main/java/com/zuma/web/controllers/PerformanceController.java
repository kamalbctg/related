package com.zuma.web.controllers;

import com.zuma.db.core.domain.InventoryStartData;
import com.zuma.db.core.domain.TrackerData;
import com.zuma.web.controllers.helper.analysis.DiscountsLoader;
import com.zuma.web.controllers.helper.analysis.FBLoader;
import com.zuma.web.controllers.helper.analysis.LaborLoader;
import com.zuma.web.controllers.helper.analysis.Net2Loader;
import com.zuma.web.controllers.helper.analysis.PaperLoader;
import com.zuma.db.service.util.Week;
import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Calendar;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.DefaultCategoryDataset;

@Component
@Scope("session")
public class PerformanceController extends AnalysisParentController {

    public static final String foodItems = "Food Items";
    private InventoryStartData inventoryStartData;

    @Override
    public void recalculate(PhaseEvent ev) throws ParseException, Exception {
        super.recalculate(ev);
        if (ev.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            String sql = "FROM InventoryStartData WHERE store.id=" + getStore().getId().toString();
            inventoryStartData = dao.getObject(sql, InventoryStartData.class);
            if (inventoryStartData == null) {
                inventoryStartData = new InventoryStartData(getStore());

            }
        }
        //  summaryController.recalculate(ev);
        //  wasteController.recalculate(ev);
        //  purchaseViewController.reInitDataList();
        //   inventoryViewController.isCopyFromPreviousWeekEnabled();
        //   prepareMaps();
    }

    public boolean isNeededStartDataDialog() {
        return getInventoryStartData().getId() == null && getStore().getRequestDate().after(Week.getCurrentWeekBegin());
    }

    public String saveInventoryStartData() {
        dao.saveOrUpdate(getInventoryStartData());
        return null;
    }

    public BigDecimal getBegInventory() {
        BigDecimal result = BigDecimal.ZERO;
        FBLoader loader = new FBLoader(dao, getStore(), getStartDate(), getFinishDate());
        try {
            Calendar date = (Calendar) getStartDate().clone();
            date.add(Calendar.DAY_OF_YEAR, -7);
            BigDecimal tmp = loader.getBeginInventory(date);
            if (tmp != null) {
                result = tmp;
            }
        } catch (ParseException ex) {
            Logger.getLogger(PerformanceController.class.getName()).error(null, ex);
        }
        return result;
    }

    public BigDecimal getEndInventory() {
        BigDecimal result = BigDecimal.ZERO;
        FBLoader loader = new FBLoader(dao, getStore(), getStartDate(), getFinishDate());
        try {
            BigDecimal tmp = loader.getEndInventory(getFinishDate());
            if (tmp != null) {
                result = tmp;
            }
        } catch (ParseException ex) {
            Logger.getLogger(PerformanceController.class.getName()).error(null, ex);
        }
        return result;
    }

    public BigDecimal getPurchase() {
        BigDecimal result = BigDecimal.ZERO;
        FBLoader loader = new FBLoader(dao, getStore(), getStartDate(), getFinishDate());
        try {
            BigDecimal tmp = loader.getPurchaseTotal(getStore(), getStartDate(), getFinishDate());
            if (tmp != null) {
                result = tmp;
            }
        } catch (ParseException ex) {
            Logger.getLogger(PerformanceController.class.getName()).error(null, ex);
        }
        return result;
    }

    public BigDecimal getWaste() {
        BigDecimal result = BigDecimal.ZERO;
        FBLoader loader = new FBLoader(dao, getStore(), getStartDate(), getFinishDate());
        try {
            BigDecimal tmp = loader.getWaste(getStartDate(), getFinishDate());
            if (tmp != null) {
                result = tmp;
            }
        } catch (ParseException ex) {
            Logger.getLogger(PerformanceController.class.getName()).error(null, ex);
        }
        return result;
    }

    public BigDecimal getLaborHours() {
        BigDecimal result = BigDecimal.ZERO;
        LaborLoader loader = new LaborLoader(dao, getStore(), getStartDate(), getFinishDate());
        try {
            BigDecimal tmp = loader.getLaborHours(getStore(), getStartDate(), getFinishDate());
            if (tmp != null) {
                result = tmp;
            }
        } catch (ParseException ex) {
            Logger.getLogger(PerformanceController.class.getName()).error(null, ex);
        }
        return result;
    }

    public BigDecimal getAverageLaborHours() {
        BigDecimal result = BigDecimal.ZERO;
        LaborLoader loader = new LaborLoader(dao, getStore(), getStartDate(), getFinishDate());
        try {
            BigDecimal tmp = loader.getLaborHours(getStore(), getStartDate(), getFinishDate());
            TrackerData data = getDataTargetMap().get("Labor");
            if (data != null && tmp != null && !tmp.equals(BigDecimal.ZERO)) {
                result = data.getActualCurrent().divide(tmp, 4, RoundingMode.HALF_UP);
            }
        } catch (ParseException ex) {
            Logger.getLogger(PerformanceController.class.getName()).error(null, ex);
        }
        return result;
    }

    public BigDecimal getPaperBegInventory() {
        BigDecimal result = BigDecimal.ZERO;

        PaperLoader loader = new PaperLoader(dao, getStore(), getStartDate(), getFinishDate());
        try {
            Calendar date = (Calendar) getStartDate().clone();
            date.add(Calendar.DAY_OF_YEAR, -7);
            BigDecimal tmp = loader.getBeginInventory(date);
            if (tmp != null) {
                result = tmp;
            }
        } catch (ParseException ex) {
            Logger.getLogger(PerformanceController.class.getName()).error(null, ex);
        }
        return result;
    }

    public BigDecimal getPaperEndInventory() {
        BigDecimal result = BigDecimal.ZERO;
        PaperLoader loader = new PaperLoader(dao, getStore(), getStartDate(), getFinishDate());
        try {
            BigDecimal tmp = loader.getEndInventory(getFinishDate());
            if (tmp != null) {
                result = tmp;
            }
        } catch (ParseException ex) {
            Logger.getLogger(PerformanceController.class.getName()).error(null, ex);
        }
        return result;
    }

    public BigDecimal getPaperPurchase() {
        BigDecimal result = BigDecimal.ZERO;
        PaperLoader loader = new PaperLoader(dao, getStore(), getStartDate(), getFinishDate());
        try {
            BigDecimal tmp = loader.getPurchaseTotal(getStore(), getStartDate(), getFinishDate());
            if (tmp != null) {
                result = tmp;
            }
        } catch (ParseException ex) {
            Logger.getLogger(PerformanceController.class.getName()).error(null, ex);
        }
        return result;
    }

    public BigDecimal getSales() {
        BigDecimal result = BigDecimal.ZERO;
        Net2Loader loader = new Net2Loader(dao, getStore(), getStartDate(), getFinishDate());
        try {
            //BigDecimal tmp = loader.getTotalFor("Sales", getStore(), getStartDate(), getFinishDate());
            BigDecimal tmp = loader.getTotalFor("", getStore(), getStartDate(), getFinishDate());
            if (tmp != null) {
                result = tmp;
            }
        } catch (ParseException ex) {
            Logger.getLogger(PerformanceController.class.getName()).error(null, ex);
        }
        return result;
    }

    public BigDecimal getDiscounts() {
        BigDecimal result = BigDecimal.ZERO;
        DiscountsLoader loader = new DiscountsLoader(dao, getStore(), getStartDate(), getFinishDate());
        try {
            BigDecimal tmp = loader.getCurrent();
            if (tmp != null) {
                result = tmp;
            }
        } catch (ParseException ex) {
            Logger.getLogger(PerformanceController.class.getName()).error(null, ex);
        }
        return result;
    }

    public BigDecimal getPercentDiscounts() {
        BigDecimal result = BigDecimal.ZERO;

        BigDecimal tmp = getSales();
        if (tmp != null && !tmp.equals(BigDecimal.ZERO)) {
            result = getDiscounts().divide(tmp, 4, RoundingMode.HALF_UP);
        }
        return result;
    }

    public void generateChart(double target, double current, double mtd, double ytd, OutputStream out) throws IOException {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        dataset.addValue(target, "", "Target");
        dataset.addValue(current, "", "Active");
        dataset.addValue(mtd, "", "MTD");
        dataset.addValue(ytd, "", "YTD");
        double max = target;
        double min = target;

        if (current > max) {
            max = current;
        }
        if (mtd > max) {
            max = mtd;
        }
        if (ytd > max) {
            max = ytd;
        }
        if (current < min) {
            min = current;
        }
        if (mtd < min) {
            min = mtd;
        }
        if (ytd < min) {
            min = ytd;
        }
        double range = 1.1 * (max - min);
        if (range == 0) {
            range = 1;
        }

        JFreeChart chart = ChartFactory.createBarChart(
                "",
                "",
                "",
                dataset,
                PlotOrientation.VERTICAL,
                false, //include legend
                false, //tooltips
                false); //urls
        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...

        // set the background color for the chart...
        chart.setBackgroundPaint(Color.white);
        chart.setBorderVisible(false);



        // get a reference to the plot for further customisation...
        CategoryPlot plot = chart.getCategoryPlot();
        plot.setBackgroundPaint(Color.white);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        plot.setDomainGridlinesVisible(false);
        plot.setDrawSharedDomainAxis(false);
        plot.setOutlinePaint(Color.white);


        plot.setRangeAxisLocation(AxisLocation.TOP_OR_LEFT);

        Font fnt = new Font("Arial", Font.PLAIN, 10);
        NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        //hide left axis
        rangeAxis.setVisible(false);
        rangeAxis.setAxisLineVisible(false);
        rangeAxis.setAutoRangeIncludesZero(true);
        rangeAxis.setUpperMargin(0.1);
        rangeAxis.setLowerMargin(0.13);
        // rangeAxis.setRange(0, range);

        CategoryAxis domainAxis = plot.getDomainAxis();

        domainAxis.setTickLabelFont(fnt);


        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setDrawBarOutline(false);
        renderer.setSeriesPaint(0, new Color(16, 146, 16));
        renderer.setItemMargin(0.01);



        renderer.setBaseItemLabelFont(fnt);
        renderer.setBaseLegendTextFont(fnt);


        DecimalFormat decimalformat1 = new DecimalFormat("##0.00%");
        renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator("{2}", decimalformat1));
        renderer.setBaseItemLabelsVisible(true);



        BufferedImage buffImg = chart.createBufferedImage(
                230, //width
                175, //height
                BufferedImage.TYPE_INT_RGB, //image type
                null);
        ImageIO.write(buffImg, "gif", out);

    }

    public void generateLaborChart(OutputStream out, Object data) throws IOException {


        float current = 0;
        try {
            current = 0;//getLaborPerSales();
        } catch (Exception ex) {
            Logger.getLogger(PerformanceController.class.getName()).info(null, ex);
        }
        generateChart(1.0, current, -3.0, 5.0, out);
    }

    public void generateChart(OutputStream out, Object data) throws IOException {
        TrackerData tmp = (TrackerData) data;
        generateChart(tmp.getTarget().doubleValue() / 100,
                tmp.getPercentCurrent().doubleValue(),
                tmp.getPercentMTD().doubleValue(),
                tmp.getPercentYTD().doubleValue(), out);
    }

    public boolean isPrevWeekAvaible() {
        Calendar startDate = (Calendar) getStartDate().clone();
        startDate.add(Calendar.DAY_OF_YEAR, -7);
        return userManagedBean.isWeekAvaible(startDate.getTime());
    }

    public void setNewWeek(String value) {
        Calendar startDate = (Calendar) getStartDate().clone();
        startDate.add(Calendar.DAY_OF_YEAR, -4);
        userManagedBean.setActiveWeekDay(Week.getWeekBegin(startDate.getTime()));
    }

    public InventoryStartData getInventoryStartData() {
        return inventoryStartData;
    }

    public void setInventoryStartData(InventoryStartData inventoryStartData) {
        this.inventoryStartData = inventoryStartData;
    }
}
