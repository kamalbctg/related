package com.zuma.web.controllers;

import com.zuma.db.service.PurchaseWeeklyService;
import com.zuma.db.service.util.Week;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Date;

import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.zuma.db.core.domain.Purchase;
import com.zuma.db.core.domain.Store;
import com.zuma.db.service.impl.GenericServiceImpl;

@Component
@Scope("session")
public class PurchaseViewController implements Serializable {

    private int emptyRowsCount = 5;
    @Autowired
    private UserManagedBean userManagedBean;
    @Autowired
    @Qualifier("storeService")
    protected GenericServiceImpl<Store, Integer> storeService;
    @Autowired
    protected PurchaseWeeklyService purchaseWeeklyService;
    @Autowired
    @Qualifier("purchaseService")
    protected GenericServiceImpl<Purchase, Integer> purchaseService;
    protected Collection<Purchase> purchaseList;
    private List<Purchase> changedRecords = new LinkedList<Purchase>();
    private List<Purchase> removeRecords = new LinkedList<Purchase>();
    private Purchase selectedPurchase;
    private Store currentStore;
    private Date currentWeekDate;

    // session
    @PostConstruct
    public void loadData() {
        Store store = userManagedBean.getActiveStore();
        currentStore = storeService.merge(store);
        userManagedBean.refreshReportingWeeks();
        currentWeekDate = userManagedBean.getActiveWeekDay();
        purchaseList = purchaseWeeklyService.getAll(store,
                Week.getWeekBegin(currentWeekDate), Week.getWeekEndTime(currentWeekDate));
    }

    public void reInitDataList() {
        Store store = userManagedBean.getActiveStore();
        Date date = userManagedBean.getActiveWeekDay();

        if (!currentStore.equals(store) || !currentWeekDate.equals(date)) {
            // data for current week and store will be droped
            // after changes;
            loadData();
            changedRecords.clear(); // clear all changes!
            removeRecords.clear();
        }
    }

    public Collection<Purchase> getPurchaseList() {
        reInitDataList();
        addEmptyRows();
        return purchaseList;
    }

    public boolean verifyDate(Date day) {
       if (day == null) {
           return false;
       }
       if (day.compareTo(Week.getWeekBegin(currentWeekDate))<0 ||
           day.compareTo(Week.getWeekEndTime(currentWeekDate))>0) {
           return false;
       }
       return true;
    }

    public boolean verify() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        int i=0;
        for (Purchase item : purchaseList) {
            if (!verifyDate(item.getDate())) {
                ctx.addMessage("pur:tbl:"+String.valueOf(i)+":cDat",
                        new FacesMessage(FacesMessage.SEVERITY_ERROR,
                        "Error: The date you have entered is not in the active week. Please change the date or select the correct Active Week at the top of the page.", null));
                return false;
            }
            i++;
        }
        return true;
    }

    public String save() {
        if (!verify()) {
            return null;
        }
        
        removeEmptyRows();
        for (Purchase item : purchaseList) {
            changedRecords.add(item);
        }

        purchaseService.removeAll(removeRecords);
        purchaseService.mergeAll(changedRecords);

        changedRecords.clear();
        removeRecords.clear();
        loadData();

        return null;
    }

    private Purchase createNewItem() {
        Purchase newPurchase = new Purchase();
        newPurchase.setDate(currentWeekDate);
        newPurchase.setVendor("");
        newPurchase.setDescription("");
        newPurchase.setPayment("");
        newPurchase.setInvoice("");
        newPurchase.zeroDecimal();
        newPurchase.setStore(currentStore);
        return newPurchase;
    }

    public void addEmptyRows() {
        int additionalRows = emptyRowsCount - purchaseList.size();
        for (int i = 0; i <
                additionalRows; i++) {
            Purchase newPurchase = createNewItem();
            newPurchase.setVisible(false);
            purchaseList.add(newPurchase);
        }

    }

    private boolean isModified(Purchase purchase) {
        if (purchase.getId() != null) {
            return true;
        }

        if (!purchase.getVendor().trim().isEmpty() ||
                !purchase.getDescription().trim().isEmpty() ||
                !purchase.getInvoice().trim().isEmpty() ||
                !purchase.getPayment().trim().isEmpty()) {
            return true;
        }

        if (!purchase.getInvoiceTotal().equals(BigDecimal.ZERO)) {
            return true;
        }

        return false;
    }

    public void removeEmptyRows() {
        Collection<Purchase> removeList = new LinkedList<Purchase>();

        for (Purchase purchase : purchaseList) {
            boolean modified = isModified(purchase);
            if (modified) {
                // verify date and other fields or do nothing
                purchase.setVisible(true);
            } else {
                removeList.add(purchase);
            }

        }
        purchaseList.removeAll(removeList);
    }

    public String insertOneItem() {
        Purchase newPurchase = createNewItem();
        newPurchase.setVisible(true); //default = true
        purchaseList.add(newPurchase);
        return null;
    }

    public int getCount() {
        return purchaseList.size();
    }

    public Purchase getTotal() {
        Purchase newPurchase = new Purchase();

        newPurchase.setDate(new Date());
        newPurchase.setDescription("Total");
        newPurchase.zeroDecimal();

        BigDecimal foodAndBeverage = BigDecimal.ZERO;
        BigDecimal paper = BigDecimal.ZERO;
        BigDecimal fAndBTaxes = BigDecimal.ZERO;
        BigDecimal otherTaxes = BigDecimal.ZERO;
        BigDecimal freight = BigDecimal.ZERO;
        BigDecimal other = BigDecimal.ZERO;

        // if or all fields nullable = false
        for (Purchase item : purchaseList) {
            if (item.getFoodAndBeverage() != null) {
                foodAndBeverage = foodAndBeverage.add(item.getFoodAndBeverage());
            }
            if (item.getPaper() != null) {
                paper = paper.add(item.getPaper());
            }
            if (item.getFAndBTaxes() != null) {
                fAndBTaxes = fAndBTaxes.add(item.getFAndBTaxes());
            }
            if (item.getOtherTaxes() != null) {
                otherTaxes = otherTaxes.add(item.getOtherTaxes());
            }
            if (item.getFreight() != null) {
                freight = freight.add(item.getFreight());
            }
            if (item.getOther() != null) {
                other = other.add(item.getOther());
            }
        }

        newPurchase.setFoodAndBeverage(foodAndBeverage);
        newPurchase.setPaper(paper);
        newPurchase.setFAndBTaxes(fAndBTaxes);
        newPurchase.setOtherTaxes(otherTaxes);
        newPurchase.setFreight(freight);
        newPurchase.setOther(other);

        return newPurchase;
    }

    public void processValueChange(ValueChangeEvent event)
            throws AbortProcessingException {

        DecimalFormat twoDForm = new DecimalFormat("########.##");
        BigDecimal newValue = BigDecimal.ZERO;
        BigDecimal oldValue = BigDecimal.ZERO;
        Double oldDouble = Double.valueOf(twoDForm.format(((BigDecimal) event.getOldValue()).doubleValue()));
        oldValue =
                BigDecimal.valueOf(oldDouble);

        if (event.getNewValue() instanceof Long) {
            newValue = BigDecimal.valueOf((Long) event.getNewValue());
        } else if (event.getNewValue() instanceof Double) {
            Double newDouble = Double.valueOf(twoDForm.format((Double) event.getNewValue()));
            newValue =
                    BigDecimal.valueOf(newDouble);

        }

        if (oldValue.compareTo((newValue)) != 0) {
            System.out.println("!!!!!!!!!!!!!!!!!!!!!");
            System.out.println("Prev Value =" + oldValue);
            System.out.println("New Value =" + newValue);
            changedRecords.add((Purchase) (((UIParameter) (event.getComponent()).getChildren().get(0)).getValue()));
        }
// TODO: refactor this

    }

    public String deletePurchase() {
        purchaseList.remove(selectedPurchase);
        removeRecords.add(selectedPurchase);

        return null;
    }

    public Purchase getSelectedPurchase() {
        return selectedPurchase;
    }

    public void setSelectedPurchase(Purchase selectedPurchase) {
        this.selectedPurchase = selectedPurchase;
    }
}
