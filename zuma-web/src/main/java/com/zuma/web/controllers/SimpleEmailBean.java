package com.zuma.web.controllers;

import java.io.Serializable;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

public class SimpleEmailBean extends SimpleEmail implements Serializable {
    private String host;
    private int port = 25;

    private boolean popAuthorization = false;
    private String authPopStatus = "false";
    private String authPopHost;
    private String authPopUsername;
    private String authPopPassword;
    private String body;

    public void SimpleEmail() {
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getAuthPopHost() {
        return authPopHost;
    }

    public void setAuthPopHost(String authPopHost) {
        this.authPopHost = authPopHost;
    }

    public String getAuthPopUsername() {
        return authPopUsername;
    }

    public void setAuthPopUsername(String authPopUsername) {
        this.authPopUsername = authPopUsername;
    }

    public String getAuthPopPassword() {
        return authPopPassword;
    }

    public void setAuthPopPassword(String authPopPassword) {
        this.authPopPassword = authPopPassword;
    }

    @Override
    public String send() throws EmailException {
        setHostName(getHost());
        setSmtpPort(getPort());

        if (isPopAuthorization()) {
            super.setPopBeforeSmtp(true,
                                   getAuthPopHost(),
                                   getAuthPopUsername(),
                                   getAuthPopPassword());
        }

        //setMsg(message);
        //super.updateContentType(Email.TEXT_HTML);
        
        try {
        MimeMultipart mm = getMultipartMessage(getBody());
        super.setContent(mm);
        } catch (MessagingException me) {
            throw new EmailException(me.getMessage());
        }
        super.buildMimeMessage();
        return super.sendMimeMessage();
    }

    @Override
    protected MimeMessage createMimeMessage(Session aSession)
    {
           SimpleMimeMessage mimeMessage = new SimpleMimeMessage(aSession);
           mimeMessage.setFromHost(getHost());
           return mimeMessage;
    }

    public boolean isPopAuthorization() {
        return popAuthorization;
    }

    public void setPopAuthorization(boolean popAuthorization) {
        this.popAuthorization = popAuthorization;
    }

    public String getAuthPopStatus() {
        return authPopStatus;
    }

    public void setAuthPopStatus(String authPopStatus) {
        this.authPopStatus = authPopStatus;
        popAuthorization = (authPopStatus!=null && authPopStatus.equalsIgnoreCase("true"));
    }
    public MimeMultipart getMultipartMessage(String msg) throws MessagingException {
        MimeMultipart multipart = new MimeMultipart("alternative");
        MimeBodyPart bodyPart = new MimeBodyPart();

        bodyPart.setText(msg,"utf-8","html");
        multipart.addBodyPart(bodyPart);

        return multipart;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}