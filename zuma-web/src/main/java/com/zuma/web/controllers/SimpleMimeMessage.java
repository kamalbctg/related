package com.zuma.web.controllers;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SimpleMimeMessage  extends MimeMessage {
    private static int id = 0;
    private String fromHost;

    public SimpleMimeMessage(Session session) {
	super(session);
    }

    public String getUniqueMessageIDValue(Session ssn) {
	String suffix = null;

        InternetAddress addr = InternetAddress.getLocalAddress(ssn);
	if (addr != null)
	    suffix = addr.getAddress();
	else {
	    suffix = "javamailuser@localhost"; // worst-case default
	}
        /*
	if (getFromHost() != null)
	    suffix = "@"+getFromHost();
	else {
	    suffix = "@RelatedGlobal.com";
	}
        */
	StringBuffer s = new StringBuffer();

	// Unique string is <hashcode>.<id>.<currentTime>.JavaMail.<suffix>
	s.append(s.hashCode()).append('.').append(id++).
	  append(System.currentTimeMillis()).append('.').
	  append(suffix);
	return s.toString();
    }

    @Override
    protected void updateMessageID() throws MessagingException {
        setHeader("Message-ID",
		  "<" + getUniqueMessageIDValue(session) + ">");

        setHeader("X-Priority","3");
        setHeader("X-MSMail-Priority","Normal");
        setHeader("X-Mailer","Microsoft Outlook Express 6.00.2900.5843");
        setHeader("X-MimeOLE","Produced By Microsoft MimeOLE V6.00.2900.5579");
/*
X-Priority: 3
X-MSMail-Priority: Normal
X-Mailer: Microsoft Outlook Express 6.00.2900.5843
X-MimeOLE: Produced By Microsoft MimeOLE V6.00.2900.5579
*/
    }

    public String getFromHost() {
        return fromHost;
    }

    public void setFromHost(String fromHost) {
        this.fromHost = fromHost;
    }
}

