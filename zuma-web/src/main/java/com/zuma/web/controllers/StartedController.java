package com.zuma.web.controllers;

import com.zuma.db.core.domain.StoreDictionary;
import com.zuma.db.dao.UniversalDAO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.richfaces.model.DataProvider;
import org.richfaces.model.ExtendedTableDataModel;
import org.richfaces.model.selection.SimpleSelection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class StartedController implements Serializable {

    private SimpleSelection selectionStore = new SimpleSelection();
    private boolean showResult;
    private String zipCode;
    private String franchiseCaption;
    @Autowired
    protected FranchiseManagedBean franchiseManagedBean;
    @Autowired
    protected UniversalDAO dao;
    @Autowired
    OpenAccountManagedBean openAccountManagedBean;
    private List<StoreDictionary> stores = new ArrayList<StoreDictionary>();
    private String targetPage;

    public String getTargetPage() {
        return targetPage;
    }

    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }
  
    private ExtendedTableDataModel dataModel;
    private Object tableState;

    public boolean isShowResult() {
        return showResult;
    }

    public String getFranchiseCaption() {
        return franchiseCaption;
    }

    public void setFranchiseCaption(String franchiseCaption) {
        this.franchiseCaption = franchiseCaption;
    }

   public void setStores(List stores) {
        this.stores = stores;
    }

    public List<StoreDictionary> getStores() {
         return stores;
    }

    public SimpleSelection getSelectionStore() {
        return selectionStore;
    }

    public void setSelectionStore(SimpleSelection selectionStore) {
        this.selectionStore = selectionStore;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String ZipCode) {
        this.zipCode = ZipCode;
    }

    public StartedController() {
        showResult = false;
    }

    public String findStore() {
        if (franchiseManagedBean.getByCaption(franchiseCaption) == null) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage("fFind:statesinput",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "We did not recognize your franchise. \r\n Click \"Create My Profile\" if your franchise is not listed in the drop-down menu.", null));
            return null;
        }

        dataModel = null;
        stores.clear();
        selectionStore.clear();
        stores = dao.get("from StoreDictionary sd where sd.franchiseName like '" + franchiseCaption.replace("'", "''") + "' and sd.zip like '"+getZipCode()+"%'") ;
        showResult = true;
        return "getstarted";

    }

    public String btnContinueClick() {
        if (selectionStore.size() == 0) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ctx.addMessage("fFind:statesinput",
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Error: Please Select Your Store", null));
            return null;
        }

        Iterator<Object> iterator = getSelectionStore().getKeys();
        StoreDictionary selStore = null;
        while (iterator.hasNext()) {
            selStore = getStoresDataModel().getObjectByKey(iterator.next());
            //   selStore = getStores().get((Integer)iterator.next());
            break;
        }
        openAccountManagedBean.edit(selStore);
        targetPage="openaccount";
        return refreshData();

    }

    public String createProfile() {        
        openAccountManagedBean.renew();
        targetPage= "openaccount";
        return refreshData();
    }

    public String refreshData() {
        selectionStore.clear();
        franchiseCaption = null;
        zipCode = null;
        showResult = false;
        return targetPage;
    }

    public ExtendedTableDataModel<StoreDictionary> getStoresDataModel() {
        if (dataModel == null) {
            dataModel = new ExtendedTableDataModel<StoreDictionary>(new DataProvider<StoreDictionary>() {

                private static final long serialVersionUID = 5054087821033164847L;


                public List<StoreDictionary> getItemsByRange(int firstRow, int endRow) {
                    return stores.subList(firstRow, endRow);
                }

                public Object getKey(StoreDictionary item) {
                    return item.getId();
                }

                public int getRowCount() {
                    return stores.size();
                }


                public StoreDictionary getItemByKey(Object arg0) {
                    for (StoreDictionary c : stores) {
                        if (arg0.equals(getKey(c))) {
                            return c;
                        }
                    }
                    return null;
                }
            });
        }
           
            return dataModel;
        }
        /*public void setStores(List<Store> Stores) {
        this.Stores = Stores;
        }*/
public  Object getTableState() {
        return tableState;
    }

    public void setTableState(Object tableState) {
        this.tableState = tableState;
    }
    /*public Selection getSelection() {
    return selection;
    }
    public void setSelection(Selection selection) {
    this.selection = selection;
    }

    public List<Store> getSelectedStores() {
    return selectedStores;
    }
    public void setSelectedStores(List<Store> selectedStores) {
    this.selectedStores = selectedStores;
    }*/
}
