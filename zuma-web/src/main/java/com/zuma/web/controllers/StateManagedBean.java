package com.zuma.web.controllers;

import com.zuma.db.core.domain.USState;
import com.zuma.db.service.impl.GenericServiceImpl;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

@Component
@Scope("application")
public class StateManagedBean {

    @Autowired
    @Qualifier("stateService")
    private GenericServiceImpl<USState, Integer> stateService;
    private Collection<USState> stateList;
    private Map<String, USState> stateMap = new TreeMap<String, USState>();
    private USState nil = null;

    @PostConstruct
    private void loadData() {
        stateList = stateService.getAll();
        for (USState state : stateList) {
            stateMap.put(state.getAbbreviation() + " - " + state.getCaption(), state);
        }
        stateMap.put("- Select One -", new USState(null,"null"));
    }

    public Collection<USState> getStateList() {
        return stateList;
    }

    public Map getStateItems() {
        return stateMap;
    }

    public void refresh() {
        loadData();
    }

    public USState getByValue(String value) {
        USState selectedState = null;
        for (USState state : stateList) {
            if (state.toString().equals(value)) {
                selectedState = state;
                break;
            }
        }
        return selectedState;
    }

    public USState getNil() {
        return nil;
    }
}
