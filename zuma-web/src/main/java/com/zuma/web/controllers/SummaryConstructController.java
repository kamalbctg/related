/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers;

import com.zuma.web.controllers.helper.SelectMap;
import com.zuma.db.core.domain.Company;
import com.zuma.db.core.domain.Contact;
import com.zuma.db.core.domain.Store;
import com.zuma.db.core.domain.SummaryCategory;
import com.zuma.db.core.domain.SummaryItem;
import com.zuma.db.core.domain.SummarySubCategory;
import com.zuma.db.core.domain.TreeNodeParent;
import com.zuma.web.controllers.helper.summary.SummaryCategoryData;
import com.zuma.web.controllers.helper.summary.SummaryDataSet;
import com.zuma.web.controllers.helper.summary.SummaryItemData;
import com.zuma.web.controllers.helper.summary.SummarySubCategoryData;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.faces.component.UIParameter;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author Roman
 */
@Component
@Scope("session")
public class SummaryConstructController extends SummaryParentController {

    private SummaryCategory selectedCategory;
    private SummarySubCategory selectedSubCategory;
    private SummaryItem selectedItem;
    Set objectForUpdate = new HashSet();
    Set objectForDelete = new HashSet();
    private Store storeToCopyFrom;
    private Company companyToCopyFrom;

    public SummaryConstructController() {
        periodBegin = Calendar.getInstance();
        // periodBegin.set(2009, 8, 7, 0, 0, 0);
        dataSet = new SummaryDataSet(periodBegin);

    }

    @PostConstruct
    public void refreshData() throws ParseException {
        if (getStore() != null) {
            loadData();
            objectForUpdate.clear();
            objectForDelete.clear();
            companyToCopyFrom = getStore().getCompany();
            storeToCopyFrom = getStore();
        }
    }

    public void recalculate(PhaseEvent ev) throws ParseException {
        if (ev.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            if (isReloadingNeeded()) {
                refreshData();
            }
            //  dataSet.recalculate();
        }

    }

    public void save() throws ParseException {

        objectForUpdate.removeAll(objectForDelete);
        dao.saveOrUpdateAll(objectForUpdate);
        dao.deleteAll(objectForDelete);
        objectForUpdate.clear();
        objectForDelete.clear();
        //getDataSet().save(dao);
        setNeedRefresh(true);
    }

    public void deleteCategory() throws ParseException {
        if (getSelectedCategory() != null) {
            String sql = "SELECT count(*) FROM SummaryData WHERE summaryItem.summarySubCategory.summaryCategory.id=" + String.valueOf(getSelectedCategory().getId());
            int dataCount = ((Long) dao.get(sql).iterator().next()).intValue();
            if (dataCount > 0) {
                getSelectedCategory().setDeleted(true);
                objectForUpdate.add(getSelectedCategory());

            } else {
                objectForDelete.add(getSelectedCategory());
            }
            getDataSet().remove(getSelectedCategory());
            setSelectedCategory(null);
        }
    }

    public SummaryCategory getSelectedCategory() {
        return selectedCategory;
    }

    public void insertOneCategory() throws ParseException {
        newCategory();
    }

    public void setSelectedCategory(SummaryCategory selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public SummaryCategory newCategory() {

        SummaryCategory category = new SummaryCategory();
        category.setStore(getStore());
        category.setCaption("Category_" + RandomStringUtils.randomAlphabetic(6));
        TreeMap map = (TreeMap) getDataSet().getDataMap();
        SummaryCategory last = null;
        if (!map.isEmpty()) {
            last = (SummaryCategory) (map).lastKey();
        }

        int sequenceNumber = 0;
        if (last != null) {
            sequenceNumber = last.getSequenceNumber() + 1;
        }
        category.setSequenceNumber(sequenceNumber);

        SummarySubCategory subCategory = new SummarySubCategory();
        subCategory.setCaption("Subcategory_" + RandomStringUtils.randomAlphabetic(6));
        subCategory.setSummaryCategory(category);
        subCategory.setSequenceNumber(0);
        category.getSummarySubCategoryList().add(subCategory);

        getDataSet().add(subCategory);
        //objectForUpdate.add(category);
        objectForUpdate.add(subCategory);


        return category;
    }

    public void deleteSubCategory() throws ParseException {

        if (getSelectedSubCategory() != null) {
            String sql = "SELECT count(*) FROM SummaryData WHERE summaryItem.summarySubCategory.id=" + String.valueOf(getSelectedSubCategory().getId());
            int dataCount = ((Long) dao.get(sql).iterator().next()).intValue();
            if (dataCount > 0) {
                getSelectedSubCategory().setDeleted(true);
                objectForUpdate.add(getSelectedSubCategory());

            } else {
                SummaryCategory cat = getSelectedSubCategory().getSummaryCategory();

                if (cat.getSummarySubCategoryList().contains(getSelectedSubCategory())) {
                    cat.getSummarySubCategoryList().remove(getSelectedSubCategory());
                }
                objectForDelete.add(getSelectedSubCategory());
            }

            getDataSet().remove(selectedSubCategory);

            setSelectedSubCategory(null);
        }

    }

    public void insertOneSubCategory() throws ParseException {
        if (selectedCategory != null) {

            SummarySubCategory newCategory = new SummarySubCategory();
            newCategory.setSummaryCategory(selectedCategory);
            newCategory.setCaption("Subcategory_" + RandomStringUtils.randomAlphabetic(6));
            SummaryCategoryData categoryData = getDataSet().getDataMap().get(selectedCategory);

            selectedCategory.getSummarySubCategoryList().add(newCategory);
            SummarySubCategory last = null;
            if (categoryData != null && !categoryData.getDataMap().isEmpty()) {
                last = (SummarySubCategory) ((TreeMap) categoryData.getDataMap()).lastKey();
            }

            int sequenceNumber = 0;
            if (last != null) {
                sequenceNumber = last.getSequenceNumber() + 1;
            }
            newCategory.setSequenceNumber(sequenceNumber);
            // selectedCategory.getSummarySubCategoryList().add(newCategory);

            getDataSet().add(newCategory);

            objectForUpdate.add(newCategory);


            selectedCategory = null;

        }

    }

    public void deleteItem() throws ParseException {

        if (getSelectedItem() != null) {
            String sql = "SELECT count(*) FROM SummaryData WHERE summaryItem.id=" + String.valueOf(getSelectedItem().getId());
            int dataCount = ((Long) dao.get(sql).iterator().next()).intValue();
            if (dataCount > 0) {
                getSelectedItem().setDeleted(true);
                objectForUpdate.add(getSelectedItem());

            } else {
                SummarySubCategory subCat = getSelectedItem().getSummarySubCategory();
                if (subCat.getSummaryItemList().contains(getSelectedItem())) {
                    subCat.getSummaryItemList().remove(getSelectedItem());
                }
                objectForDelete.add(getSelectedItem());
            }
            getDataSet().remove(getSelectedItem());
            setSelectedItem(null);

        }

    }

    public SummarySubCategory getSelectedSubCategory() {
        return selectedSubCategory;
    }

    public void setSelectedSubCategory(SummarySubCategory selectedSubCategory) {
        this.selectedSubCategory = selectedSubCategory;
    }

    public SummaryItem getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(SummaryItem selectedItem) {
        this.selectedItem = selectedItem;
    }

    public void insertOneItem() throws ParseException {
        if (selectedSubCategory != null) {

            SummaryItem newItem = new SummaryItem();
            newItem.setCaption("Item_" + RandomStringUtils.randomAlphabetic(6));
            newItem.setSummarySubCategory(selectedSubCategory);
//            newItem.setStore(getStore());
            TreeMap itemsMap = (TreeMap) getDataSet().getDataMap().
                    get(selectedSubCategory.getSummaryCategory()).getDataMap().get(selectedSubCategory).getDataMap();
            SummaryItem last = null;
            if (!itemsMap.isEmpty()) {
                last = (SummaryItem) itemsMap.lastKey();
            }

            int sequenceNumber = 0;
            if (last != null) {
                sequenceNumber = last.getSequenceNumber() + 1;
            }
            newItem.setSequenceNumber(sequenceNumber);
            selectedSubCategory.getSummaryItemList().add(newItem);
            objectForUpdate.add(newItem);
            getDataSet().add(newItem);

        }

    }

    public void putItemUp() throws ParseException {

        SummaryItem prev = (SummaryItem) ((TreeMap) getDataSet().getDataMap().
                get(selectedItem.getSummarySubCategory().getSummaryCategory()).getDataMap().
                get(selectedItem.getSummarySubCategory()).getDataMap()).lowerKey(selectedItem);

        if (prev != null) {
            SummaryItemData data = getDataSet().remove(selectedItem);
            int tmp = selectedItem.getSequenceNumber();
            selectedItem.setSequenceNumber(prev.getSequenceNumber());
            prev.setSequenceNumber(tmp);
            getDataSet().add(selectedItem, data);
            objectForUpdate.add(prev);
            objectForUpdate.add(selectedItem);
            selectedItem = null;
        }

    }

    public void putItemDown() throws ParseException {
        SummaryItem prev = (SummaryItem) ((TreeMap) getDataSet().getDataMap().
                get(selectedItem.getSummarySubCategory().getSummaryCategory()).getDataMap().
                get(selectedItem.getSummarySubCategory()).getDataMap()).higherKey(selectedItem);

        if (prev != null) {
            SummaryItemData data = getDataSet().remove(selectedItem);
            int tmp = selectedItem.getSequenceNumber();
            selectedItem.setSequenceNumber(prev.getSequenceNumber());
            prev.setSequenceNumber(tmp);


            getDataSet().add(selectedItem, data);
            objectForUpdate.add(prev);
            objectForUpdate.add(selectedItem);
            selectedItem = null;
        }

    }

    public void putSubCategoryUp() throws ParseException {

        SummarySubCategory prev = (SummarySubCategory) ((TreeMap) getDataSet().getDataMap().
                get(selectedSubCategory.getSummaryCategory()).getDataMap()).lowerKey(selectedSubCategory);

        if (prev != null) {
            SummarySubCategoryData data = getDataSet().remove(selectedSubCategory);
            int tmp = selectedSubCategory.getSequenceNumber();
            selectedSubCategory.setSequenceNumber(prev.getSequenceNumber());
            prev.setSequenceNumber(tmp);

            getDataSet().add(selectedSubCategory, data);
            objectForUpdate.add(prev);
            objectForUpdate.add(selectedSubCategory);
            selectedSubCategory = null;
        }

    }

    public void putSubCategoryDown() throws ParseException {
        SummarySubCategory prev = (SummarySubCategory) ((TreeMap) getDataSet().getDataMap().
                get(selectedSubCategory.getSummaryCategory()).getDataMap()).higherKey(selectedSubCategory);

        if (prev != null) {
            SummarySubCategoryData data = getDataSet().remove(selectedSubCategory);
            int tmp = selectedSubCategory.getSequenceNumber();
            selectedSubCategory.setSequenceNumber(prev.getSequenceNumber());
            prev.setSequenceNumber(tmp);

            getDataSet().add(selectedSubCategory, data);
            objectForUpdate.add(prev);
            objectForUpdate.add(selectedSubCategory);
            selectedSubCategory = null;
        }

    }

    public void processValueChange(ValueChangeEvent event)
            throws AbortProcessingException {
        if (!event.getNewValue().equals(event.getOldValue())) {
            Object temp = (((UIParameter) (event.getComponent()).getChildren().get(0)).getValue());
            objectForUpdate.add(temp);
        }
    }

    protected void copySummary(Store from) {
        String sql = "FROM SummaryCategory WHERE store.id=" + String.valueOf(getStore().getId());
        List<SummaryCategory> oldCategories = dao.get(sql);

        sql = "FROM SummaryCategory WHERE store.id=" + String.valueOf(getStoreToCopyFrom().getId());
        List<SummaryCategory> categories = dao.get(sql);
        List<SummaryCategory> newCategories = new ArrayList<SummaryCategory>();
        for (SummaryCategory cat : categories) {
            SummaryCategory oldCat = (SummaryCategory) findByName((Collection) oldCategories, cat);
            if (oldCat == null) {
                oldCat = cat.clone();
                oldCat.setStore(getStore());
                correctSequenceNumber(oldCat, (Collection)oldCategories);
            } else {
                Collection<SummarySubCategory> oldSubCats = oldCat.getSummarySubCategoryList();
                Collection<SummarySubCategory> subCats = cat.getSummarySubCategoryList();
                Collection<TreeNodeParent> resultSubCats = new ArrayList<TreeNodeParent>();
                for (SummarySubCategory subCat : subCats) {
                    if (subCat == null) {
                        continue;
                    }
                    SummarySubCategory oldSubCat = (SummarySubCategory) findByName((Collection) oldSubCats, subCat);
                    if (oldSubCat == null) {
                        oldSubCat = subCat.clone();
                        correctSequenceNumber(oldSubCat, resultSubCats);
                        resultSubCats.add(oldSubCat);

                        oldSubCat.setSummaryCategory(oldCat);
                        oldCat.getSummarySubCategoryList().add(oldSubCat);
                    } else {
                        Collection<SummaryItem> oldItems = oldSubCat.getSummaryItemList();
                        Collection<SummaryItem> items = subCat.getSummaryItemList();
                        Collection<TreeNodeParent> resultItems = new ArrayList<TreeNodeParent>();
                        resultItems.addAll(items);
                        for (SummaryItem item : items) {
                            if (item == null) {
                                continue;
                            }
                            SummaryItem oldItem = (SummaryItem) findByName((Collection) oldItems, item);
                            if (oldItem == null) {
                                oldItem = item.clone();
                                correctSequenceNumber(oldItem, resultItems);
                                resultItems.add(oldItem);
                                oldItem.setSummarySubCategory(oldSubCat);
                                oldSubCat.getSummaryItemList().add(oldItem);

                            }
                        }
                    }
                }

            }
            newCategories.add(oldCat);
        }
        List<SummaryItem> items = new ArrayList<SummaryItem>();
        for (SummaryCategory category : newCategories) {
            List<SummarySubCategory> subcategories = category.getSummarySubCategoryList();
            for (SummarySubCategory subCategory : subcategories) {
                if (subCategory == null) {
                    continue;
                }
                Collection<SummaryItem> itemsList = subCategory.getSummaryItemList();
                for (SummaryItem item : itemsList) {
                    if (item == null) {
                        continue;
                    }
                    items.add(item);

                }
            }
        }
        dao.saveOrUpdateAll(items);
        setNeedRefresh(true);
    }

    protected boolean correctSequenceNumber(TreeNodeParent obj, Collection<TreeNodeParent> coll) {
        for (TreeNodeParent tmp : coll) {
            if (tmp.getSequenceNumber() == obj.getSequenceNumber()) {
                Iterator it = coll.iterator();
                SummaryItem last = null;
                while (it.hasNext()) {
                    last = (SummaryItem) it.next();
                }
                if (last != null) {
                    obj.setSequenceNumber(last.getSequenceNumber() + 1);
                }
                return true;
            }
        }
        return false;
    }

    protected TreeNodeParent findByName(Collection<TreeNodeParent> categories, TreeNodeParent obj) {
        for (TreeNodeParent tmp : categories) {
            if (tmp == null) {
                continue;
            }
            if ((tmp.getCaption() == null && obj.getCaption() == null)
                    || tmp.getCaption().equals(obj.getCaption())) {
                return tmp;
            }
        }
        return null;
    }

    public String processDataCopying() {
        if (getStoreToCopyFrom() != null && !getStore().equals(getStoreToCopyFrom())) {
            copySummary(getStoreToCopyFrom());
        }
        return null;
    }

    public Company getCompanyToCopyFrom() {
        return companyToCopyFrom;
    }

    public void setCompanyToCopyFrom(Company companyToCopyFrom) {

        this.companyToCopyFrom = dao.findById(Company.class, companyToCopyFrom.getId());
        this.storeToCopyFrom = this.companyToCopyFrom.getStoreList().iterator().next();
    }

    public Store getStoreToCopyFrom() {
        return storeToCopyFrom;
    }

    public void setStoreToCopyFrom(Store storeToCopyFrom) {
        this.storeToCopyFrom = storeToCopyFrom;
    }

    public Map getStoresToCopyFrom() {
        Map<String, Store> choices = new SelectMap<Store>();
        if (companyToCopyFrom != null) {
            companyToCopyFrom = dao.findById(Company.class, companyToCopyFrom.getId());
            for (Store store : companyToCopyFrom.getStoreList()) {
                int size = store.getContactList().size();
                Contact storeContact = store.getContactList().get(0);
                String storeName = storeContact.getAddress();
                if (storeName == null || storeName.isEmpty()) {
                    storeName = store.getCaption();
                }
                choices.put(storeName, store);
            }
        }
        return choices;
    }
}
