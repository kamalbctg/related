/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers;

import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.SummaryData;
import com.zuma.db.core.domain.SummaryItem;
import com.zuma.db.service.util.Week;
import com.zuma.web.controllers.helper.summary.SummaryDataSet;
import com.zuma.web.util.Util;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
//import javax.el.ELContext;
//import javax.el.ExpressionFactory;
//import javax.el.ValueExpression;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;

import javax.faces.event.AbortProcessingException;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.swing.text.DateFormatter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class SummaryController extends SummaryParentController {

    List<SummaryData> changedObjects = new ArrayList<SummaryData>();
    List<SummaryData> objectsForRemoving = new ArrayList<SummaryData>();
    private String dateType = "Custom";
    private Boolean fromEnabled;
    private Date fromDate;
    private Date toDate;
    private Boolean toEnabled;
    private String columnFrequency;

    public SummaryController() {
        super();
    }

    @PostConstruct
    protected void init() throws ParseException {

        ReportingWeek week = userManagedBean.getActiveWeek();
        if (week != null) {

            periodBegin.setTime(getStartDate().getTime());
        }
        dataSet = new SummaryDataSet(periodBegin);
        setNeedRefresh(true);
        //loadData();
        fromEnabled = true;
        toEnabled = true;
    }

    public static <T> T evaluate(String value, Class<T> resultClass) {
        FacesContext context = FacesContext.getCurrentInstance();

        ELContext elContext = context.getELContext();
        ExpressionFactory expressionFactory = context.getApplication().getExpressionFactory();
        ValueExpression expression = expressionFactory.createValueExpression(
                elContext, value, resultClass);
        Object tmp = expression.getValue(elContext);
        T result = resultClass.cast(tmp);
        return result;
    }

    @Override
    public void loadData() throws ParseException {
        dataSet.clear();
        if (getStore() == null) {
            return;
        }
        dataSet.setStartDate((Calendar) getStartDate().clone());
        dataSet.setStore(getStore());
        /*String sql = "from SummaryCategory WHERE summarySubCategory.summaryItem.store.id="+
         String.valueOf(getStore().getId());
         List<SummaryCategory> categories;
         categories = dao.get(sql);
         for (SummaryCategory it : categories) {
         getDataSet().add(it);
         }

         sql = "from SummarySubCategory WHERE summaryItem.store.id="+
         String.valueOf(getStore().getId());

         List<SummarySubCategory> subCategories;
         subCategories = dao.get(sql);

         for (SummarySubCategory it : subCategories) {
         getDataSet().add(it);
         }
         */
        String sql = "from SummaryItem WHERE summarySubCategory.summaryCategory.store.id="
                + String.valueOf(getStore().getId()) + " AND deleted=false AND summarySubCategory.deleted=false AND summarySubCategory.summaryCategory.deleted=false";
        List<SummaryItem> items = dao.get(sql);
        for (SummaryItem it : items) {            
            getDataSet().add(it);
        }
        //Check Both DataSet are same
        System.out.println("Check Both DataSet are same" + (getDataSet() == dataSet));
       


        DateFormatter formatter = new DateFormatter(new SimpleDateFormat(DATE_FORMAT));
        sql = "FROM SummaryData WHERE date BETWEEN '"
                + formatter.valueToString(getStartDate().getTime()) + "' AND '"
                + formatter.valueToString(getFinishDate().getTime()) + "' AND summaryItem.summarySubCategory.summaryCategory.store.id="
                + String.valueOf(getStore().getId());
        List<SummaryData> data = dao.get(sql);
        for (Iterator<SummaryData> it = data.iterator(); it.hasNext();) {
            SummaryData summaryItem = it.next();
            dataSet.add(summaryItem);
        }

    }

    public void recalculate(PhaseEvent ev) throws ParseException {
        if (ev.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            Calendar calendar = Calendar.getInstance();
            DateFormatter format = new DateFormatter(new SimpleDateFormat("HH-mm-ss-SS"));

            if (isReloadingNeeded()) {
                Logger.getLogger(SummaryController.class.getName()).log(Level.INFO, "Start-" + format.valueToString(calendar.getTime()));
                loadData();
                calendar = Calendar.getInstance();
                Logger.getLogger(SummaryController.class.getName()).log(Level.INFO, "Data loaded at-" + format.valueToString(calendar.getTime()));
                changedObjects.clear();
                objectsForRemoving.clear();
            }
            try {
                dataSet.recalculate();
                calendar = Calendar.getInstance();
                Logger.getLogger(SummaryController.class.getName()).log(Level.INFO, "Data recalculated at-" + format.valueToString(calendar.getTime()));

            } catch (Exception ex) {
                Logger.getLogger(SummaryController.class.getName()).log(Level.SEVERE, null, ex);
                addFacesMessageForUI(ex.getMessage() + " Please contact us.");
            }
        }

    }

    public void save() throws ParseException {
        dao.mergeAll(changedObjects);
        changedObjects.clear();
        for (SummaryData summaryData : objectsForRemoving) {
            SummaryData data = new SummaryData(summaryData.getDate(), summaryData.getSummaryItem());
            getDataSet().add(data);
        }
        dao.deleteAll(objectsForRemoving);
        objectsForRemoving.clear();
        setNeedRefresh(true);
        //  dataSet.save(dao);

    }

    public void processValueChange(ValueChangeEvent event)
            throws AbortProcessingException {
        BigDecimal newValue = (BigDecimal) event.getNewValue();
        BigDecimal oldValue = (BigDecimal) event.getOldValue();
        if (oldValue.compareTo(newValue) != 0) {
            SummaryData temp = (SummaryData) (((UIParameter) (event.getComponent()).getChildren().get(0)).getValue());
            if (newValue != null) {
                if (newValue.doubleValue() != 0) {
                    changedObjects.add(temp);
                } else if (temp.getId() != null) {
                    objectsForRemoving.add(temp);
                }
            }
        }
    }

    public List<SelectItem> getDateTypes() {
        List<SelectItem> items = new LinkedList<SelectItem>();
        items.add(new SelectItem("Custom", "Custom"));
        items.add(new SelectItem("Week-to-date", "Week-to-date"));
        items.add(new SelectItem("Month-to-date", "Month-to-date"));
        items.add(new SelectItem("Year-to-date", "Year-to-date"));
        return items;
    }

    public List<SelectItem> getColumnFrequencies() {
        List<SelectItem> items = new LinkedList<SelectItem>();
        items.add(new SelectItem("Daily", "Daily"));
        items.add(new SelectItem("Weekly", "Weekly"));
        items.add(new SelectItem("Monthly", "Monthly"));
        return items;
    }

    public void displayData() {
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public Boolean getFromEnabled() {
        return fromEnabled;
    }

    public void setFromEnabled(Boolean fromEnabled) {
        this.fromEnabled = fromEnabled;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Boolean getToEnabled() {
        return toEnabled;
    }

    public void setToEnabled(Boolean toEnabled) {
        this.toEnabled = toEnabled;
    }

    public String getColumnFrequency() {
        return columnFrequency;
    }

    public void setColumnFrequency(String columnFrequency) {
        this.columnFrequency = columnFrequency;
    }
}
