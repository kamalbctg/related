/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers;

import com.zuma.db.core.domain.SummaryCategory;
import com.zuma.db.core.domain.SummaryItem;
import com.zuma.db.core.domain.SummarySubCategory;
import com.zuma.web.controllers.helper.summary.SummaryDataSet;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;

public abstract class SummaryParentController extends ParentController {

    protected SummaryDataSet dataSet;

    public void loadData() throws ParseException {
        dataSet.clear();
        dataSet.setStore(getStore());
        dao.clear();
        String sql = "from SummaryCategory WHERE deleted=false AND store.id="
                + String.valueOf(getStore().getId());
        List<SummaryCategory> categories;
        categories = dao.get(sql, false);
        for (SummaryCategory category : categories) {
            if (category.isDeleted()) {
                continue;
            }
            getDataSet().add(category);
            Collection<SummarySubCategory> subCategories = category.getSummarySubCategoryList();
            for (SummarySubCategory subCategory : subCategories) {
                if (subCategory == null || subCategory.isDeleted()) {
                    continue;
                }
                getDataSet().add(subCategory);
                Collection<SummaryItem> items = subCategory.getSummaryItemList();
                for (SummaryItem item : items) {
                    if (item!=null && !item.isDeleted()) {
                        getDataSet().add(item);
                    }
                }
            }
        }

        /*  sql = "from SummarySubCategory WHERE deleted=false AND summaryCategory.deleted=false and summaryCategory.store.id=" +
        String.valueOf(getStore().getId());

        List<SummarySubCategory> subCategories;
        subCategories = dao.get(sql,false);

        for (SummarySubCategory it : subCategories) {
        int size = it.getSummaryItemList().size();
        getDataSet().add(it);
        }

        sql = "from SummaryItem WHERE summarySubCategory.summaryCategory.store.id=" +
        String.valueOf(getStore().getId()) + " AND deleted=false AND summarySubCategory.deleted=false AND summarySubCategory.summaryCategory.deleted=false";
        List<SummaryItem> items = dao.get(sql,false);
        for (SummaryItem it : items) {
        getDataSet().add(it);
        }

         */
    }

    /**
     * @return the dataSet
     */
    public SummaryDataSet getDataSet() {
        return dataSet;
    }

    protected boolean isReloadingNeeded() {
        boolean flag = isNeedRefresh();
        if (flag) {
            setNeedRefresh(false);
        }
        return flag || !getStore().equals(getDataSet().getStore()) || !getStartDate().equals(getDataSet().getStartDate());
    }
}
