/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers;

import com.zuma.db.core.domain.EditableTrackerData;
import com.zuma.db.core.domain.TrackerData;
import com.zuma.db.service.util.Week;
import com.zuma.web.controllers.helper.analysis.DataLoader;
import com.zuma.web.controllers.helper.analysis.Period;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import javax.faces.component.UIParameter;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ValueChangeEvent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class TrackerController extends AnalysisParentController {
    String dateMessage = "Invalid date range. The date range must begin on a Monday and end on a Sunday to account for inventory balances in Food & Beverage and Paper calculations. Please try again.";
    private Collection<TrackerData> objForSaving = new HashSet<TrackerData>();
    private Collection<TrackerData> objForDeleting = new HashSet<TrackerData>();
    private List<EditableTrackerData> editableVarCosts = new LinkedList<EditableTrackerData>();
    private List<EditableTrackerData> editableFixedCosts = new LinkedList<EditableTrackerData>();
    private TrackerData margin = new TrackerData();
    private TrackerData profit = new TrackerData();
    private EditableTrackerData selectedVarData = null;
    private EditableTrackerData selectedFixData = null;
    private Period reviewPeriod = new Period();
    private Period comparisonPeriod = new Period();

    TrackerController() {
        super();

        Date from = Week.getWeekBegin((Calendar.getInstance().getTime()));
        Date to = Week.getWeekEnd((Calendar.getInstance().getTime()));
        reviewPeriod.setFrom(from);
        reviewPeriod.setTo(to);
        comparisonPeriod.setFrom(from);
        comparisonPeriod.setTo(to);

    }

    public void setSelectedFixData(EditableTrackerData selectedFixData) {
        this.selectedFixData = selectedFixData;
    }

    public void setSelectedVarData(EditableTrackerData selectedVarData) {
        this.selectedVarData = selectedVarData;
    }

    public EditableTrackerData getSelectedFixData() {
        return selectedFixData;
    }

    public EditableTrackerData getSelectedVarData() {
        return selectedVarData;
    }

    public TrackerData getMargin() {
        return margin;
    }

    public TrackerData getProfit() {
        return profit;
    }

    public List<EditableTrackerData> getEditableFixedCosts() {
        return editableFixedCosts;
    }

    public List<EditableTrackerData> getEditableVarCosts() {
        return editableVarCosts;
    }

    @Override
    protected void clear() {
        super.clear();
        editableVarCosts.clear();
        editableFixedCosts.clear();


        objForSaving.clear();
        objForDeleting.clear();
    }

    protected TrackerData calculateMargin(TrackerData net3) throws Exception {

        margin.setCaption("Contribution Margin");
        BigDecimal sum = BigDecimal.ZERO;
        for (String name : getNames()) {
            TrackerData data = getDataTargetMap().get(name);
            sum = sum.add(data.getCalculatedTarget());
        }
        margin.setCalculatedTarget(net3.getCalculatedTarget().subtract(sum));
        /* margin.setActualCurrent(calculateMarginValue(net3, 0));
        margin.setActualMTD(calculateMarginValue(net3, 1));
        margin.setActualYTD(calculateMarginValue(net3, 2));
        margin.setBaseCurrent(net3.getActualCurrent());
        margin.setBaseMTD(net3.getActualMTD());
        margin.setBaseYTD(net3.getActualYTD());
         */
        return margin;
    }

    protected BigDecimal calculateMarginValue(TrackerData net3, int index) throws Exception {
        BigDecimal tmp = net3.getActual(index);
        for (String name : getNames()) {
            TrackerData data = getDataTargetMap().get(name);
            if (data != null) {
                tmp = tmp.subtract(data.getActual(index));
            }
        }
        for (TrackerData data : editableVarCosts) {
            tmp = tmp.subtract(data.getActual(index));
        }
        return tmp;
    }

    protected TrackerData calculateProfit(TrackerData margin, TrackerData net3) throws Exception {
        profit.setCaption("Cash Available After Fixed Costs");
        BigDecimal tmp = margin.getCalculatedTarget();
        for (TrackerData data : editableFixedCosts) {
            tmp = tmp.subtract(data.getTarget());
        }
        profit.setCalculatedTarget(tmp);
        /*   profit.setActualCurrent(calculateProfitValue(margin, 0));
        profit.setActualMTD(calculateProfitValue(margin, 1));
        profit.setActualYTD(calculateProfitValue(margin, 2));
        profit.setBaseCurrent(net3.getActualCurrent());
        profit.setBaseMTD(net3.getActualMTD());
        profit.setBaseYTD(net3.getActualYTD());

         */
        return profit;
    }

    protected BigDecimal calculateProfitValue(TrackerData margin, int index) throws Exception {
        BigDecimal tmp = margin.getActual(index);
        for (TrackerData data : editableFixedCosts) {
            tmp = tmp.subtract(data.getActual(index));
        }
        return tmp;
    }

    @Override
    protected TrackerData prepareData(TrackerData data, DataLoader loader) throws ParseException {
        Calendar from = Calendar.getInstance();
        from.setTime(getReviewPeriod().getFrom());
        Calendar to = Calendar.getInstance();
        to.setTime(getReviewPeriod().getTo());
        data.setActualCurrent(loader.getData(from, to));

        from = Calendar.getInstance();
        from.setTime(getComparisonPeriod().getFrom());
        to = Calendar.getInstance();
        to.setTime(getComparisonPeriod().getTo());
        data.setActualMTD(loader.getData(from, to));
        //data.setActualYTD(loader.getYTD());
        return data;
    }

    protected void loadEditableData(boolean fixCostsFlag, List<EditableTrackerData> container, TrackerData base) {
        container.clear();
        String sql = "FROM EditableTrackerData WHERE fixedCost=" + String.valueOf(fixCostsFlag)
                + " AND store.id=" + getStore().getId().toString();
        List<EditableTrackerData> tmpList = dao.get(sql);
        if (tmpList == null || tmpList.isEmpty()) {
            EditableTrackerData data = new EditableTrackerData();
            data.setFixedCost(fixCostsFlag);
            data.setStore(getStore());
            container.add(data);
        } else {
            container.addAll(tmpList);
        }
        for (EditableTrackerData tmp : container) {
            tmp.setBaseCurrent(base.getActualCurrent());
            tmp.setBaseMTD(base.getActualMTD());
            tmp.setBaseYTD(base.getActualYTD());
        }
    }

    @Override
    protected void loadData() throws ParseException, Exception {
        super.loadData();
        TrackerData net3 = getDataMap().get(net3Caption);
        loadEditableData(true, editableFixedCosts, net3);
        loadEditableData(false, editableVarCosts, net3);
        TrackerData net2 = getDataMap().get(net2Caption);
        for (TrackerData data : editableVarCosts) {
            data.setCalculatedTarget(data.getTarget().multiply(net2.getTarget()).divide(new BigDecimal(100)));
        }
        TrackerData tmp = calculateMargin(net3);
        calculateProfit(tmp, net3);
    }

    public String addVarCostsData() {
        EditableTrackerData data = new EditableTrackerData();
        data.setFixedCost(false);
        data.setStore(getStore());
        editableVarCosts.add(data);
        TrackerData net3 = getDataMap().get(net3Caption);
        data.setBaseCurrent(net3.getActualCurrent());
        data.setBaseMTD(net3.getActualMTD());
        data.setBaseYTD(net3.getActualYTD());
        return null;
    }

    public String deleteVarData() {
        if (selectedVarData != null) {
            editableVarCosts.remove(selectedVarData);
            if (selectedVarData.getId() != null) {
                objForDeleting.add(selectedVarData);
            }
            if (editableVarCosts.isEmpty()) {
                addVarCostsData();
            }
        }
        selectedVarData = null;
        return null;
    }

    public String addFixCostsData() {
        EditableTrackerData data = new EditableTrackerData();
        data.setFixedCost(true);
        data.setStore(getStore());
        editableFixedCosts.add(data);
        TrackerData net3 = getDataMap().get(net3Caption);
        data.setBaseCurrent(net3.getActualCurrent());
        data.setBaseMTD(net3.getActualMTD());
        data.setBaseYTD(net3.getActualYTD());

        return null;
    }

    public String deleteFixCostsData() {
        if (selectedFixData != null) {
            editableFixedCosts.remove(selectedFixData);
            if (selectedFixData.getId() != null) {
                objForDeleting.add(selectedFixData);
            }
            if (editableFixedCosts.isEmpty()) {
                addFixCostsData();
            }
        }
        selectedFixData = null;
        return null;
    }

    public void processValueChange(ValueChangeEvent event)
            throws AbortProcessingException {
        if (!event.getNewValue().equals(event.getOldValue())) {
            TrackerData temp = (TrackerData) (((UIParameter) (event.getComponent()).getChildren().get(0)).getValue());
            objForSaving.add(temp);
        }
    }

    public String save() {
        objForSaving.removeAll(objForDeleting);
        dao.deleteAll(objForDeleting);
        dao.saveOrUpdateAll(objForSaving);
        objForDeleting.clear();
        objForSaving.clear();
        setNeedRefresh(true);
        return null;
    }

    public String acceptPeriod() {
        objForDeleting.clear();
        objForSaving.clear();

        if (comparisonPeriod.getFrom().after(comparisonPeriod.getTo())) {
            addFacesMessageForUI("Monday date should be less then Sunday date.");
        } else {
            setNeedRefresh(true);
        }
        return null;
    }

    public void processMondayChange(ValueChangeEvent event) {
        Calendar from = Calendar.getInstance();
        from.setTime((Date) event.getNewValue());
        if (from.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            addFacesMessageForUI(dateMessage);
        } else {
            if (from != null) {
                comparisonPeriod.setFrom(from.getTime());
            }
        }
    }

    public void processSundayChange(ValueChangeEvent event) {
        Calendar to = Calendar.getInstance();
        to.setTime((Date) event.getNewValue());
        if (to.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
            addFacesMessageForUI(dateMessage);
        } else {
            if (to != null) {
                comparisonPeriod.setTo(to.getTime());
            }
        }
    }

    public Period getReviewPeriod() {
        return reviewPeriod;
    }

    public void setReviewPeriod(Period reviewPeriod) {
        this.reviewPeriod = reviewPeriod;
    }

    public Period getComparisonPeriod() {
        return comparisonPeriod;
    }

    public void setComparisonPeriod(Period comparisonPeriod) {
        this.comparisonPeriod = comparisonPeriod;
    }
}
