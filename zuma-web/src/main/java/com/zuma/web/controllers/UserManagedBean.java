package com.zuma.web.controllers;

import com.zuma.db.core.domain.Contact;
import com.zuma.db.core.domain.PersonPage;
import com.zuma.db.service.CompanyService;
import com.zuma.db.service.ReportingWeekService;
import com.zuma.web.controllers.helper.TreeSelectMap;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import java.util.Locale;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.zuma.db.core.domain.Company;
import com.zuma.db.core.domain.Page;
import com.zuma.db.core.domain.Person;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import com.zuma.db.service.impl.GenericServiceImpl;
import com.zuma.db.service.util.Week;
import com.zuma.db.service.util.MD5;
import com.zuma.web.util.FacesUtils;


@Component
@Scope("session")
public class UserManagedBean implements Serializable {

    private String name = "";
    private String password = "";
    private boolean logon = false;
    private boolean admin = false; // for JSF
    private boolean loginAttempt = false;
    private HashMap<String, Page> securePages = new HashMap<String, Page>();
    private HashMap<String, Page> personPages = new HashMap<String, Page>();
    @Autowired
    @Qualifier("personService")
    private GenericServiceImpl<Person, Integer> personService;
    private Person activeUser;
    @Autowired
    private ReportingWeekService reportingWeekService;
    private ReportingWeek activeWeek;
    private Date activeWeekDay = Week.getCurrentWeekBegin();
    @Autowired
    @Qualifier("pageService")
    private GenericServiceImpl<Page, Integer> pageService;
    @Autowired
    @Qualifier("storeService")
    private GenericServiceImpl<Store, Integer> storeService;
    private Store activeStore;
    private List<ReportingWeek> weeks = null;
    @Autowired
    private CompanyService companyService;
    private Company activeAccount;

    public UserManagedBean() {
        // System.out.println(":::Created UserManagedBean"); // DEBUG
        Locale.setDefault(Locale.US);
    }

    @PostConstruct
    public void loadData() {
        loadSecurePages(pageService.getAll());
    }

    // == Simple user properties ====================
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLogon() {
        return logon;
    }

    public void setLogon(boolean logon) {
        this.logon = logon;
    }

    public boolean isLoginAttempt() {
        return loginAttempt;
    }

    public void setLoginAttempt(boolean loginAttempt) {
        this.loginAttempt = loginAttempt;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
    // == end user properties =======================

    // == login features ============================
    // It is login method - login()
    public String verifyUser() {
        // boolean attempt = loginAttempt;
        // logout();
        // loginAttempt = attempt;
        logon = false;
        List<Person> users = personService.getAll();
        String pass = MD5.encode(password);
        for (Iterator<Person> it = users.iterator(); it.hasNext();) {
            Person user = it.next();
            if (user.getLogin().equals(name) && user.getPassword().equalsIgnoreCase(MD5.encode(password))) {
                if (user.getRole() > 0) // if ==0, account disabled
                {
                    logon = true;
                    activeUser = user;
                    setAdmin(user.isAdmin());
                    loadSecurePages(pageService.getAll());
                    if (user.getRole() == 1) {
                        loadPersonPages(activeUser.getPersonPageList());
                    } else // for admin don't need to load separate pages, all accessible
                    {
                        personPages.clear();
                    }
                } else {
                    FacesUtils.addInfoMessage("You are registered but not activated in system!");
                    return null;
                }
                break;
            }
        }
        loginAttempt = true;

        if (getStoreList().size() == 0) {
            // user can not access to any store!
            logon = false;
        }
        return (logon) ? "success" : "failure";
    }

    public String logout() {
        name = "";
        password = "";

        logon = false;
        admin = false;
        loginAttempt = false;

        activeUser = null;
        activeAccount = null;
        activeStore = null;
        activeWeek = null;

        activeWeekDay = Week.getCurrentWeekBegin();
        if (weeks!=null) {
            weeks.clear();
            weeks = null;
        }
        if (personPages!=null) {
            personPages.clear();
        }

        return "logout";
    }

    public Person getActiveUser() {
        return activeUser;
    }

    public String getWelcomeMessage() {
        String welcomeStr = "";
        if (logon) {
            String firstStr = activeUser.getFirstName();
            String lastStr = activeUser.getLastName();
            String loginStr = activeUser.getLogin();

            if (firstStr.isEmpty() && lastStr.isEmpty()) {
                welcomeStr = loginStr;
            } else {
                welcomeStr = firstStr + " " + lastStr;
            }
        }
        return welcomeStr.trim();
    }
    // == end login features ========================

    // == page security =============================
    private Map<String, Page> loadSecurePages(Collection<Page> pages) {
        securePages.clear();
        for (Page page : pages) {
            securePages.put(page.getUrl(), page);
        }

        return securePages;
    }

    private Map<String, Page> loadPersonPages(Collection<PersonPage> pages) {
        personPages.clear();
        for (PersonPage page : pages) {
            Page item = page.getPage();
            personPages.put(item.getUrl(), item);
        }

        return personPages;
    }

    public Page getSecurePage(String pageUrl) {
        return securePages.get(pageUrl);
    }

    public Page getPersonPage(String pageUrl) {
        return personPages.get(pageUrl);
    }

    public boolean isAccessToPage(String pageUrl, boolean guestResult) {
        Page page = getSecurePage(pageUrl);

        if (isAdmin()) {
            if (page != null && page.getRole() == 3) {
                return false; // for System pages
            }
            return true;
        }

        if (isLogon()) {
            if (page != null) {
                switch (page.getRole()) {
                    case 3: // System page
                        return false;
                    case 2:
                        return false;
                    case 1:
                        return true;
                    case 0:
                        return (getPersonPage(pageUrl) != null);
                }
            }
        }

        return guestResult;
    }
    // == end page security =========================

    // == week combo ================================
    public Date getActiveWeekDay() {
        return activeWeekDay;
    }

    public void setActiveWeekDay(Date activeWeekDay) {
        this.activeWeekDay = activeWeekDay;
    }

    /*public static boolean isCurrentWeek(List<ReportingWeek> weeks) {
    Date currentWeekBegin = Week.getCurrentWeekBegin();
    for (ReportingWeek week : weeks) {
    Date weekBegin = Week.getWeekBegin(week.getWeekDate());
    if (Week.isEqualDate(currentWeekBegin, weekBegin)) {
    return true;
    }
    }
    return false;
    }
     */
    public void refreshReportingWeeks() {
        weeks = reportingWeekService.getAllUnlocked(getActiveStore());

        /*if (!isCurrentWeek(weeks)) // add new week to list!
        {
        //ReportingWeek current = new ReportingWeek(Week.getCurrentWeekBegin());
        // reportingWeekService.saveOrUpdate(current);
        // weeks = reportingWeekService.getAllUnlocked(getActiveStore());
        //weeks.add(current);
        //weekService.mergeAll(weeks);
        weeks = reportingWeekService.getAllUnlocked(getActiveStore());
        }*/
    }

    public List<SelectItem> getReportingWeeks() {
        refreshReportingWeeks();
        List<SelectItem> items = new LinkedList<SelectItem>();
        for (ReportingWeek week : weeks) {
            Date weekDate = week.getWeekDate();
            items.add(new SelectItem(weekDate, Week.toString(weekDate)));
        }
        return items;
    }

    public ReportingWeek getActiveWeek() {
        activeWeek = null;
        if (weeks == null || weeks.size() == 0) {
            refreshReportingWeeks();
        }

        for (ReportingWeek week : weeks) {
            Date weekDay = week.getWeekDate();
            if (Week.isEqualDate(weekDay, activeWeekDay)) {
                activeWeek = week;
                break;
            }
        }
        if (activeWeek == null) {
            Iterator<ReportingWeek> it = weeks.iterator();
            if (it.hasNext()) {
                activeWeek = it.next();
            }
        }
        return activeWeek;
    }

    public String getWeekString(ReportingWeek week) {
        Date rWeekDate = null;
        if (week != null) {
            rWeekDate = week.getWeekDate();
        }
        return Week.toString(rWeekDate);
    }

    public String getActiveWeekString() {
        return getWeekString(getActiveWeek());
    }
    // == end week combo ============================

    // == store combo ===============================
    public Collection<Store> getStoreList() {
        Collection<Store> storeList;
        if (activeUser == null) {
            return new LinkedList<Store>();
        }

        if (isAdmin()) {
            getActiveAccount();
            activeAccount = companyService.findById(activeAccount.getId());
            //activeAccount = companyService.merge(activeAccount);
            storeList = activeAccount.getStoreList(); //todo: only accepter stores
        } else {
            getActiveUser();
            activeUser = personService.findById(activeUser.getId());
            //activeUser = personService.merge(activeUser);
            storeList = activeUser.getStoreList(); //todo: only accepter stores
        }

        // removing of not activated stores from list
        Collection<Store> deleteList = new LinkedList<Store>();

        for (Store item : storeList) {
            if (!item.isAccepted()) {
                deleteList.add(item);
            }
        }
        storeList.removeAll(deleteList);

        return storeList;
    }

    // items for combo
    public Map getStoreItems() {
        Map<String, Store> choices = new TreeSelectMap<Store>();
        for (Store store : getStoreList()) {
            int size = 0;
            String storeName = "";
            if (store.getContactList() != null) {
                size = store.getContactList().size();
                if (size > 0) {
                    Contact storeContact = store.getContactList().get(0);
                    if (storeContact != null) {
                        storeName = storeContact.getAddress();
                    }
                }
            }
            if (storeName == null || storeName.isEmpty()) {
                storeName = (store.getCaption() != null) ? store.getCaption() : "";
            }
            choices.put(storeName, store);
        }

        return choices;
    }

    public Store getActiveStore() {
        if (activeStore == null) {
            // first store from list
            Collection<Store> storeList = getStoreList();
            if (!storeList.isEmpty()) {
                activeStore = storeList.iterator().next();
            }
        }
        // activeStore = storeService.merge(activeStore); // refresh Id
        return activeStore;
    }

    public void setActiveStore(Store store) {
        if (store == null) {
            return;
        }

        activeStore = store;
    }
    // == end store combo ===========================

    // == acount combo ==============================
    // companies list for current user
    public Collection<Company> getAccountList() {
        Collection<Company> companies = new LinkedList<Company>();
        if (activeUser == null) {
            return companies;
        }
        if (isAdmin()) {
            // TODO: Filter Verified or no Verified
            //companies = companyService.getExisting();
            companies = companyService.getFull();
        } else {
            // if user not a guest
            if (activeUser != null) {
                // prevent lazy exception
                activeUser = personService.findById(activeUser.getId());
                // activeUser = personService.merge(activeUser);
                companies.add(activeUser.getCompany());
            }
        }
        return companies;
    }

    // items for combo
    public Map getAccountItems() {
        Map<String, Company> choices = new TreeSelectMap<Company>();
        for (Company company : getAccountList()) {
            choices.put(company.getCaption(), company);
        }
        return choices;
    }

    public boolean hasCompanyThisStore(Store store) {
        if (store == null) {
            return false;
        }
        if (activeAccount == null) {
            return false;
        }
        //Store other = storeService.merge(store); // refresh Id
        Store other = storeService.findById(store.getId()); // refresh Id
        // List all stores for Company
        for (Store item : activeAccount.getStoreList()) {
            //item = storeService.merge(item); // refresh Id
            if (item.equals(other)) {
                return true;
            }
        }
        return false;
    }

    public Company getActiveAccount() {
        if (activeAccount == null) {
            Collection<Company> companies = getAccountList();
            // get first account from list
            if (!companies.isEmpty()) {
                activeAccount = companies.iterator().next();
            }
        }
        //activeAccount = companyService.merge(activeAccount); // refresh Id
        return activeAccount;
    }

    public void setActiveAccount(Company activeAccount) {
        if (activeAccount == null) {
            return; //todo: refresh store list
        }
        if (this.activeAccount == activeAccount) // nothing to do
        {
            return; //todo: refresh store list
        } else {
            this.activeAccount = activeAccount;
        }

        // set first store for new active account
        if (!hasCompanyThisStore(activeStore)) {
            Collection<Store> storeList = activeAccount.getStoreList();

            // removing of not activated stores from list
            for (Store item : storeList) {
                if (!item.isAccepted()) {
                    storeList.remove(item);
                }
            }

            // Collection<Store> storeList = getStoreList(); //todo: optimize
            if (!storeList.isEmpty()) {
                activeStore = storeList.iterator().next();
            }
        }
    }
    // == end acount combo ==========================

    // ACCOUNT = USER
    /*
    public List<Person> getAccountList() {
    List<Person> users = new LinkedList<Person>();
    if (isAdmin())
    users = personService.getAll();
    //    else
    //        users.add(activeUser);
    return users;
    }

    public Person getActiveAccount() {
    return activeAccount;
    }

    public void setActiveAccount(Person activeAccount) {
    this.activeAccount = activeAccount;
    }
     */
    // templates for converters
    public Collection<Person> getPersonList() {
        // return getAccountList(); // ACCOUNT == USER
        return new LinkedList<Person>();
    }

    public Collection<Company> getCompanyList() {
        return getAccountList(); // ACCOUNT == COMPANY
        // return new LinkedList<Company>();
    }

    public List<ReportingWeek> getWeeks() {
        return weeks;
    }

    public Company getCompanyOfActiveUser() {
        return getActiveUser().getCompany();
    }

    public boolean isWeekAvaible(Date date) {
        //refreshReportingWeeks();
        boolean result = false;
        date = Week.getWeekBegin(date);
        for (ReportingWeek week : weeks) {
            if (Week.getWeekBegin(week.getWeekDate()).equals(date)) {
                result = true;
                break;
            }
        }
        return result;
    }
}
