/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers;

import com.zuma.db.core.domain.InventoryItem;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.InventoryWeeklyRecord;
import com.zuma.db.core.domain.RetailWasteData;
import com.zuma.db.core.domain.RetailWasteItem;
import com.zuma.db.core.domain.WasteData;
import com.zuma.db.core.domain.WasteSalesPercentage;
import com.zuma.web.controllers.helper.waste.RetailWasteItemData;
import com.zuma.web.controllers.helper.waste.WasteDataSet;
import com.zuma.web.controllers.helper.waste.WasteItemData;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.component.UIParameter;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.ValueChangeEvent;
import javax.swing.text.DateFormatter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class WasteController extends ParentController {

    public final static int defaultRetailItemsCount = 5;
    Collection changedObjects = new LinkedHashSet();
    Collection retailChangedObjects = new LinkedHashSet();
    List objectsForRemoving = new ArrayList();
    List<RetailWasteItem> retailItemsForRemoving = new ArrayList<RetailWasteItem>();
    protected WasteDataSet dataSet;
    private RetailWasteItem selectedItem = null;

    @PostConstruct
    protected void init() throws ParseException {
        periodBegin = Calendar.getInstance();
        ReportingWeek week = userManagedBean.getActiveWeek();
        if (week != null) {
            periodBegin.setTime(week.getWeekDate());
        }
        dataSet = new WasteDataSet(periodBegin);
        setNeedRefresh(true);
        // loadData();
    }

    public void loadData() throws ParseException {
        dataSet.clear();
        if (getStore() == null) {
            return;
        }
        dataSet.setStore(getStore());
        dataSet.setStartDate((Calendar) getStartDate().clone());
        DateFormatter formatter = new DateFormatter(new SimpleDateFormat(DATE_FORMAT));
        String sql = "FROM InventoryWeeklyRecord WHERE reportingWeek.weekDate='" +
                formatter.valueToString(getStartDate().getTime()) +
                "' AND inventoryItem.inventorySubCategory.inventoryCategory.store.id=" +
                String.valueOf(getStore().getId()) +
                " AND inventoryItem.deleted=0 AND inventoryItem.inventorySubCategory.deleted=0 AND inventoryItem.inventorySubCategory.inventoryCategory.deleted = 0";
        List<InventoryWeeklyRecord> records = dao.get(sql);
        for (InventoryWeeklyRecord inventoryWeeklyRecord : records) {
            WasteItemData data = getDataSet().add(inventoryWeeklyRecord.getInventoryItem());
            data.setUnitCost(inventoryWeeklyRecord.getUnitCost());
            data.setQuantity(inventoryWeeklyRecord.getQuantity());
        }

        sql = "FROM InventoryItem WHERE inventorySubCategory.inventoryCategory.store.id=" +
                String.valueOf(getStore().getId()) +
                " AND deleted=0 AND inventorySubCategory.deleted=0 AND inventorySubCategory.inventoryCategory.deleted = 0";
        List<InventoryItem> items = dao.get(sql);
        for (InventoryItem it : items) {
            getDataSet().add(it);
        }

        sql = "FROM WasteData WHERE date BETWEEN '" +
                formatter.valueToString(getStartDate().getTime()) + "' AND '" +
                formatter.valueToString(getFinishDate().getTime()) +
                "' AND inventoryItem.inventorySubCategory.inventoryCategory.store.id=" +
                String.valueOf(getStore().getId()) +
                " AND inventoryItem.deleted=0 AND inventoryItem.inventorySubCategory.deleted=0 AND inventoryItem.inventorySubCategory.inventoryCategory.deleted = 0";
        List<WasteData> data = dao.get(sql);
        for (Iterator<WasteData> it = data.iterator(); it.hasNext();) {
            WasteData dataItem = it.next();
            dataSet.add(dataItem);

        }
        sql = "FROM RetailWasteItem WHERE date BETWEEN '" +
                formatter.valueToString(getStartDate().getTime()) + "' AND '" +
                formatter.valueToString(getFinishDate().getTime()) +
                "' AND store.id=" +
                String.valueOf(getStore().getId());
        List<RetailWasteItem> retailItems = dao.get(sql);
        for (RetailWasteItem item : retailItems) {
            dataSet.add(item);
        }
        sql = "FROM RetailWasteData WHERE date BETWEEN '" +
                formatter.valueToString(getStartDate().getTime()) + "' AND '" +
                formatter.valueToString(getFinishDate().getTime()) +
                "' AND item.store.id=" +
                String.valueOf(getStore().getId());
        List<RetailWasteData> retailData = dao.get(sql);
        for (RetailWasteData retail : retailData) {
            dataSet.add(retail);
        }

        int retailSize = dataSet.getRetailData().getDataMap().size();
        generateRetailItems(defaultRetailItemsCount - retailSize);

        sql = "FROM WasteSalesPercentage WHERE date=(SELECT max(date) FROM WasteSalesPercentage WHERE store.id =" +
                String.valueOf(getStore().getId()) + " AND date < '" +
                formatter.valueToString(getFinishDate().getTime()) + "')";
        List<WasteSalesPercentage> salesPercentageList = dao.get(sql);
        if (salesPercentageList != null && !salesPercentageList.isEmpty()) {
            WasteSalesPercentage loaded = salesPercentageList.iterator().next();
            if (!loaded.getDate().equals(getStartDate())) {//data from previous week
                WasteSalesPercentage newObject = new WasteSalesPercentage();
                newObject.setDate((Calendar) getStartDate().clone());
                newObject.setStore(loaded.getStore());
                newObject.setValue(loaded.getValue());
                dataSet.setSalesPercentage(newObject);
            } else {
                dataSet.setSalesPercentage(loaded);
            }
        }
    }

    protected void generateRetailItems(int count) {
        for (int i = 0; i < count; i++) {
            createNewWasteItem();
        }
    }

    public WasteDataSet getDataSet() {
        return dataSet;
    }

    public void recalculate(PhaseEvent ev) throws ParseException {
        if (ev.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            if (isReloadingNeeded()) {
                loadData();
                changedObjects.clear();
                objectsForRemoving.clear();
            }
            try {
                getDataSet().recalculate();
            } catch (Exception ex) {
                Logger.getLogger(WasteController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    protected void normalize() {
        Collection unnecessary = new ArrayList();
        for (Object o : changedObjects) {
            if (o instanceof RetailWasteData) {
                RetailWasteData tmp = (RetailWasteData) o;
                unnecessary.add(tmp.getItem());
            }
        }
        changedObjects.removeAll(unnecessary);
    }

    public void save() throws ParseException {
        changedObjects.removeAll(objectsForRemoving);
//        normalize();
        Collection items = new ArrayList();
        for (Object obj : retailChangedObjects) {
            if (obj instanceof RetailWasteData) {
                RetailWasteData data = (RetailWasteData) obj;
                if (data.getItem() != null && data.getItem().getId() == null) {
                    RetailWasteItem it = data.getItem();
                    items.add(it);
                    RetailWasteItem newIt = dao.merge(data.getItem());
                    for (Object tmp : retailChangedObjects) {
                        if (tmp instanceof RetailWasteData) {
                            RetailWasteData tmpData = (RetailWasteData) tmp;
                            if (tmpData.getItem().compareTo(it) == 0) {
                                tmpData.setItem(newIt);
                            }
                        }
                    }
                }
            }
        }
        ;
        dao.mergeAll(changedObjects);
        retailChangedObjects.removeAll(items);
        dao.saveOrUpdateAll(retailChangedObjects);
        changedObjects.clear();
        retailChangedObjects.clear();
        for (Object obj : objectsForRemoving) {
            if (obj instanceof WasteData) {
                WasteData data = (WasteData) obj;
                WasteData newdata = new WasteData(data.getDate(), data.getInventoryItem());
                getDataSet().add(newdata);
            } else if (obj instanceof RetailWasteData) {
                RetailWasteData data = (RetailWasteData) obj;
                RetailWasteData newdata = new RetailWasteData(data.getDate(), data.getItem());
                getDataSet().add(newdata);
            }

        }
        dao.mergeAll(objectsForRemoving);
        dao.deleteAll(objectsForRemoving);
        Collection<RetailWasteData> dataForRemoving = new ArrayList<RetailWasteData>();
        for (RetailWasteItem item : retailItemsForRemoving) {
            RetailWasteItemData itemData = getDataSet().getRetailData().getDataMap().get(item);
            if (itemData != null) {
                for (RetailWasteData retData : itemData.getDataValues()) {
                    if (retData.getId() != null) {
                        dataForRemoving.add(retData);
                    }
                }
            }
        }
        dao.deleteAll(dataForRemoving);
        for (RetailWasteItem item : retailItemsForRemoving) {
            RetailWasteItem newItem = dao.merge(item);
            deleteRetailData(item);
            dao.delete(newItem);
        }
        //dao.deleteAll(retailItemsForRemoving);
        retailItemsForRemoving.clear();
        objectsForRemoving.clear();
        setNeedRefresh(true);
        //  dataSet.save(dao);

    }

    protected void deleteRetailData(RetailWasteItem item) {
        if (item == null || item.getId() == null) {
            return;
        }
        String sql = "FROM RetailWasteData where item.id=" + item.getId().toString();
        Collection data = dao.get(sql);
        dao.deleteAll(data);

    }

    protected boolean isReloadingNeeded() {
        boolean flag = isNeedRefresh();
        if (flag) {
            setNeedRefresh(false);
        }
        return flag || !getStore().equals(getDataSet().getStore()) || !getStartDate().equals(getDataSet().getStartDate());
    }

    public void processValueChange(ValueChangeEvent event)
            throws AbortProcessingException {
        if (!event.getNewValue().equals(event.getOldValue())) {
            WasteData temp = (WasteData) (((UIParameter) (event.getComponent()).getChildren().get(0)).getValue());
            Object obj = event.getNewValue();

            BigDecimal newValue = convertToBigDecimal(obj);

            if (newValue != null && !newValue.equals(temp.getValue())) {
                if (newValue.doubleValue() != 0) {
                    changedObjects.add(temp);
                } else if (temp.getId() != null) {
                    objectsForRemoving.add(temp);
                }
            }
        }
    }

    public void processRetailValueChange(ValueChangeEvent event)
            throws AbortProcessingException {
        if (!event.getNewValue().equals(event.getOldValue())) {
            RetailWasteData temp = (RetailWasteData) (((UIParameter) (event.getComponent()).getChildren().get(0)).getValue());
            Object obj = event.getNewValue();

            BigDecimal newValue = convertToBigDecimal(obj);

            if (newValue != null && !newValue.equals(temp.getValue())) {
                if (newValue.doubleValue() != 0) {
                    retailChangedObjects.add(temp);
                } else if (temp.getId() != null) {
                    objectsForRemoving.add(temp);
                }
            }
        }
    }

    public void processItemChange(ValueChangeEvent event)
            throws AbortProcessingException {
        if (!event.getNewValue().equals(event.getOldValue())) {
            RetailWasteItem temp = (RetailWasteItem) (((UIParameter) (event.getComponent()).getChildren().get(0)).getValue());

            retailChangedObjects.add(temp);
        }
    }

    public void processSalesPercentageChange(ValueChangeEvent event)
            throws AbortProcessingException {
        if (!event.getNewValue().equals(event.getOldValue())) {
            WasteSalesPercentage temp = (WasteSalesPercentage) (((UIParameter) (event.getComponent()).getChildren().get(0)).getValue());
            Object obj = event.getNewValue();

            BigDecimal newValue = convertToBigDecimal(obj);

            if (newValue != null && !newValue.equals(temp.getValue())) {
                changedObjects.add(temp);
            }
        }
    }

    protected BigDecimal convertToBigDecimal(Object obj) {
        BigDecimal newValue = null;
        if (obj instanceof java.math.BigDecimal) {
            newValue = (BigDecimal) obj;
        } else if (obj instanceof java.lang.Long) {
            newValue = BigDecimal.valueOf((Long) obj);
        } else if (obj instanceof java.lang.Double) {
            newValue = BigDecimal.valueOf((Double) obj);
        }
        return newValue;
    }

    public void createNewWasteItem() {
        RetailWasteItem item = new RetailWasteItem(Calendar.getInstance());
        TreeMap map = (TreeMap) getDataSet().getRetailData().getDataMap();
        RetailWasteItem last = null;
        if (!map.isEmpty()) {
            last = (RetailWasteItem) (map).lastKey();
        }
        int sequenceNumber = 0;
        if (last != null) {
            sequenceNumber = last.getSequenceNumber() + 1;
        }
        item.setSequenceNumber(sequenceNumber);
        item.setStore(getStore());
        item.setDate((Calendar) periodBegin.clone());
        getDataSet().add(item);
        retailChangedObjects.add(item);
    }

    public void deleteWasteItem() {
        if (getSelectedItem() != null) {
            if (getSelectedItem().getId() != null) {
                retailItemsForRemoving.add(selectedItem);
                //RetailWasteItemData itemData = getDataSet().getRetailData().getDataMap().get(getSelectedItem());

                /*
                for (RetailWasteData it : itemData.getDataValues()) {
                if (it.getId() != null) {
                objectsForRemoving.add(it);
                }
                }

                objectsForRemoving.add(getSelectedItem());
                } else {
                changedObjects.remove(getSelectedItem());
                }*/
            }
            dataSet.remove(getSelectedItem());
            setSelectedItem(null);

        }
    }

    public RetailWasteItem getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(RetailWasteItem selectedItem) {
        this.selectedItem = selectedItem;
    }
}
