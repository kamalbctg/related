/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper;

import com.zuma.db.dao.UniversalDAO;
import com.zuma.web.reports.ObjectDataSource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import net.sf.jasperreports.engine.JRDataSource;

public class DataSet<T, V, TDATA> implements Serializable {

    private Calendar startDate;
    private Map<Long, Date> dates = new TreeMap<Long, Date>();
    private V parent;
    private Map<T, TDATA> dataMap = new TreeMap<T, TDATA>();
    private Map<String, TDATA> idDataMap = new TreeMap<String, TDATA>();
    private String patt = "##0.00%";

    public DataSet(Calendar startDate) {
        setStartDate(startDate);
    }

    public DataSet(Calendar startDate, V parent) {
        setStartDate(startDate);
        this.parent = parent;
    }

    public Map<T, TDATA> getDataMap() {
        return dataMap;
    }

    public Collection<T> getDataKeys() {
        return dataMap.keySet();
    }

    public Collection<TDATA> getDataValues() {

        Collection<TDATA> list = new ArrayList<TDATA>();
        for (Iterator<TDATA> it = dataMap.values().iterator(); it.hasNext();) {
            list.add(it.next());
        }
        return list;
    }

    public JRDataSource getDataSource() {
        return new ObjectDataSource(getDataValues());
    }

    public V getParent() {
        return parent;
    }

    public void setParent(V parent) {
        this.parent = parent;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
        reinitDates(startDate);
    }

    protected void reinitDates(Calendar startDate) {
        getDates().clear();

        if (startDate != null) {
            Calendar tmp = (Calendar) startDate.clone();
            for (long i = 0; i < 7; i++) {
                getDates().put(i, tmp.getTime());
                tmp.add(Calendar.DAY_OF_YEAR, 1);
            }
        }
    }

    public void clear() {
        dataMap.clear();
        parent = null;
    }

    public void save(UniversalDAO dao) {
        dao.saveOrUpdate(parent);
    }

    public Map<String, TDATA> getIdDataMap() {
        return idDataMap;
    }

    public void add(T ibj, String str, TDATA data) {
        dataMap.put(ibj, data);
        idDataMap.put(str, data);
    }

    public void recalculate() throws Exception {
    }

    public String getPattern() {
        String pattern = "$##0.00";
        return pattern;
    }

    /**
     * @return the patt
     */
    public String getPatt() {
        return patt;
    }

    /**
     * @param patt the patt to set
     */
    public void setPatt(String patt) {
        this.patt = patt;
    }

    public Map<Long, Date> getDates() {
        return dates;
    }
}
