/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper;

import java.util.HashMap;

public class SelectMap<V> extends HashMap<String, V> {

    @Override
    public V put(String key, V value) {
        if (containsKey(key)) {
            putAndCheck(key, 1, value);
        } else {
            super.put(key, value);
        }
        return value;
    }

    protected V putAndCheck(String key, int index, V value) {

        String newKey = key + " [" + String.valueOf(index) + "]";
        if (containsKey(newKey)) {
            putAndCheck(key, index + 1, value);
        } else {
            super.put(newKey, value);
        }
        return value;

    }
}
