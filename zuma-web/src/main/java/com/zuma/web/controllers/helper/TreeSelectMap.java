/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper;

import java.util.TreeMap;

public class TreeSelectMap<V> extends TreeMap<String, V>{

    @Override
    public V put(String key, V value) {
        if(key == null)
            key = new String();
        if (containsKey(key)) {
            putAndCheck(key, 1, value);
        } else {
            super.put(key, value);
        }
        return value;
    }

    protected V putAndCheck(String key, int index, V value) {

        String newKey = key + " [" + String.valueOf(index) + "]";
        if (containsKey(newKey)) {
            putAndCheck(key, index + 1, value);
        } else {
            super.put(newKey, value);
        }
        return value;

    }
}
