/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.analysis;

import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.util.Calendar;

public class AdvertizingFeesLoader extends RoyaltyFeesLoader {

    public AdvertizingFeesLoader(UniversalDAO dao, Store store, Calendar from, Calendar to) {
        super(dao, store, from, to);
        multiplier = new BigDecimal(0.04);
    }
}
