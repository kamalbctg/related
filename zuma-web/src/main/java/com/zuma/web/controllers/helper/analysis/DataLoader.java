/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.analysis;

import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.UniversalDAO;
import com.zuma.db.service.util.Week;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;

public abstract class DataLoader {

    public final static String DATE_FORMAT = "yyyy-MM-dd";
    protected UniversalDAO dao;
    protected Store store;
    protected Calendar from;
    protected Calendar to;
    protected String category;

    public DataLoader(UniversalDAO dao, Store store, Calendar from, Calendar to) {
        this.dao = dao;
        this.store = store;
        this.from = from;
        this.to = to;
    }

    public static Calendar getDayBegin(Calendar calendar) {
        calendar = Week.nullTime(calendar);
        return calendar;
    }

    public static Calendar getWeekBegin(Calendar calendar) {
        calendar.setTime(Week.getWeekBegin(calendar.getTime()));
        return calendar;
    }

    public static Calendar getWeekEnd(Calendar calendar) {
        calendar.setTime(Week.getWeekEnd(calendar.getTime()));
        return calendar;
    }

    public static Calendar getMonthBegin(Calendar calendar) {
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return getDayBegin(calendar);
    }

    public static Calendar getMonthEnd(Calendar calendar) {
        int lastDate = calendar.getActualMaximum(Calendar.DATE);
        calendar.set(Calendar.DATE, lastDate);
        return getDayBegin(calendar);
    }

    public static Calendar getYearBegin(Calendar calendar) {
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        return getDayBegin(calendar);
    }

    public abstract BigDecimal getTotalFor(String category, Store store, Calendar from, Calendar to) throws ParseException;

    public BigDecimal getCurrent() throws ParseException {
        BigDecimal result = getTotalFor(category, store, from, to);
        return result;
    }

    public BigDecimal getMTD() throws ParseException {
        Calendar today = getWeekEnd((Calendar) to.clone());
        Calendar monthBegin;
        if (from.get(Calendar.MONTH) != to.get(Calendar.MONTH)) {
            monthBegin = getMonthBegin((Calendar) to.clone());
        } else {
            monthBegin = getMonthBegin((Calendar) from.clone());
        }
        monthBegin = getWeekBegin(monthBegin);
        BigDecimal result = getTotalFor(category, store, monthBegin, today);
        return result;
    }

    public BigDecimal getYTD() throws ParseException {
        Calendar today = getWeekEnd((Calendar) to.clone());
         Calendar monthBegin;
        if (from.get(Calendar.YEAR) != to.get(Calendar.YEAR)) {
            monthBegin = getYearBegin((Calendar) to.clone());
        } else {
            monthBegin = getYearBegin((Calendar) from.clone());
        }
        //Calendar monthBegin = getYearBegin((Calendar) from.clone());
        monthBegin = getWeekBegin(monthBegin);
        BigDecimal result = getTotalFor(category, store, monthBegin, today);
        return result;
    }

    public BigDecimal getData(Calendar from, Calendar to) throws ParseException {
        BigDecimal result = getTotalFor(category, store, from, to);
        return result;

    }
}
