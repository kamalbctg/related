/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.analysis;

import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.UniversalDAO;
import com.zuma.db.service.util.Week;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.text.DateFormatter;

public class FBLoader extends InventoryLoader {

    protected String summaryPurchaseCategory = "Adjustments";
    protected String summaryPurchaseItem = "Food Purchases";
    protected String purchaseField = "F&B";

    public FBLoader(UniversalDAO dao, Store store, Calendar from, Calendar to) {
        super(dao, store, from, to);
        category = "Food Items";
    }

    protected BigDecimal getItemTotal(String category, String item, Store store, Calendar from, Calendar to) throws ParseException {
        DateFormatter formatter = new DateFormatter(new SimpleDateFormat(DATE_FORMAT));
        String sql = "SELECT Sum(value) FROM SummaryData WHERE date BETWEEN '"
                + formatter.valueToString(from.getTime()) + "' AND '"
                + formatter.valueToString(to.getTime())
                + "' AND summaryItem.summarySubCategory.summaryCategory.store.id=" + String.valueOf(store.getId())
                + " AND (summaryItem.summarySubCategory.caption like '" + category
                + "' OR summaryItem.summarySubCategory.editedName like '" + category + "')"
                + " AND (summaryItem.caption like '" + item
                + "' OR summaryItem.editedCaption like '" + item + "')";
        return dao.getScalar(sql);
    }

    public BigDecimal getPurchaseTotal(Store store, Calendar from, Calendar to) throws ParseException {
        DateFormatter formatter = new DateFormatter(new SimpleDateFormat(DATE_FORMAT));
        String sql = "SELECT Sum(price) FROM InvoiceDetail WHERE invoice.date BETWEEN '"
                + formatter.valueToString(from.getTime()) + "' AND '"
                + formatter.valueToString(to.getTime())
                + "' AND invoice.store.id=" + String.valueOf(store.getId())
                + " AND invoiceAccount.type like '"+purchaseField+"'";
        BigDecimal result = BigDecimal.ZERO;
        BigDecimal purchase = dao.getScalar(sql);
        BigDecimal summaryPurchase = getItemTotal(summaryPurchaseCategory, summaryPurchaseItem, store, from, to);
        if (purchase != null) {
            result = result.add(purchase);
        }
        if (summaryPurchase != null) {
            result = result.add(summaryPurchase);
        }
        return result;

    }

    public BigDecimal getBeginInventory(Calendar to) throws ParseException {
        Calendar prevWeek = (Calendar) to.clone();
        // prevWeek.add(Calendar.DAY_OF_YEAR, -7);
        prevWeek.setTime(Week.getWeekBegin(prevWeek.getTime()));

        BigDecimal beginInv = super.getTotalFor(category, store, prevWeek, null);
        return beginInv;
    }

    public BigDecimal getEndInventory(Calendar to) throws ParseException {
        Calendar prevWeek = (Calendar) to.clone();
        prevWeek.setTime(Week.getWeekBegin(prevWeek.getTime()));
        BigDecimal endInv = super.getTotalFor(category, store, prevWeek, null);
        return endInv;
    }

    public BigDecimal getWaste(Calendar from, Calendar to) throws ParseException {
        WasteLoader wasteLoader = new WasteLoader(dao, store, from, to);
        BigDecimal waste = wasteLoader.getTotalFor(category, store, from, to);
        return waste;
    }

    @Override
    public BigDecimal getTotalFor(String category, Store store, Calendar from, Calendar to) throws ParseException {

        Calendar date = (Calendar) from.clone();
        date.add(Calendar.DAY_OF_YEAR, -7);

        BigDecimal beginInv = getBeginInventory(date);
        BigDecimal endInv = getEndInventory(to);
        BigDecimal waste = getWaste(from, to);


        BigDecimal purchase = getPurchaseTotal(store, from, to);


        BigDecimal result = BigDecimal.ZERO;
        if (beginInv != null) {
            result = result.add(beginInv);
        }
        if (purchase != null) {
            result = result.add(purchase);
        }
        if (waste != null) {
            result = result.subtract(waste);
        }
        if (endInv != null) {
            result = result.subtract(endInv);
        }

        return result;
    }
}
