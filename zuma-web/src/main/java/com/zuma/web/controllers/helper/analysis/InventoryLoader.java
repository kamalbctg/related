/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.analysis;

import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.UniversalDAO;
import com.zuma.db.service.util.Week;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.text.DateFormatter;

public class InventoryLoader extends DataLoader {

    public InventoryLoader(UniversalDAO dao, Store store, Calendar from, Calendar to) {
        super(dao, store, from, to);
    }

    @Override
    public BigDecimal getTotalFor(String category, Store store, Calendar from, Calendar to) throws ParseException {
        Date week = Week.getWeekBegin(from.getTime());
        Calendar requestDate = Calendar.getInstance();
        requestDate.setTime(store.getRequestDate());
        requestDate.add(Calendar.DAY_OF_YEAR, -7);
        DateFormatter formatter = new DateFormatter(new SimpleDateFormat(DATE_FORMAT));
        String sql;
        BigDecimal result = null;
        if (from.before(requestDate)) {
            if (category.contains("Paper")) {
                sql = "SELECT paperValue FROM InventoryStartData WHERE store.id=" + String.valueOf(store.getId());
            } else {
                sql = "SELECT foodValue FROM InventoryStartData WHERE store.id=" + String.valueOf(store.getId());
            }
            result = dao.getScalar(sql);
        }

        sql = "select sum(unitCost*quantity) from InventoryWeeklyRecord where inventoryItem.inventorySubCategory.inventoryCategory.categoryName like '"
                + category
                + "' and reportingWeek.weekDate = (select max(weekDate) from ReportingWeek WHERE weekDate <='"
                + formatter.valueToString(week)
                + "') AND inventoryItem.inventorySubCategory.inventoryCategory.store.id=" + String.valueOf(store.getId());
        BigDecimal tmp = dao.getScalar(sql);
        if(result!=null)
        {
            if(tmp!=null)
            {
                result = result.add(tmp);
            }
        }else
            result = tmp;
        return result;
    }
}
