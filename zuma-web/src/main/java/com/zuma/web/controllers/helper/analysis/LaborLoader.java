/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.analysis;

import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;

public class LaborLoader extends SummaryLoader {

    public LaborLoader(UniversalDAO dao, Store store, Calendar from, Calendar to) {
        super(dao, store, from, to);
        category = "Labor Cost";
    }
    public BigDecimal getLaborHours(Store store, Calendar from, Calendar to) throws ParseException
    {
        String laborCategory = "Labor Hours";
        BigDecimal result = getTotalFor(laborCategory, store, from, to);
        return result;
    }
}
