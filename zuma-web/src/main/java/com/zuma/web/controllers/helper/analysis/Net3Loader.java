/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.analysis;

import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;

public class Net3Loader extends DiscountsLoader {

    public Net3Loader(UniversalDAO dao, Store store, Calendar from, Calendar to) {
        super(dao, store, from, to);
    }

    @Override
    public BigDecimal getTotalFor(String category, Store store, Calendar from, Calendar to) throws ParseException {
        SalesLoader sales = new SalesLoader(dao, store, from, to);
        BigDecimal net2 = sales.getTotalFor("", store, from, to);
        BigDecimal discounts = super.getTotalFor("Discounts", store, from, to);

        BigDecimal result = null;
        if (net2 != null) {
            result = net2;
        }
        if (discounts != null) {
            if (result != null) {
                result = result.subtract(discounts);
            } else {
                result = discounts.negate();
            }
        }
        return result;
    }
}
