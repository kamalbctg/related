/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.analysis;

import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;

public class PaperLoader extends FBLoader {

    public PaperLoader(UniversalDAO dao, Store store, Calendar from, Calendar to) {
        super(dao, store, from, to);
        category = "Paper Items";
        summaryPurchaseItem = "Paper Purchases";
        purchaseField = "PAPER";
    }

    @Override
    public BigDecimal getTotalFor(String category, Store store, Calendar from, Calendar to) throws ParseException {
        Calendar date = (Calendar) from.clone();
        date.add(Calendar.DAY_OF_YEAR, -7);
        
        BigDecimal beginInv = getBeginInventory(date);
        BigDecimal endInv = getEndInventory(to);
        //BigDecimal waste = getWaste(from, to);

        BigDecimal purchase = getPurchaseTotal(store, from, to);


        BigDecimal result = BigDecimal.ZERO;
        if (beginInv != null) {
            result = result.add(beginInv);
        }
        
        if (purchase != null) {
            result = result.add(purchase);
        }
       /* if (waste != null) {
            result = result.subtract(waste);
        }
        */
        if (endInv != null) {
            result = result.subtract(endInv);
        }

        return result;
    }
}
