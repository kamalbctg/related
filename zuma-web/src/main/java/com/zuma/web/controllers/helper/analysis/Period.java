/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zuma.web.controllers.helper.analysis;

import java.util.Date;


public class Period {
    private Date from;
    private Date to;

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }
}
