/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.analysis;

import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;

public class RoyaltyFeesLoader extends Net3Loader {

    protected BigDecimal multiplier = new BigDecimal(0.07);

    public RoyaltyFeesLoader(UniversalDAO dao, Store store, Calendar from, Calendar to) {
        super(dao, store, from, to);
    }

    @Override
    public BigDecimal getTotalFor(String category, Store store, Calendar from, Calendar to) throws ParseException {
        BigDecimal result = super.getTotalFor(category, store, from, to);
        if(result!=null)
            result = result.multiply(multiplier);
        return result;
    }
}
