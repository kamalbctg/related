/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.analysis;

import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;

public class SalesLoader extends DiscountsLoader {

    public SalesLoader(UniversalDAO dao, Store store, Calendar from, Calendar to) {
        super(dao, store, from, to);
    }

    @Override
    public BigDecimal getTotalFor(String category, Store store, Calendar from, Calendar to) throws ParseException {
        BigDecimal net1 = super.getTotalFor("Sales", store, from, to);
        BigDecimal voids = super.getTotalFor("Voids", store, from, to);

        BigDecimal result = null;
        if (net1 != null) {
            result = net1;
        }
        if (voids != null) {
            if (result != null) {
                result = result.subtract(voids);
            } else {
                result = voids.negate();
            }
        }
        return result;
    }

  
}
