/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.analysis;

import com.zuma.db.core.domain.Store;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.text.DateFormatter;

public abstract class SummaryLoader extends DataLoader {
    
    SummaryLoader(UniversalDAO dao, Store store, Calendar from, Calendar to) {
        super(dao, store, from, to);
    }
    
    @Override
    public BigDecimal getTotalFor(String category, Store store, Calendar from, Calendar to) throws ParseException {
        DateFormatter formatter = new DateFormatter(new SimpleDateFormat(DATE_FORMAT));
        String sql = "SELECT Sum(value) FROM SummaryData WHERE date BETWEEN '" +
                formatter.valueToString(from.getTime()) + "' AND '" +
                formatter.valueToString(to.getTime()) +
                "' AND summaryItem.summarySubCategory.summaryCategory.store.id=" + String.valueOf(store.getId()) +
                " AND (summaryItem.summarySubCategory.caption like '" + category +
                "' OR summaryItem.summarySubCategory.editedName like '" + category + "')";
        return dao.getScalar(sql);
    }
}
