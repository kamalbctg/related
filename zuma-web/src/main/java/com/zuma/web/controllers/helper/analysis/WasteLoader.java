/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.analysis;

import com.zuma.db.core.domain.InventoryWeeklyRecord;
import com.zuma.db.core.domain.Store;
import com.zuma.db.core.domain.WasteData;
import com.zuma.db.core.domain.WasteSalesPercentage;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import javax.swing.text.DateFormatter;

public class WasteLoader extends DiscountsLoader {

    public WasteLoader(UniversalDAO dao, Store store, Calendar from, Calendar to) {
        super(dao, store, from, to);
    }

    @Override
    public BigDecimal getTotalFor(String category, Store store, Calendar from, Calendar to) throws ParseException {
        DateFormatter formatter = new DateFormatter(new SimpleDateFormat(DATE_FORMAT));
        String sql = "FROM WasteData WHERE date BETWEEN '" +
                formatter.valueToString(from.getTime()) + "' AND '" +
                formatter.valueToString(to.getTime()) +
                "' AND inventoryItem.inventorySubCategory.inventoryCategory.store.id=" + String.valueOf(store.getId());

        List<WasteData> wasteList = dao.get(sql);

        sql = "FROM InventoryWeeklyRecord WHERE reportingWeek.weekDate BETWEEN '" +
                formatter.valueToString(from.getTime()) + "' AND '" +
                formatter.valueToString(to.getTime()) +
                "' AND inventoryItem.inventorySubCategory.inventoryCategory.store.id=" + String.valueOf(store.getId());
        List<InventoryWeeklyRecord> weeklyRecords = dao.get(sql);
        BigDecimal result = BigDecimal.ZERO;
        for (InventoryWeeklyRecord record : weeklyRecords) {

            Collection<WasteData> delList = new ArrayList<WasteData>();
            Calendar start = Calendar.getInstance();
            start.setTime(record.getReportingWeek().getWeekDate());

            Calendar finish = (Calendar) start.clone();
            finish.add(Calendar.DATE, 6);
            for (WasteData data : wasteList) {
                if (record.getInventoryItem().getId().equals(data.getInventoryItem().getId()) &&
                    data.getDate().compareTo(start)>=0 && data.getDate().compareTo(finish)<=0) {
                    delList.add(data);

                    BigDecimal tmp = data.getValue().multiply(record.getUnitCost());
                    result = result.add(tmp);
                }
            }
            wasteList.removeAll(delList);
        }

        //BigDecimal result = dao.getScalar(sql);
        sql = "SELECT Sum(value * item.unitCost) FROM RetailWasteData WHERE date BETWEEN '" +
                formatter.valueToString(from.getTime()) + "' AND '" +
                formatter.valueToString(to.getTime()) +
                "' AND item.store.id=" + String.valueOf(store.getId());
        BigDecimal tmp = dao.getScalar(sql);

        sql = "FROM WasteSalesPercentage WHERE date=(SELECT max(date) FROM WasteSalesPercentage WHERE store.id =" +
                String.valueOf(store.getId()) + " AND date < '" +
                formatter.valueToString(to.getTime()) + "')";
        WasteSalesPercentage percent = dao.getObject(sql, WasteSalesPercentage.class);

        if (result != null) {
            if (tmp != null) {
                if(percent!=null)
                {
                    tmp = tmp.multiply(percent.getValue()).divide(BigDecimal.valueOf(100));
                }
                else
                    tmp = BigDecimal.ZERO;
                result = result.add(tmp);
            }
        } else {
            result = tmp;
        }
        return result;
    }
}
