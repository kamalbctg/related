/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.summary;

import com.zuma.db.core.domain.SummaryData;
import com.zuma.db.core.domain.SummaryItem;
import com.zuma.web.controllers.SummaryController;
import com.zuma.web.converters.UIPatternConverter;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Iterator;
import java.util.TreeMap;
import org.apache.log4j.Logger;

public class CalculatedSummaryItemData extends SummaryItemData {

    private BigDecimal total = BigDecimal.ZERO;

    public CalculatedSummaryItemData(Calendar startDate) {
        super(startDate);
    }

    public CalculatedSummaryItemData(Calendar startDate, SummaryItem item) {
        super(startDate, item);
    }

    @Override
    public void recalculate() throws Exception {

        //"${summaryManagedBean.dataSet.getSubcategoryData(1).getValue($day$)/2}";
        String formula = getParent().getFormula();

        for (Iterator<Long> it = getDataMap().keySet().iterator(); it.hasNext();) {

            Long day = it.next();
            Calendar calendar = (Calendar) getDataMap().get(day).getDate().clone();

            int dayNumber = calendar.get(Calendar.DAY_OF_WEEK);
            String tmp = formula.replace("$day$", String.valueOf(dayNumber));
            BigDecimal value = new BigDecimal(0);
            try {
                value = UIPatternConverter.convert((Number) SummaryController.evaluate(tmp, Object.class));
            } catch (NumberFormatException e) {
                value = handleException(day, e);
            } catch (ArithmeticException e) {
                value = handleException(day, e);
            } catch (Exception e) {
                Logger log = Logger.getRootLogger();
                log.info("Expression evaluating exception", e);
                getDataMap().get(day).setValue(null);
                getIdDataMap().get(day.toString()).setValue(null);
                throw new Exception("Formula error detected in row '" + getParent().getEditedCaption() + "'. ");
            }
            if (getParent().isPrevDaysDepended() && !getParent().isAllowsException()) {
                TreeMap<Long, SummaryData> tempMap = (TreeMap<Long, SummaryData>) getDataMap();
                Long prevDay = tempMap.lowerKey(day);

                if (prevDay != null) {
                    value = value.add(getDataMap().get(prevDay).getValue());
                }

            }
            getDataMap().get(day).setValue(value);
            getIdDataMap().get(day.toString()).setValue(value);
        }
        recalculateTotal(formula);
    }

    BigDecimal recalculateTotal(String formula) {
        String tmp = formula.replace("getValue($day$)", "getTotalValue()");
        try {
            total = UIPatternConverter.convert((Number) SummaryController.evaluate(tmp, Object.class));
        } catch (NumberFormatException e) {
            total = null;
        } catch (ArithmeticException e) {
            total = null;
        } catch (Exception e) {
            Logger log = Logger.getRootLogger();
            log.info("Expression evaluating exception", e);
        }
        return total;
    }

    BigDecimal handleException(Long day, Exception e) throws Exception {
        if (getParent().isAllowsException()) {
            return null;
        } else {
            Logger log = Logger.getRootLogger();
            log.info("Expression evaluating exception", e);
            getDataMap().get(day).setValue(null);
            getIdDataMap().get(day.toString()).setValue(null);
            throw new Exception("Formula error detected in row '" + getParent().getEditedCaption() + "'. ");
        }
    }

    @Override
    public BigDecimal getTotalValue() {
        BigDecimal result = BigDecimal.ZERO;
        if (getParent().getPattern().contains("%") ||
            getParent().getFormula().contains("/")) {
            result = total;
        } else {
            if (getParent().isPrevDaysDepended()) {
                result = ((TreeMap<Long, SummaryData>) getDataMap()).lastEntry().getValue().getValue();
            } else {
                result = super.getTotalValue();
            }
        }
        return result;
    }
}
