/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.summary;

import com.zuma.web.controllers.helper.DataSet;
import com.zuma.db.core.domain.SummaryCategory;
import com.zuma.db.core.domain.SummaryData;
import com.zuma.db.core.domain.SummaryItem;
import com.zuma.db.core.domain.SummarySubCategory;
import com.zuma.db.dao.UniversalDAO;
import java.util.Calendar;
import java.util.Iterator;

public class SummaryCategoryData extends DataSet<SummarySubCategory, SummaryCategory, SummarySubCategoryData> {

    protected static boolean searchById = false;

    public SummaryCategoryData(Calendar startDate) {
        super(startDate);
    }

    public SummaryCategoryData(Calendar startDate, SummaryCategory category) {
        super(startDate, category);
    }

    public SummaryItemData add(SummaryData data) {
        SummarySubCategory summarySubCategory = data.getSummaryItem().getSummarySubCategory();
        if (getParent() != null && !getParent().equals(summarySubCategory.getSummaryCategory())) {
            return null;
        }
        SummarySubCategoryData subCategoryData = getDataMap().get(summarySubCategory);
        if (subCategoryData == null) {
            subCategoryData = new SummarySubCategoryData(getStartDate(), summarySubCategory);
            add(summarySubCategory, subCategoryData);
        }
        return subCategoryData.add(data);
    }

    public SummaryItemData add(SummaryItem summaryItem) {
        SummarySubCategory summarySubCategory = summaryItem.getSummarySubCategory();
        if (getParent() != null && !getParent().equals(summarySubCategory.getSummaryCategory())) {
            return null;
        }
        SummarySubCategoryData subCategoryData = getDataMap().get(summarySubCategory);
        if (subCategoryData == null) {
            subCategoryData = new SummarySubCategoryData(getStartDate(), summarySubCategory);
            add(summarySubCategory, subCategoryData);
        }
        return subCategoryData.add(summaryItem);
    }

    public SummaryItemData add(SummaryItem summaryItem, SummaryItemData data) {
        SummarySubCategory summarySubCategory = summaryItem.getSummarySubCategory();
        if (getParent() != null && !getParent().equals(summarySubCategory.getSummaryCategory())) {
            return null;
        }
        SummarySubCategoryData subCategoryData = getDataMap().get(summarySubCategory);
        if (subCategoryData == null) {
            subCategoryData = new SummarySubCategoryData(getStartDate(), summarySubCategory);
            add(summarySubCategory, subCategoryData);
        }
        return subCategoryData.add(summaryItem, data);

    }

    public SummaryItemData remove(SummaryItem item) {
        SummarySubCategory summarySubCategory = item.getSummarySubCategory();
        SummarySubCategoryData subCategoryData = getDataMap().get(summarySubCategory);
        if (subCategoryData != null) {
            return subCategoryData.remove(item);
        }
        return null;
    }

    public SummarySubCategoryData add(SummarySubCategory summarySubCategory) {
        SummaryCategory summaryCategory = summarySubCategory.getSummaryCategory();
        if (getParent() != null &&!getParent().equals(summaryCategory)) {
            return null;
        }

        SummarySubCategoryData subCategoryData = getDataMap().get(summarySubCategory);
        if (subCategoryData == null) {
            subCategoryData = new SummarySubCategoryData(getStartDate(), summarySubCategory);
            add(summarySubCategory, subCategoryData);
        }

        return subCategoryData;
    }

    public SummarySubCategoryData remove(SummarySubCategory item) {
        SummarySubCategoryData subCategoryData = getDataMap().remove(item);
        if (subCategoryData != null) {
            if (searchById && item.getId() != null) {
                getIdDataMap().remove(item.getId().toString());
            } else {
                getIdDataMap().remove(item.getCaption());
            }

        }
        return subCategoryData;
    }

    public SummarySubCategoryData add(SummarySubCategory obj, SummarySubCategoryData data) {
        getDataMap().put(obj, data);
        if (searchById) {
            getIdDataMap().put(obj.getId().toString(), data);
        } else {
            getIdDataMap().put(obj.getCaption(), data);
        }
        return data;
    }

    @Override
    public void save(UniversalDAO dao) {
        super.save(dao);
        for (Iterator<SummarySubCategoryData> it = getDataMap().values().iterator(); it.hasNext();) {
            SummarySubCategoryData summarySubCategoryData = it.next();
            summarySubCategoryData.save(dao);

        }

    }

    @Override
    public void recalculate() throws Exception {
        for (SummarySubCategoryData it : getDataMap().values()) {
            it.recalculate();
        }
    }
}
