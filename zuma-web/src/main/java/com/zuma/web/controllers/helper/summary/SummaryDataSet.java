/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.summary;

import com.zuma.db.core.domain.Store;
import com.zuma.web.controllers.helper.DataSet;
import com.zuma.db.core.domain.SummaryCategory;
import com.zuma.db.core.domain.SummaryData;
import com.zuma.db.core.domain.SummaryItem;
import com.zuma.db.core.domain.SummarySubCategory;
import com.zuma.db.dao.UniversalDAO;
import com.zuma.web.util.Util;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class SummaryDataSet extends DataSet<SummaryCategory, Object, SummaryCategoryData> {

    private Store store;
    Collection<SummaryCategory> removedCategories = new ArrayList<SummaryCategory>();

    public SummaryDataSet(Calendar startDate) {
        super(startDate);
    }

    public SummaryItemData add(SummaryData data) {

        SummaryCategory summaryCategory = data.getSummaryItem().getSummarySubCategory().getSummaryCategory();

        SummaryCategoryData categoryData = getDataMap().get(summaryCategory);
        if (categoryData == null) {
            categoryData = new SummaryCategoryData(getStartDate(), summaryCategory);
            add(summaryCategory, summaryCategory.getId().toString(), categoryData);
        }
        return categoryData.add(data);
    }

    public SummaryItemData add(SummaryItem summaryItem) {
        SummaryCategory summaryCategory = summaryItem.getSummarySubCategory().getSummaryCategory();

        

        SummaryCategoryData categoryData = getDataMap().get(summaryCategory);
        if (categoryData == null) {
            categoryData = new SummaryCategoryData(getStartDate(), summaryCategory);
            add(summaryCategory, summaryCategory.getId().toString(), categoryData);
        }
        
        return categoryData.add(summaryItem);
    }

    public SummaryItemData add(SummaryItem summaryItem, SummaryItemData data) {
        SummaryCategory summaryCategory = summaryItem.getSummarySubCategory().getSummaryCategory();
       
        SummaryCategoryData categoryData = getDataMap().get(summaryCategory);
        if (categoryData == null) {
            categoryData = new SummaryCategoryData(getStartDate(), summaryCategory);
            add(summaryCategory, summaryCategory.getId().toString(), categoryData);
        }
       
        return categoryData.add(summaryItem, data);
    }

    public SummaryItemData remove(SummaryItem item) {
        SummaryCategory summaryCategory = item.getSummarySubCategory().getSummaryCategory();

        SummaryCategoryData categoryData = getDataMap().get(summaryCategory);
        if (categoryData != null) {
            return categoryData.remove(item);
        }
        return null;
    }

    public SummarySubCategoryData add(SummarySubCategory summarySubCategory) {
        SummaryCategory summaryCategory = summarySubCategory.getSummaryCategory();
        SummaryCategoryData categoryData = getDataMap().get(summaryCategory);
        if (categoryData == null) {
            categoryData = new SummaryCategoryData(getStartDate(), summaryCategory);
            if (summaryCategory.getId() != null) {
                add(summaryCategory, summaryCategory.getId().toString(), categoryData);
            } else {
                getDataMap().put(summaryCategory, categoryData);
            }
        }
        return categoryData.add(summarySubCategory);
    }

    public SummarySubCategoryData add(SummarySubCategory summarySubCategory, SummarySubCategoryData data) {
        SummaryCategory summaryCategory = summarySubCategory.getSummaryCategory();
        SummaryCategoryData categoryData = getDataMap().get(summaryCategory);
        if (categoryData == null) {
            categoryData = new SummaryCategoryData(getStartDate(), summaryCategory);
            if (summaryCategory.getId() != null) {
                add(summaryCategory, summaryCategory.getId().toString(), categoryData);
            } else {
                getDataMap().put(summaryCategory, categoryData);
            }
        }
        return categoryData.add(summarySubCategory, data);
    }

    public SummarySubCategoryData remove(SummarySubCategory item) {
        SummaryCategory summaryCategory = item.getSummaryCategory();

        SummaryCategoryData categoryData = getDataMap().get(summaryCategory);
        if (categoryData != null) {
            return categoryData.remove(item);
        }
        return null;
    }

    public SummaryCategoryData add(SummaryCategory summaryCategory) {
        SummaryCategoryData categoryData = getDataMap().get(summaryCategory);
        if (categoryData == null) {
            categoryData = new SummaryCategoryData(getStartDate(), summaryCategory);
            add(summaryCategory, summaryCategory.getId().toString(), categoryData);
        }
        return categoryData;
    }

    public void remove(SummaryCategory item) {
        SummaryCategoryData categoryData = getDataMap().remove(item);
        if (categoryData != null && item.getId() != null) {
            getIdDataMap().remove(item.getId().toString());
        }
    }

    @Override
    public void save(UniversalDAO dao) {
        for (Iterator<SummaryCategoryData> it = getDataMap().values().iterator(); it.hasNext();) {
            SummaryCategoryData summaryCategoryData = it.next();
            summaryCategoryData.save(dao);
        }
    }

    public SummarySubCategoryData getSubCategoryData(String name) {
        SummarySubCategoryData result = null;
        for (SummaryCategoryData categoryData : getDataMap().values()) {
            result = categoryData.getIdDataMap().get(name);
            if (result != null) {
                break;
            }
        }
        return result;
    }

    public SummarySubCategoryData getSubCategoryDataById(int id) {
//        SummarySubCategoryData result = null;
        for (SummaryCategoryData categoryData : getDataMap().values()) {
            for (SummarySubCategoryData subCat : categoryData.getDataMap().values()) {
                if (subCat.getParent().getId().equals(id)) {
                    return subCat;
                }
            }
        }
        return null;
    }

    public SummaryItemData getItemData(String subCategory, String item) {
        SummaryItemData result = null;
        Map<SummaryItem, SummaryItemData> dataMap = getSubCategoryData(subCategory).getDataMap();
        for (Entry<SummaryItem, SummaryItemData> entry : dataMap.entrySet()) {
            if (entry.getKey().getCaption().equals(item)
                    || entry.getKey().getEditedCaption().equals(item)) {
                result = entry.getValue();
                break;
            }
        }
        return result;
    }

    public SummaryItemData getItemDataById(int subCategory, int item) {
        SummaryItemData result = null;
        Map<SummaryItem, SummaryItemData> dataMap = getSubCategoryDataById(subCategory).getDataMap();
        for (Entry<SummaryItem, SummaryItemData> entry : dataMap.entrySet()) {
            if (entry.getKey().getId().equals(item)) {
                result = entry.getValue();
                break;
            }
        }

        return result;
    }

    @Override
    public void recalculate() throws Exception {
        for (SummaryCategoryData it : getDataMap().values()) {
            it.recalculate();
        }
    }

    /**
     * @return the store
     */
    public Store getStore() {
        return store;
    }

    /**
     * @param store the store to set
     */
    public void setStore(Store store) {
        this.store = store;
    }
}
