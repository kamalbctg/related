/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.summary;

import com.zuma.web.controllers.helper.DataSet;
import com.zuma.db.core.domain.SummaryData;
import com.zuma.db.core.domain.SummaryItem;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Iterator;

public class SummaryItemData extends DataSet<Long, SummaryItem, SummaryData> {

    private void init() {
        Calendar tmp = Calendar.getInstance();
        tmp.setTime(getStartDate().getTime());

        add(milisToDays(tmp.getTimeInMillis()), new SummaryData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(milisToDays(tmp.getTimeInMillis()), new SummaryData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(milisToDays(tmp.getTimeInMillis()), new SummaryData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(milisToDays(tmp.getTimeInMillis()), new SummaryData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(milisToDays(tmp.getTimeInMillis()), new SummaryData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(milisToDays(tmp.getTimeInMillis()), new SummaryData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(milisToDays(tmp.getTimeInMillis()), new SummaryData(tmp, getParent()));

    }

    public SummaryItemData(Calendar startDate) {
        super(startDate);
        init();
    }

    public SummaryItemData(Calendar startDate, SummaryItem item) {
        super(startDate, item);
        init();
    }

    protected void add(long day, SummaryData data) {
        getDataMap().put(Long.valueOf(day), data);
        getIdDataMap().put(String.valueOf(day), data);
    }

    public boolean add(SummaryData data) {

        if (getParent() != null && getParent() != data.getSummaryItem()) {
            return false;
        }
        setParent(data.getSummaryItem());
        add(milisToDays(data.getDate().getTimeInMillis()), data);
        return true;
    }

    @Override
    public void setParent(SummaryItem item) {
        super.setParent(item);
        for (Iterator<SummaryData> it = getDataMap().values().iterator(); it.hasNext();) {
            it.next().setSummaryItem(item);
        }
    }

    public BigDecimal getTotalValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (Iterator<SummaryData> it = getDataMap().values().iterator(); it.hasNext();) {
            SummaryData summaryData = it.next();
            BigDecimal tmp = summaryData.getValue();
            if (tmp != null) {
                result = result.add(tmp);
            }
        }
        return result;
    }

    public String getTotalValueText() {
        BigDecimal value = getTotalValue();
        return getParent().convertValueToText(value);
    }

    public SummaryData getData(int day) {
        Calendar tmp = (Calendar) getStartDate().clone();
        if (day > 1) {
            day -= 1;
        } else {
            day = 7;    //SUNDAY should be last day in week
        }
        tmp.add(Calendar.DAY_OF_WEEK, day - 1);
        Long time = milisToDays(tmp.getTimeInMillis());
        return getDataMap().get(time);
    }

    public BigDecimal getDataValue(int day) {
        return getData(day).getValue();
    }

    public BigDecimal getValue(int day) {
        return getDataValue(day);
    }

    public SummaryData getMondayData() {
        return getData(Calendar.MONDAY);
    }

    public SummaryData getTuesdayData() {
        return getData(Calendar.TUESDAY);
    }

    public SummaryData getWednesdayData() {
        return getData(Calendar.WEDNESDAY);
    }

    public SummaryData getThursdayData() {
        return getData(Calendar.THURSDAY);
    }

    public SummaryData getFridayData() {
        return getData(Calendar.FRIDAY);
    }

    public SummaryData getSaturdayData() {
        return getData(Calendar.SATURDAY);
    }

    public SummaryData getSundayData() {
        return getData(Calendar.SUNDAY);
    }

    public BigDecimal getMondayValue() {

        return getMondayData().getValue();
    }

    public BigDecimal getTuesdayValue() {
        return getTuesdayData().getValue();
    }

    public BigDecimal getWednesdayValue() {
        return getWednesdayData().getValue();
    }

    public BigDecimal getThursdayValue() {
        return getThursdayData().getValue();
    }

    public BigDecimal getFridayValue() {
        return getFridayData().getValue();
    }

    public BigDecimal getSaturdayValue() {
        return getSaturdayData().getValue();
    }

    public BigDecimal getSundayValue() {
        return getSundayData().getValue();
    }

    @Override
    public void save(UniversalDAO dao) {
        super.save(dao);
        /*List<SummaryData> delList = new ArrayList<SummaryData>();
        List<SummaryData> updateList = new ArrayList<SummaryData>();
        if (!getParent().isCalculated()) {
        for (Iterator<SummaryData> it = getDataMap().values().iterator(); it.hasNext();) {
        SummaryData summaryData = it.next();
        if (summaryData.getValue().doubleValue() != 0) {
        updateList.add(summaryData);
        //dao.saveOrUpdate(summaryData);
        } else if (summaryData.getId() != null) {
        SummaryData data = new SummaryData(summaryData.getDate(), summaryData.getSummaryItem());
        getParent().getSummaryDataList().remove(summaryData);
        getParent().getSummaryDataList().add(data);
        add(data);
        //delList.add(summaryData);
        }

        }
        }
        dao.saveOrUpdateAll(updateList);
        dao.saveOrUpdate(getParent());
        /*
        for (SummaryData elem : delList) {
        SummaryData data = new SummaryData(elem.getDate(), elem.getSummaryItem());
        dao.delete(elem);
        add(data);
        }
         */
    }

    long milisToDays(long milis) {
        return milis / 86400000;
    }
}
