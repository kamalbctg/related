/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.summary;

import com.zuma.web.controllers.helper.DataSet;
import com.zuma.db.core.domain.SummaryData;
import com.zuma.db.core.domain.SummaryItem;
import com.zuma.db.core.domain.SummarySubCategory;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.util.Calendar;

public class SummarySubCategoryData extends DataSet<SummaryItem, SummarySubCategory, SummaryItemData> {

    public SummarySubCategoryData(Calendar startDate) {
        super(startDate);
    }

    public SummarySubCategoryData(Calendar startDate, SummarySubCategory subCategory) {
        super(startDate, subCategory);
    }

    protected SummaryItemData createSummaryItemData(SummaryItem summaryItem) {
        if (summaryItem.isCalculated()) {
            return new CalculatedSummaryItemData(getStartDate(), summaryItem);
        } else {
            return new SummaryItemData(getStartDate(), summaryItem);
        }
    }

    public SummaryItemData add(SummaryData data) {
        if (getParent() != null && !getParent().equals(data.getSummaryItem().getSummarySubCategory())) {
            return null;
        }
        SummaryItem summaryItem = data.getSummaryItem();
        SummaryItemData summaryItemData = getDataMap().get(summaryItem);
        if (summaryItemData == null) {
            summaryItemData = createSummaryItemData(summaryItem);
            add(summaryItem, summaryItem.getId().toString(), summaryItemData);
        }
        summaryItemData.add(data);
        return summaryItemData;
    }

    public SummaryItemData add(SummaryItem summaryItem) {
        if (getParent() != null && !getParent().equals(summaryItem.getSummarySubCategory())) {
            return null;
        }

        SummaryItemData summaryItemData = getDataMap().get(summaryItem);
        if (summaryItemData == null) {
            summaryItemData = createSummaryItemData(summaryItem);
            if (summaryItem.getId() != null) // just created item can be  without id
            {
                add(summaryItem, summaryItem.getId().toString(), summaryItemData);
            } else {
                getDataMap().put(summaryItem, summaryItemData);

            }
        }
        return summaryItemData;
    }

    public SummaryItemData add(SummaryItem summaryItem, SummaryItemData data) {
        if (getParent() != null && !getParent().equals(summaryItem.getSummarySubCategory())) {
            return null;
        }

        SummaryItemData summaryItemData = data;

        if (summaryItem.getId() != null) // just created item can be  without id
        {
            add(summaryItem, summaryItem.getId().toString(), summaryItemData);
        } else {
            getDataMap().put(summaryItem, summaryItemData);
        }

        return summaryItemData;
    }

    public SummaryItemData remove(SummaryItem item) {
        SummaryItemData summaryItemData = getDataMap().get(item);
        getDataMap().remove(item);
        if (summaryItemData != null && item.getId() != null) {
            getIdDataMap().remove(item.getId().toString());
        }
        return summaryItemData;
    }

    public BigDecimal getValue(int day) {
        BigDecimal result = BigDecimal.ZERO;
        for (SummaryItemData it : getDataMap().values()) {
            result = result.add(it.getDataValue(day));
        }
        return result;

    }

    public BigDecimal getTotalValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (SummaryItemData it : getDataMap().values()) {
            result = result.add(it.getTotalValue());
        }
        return result;

    }

    public BigDecimal getMondayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (SummaryItemData it : getDataMap().values()) {
            result = result.add(getMondayValue());
        }
        return result;
    }

    public BigDecimal getTuesdayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (SummaryItemData it : getDataMap().values()) {
            result = result.add(getTuesdayValue());
        }
        return result;
    }

    public BigDecimal getWednesdayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (SummaryItemData it : getDataMap().values()) {
            result = result.add(getWednesdayValue());
        }
        return result;
    }

    public BigDecimal getThursdayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (SummaryItemData it : getDataMap().values()) {
            result = result.add(getThursdayValue());
        }
        return result;
    }

    public BigDecimal getFridayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (SummaryItemData it : getDataMap().values()) {
            result = result.add(getFridayValue());
        }
        return result;
    }

    public BigDecimal getSaturdayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (SummaryItemData it : getDataMap().values()) {
            result = result.add(getSaturdayValue());
        }
        return result;
    }

    public BigDecimal getSundayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (SummaryItemData it : getDataMap().values()) {
            result = result.add(getSundayValue());
        }
        return result;
    }

    @Override
    public void save(UniversalDAO dao) {
        super.save(dao);
        for (SummaryItemData it : getDataMap().values()) {
            SummaryItemData summaryItemData = it;
            summaryItemData.save(dao);

        }
    }

    @Override
    public void recalculate() throws Exception {
        for (SummaryItemData it : getDataMap().values()) {
            it.recalculate();
        }
    }
}
