/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zuma.web.controllers.helper.waste;

import com.zuma.db.core.domain.RetailWasteData;
import com.zuma.db.core.domain.RetailWasteItem;
import com.zuma.web.controllers.helper.DataSet;
import java.math.BigDecimal;
import java.util.Calendar;


public class RetailWasteItemData extends DataSet<Long, RetailWasteItem, RetailWasteData> {

    private void init() {
        Calendar tmp = Calendar.getInstance();
        tmp.setTime(getStartDate().getTime());

        add(WasteItemData.milisToDays(tmp.getTimeInMillis()), new RetailWasteData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(WasteItemData.milisToDays(tmp.getTimeInMillis()), new RetailWasteData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(WasteItemData.milisToDays(tmp.getTimeInMillis()), new RetailWasteData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(WasteItemData.milisToDays(tmp.getTimeInMillis()), new RetailWasteData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(WasteItemData.milisToDays(tmp.getTimeInMillis()), new RetailWasteData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(WasteItemData.milisToDays(tmp.getTimeInMillis()), new RetailWasteData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(WasteItemData.milisToDays(tmp.getTimeInMillis()), new RetailWasteData(tmp, getParent()));

    }

    public RetailWasteItemData(Calendar startDate) {
        super(startDate);
        init();
    }

    public RetailWasteItemData(Calendar startDate, RetailWasteItem item) {
        super(startDate, item);
        init();
    }

    protected void add(long day, RetailWasteData data) {
        getDataMap().put(Long.valueOf(day), data);
        getIdDataMap().put(String.valueOf(day), data);
    }

    public boolean add(RetailWasteData data) {

        setParent(data.getItem());
        add(WasteItemData.milisToDays(data.getDate().getTimeInMillis()), data);
        return true;
    }

    @Override
    public void setParent(RetailWasteItem item) {
        super.setParent(item);
        for(RetailWasteData it :getDataMap().values()) {
            it.setItem(item);
        }
    }

    public BigDecimal getTotalValue() {
        BigDecimal result = BigDecimal.ZERO;
        for(RetailWasteData it :getDataMap().values()) {
            result = result.add( it.getValue());
        }
        return result;
    }

    public BigDecimal getTotalCost() {
        return getTotalValue().multiply(getParent().getUnitCost());
    }

    public RetailWasteData getData(int day) {
        Calendar tmp = (Calendar) getStartDate().clone();
        if (day > 1) {
            day -= 1;
        } else {
            day = 7;    //SUNDAY should be last day in week
        }
        tmp.add(Calendar.DAY_OF_WEEK, day - 1);
        Long time = WasteItemData.milisToDays(tmp.getTimeInMillis());
        return getDataMap().get(time);
    }

    public BigDecimal getDataValue(int day) {
        return getData(day).getValue();
    }

    public RetailWasteData getMondayData() {
        return getData(Calendar.MONDAY);
    }

    public RetailWasteData getTuesdayData() {
        return getData(Calendar.TUESDAY);
    }

    public RetailWasteData getWednesdayData() {
        return getData(Calendar.WEDNESDAY);
    }

    public RetailWasteData getThursdayData() {
        return getData(Calendar.THURSDAY);
    }

    public RetailWasteData getFridayData() {
        return getData(Calendar.FRIDAY);
    }

    public RetailWasteData getSaturdayData() {
        return getData(Calendar.SATURDAY);
    }

    public RetailWasteData getSundayData() {
        return getData(Calendar.SUNDAY);
    }

    public BigDecimal getMondayValue() {

        return getMondayData().getValue();
    }

    public BigDecimal getTuesdayValue() {
        return getTuesdayData().getValue();
    }

    public BigDecimal getWednesdayValue() {
        return getWednesdayData().getValue();
    }

    public BigDecimal getThursdayValue() {
        return getThursdayData().getValue();
    }

    public BigDecimal getFridayValue() {
        return getFridayData().getValue();
    }

    public BigDecimal getSaturdayValue() {
        return getSaturdayData().getValue();
    }

    public BigDecimal getSundayValue() {
        return getSundayData().getValue();
    }
   
}
