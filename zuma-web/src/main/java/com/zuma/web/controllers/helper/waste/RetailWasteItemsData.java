/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.waste;

import com.zuma.db.core.domain.RetailWasteData;
import com.zuma.db.core.domain.RetailWasteItem;
import com.zuma.web.controllers.helper.DataSet;
import java.math.BigDecimal;
import java.util.Calendar;

public class RetailWasteItemsData extends DataSet<RetailWasteItem, Object, RetailWasteItemData> {

    public RetailWasteItemsData(Calendar startDate) {
        super(startDate);
    }

    protected RetailWasteItemData createInventoryItemData(RetailWasteItem inventoryItem) {
        return new RetailWasteItemData(getStartDate(), inventoryItem);

    }

    public RetailWasteItemData add(RetailWasteData data) {

        RetailWasteItem inventoryItem = data.getItem();
        RetailWasteItemData inventoryItemData = getDataMap().get(inventoryItem);
        if (inventoryItemData == null) {
            inventoryItemData = createInventoryItemData(inventoryItem);
            add(inventoryItem, inventoryItem.getId().toString(), inventoryItemData);
        }
        inventoryItemData.add(data);
        return inventoryItemData;
    }

    public RetailWasteItemData add(RetailWasteItem inventoryItem) {

        RetailWasteItemData inventoryItemData = getDataMap().get(inventoryItem);
        if (inventoryItemData == null) {
            inventoryItemData = createInventoryItemData(inventoryItem);
            getDataMap().put(inventoryItem, inventoryItemData);
            //add(inventoryItem, inventoryItem.getId().toString(), inventoryItemData);

        }
        return inventoryItemData;
    }

    public RetailWasteItemData remove(RetailWasteItem inventoryItem) {

        RetailWasteItemData inventoryItemData = getDataMap().remove(inventoryItem);
        return inventoryItemData;
    }

    public BigDecimal getValue(int day) {
        BigDecimal result = BigDecimal.ZERO;
        for (RetailWasteItemData it : getDataMap().values()  ) {
            result = result.add(it.getDataValue(day));
        }
        return result;

    }

    public BigDecimal getMondayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (RetailWasteItemData it : getDataMap().values()  ) {
            result = result.add(it.getMondayValue());
        }
        return result;
    }

    public BigDecimal getTuesdayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (RetailWasteItemData it : getDataMap().values()) {
            result = result.add(it.getTuesdayValue());
        }
        return result;
    }

    public BigDecimal getWednesdayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (RetailWasteItemData it : getDataMap().values()) {
            result = result.add(it.getWednesdayValue());
        }
        return result;
    }

    public BigDecimal getThursdayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (RetailWasteItemData it : getDataMap().values()) {
            result = result.add(it.getThursdayValue());
        }
        return result;
    }

    public BigDecimal getFridayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (RetailWasteItemData it : getDataMap().values()) {
            result = result.add(it.getFridayValue());
        }
        return result;
    }

    public BigDecimal getSaturdayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (RetailWasteItemData it : getDataMap().values()) {
            result = result.add(it.getSaturdayValue());
        }
        return result;
    }

    public BigDecimal getSundayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (RetailWasteItemData it : getDataMap().values()) {
            result = result.add(it.getSundayValue());
        }
        return result;
    }

    @Override
    public void recalculate() throws Exception {
        for (RetailWasteItemData it : getDataMap().values()) {
            it.recalculate();
        }
    }

    public BigDecimal getTotalValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (RetailWasteItemData it : getDataMap().values()) {
            result = result.add(it.getTotalValue());
        }
        return result;
    }

    public BigDecimal getTotalCost() {
        BigDecimal result = BigDecimal.ZERO;
        for (RetailWasteItemData it : getDataMap().values()) {
            result = result.add(it.getTotalCost());
        }
        return result;
    }
}
