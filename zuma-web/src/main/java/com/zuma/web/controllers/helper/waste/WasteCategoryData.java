/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.waste;

import com.zuma.web.controllers.helper.DataSet;
import com.zuma.db.core.domain.InventoryCategory;
import com.zuma.db.core.domain.WasteData;
import com.zuma.db.core.domain.InventoryItem;
import com.zuma.db.core.domain.InventorySubCategory;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.util.Calendar;

public class WasteCategoryData extends DataSet<InventorySubCategory, InventoryCategory, WasteSubCategoryData> {

    public WasteCategoryData(Calendar startDate) {
        super(startDate);
    }

    public WasteCategoryData(Calendar startDate, InventoryCategory category) {
        super(startDate, category);
    }

    public WasteItemData add(WasteData data) {
        InventorySubCategory inventorySubCategory = data.getInventoryItem().getInventorySubCategory();
        if (getParent() != null && !getParent().equals(inventorySubCategory.getInventoryCategory())) {
            return null;
        }
        WasteSubCategoryData subCategoryData = getDataMap().get(inventorySubCategory);
        if (subCategoryData == null) {
            subCategoryData = new WasteSubCategoryData(getStartDate(), inventorySubCategory);
            add(inventorySubCategory, inventorySubCategory.getId().toString(), subCategoryData);
        }
        return subCategoryData.add(data);
    }

    public WasteItemData add(InventoryItem inventoryItem) {
        InventorySubCategory inventorySubCategory = inventoryItem.getInventorySubCategory();
        if (getParent() != null && !getParent().equals(inventorySubCategory.getInventoryCategory())) {
            return null;
        }
        WasteSubCategoryData subCategoryData = getDataMap().get(inventorySubCategory);
        if (subCategoryData == null) {
            subCategoryData = new WasteSubCategoryData(getStartDate(), inventorySubCategory);
            add(inventorySubCategory, inventorySubCategory.getId().toString(), subCategoryData);
        }
        return subCategoryData.add(inventoryItem);
    }
    public WasteItemData remove(InventoryItem inventoryItem) {
        InventorySubCategory inventorySubCategory = inventoryItem.getInventorySubCategory();
        if (getParent() != null && !getParent().equals(inventorySubCategory.getInventoryCategory())) {
            return null;
        }
        WasteSubCategoryData subCategoryData = getDataMap().get(inventorySubCategory);
        if (subCategoryData == null) {
            return null;
        }
        return subCategoryData.remove(inventoryItem);
    }

    public WasteSubCategoryData add(InventorySubCategory inventorySubCategory) {
        InventoryCategory inventoryCategory = inventorySubCategory.getInventoryCategory();
        if (getParent() != null && !getParent().equals(inventoryCategory)) {
            return null;
        }
        WasteSubCategoryData subCategoryData = getDataMap().get(inventorySubCategory);
        if (subCategoryData == null) {
            subCategoryData = new WasteSubCategoryData(getStartDate(), inventorySubCategory);
            add(inventorySubCategory, inventorySubCategory.getId().toString(), subCategoryData);
        }
        return subCategoryData;
    }

    @Override
    public void save(UniversalDAO dao) {
        super.save(dao);
        for (WasteSubCategoryData it : getDataMap().values()) {
            it.save(dao);
        }
    }

    @Override
    public void recalculate() throws Exception {
        for (WasteSubCategoryData it : getDataMap().values()) {
            it.recalculate();
        }
    }

    public BigDecimal getTotalValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteSubCategoryData it : getDataMap().values()) {
            result = result.add(it.getTotalValue());
        }
        return result;
    }

    public BigDecimal getTotalCost() {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteSubCategoryData it : getDataMap().values()) {
            result = result.add(it.getTotalCost());
        }
        return result;
    }
}
