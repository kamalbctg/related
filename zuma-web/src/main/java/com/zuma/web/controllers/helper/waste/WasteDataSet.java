/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.waste;

import com.zuma.web.controllers.helper.DataSet;
import com.zuma.db.core.domain.InventoryCategory;
import com.zuma.db.core.domain.WasteData;
import com.zuma.db.core.domain.InventoryItem;
import com.zuma.db.core.domain.InventorySubCategory;
import com.zuma.db.core.domain.RetailWasteData;
import com.zuma.db.core.domain.RetailWasteItem;
import com.zuma.db.core.domain.Store;
import com.zuma.db.core.domain.WasteSalesPercentage;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;

public class WasteDataSet extends DataSet<InventoryCategory, Object, WasteCategoryData> {

    private Store store;
    private RetailWasteItemsData retailData;
    private WasteSalesPercentage salesPercentage = new WasteSalesPercentage();

    public WasteDataSet(Calendar startDate) {
        super(startDate);
        salesPercentage.setDate((Calendar) startDate.clone());
        retailData = new RetailWasteItemsData(startDate);
    }

    public WasteItemData add(WasteData data) {

        InventoryCategory inventoryCategory = data.getInventoryItem().getInventorySubCategory().getInventoryCategory();

        WasteCategoryData categoryData = getDataMap().get(inventoryCategory);
        if (categoryData == null) {
            categoryData = new WasteCategoryData(getStartDate(), inventoryCategory);
            add(inventoryCategory, inventoryCategory.getId().toString(), categoryData);
        }
        return categoryData.add(data);
    }

    public WasteItemData add(InventoryItem inventoryItem) {
        InventoryCategory inventoryCategory = inventoryItem.getInventorySubCategory().getInventoryCategory();

        WasteCategoryData categoryData = getDataMap().get(inventoryCategory);
        if (categoryData == null) {
            categoryData = new WasteCategoryData(getStartDate(), inventoryCategory);
            add(inventoryCategory, inventoryCategory.getId().toString(), categoryData);
        }
        return categoryData.add(inventoryItem);
    }

    public WasteItemData remove(InventoryItem inventoryItem) {
        InventoryCategory inventoryCategory = inventoryItem.getInventorySubCategory().getInventoryCategory();
        WasteCategoryData categoryData = getDataMap().get(inventoryCategory);
        if (categoryData == null) {
            return null;
        }
        return categoryData.remove(inventoryItem);
    }

    public WasteSubCategoryData add(InventorySubCategory inventorySubCategory) {
        InventoryCategory inventoryCategory = inventorySubCategory.getInventoryCategory();
        WasteCategoryData categoryData = getDataMap().get(inventoryCategory);
        if (categoryData == null) {
            categoryData = new WasteCategoryData(getStartDate(), inventoryCategory);
            add(inventoryCategory, inventoryCategory.getId().toString(), categoryData);
        }
        return categoryData.add(inventorySubCategory);
    }

    public WasteCategoryData add(InventoryCategory inventoryCategory) {
        WasteCategoryData categoryData = getDataMap().get(inventoryCategory);
        if (categoryData == null) {
            categoryData = new WasteCategoryData(getStartDate(), inventoryCategory);
            add(inventoryCategory, inventoryCategory.getId().toString(), categoryData);
        }
        return categoryData;
    }

    @Override
    public void save(UniversalDAO dao) {
        for (WasteCategoryData it : getDataMap().values()) {
            it.save(dao);
        }
    }

    public WasteSubCategoryData getSubCategoryData(String ID) {
        WasteSubCategoryData result = null;
        for (WasteCategoryData categoryData : getDataMap().values()) {
            result = categoryData.getIdDataMap().get(ID);
            if (result != null) {
                break;
            }
        }
        return result;
    }

    @Override
    public void recalculate() throws Exception {
        for (WasteCategoryData it : getDataMap().values()) {
            it.recalculate();
        }
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
        salesPercentage.setStore(store);
    }

    public RetailWasteItemData add(RetailWasteItem item) {
        return getRetailData().add(item);
    }

    public RetailWasteItemData remove(RetailWasteItem item) {
        return getRetailData().remove(item);
    }

    public RetailWasteItemData add(RetailWasteData data) {
        return getRetailData().add(data);
    }

    public RetailWasteItemsData getRetailData() {
        return retailData;
    }

    /**
     * @return the salesPercentage
     */
    public WasteSalesPercentage getSalesPercentage() {
        return salesPercentage;
    }

    /**
     * @param salesPercentage the salesPercentage to set
     */
    public void setSalesPercentage(WasteSalesPercentage salesPercentage) {
        this.salesPercentage = salesPercentage;
    }

    @Override
    public void clear() {
        super.clear();
        retailData.clear();
        salesPercentage = new WasteSalesPercentage();
        salesPercentage.setDate((Calendar) getStartDate().clone());
    }

    @Override
    public void setStartDate(Calendar startDate) {
        super.setStartDate(startDate);
        if (retailData != null) {
            retailData.setStartDate(startDate);
        }
    }

    public BigDecimal getNetRetailWaste() {
        return retailData.getTotalCost().multiply(salesPercentage.getValue()).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
    }

    public BigDecimal getTotalProductWaste() {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteCategoryData it : getDataMap().values()) {
            result = result.add(it.getTotalCost());
        }
        return result;
    }

    public BigDecimal getTotalWaste() {
        return getTotalProductWaste().add(getNetRetailWaste());
    }
}

