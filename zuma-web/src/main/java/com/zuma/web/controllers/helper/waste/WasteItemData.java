/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.waste;

import com.zuma.web.controllers.helper.DataSet;
import com.zuma.db.core.domain.WasteData;
import com.zuma.db.core.domain.InventoryItem;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class WasteItemData extends DataSet<Long, InventoryItem, WasteData> {

    private BigDecimal unitCost = BigDecimal.ZERO;      //Weekly record unitCost
    private BigDecimal quantity = BigDecimal.ZERO;      //Weekly record quantity

    private void init() {
        Calendar tmp = Calendar.getInstance();
        tmp.setTime(getStartDate().getTime());

        add(milisToDays(tmp.getTimeInMillis()), new WasteData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(milisToDays(tmp.getTimeInMillis()), new WasteData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(milisToDays(tmp.getTimeInMillis()), new WasteData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(milisToDays(tmp.getTimeInMillis()), new WasteData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(milisToDays(tmp.getTimeInMillis()), new WasteData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(milisToDays(tmp.getTimeInMillis()), new WasteData(tmp, getParent()));

        tmp = (Calendar) tmp.clone();
        tmp.add(Calendar.DAY_OF_WEEK, 1);
        add(milisToDays(tmp.getTimeInMillis()), new WasteData(tmp, getParent()));

    }

    public WasteItemData(Calendar startDate) {
        super(startDate);
        init();
    }

    public WasteItemData(Calendar startDate, InventoryItem item) {
        super(startDate, item);
        init();
    }

    protected void add(long day, WasteData data) {
        getDataMap().put(Long.valueOf(day), data);
        getIdDataMap().put(String.valueOf(day), data);
    }

    public boolean add(WasteData data) {

        if (getParent() != null && getParent() != data.getInventoryItem()) {
            return false;
        }
        setParent(data.getInventoryItem());
        add(milisToDays(data.getDate().getTimeInMillis()), data);
        return true;
    }

    @Override
    public void setParent(InventoryItem item) {
        super.setParent(item);
        for (Iterator<WasteData> it = getDataMap().values().iterator(); it.hasNext();) {
            it.next().setInventoryItem(item);
        }
    }

    public BigDecimal getTotalValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (Iterator<WasteData> it = getDataMap().values().iterator(); it.hasNext();) {
            WasteData wasteData = it.next();
            result = result.add(wasteData.getValue());
        }
        return result;
    }

    public BigDecimal getTotalCost() {
        return getTotalValue().multiply(getUnitCost());
    }

    public WasteData getData(int day) {
        Calendar tmp = (Calendar) getStartDate().clone();
        if (day > 1) {
            day -= 1;
        } else {
            day = 7;    //SUNDAY should be last day in week
        }
        tmp.add(Calendar.DAY_OF_WEEK, day - 1);
        Long time = milisToDays(tmp.getTimeInMillis());
        return getDataMap().get(time);
    }

    public BigDecimal getDataValue(int day) {
        return getData(day).getValue();
    }

    public WasteData getMondayData() {
        return getData(Calendar.MONDAY);
    }

    public WasteData getTuesdayData() {
        return getData(Calendar.TUESDAY);
    }

    public WasteData getWednesdayData() {
        return getData(Calendar.WEDNESDAY);
    }

    public WasteData getThursdayData() {
        return getData(Calendar.THURSDAY);
    }

    public WasteData getFridayData() {
        return getData(Calendar.FRIDAY);
    }

    public WasteData getSaturdayData() {
        return getData(Calendar.SATURDAY);
    }

    public WasteData getSundayData() {
        return getData(Calendar.SUNDAY);
    }

    public BigDecimal getMondayValue() {
        return getMondayData().getValue();
    }

    public BigDecimal getTuesdayValue() {
        return getTuesdayData().getValue();
    }

    public BigDecimal getWednesdayValue() {
        return getWednesdayData().getValue();
    }

    public BigDecimal getThursdayValue() {
        return getThursdayData().getValue();
    }

    public BigDecimal getFridayValue() {
        return getFridayData().getValue();
    }

    public BigDecimal getSaturdayValue() {
        return getSaturdayData().getValue();
    }

    public BigDecimal getSundayValue() {
        return getSundayData().getValue();
    }

    @Override
    public void save(UniversalDAO dao) {
        super.save(dao);

        List<WasteData> delList = new ArrayList<WasteData>();

        for (Iterator<WasteData> it = getDataMap().values().iterator(); it.hasNext();) {
            WasteData wasteData = it.next();
            if (wasteData.getValue().intValue() != 0) {
                dao.saveOrUpdate(wasteData);

            } else if (wasteData.getId() != null) {
                WasteData data = new WasteData(wasteData.getDate(), wasteData.getInventoryItem());
                getParent().getWasteDataList().remove(wasteData);
                getParent().getWasteDataList().add(data);
                add(data);
                //delList.add(wasteData);
                }
        }

        dao.save(getParent());

        /*
        for (WasteData elem : delList) {
        WasteData data = new WasteData(elem.getDate(), elem.getInventoryItem());
        dao.delete(elem);
        add(data);
        }
         */
    }

    public static long milisToDays(long milis) {
        return milis / 86400000;
    }

    public BigDecimal getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(BigDecimal unitCost) {
        this.unitCost = unitCost;
    }

    /**
     * @return the quantity
     */
    public BigDecimal getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }
}
