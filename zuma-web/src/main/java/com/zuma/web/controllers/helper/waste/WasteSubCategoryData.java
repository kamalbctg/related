/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.controllers.helper.waste;

import com.zuma.web.controllers.helper.DataSet;
import com.zuma.db.core.domain.WasteData;
import com.zuma.db.core.domain.InventoryItem;
import com.zuma.db.core.domain.InventorySubCategory;
import com.zuma.db.dao.UniversalDAO;
import java.math.BigDecimal;
import java.util.Calendar;

public class WasteSubCategoryData extends DataSet<InventoryItem, InventorySubCategory, WasteItemData> {

    public WasteSubCategoryData(Calendar startDate) {
        super(startDate);
    }

    public WasteSubCategoryData(Calendar startDate, InventorySubCategory subCategory) {
        super(startDate, subCategory);
    }

    protected WasteItemData createInventoryItemData(InventoryItem inventoryItem) {
        return new WasteItemData(getStartDate(), inventoryItem);

    }

    public WasteItemData add(WasteData data) {
        if (getParent() != null && !getParent().equals(data.getInventoryItem().getInventorySubCategory())) {
            return null;
        }
        InventoryItem inventoryItem = data.getInventoryItem();
        WasteItemData inventoryItemData = getDataMap().get(inventoryItem);
        if (inventoryItemData == null) {
            inventoryItemData = createInventoryItemData(inventoryItem);
            add(inventoryItem, inventoryItem.getId().toString(), inventoryItemData);
        }
        inventoryItemData.add(data);
        return inventoryItemData;
    }

    public WasteItemData add(InventoryItem inventoryItem) {
        if (getParent() != null && !getParent().equals(inventoryItem.getInventorySubCategory())) {
            return null;
        }

        WasteItemData inventoryItemData = getDataMap().get(inventoryItem);
        if (inventoryItemData == null) {
            inventoryItemData = createInventoryItemData(inventoryItem);
            add(inventoryItem, inventoryItem.getId().toString(), inventoryItemData);

        }
        return inventoryItemData;
    }

    public WasteItemData remove(InventoryItem inventoryItem) {
        if (getParent() != null && !getParent().equals(inventoryItem.getInventorySubCategory())) {
            return null;
        }

        WasteItemData inventoryItemData = getDataMap().remove(inventoryItem);
        return inventoryItemData;
    }

    public BigDecimal getValue(int day) {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteItemData it : getDataMap().values()) {
            result = result.add(it.getDataValue(day));
        }
        return result;

    }

    public BigDecimal getMondayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteItemData it : getDataMap().values()) {
            result = result.add(it.getMondayValue());
        }
        return result;
    }

    public BigDecimal getTuesdayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteItemData it : getDataMap().values()) {
            result = result.add(it.getTuesdayValue());
        }
        return result;
    }

    public BigDecimal getWednesdayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteItemData it : getDataMap().values()) {
            result = result.add(it.getWednesdayValue());
        }
        return result;
    }

    public BigDecimal getThursdayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteItemData it : getDataMap().values()) {
            result = result.add(it.getThursdayValue());
        }
        return result;
    }

    public BigDecimal getFridayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteItemData it : getDataMap().values()) {
            result = result.add(it.getFridayValue());
        }
        return result;
    }

    public BigDecimal getSaturdayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteItemData it : getDataMap().values()) {
            result = result.add(it.getSaturdayValue());
        }
        return result;
    }

    public BigDecimal getSundayValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteItemData it : getDataMap().values()) {
            result = result.add(it.getSundayValue());
        }
        return result;
    }

    @Override
    public void save(UniversalDAO dao) {
        super.save(dao);
        for (WasteItemData it : getDataMap().values()) {
            it.save(dao);
        }
    }

    @Override
    public void recalculate() throws Exception {
        for (WasteItemData it : getDataMap().values()) {
            it.recalculate();
        }
    }

    public BigDecimal getTotalValue() {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteItemData it : getDataMap().values()) {
            result = result.add(it.getTotalValue());
        }
        return result;
    }

    public BigDecimal getTotalCost() {
        BigDecimal result = BigDecimal.ZERO;
        for (WasteItemData it : getDataMap().values()) {
            result = result.add(it.getTotalCost());
        }
        return result;
    }
}
