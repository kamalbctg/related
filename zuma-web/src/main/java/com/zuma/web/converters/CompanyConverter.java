package com.zuma.web.converters;

import com.zuma.db.core.domain.Company;
import java.util.Collection;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import com.zuma.web.controllers.UserManagedBean;
import org.apache.log4j.Logger;

public class CompanyConverter implements Converter {

    static private Logger log = Logger.getLogger(StoreConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
            String value) throws ConverterException {

        UserManagedBean userManagedBean = (UserManagedBean) context.getApplication().getELResolver().getValue(
                context.getELContext(), null, "userManagedBean");

        if (!userManagedBean.isAdmin()) {
            return userManagedBean.getActiveUser().getCompany();
        }

        Company selectedCompany = null;
        Collection<Company> companyList = userManagedBean.getAccountList();
        for (Company company : companyList) {
            if (company.toString().equals(value)) {
                selectedCompany = company;
                break;
            }
        }
        if (selectedCompany == null) {
            String msg = "Account not found: " + value;
            log.debug(msg);
        }

        return selectedCompany;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component,
            Object value) throws ConverterException {
        return value.toString();
    }
}
