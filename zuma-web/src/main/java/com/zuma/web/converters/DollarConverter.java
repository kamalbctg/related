package com.zuma.web.converters;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Locale;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.convert.NumberConverter;

public class DollarConverter extends NumberConverter {
	private final static String[] patterns = { "$######0.00", "######0.00" };
    {
        setPattern(patterns[0]);
        setLocale(Locale.US);
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
            String value) {
        ConverterException converterException = null;
        for (String pattern : patterns) {
            try {
                setPattern(pattern);
                try {
                    return super.getAsObject(context, component, value);
                } finally {
                    setPattern(patterns[0]);
                }
            } catch (ConverterException e) {
                converterException = e;
            }

        }
        throw converterException;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "NA";
        }
        DecimalFormat parser = new DecimalFormat(getPattern());
        parser.setRoundingMode(RoundingMode.HALF_UP);
        return parser.format(value);
    }
}
