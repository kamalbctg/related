package com.zuma.web.converters;

import com.zuma.db.core.domain.Franchise;
import com.zuma.web.controllers.FranchiseManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import org.apache.log4j.Logger;

public class FranchiseConverter implements Converter {

    static private Logger log = Logger.getLogger(StoreConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
            String value) throws ConverterException {

        FranchiseManagedBean franchiseManagedBean = (FranchiseManagedBean) context.getApplication().getELResolver().getValue(
                context.getELContext(), null, "franchiseManagedBean");

        Franchise selectedFranchise = franchiseManagedBean.getByValue(value);
        if (selectedFranchise == null) {
            String msg = "Franchise not found: " + value;
            log.debug(msg);
        }

        return selectedFranchise;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component,
            Object value) throws ConverterException {
        return value.toString();
    }
}
