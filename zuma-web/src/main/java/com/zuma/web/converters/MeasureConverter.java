package com.zuma.web.converters;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.zuma.db.core.domain.InventoryMeasure;
import com.zuma.db.service.impl.GenericServiceImpl;
import com.zuma.web.controllers.UserManagedBean;

@Component
public class MeasureConverter implements Converter {
	static private Logger logger = Logger.getLogger(MeasureConverter.class);
	protected GenericServiceImpl<InventoryMeasure, Integer> inventoryMeasureService;
	protected Map<String, InventoryMeasure> measureStringMap = new HashMap<String, InventoryMeasure>();

	public void init(FacesContext context) {
		inventoryMeasureService = (GenericServiceImpl<InventoryMeasure, Integer>) context
				.getApplication().getELResolver()
				.getValue(context.getELContext(), null,
						"inventoryMeasureService");
		;
		List<InventoryMeasure> inventoryMeasures = inventoryMeasureService
				.getAll();
		for (InventoryMeasure inventoryMeasure : inventoryMeasures) {
			measureStringMap.put(inventoryMeasure.getMeasureName(),
					inventoryMeasure);
		}
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) throws ConverterException {
		if (inventoryMeasureService == null) {
			init(context);
		}
		return measureStringMap.get(value);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object value) throws ConverterException {
		if (value instanceof String)
			return (String) value;
		return ((InventoryMeasure) value).getMeasureName();
	}
}
