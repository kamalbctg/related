package com.zuma.web.converters;

import java.util.Collection;
import java.util.Map;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

public class NullMapConverter implements Converter {
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(value.equals("null"))
            {
            return null;
            }
        
        UISelectItems items = (UISelectItems) component.getChildren().iterator().next();
        ValueExpression expression = items.getValueExpression("value");

        Map<String, Object> map = (Map<String, Object>) expression.getValue(context.getELContext());

        Collection objects = map.values();
        Object selected = null;
        for (Object obj : objects) {
            if (value.equals(obj.toString())) {
                selected = obj;
                break;
            }
        }
        return selected;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "null";
        }
        return value.toString();
    }
}
