package com.zuma.web.converters;

import com.zuma.db.core.domain.Person;
import java.util.Collection;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import com.zuma.web.controllers.UserManagedBean;
import org.apache.log4j.Logger;

public class PersonConverter implements Converter {

    static private Logger log = Logger.getLogger(StoreConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
            String value) throws ConverterException {

        UserManagedBean userManagedBean = (UserManagedBean) context.getApplication().getELResolver().getValue(
                context.getELContext(), null, "userManagedBean");

        if (!userManagedBean.isAdmin()) {
            return userManagedBean.getActiveUser();
        }

        Person selectedPerson = null;
        Collection<Person> personList = userManagedBean.getPersonList();
        for (Person person : personList) {
            if (person.toString().equals(value)) {
                selectedPerson = person;
                break;
            }
        }
        if (selectedPerson == null) {
            String msg = "Person not found: " + value;
            log.debug(msg);
        }

        return selectedPerson;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component,
            Object value) throws ConverterException {
        return value.toString();
    }
}
