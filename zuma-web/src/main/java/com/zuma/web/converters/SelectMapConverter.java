/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.converters;

import java.util.Collection;
import java.util.Map;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
/**
 *
 * converter will work only with htmlSelectOneMenu components
 * and only if they have UISelectItems inited with map<String,Object>
 */
public class SelectMapConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        UISelectItems items = (UISelectItems) component.getChildren().iterator().next();
        if(items==null)
            throw new ConverterException("The selected component was not found.");

        ValueExpression expression = items.getValueExpression("value");

        if(expression==null)
            throw new ConverterException("Value expression isn't specified for select component. [id="+component.getId()+"]");
        Map<String, Object> map = (Map<String, Object>) expression.getValue(context.getELContext());
        if(map==null)
            throw new ConverterException("Required map object can't be retreived by expression '"+expression.getExpressionString()+"'");

        Collection objects = map.values();
        Object selected = null;
        for (Object obj : objects) {
            if (value.equals(obj.toString())) {
                selected = obj;
                break;
            }
        }
        return selected;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value.toString();
    }
}
