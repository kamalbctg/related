package com.zuma.web.converters;

import com.zuma.db.core.domain.USState;
import com.zuma.web.controllers.StateManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import org.apache.log4j.Logger;

public class StateConverter implements Converter {

    static private Logger log = Logger.getLogger(StoreConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
            String value) throws ConverterException {

        StateManagedBean stateManagedBean = (StateManagedBean) context.getApplication().getELResolver().getValue(
                context.getELContext(), null, "stateManagedBean");

        USState selectedState = stateManagedBean.getByValue(value);
        if (selectedState == null) {
            String msg = "State not found: " + value;
            log.debug(msg);
        }

        return selectedState;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component,
            Object value) throws ConverterException {
        return value.toString();
    }
}
