package com.zuma.web.converters;

import java.util.Collection;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import com.zuma.web.controllers.UserManagedBean;
import com.zuma.db.core.domain.Store;
import org.apache.log4j.Logger;

public class StoreConverter implements Converter {

    static private Logger log = Logger.getLogger(StoreConverter.class.getName());

    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
            String value) throws ConverterException {

        UserManagedBean userManagedBean = (UserManagedBean) context.getApplication().getELResolver().getValue(
                context.getELContext(), null, "userManagedBean");

        Store selectedStore = null;
        Collection<Store> storeList = userManagedBean.getStoreList();
        for (Store store : storeList) {
            if (store.toString().equals(value)) {
                selectedStore = store;
                break;
            }
        }
        if (selectedStore == null) {
            String msg = "Store not found: " + value;
            log.debug(msg);
        }

        return selectedStore;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component,
            Object value) throws ConverterException {
        return value.toString();
    }
}
