
package com.zuma.web.converters;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.convert.NumberConverter;
import org.apache.log4j.Logger;

public class UIPatternConverter extends NumberConverter {

    public final static String patternKey = "converterPattern";
    protected ArrayList<String> patterns = new ArrayList<String>();

    {
        patterns.add("######0.00");
    }

    protected void initPatterns(UIComponent component) {
        String value = (String) component.getAttributes().get(patternKey);
        StringTokenizer tokens = new StringTokenizer(value, ";");
        patterns.clear();
        while (tokens.hasMoreTokens()) {
            patterns.add(tokens.nextToken());
        }
    }

    public static BigDecimal convert(Number value) throws Exception {
        BigDecimal result = null;
        if (value instanceof BigDecimal) {
            result = (BigDecimal) value;
        } else if (value instanceof Long) {
            result = new BigDecimal((Long) value);
        } else if (value instanceof Double) {
            result = new BigDecimal((Double) value);
        } else {
            throw new NumberFormatException("Unknown value type");
        }
        return result;
    }

    protected boolean check(String value) {
        boolean result = true;
        String expression = "^(([+-]?[$]?((([0-9]+(\\.)?)|([0-9]*\\.[0-9]+))([eE][+-]?[0-9]+)?[%]?))|([+-]?[$]?((([0-9]{1,3}(,[0-9]{3})*(\\.)?)|([0-9]{0,3}(,[0-9]{3})*\\.[0-9]+))([eE][+-]?[0-9]+)?[%]?)))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(expression);
        java.util.regex.Matcher m = p.matcher(value);
        if (!m.matches()) {
            result = false;
        }
        return result;
    }

    protected Object convert(FacesContext context, UIComponent component,
            String value, String pattern) throws Exception {
        BigDecimal result = BigDecimal.ZERO;

        DecimalFormat parser = new DecimalFormat(pattern);
        parser.setParseBigDecimal(true);
        parser.setRoundingMode(RoundingMode.HALF_UP);

        Number tmp = parser.parse(value);
        result = convert(tmp);

        int scale = 0;
        if (pattern.contains(".")) {
            String str = pattern.substring(pattern.lastIndexOf(".")).replace("%", "");
            scale = str.length() - 1;
        }
        if (pattern.endsWith("%")) {
            result = result.multiply(new BigDecimal(100));

        }
        result = result.setScale(scale, RoundingMode.HALF_UP);
        return result;
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component,
            String value) {
        if(value.isEmpty())
            value = "0";
        if(!check(value))
            throw new ConverterException("Value does not admit predefined pattern.");
        initPatterns(component);
        ConverterException converterException = null;
        for (String pattern : patterns) {
            try {
                return convert(context, component, value, pattern);
            } catch (Exception e) {
                Logger.getLogger(UIPatternConverter.class.getName()).info("Error value parsing. ["+value+"]");
                converterException = new ConverterException(e);
            }
        }
        throw converterException;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (value == null) {
            return "NA";
        }
        initPatterns(component);
        String pattern = patterns.get(0);
        DecimalFormat parser = new DecimalFormat(pattern);
        parser.setRoundingMode(RoundingMode.HALF_UP);
        if (pattern.endsWith("%")) {
            value = ((BigDecimal) value).divide(new BigDecimal(100), 4, RoundingMode.HALF_UP);
        }
        String result = "Invalid data entry. Use numbers only.";
        try {
            result = parser.format(value);
        } catch (Exception e) {
            int a = 0;
        }

        return result;
    }
}
