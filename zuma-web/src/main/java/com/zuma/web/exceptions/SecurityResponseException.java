/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zuma.web.exceptions;

import javax.servlet.ServletException;

public class SecurityResponseException extends ServletException {
  public SecurityResponseException() {
  }

  public SecurityResponseException(String msg) {
    super(msg);
  }
}