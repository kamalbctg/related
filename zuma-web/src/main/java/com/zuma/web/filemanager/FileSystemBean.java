package com.zuma.web.filemanager;

import com.zuma.db.dao.UniversalDAO;
import com.zuma.web.controllers.UserManagedBean;
import java.io.IOException;
import java.util.Map.Entry;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;

import com.zuma.db.core.domain.Folder;
import com.zuma.db.core.domain.InvoiceDetail;
import com.zuma.db.core.domain.Person;
import com.zuma.db.core.domain.PersonPage;
import com.zuma.db.core.domain.ReportFile;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.core.domain.Store;
import com.zuma.web.controllers.WasteController;
import com.zuma.web.controllers.SummaryController;
import com.zuma.web.reports.ParentReport;
import com.zuma.web.reports.ReportInventory;
import com.zuma.web.reports.ReportPurchase;
import com.zuma.web.reports.ReportSummary;
import com.zuma.web.reports.ReportWaste;
import com.zuma.web.reports.ReportInventoryBlank;
import com.zuma.db.service.util.Week;
import com.zuma.web.controllers.InvoiceController;
import com.zuma.web.controllers.helper.DataSet;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.FacesException;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.DateFormatter;
import org.apache.commons.lang.time.DateUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator.CellValue;
import org.apache.poi.hssf.usermodel.HSSFPrintSetup;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.richfaces.component.UITree;
import org.richfaces.component.html.HtmlTree;
import org.richfaces.component.html.HtmlTreeNode;
import org.richfaces.component.state.TreeState;
import org.richfaces.event.NodeExpandedEvent;
import org.richfaces.event.NodeSelectedEvent;
import org.richfaces.model.TreeNode;
import org.richfaces.model.TreeNodeImpl;
import org.richfaces.model.TreeRowKey;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class FileSystemBean implements Serializable {

    @Autowired
    protected UniversalDAO dao;
    @Autowired
    protected UserManagedBean userManagedBean;
    @Autowired
    protected InvoiceController invoiceController;
    @Autowired
    protected SummaryController summaryController;
    @Autowired
    protected WasteController wasteController;
    public final static String DATE_FORMAT = "yyyy-MM-dd";
    private TreeNode currentNode = null;
    private TreeNode rootNode = null;
    private HashMap<TreeNode, Folder> mapTree = new HashMap();
    private List<String> selectedNodeChildren = new ArrayList<String>();
    private List<ReportFile> fileItems = null;
    private ReportFile selectedFile = null;
    private boolean folderCreated = true;
    private String[] arReport = {"summary", "purchase", "inventoryview", "waste"};
    private String[] arReportCaption = {"Daily Summaries", "Purchase Journal", "Inventory", "Waste"};
    private List<SelectItem> userReports = new ArrayList<SelectItem>();
    private String selectedReport;
    private String newFolderName;
    private List<Calendar> listWeeks = new ArrayList<Calendar>();
    private TreeMap<Date, List<Calendar>> mapMonths = new TreeMap<Date, List<Calendar>>();
    private Calendar selectedDate;
    private String defaultFolder = "Financial Statements";
    private String nodeTitle;
    private Folder selFolder;
    private String thisPage = "filemanager";
    protected ReportingWeek currentWeek;
    private DateFormatter formatter = new DateFormatter(new SimpleDateFormat(DATE_FORMAT));
    private Map<String, Object> paramReport = new HashMap<String, Object>();

    public Map<String, Object> getParamReport() {
        Store curStore = dao.findById(Store.class, userManagedBean.getActiveStore().getId());
        paramReport.clear();
        paramReport.put("rAccount", curStore.getCompany().getCaption());
        paramReport.put("rStore", curStore.getContactList().get(0).getAddress());
        paramReport.put("rPeriod", userManagedBean.getActiveWeekString());

        return paramReport;
    }

    public String getDefaultFolder() {
        return defaultFolder;
    }

    public void setDefaultFolder(String defaultFolder) {
        this.defaultFolder = defaultFolder;
    }

    public Calendar getSelectedDate() {
        return selectedDate;
    }

    public void setSelectedDate(Calendar selectedDate) {
        this.selectedDate = selectedDate;
    }

    public TreeNode getCurrentNode() {
        return currentNode;
    }

    public Map<Date, List<Calendar>> getMapMonths() {
        return mapMonths;
    }

    public List<Date> getMonthsNames() {
        List keys = new ArrayList();
        keys.addAll(mapMonths.keySet());
        return keys;
    }

    public Date getLastDate() {
        Date dt = Week.getWeekEnd(Week.getCurrentDate());
        if (userManagedBean.getWeeks().size() > 0) {
            dt = Week.getWeekEnd(
                    userManagedBean.getWeeks().get(
                    userManagedBean.getWeeks().size() - 1).getWeekDate());
        }
        return dt;
    }

    public List<Calendar> getListWeeks() {
        return listWeeks;
    }

    public boolean isUploaded() {
        return selFolder != null && selFolder.isUploaded();
    }

    public boolean isAllowDelete() {
        return selFolder != null && selFolder.getParent() != null;
    }

    public String getNewFolderName() {
        return newFolderName;
    }

    public void setNewFolderName(String newFolderName) {
        this.newFolderName = newFolderName;
    }

    public String getSelectedReport() {
        return selectedReport;
    }

    public void setSelectedReport(String selectedReport) {
        this.selectedReport = selectedReport;
    }

    public boolean isFolderCreated() {
        return folderCreated;
    }
    private Date dateStart;
    private Date dateEnd;
    protected Store currentStore;
    protected Person currentUser;
    private String targetPage;

    @PostConstruct
    protected void init() {
        if (userManagedBean.getActiveStore() == null) {
            return;
        }
        currentStore = dao.findById(Store.class, userManagedBean.getActiveStore().getId());
        currentWeek = dao.findById(ReportingWeek.class, userManagedBean.getActiveWeek().getId());
        currentUser = dao.findById(Person.class, userManagedBean.getActiveUser().getId());
        fillListReport();
        fillMapMonths();

    }

    public List<SelectItem> getUserReports() {
        return userReports;
    }

    private void fillMapMonths() {

        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");

        mapMonths.clear();
        if (listWeeks != null) {
            listWeeks.clear();
        }
        Calendar clndr = Calendar.getInstance();
        Calendar cTemp = Calendar.getInstance();
        clndr.set(2000, 1, 1);

        for (ReportingWeek rw : userManagedBean.getWeeks()) {
            cTemp.setTime(Week.getWeekEnd(rw.getWeekDate()));
            if (clndr.get(Calendar.MONTH) == cTemp.get(Calendar.MONTH) && clndr.get(Calendar.YEAR) == cTemp.get(Calendar.YEAR)) {
                Calendar listCld = (Calendar) cTemp.clone();
                listCld.setTime(Week.getWeekEnd(listCld.getTime()));
                listWeeks.add(listCld);
            } else {
                if (listWeeks != null && listWeeks.size() > 0) {
                    mapMonths.put(Week.getWeekEnd(((Calendar) clndr.clone()).getTime()), listWeeks);
                }
                listWeeks = new ArrayList<Calendar>();
                listWeeks.add((Calendar) cTemp.clone());
            }
            clndr.setTime(cTemp.getTime());
        }
        //for last value
        if (listWeeks != null && listWeeks.size() > 0) {
            mapMonths.put(Week.getWeekEnd(((Calendar) clndr.clone()).getTime()), listWeeks);
        }

    }

    private List<SelectItem> fillListReport() {
        userReports.clear();
        if (currentUser.isAdmin()) {
            for (int i = 0; i < arReport.length; i++) {

                userReports.add(new SelectItem(arReportCaption[i], arReportCaption[i]));
            }
        } else {
            Collection<PersonPage> colPP = currentUser.getPersonPageList();
            for (int i = 0; i < arReport.length; i++) {
                for (PersonPage personPage : colPP) {
                    String page = personPage.getPage().getUrl();
                    if (page.equalsIgnoreCase(arReport[i])) {
                        userReports.add(new SelectItem(arReportCaption[i], arReportCaption[i]));
                        break;
                    }
                }
            }

        }
        return userReports;
    }

    public String getTargetPage() {
        return targetPage;
    }

    public void setTargetPage(String targetPage) {
        this.targetPage = targetPage;
    }

    public Store getCurrentStore() {
        return currentStore;
    }

    public ReportingWeek getCurrentWeek() {
        return currentWeek;
    }

    public String refreshPage() {
        return targetPage;
    }

    public void reload(PhaseEvent ev) throws ParseException {
        if (ev.getPhaseId() == PhaseId.RENDER_RESPONSE) {
            if (userManagedBean.getActiveStore() != null && isReloadingNeeded()) {

                currentStore = dao.findById(Store.class, userManagedBean.getActiveStore().getId());
                currentWeek = dao.findById(ReportingWeek.class, userManagedBean.getActiveWeek().getId());
                currentUser = dao.findById(Person.class, userManagedBean.getActiveUser().getId());
                clearTree();
                currentNode = null;
                selFolder = null;
                nodeTitle = null;
                rootNode = null;
                userManagedBean.refreshReportingWeeks();
                fillMapMonths();
                fillListReport();
            }

        }
    }

    private boolean isReloadingNeeded() {
        return !currentStore.equals(userManagedBean.getActiveStore()) ||
                !currentUser.equals(userManagedBean.getActiveUser());
    }

    public FileSystemBean() {
        Calendar clndr = Calendar.getInstance();
        clndr.setTime(Week.getWeekEnd(clndr.getTime()));
        clndr.add(Calendar.DATE, 7);
        setDateEnd(clndr.getTime());
        clndr.add(Calendar.MONTH, -6);
        clndr.set(Calendar.DATE, 1);
        setDateStart(clndr.getTime());
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public String getActiveWeek() {
        SimpleDateFormat frmttr = new SimpleDateFormat("MMMM dd, yyyy");
        return frmttr.format(Week.getWeekEnd(currentWeek.getWeekDate()));
    }

    public void setSelectedFile(ReportFile seletedFile) {
        this.selectedFile = seletedFile;
    }

    public ReportFile getSelectedFile() {
        return selectedFile;
    }

    public Folder getSelFolder() {
        return selFolder;
    }

    public void setSelFolder(Folder selFolder) {
        this.selFolder = selFolder;
    }

    private void addNodes(TreeNode node, Folder fldr) {

        String sql = "from Folder where parent= " + String.valueOf(fldr.getId());
        try {
            List<Folder> items = dao.get(sql);
            for (Folder it : items) {
                TreeNode nodeFldr = new TreeNodeImpl();

                nodeFldr.setData(it.getName());
                node.addChild(it, nodeFldr);
                addNodes(nodeFldr, it);
                mapTree.put(nodeFldr, it);
            }
        } catch (Exception e) {
            throw new FacesException(e.getMessage(), e);
        }
    }

    private void clearTree() {
        Iterator iterator = mapTree.keySet().iterator();
        while (iterator.hasNext()) {
            TreeNode node = (TreeNode) iterator.next();
            node.setParent(null);
            rootNode.removeChild(node);
        }
        mapTree.clear();
    }

    private void loadTree() {

        try {
            // Add all folders
            rootNode = new TreeNodeImpl();
            rootNode.setData(null);//and folderuser.access=1
            String sql = "";
            if (!currentUser.isAdmin()) {
                sql = "select  distinct f from PersonFolder pf,Folder f where pf.folder=f.id and access>0  " +
                        " and f.parent =null and f.store=" + String.valueOf(currentStore.getId()) +
                        " and person=" + String.valueOf(currentUser.getId());
            } else {
                sql = "select distinct f from Folder f where  f.parent=null and f.store=" + String.valueOf(currentStore.getId());
            }
            // folder.parent=null and folder.store="+String.valueOf(currentStore.getId()) ;//
                    /*"select distinct fldr from Folder as fldr join fldr.personFolderList as folderuser " +
            " where fldr.parent =null and folderuser.access>0 and fldr.store=" + String.valueOf(currentStore.getId());*/
            List<Folder> itemsNonSorted = dao.get(sql);
            List<Folder> items = getSortFolder(itemsNonSorted);
            for (Folder it : items) {

                TreeNode firstNode = new TreeNodeImpl();
                firstNode.setData(it.getName());
                rootNode.addChild(it, firstNode);
                addNodes(firstNode, it);
                mapTree.put(firstNode, it);
                if (it.getName().equalsIgnoreCase(defaultFolder)) {
                    currentNode = firstNode;
                    selFolder = it;
                    nodeTitle = defaultFolder;
                }
                /* TreeNode nodeFldr = new TreeNodeImpl();
                nodeFldr.setData(it.getName());*/
            }
        } catch (Exception e) {
            throw new FacesException(e.getMessage(), e);

        }
    }

    private List<Folder> getSortFolder(List<Folder> fldrList) {
        List<Folder> sortFolders = new ArrayList<Folder>();
        String[] sortNames = {"Financial", "Daily", "Purchase", "Inventory",
            "Waste", "Payroll", "Bank", "Other"};
        for (int i = 0; i < sortNames.length; i++) {
            for (Folder fldrItem : fldrList) {
                if (fldrItem.getName().contains(sortNames[i])) {
                    sortFolders.add(fldrItem);
                    //fldrList.remove(fldrItem);
                }
            }
        }
        return sortFolders;
    }

    public void processSelection(NodeSelectedEvent event) {
        HtmlTree tree = (HtmlTree) event.getComponent();

        nodeTitle = (String) tree.getRowData();

        selectedNodeChildren.clear();
        currentNode = tree.getModelTreeNode(tree.getRowKey());
        selFolder = (Folder) mapTree.get(currentNode);
        /*        uploaded = selFolder.isUploaded();
        if(!uploaded)
        folderCreated=true;
         */
        if (currentNode.isLeaf()) {
            selectedNodeChildren.add((String) tree.getRowData());
        } else {
            Iterator<Map.Entry<Object, TreeNode>> it = currentNode.getChildren();
            while (it != null && it.hasNext()) {
                Map.Entry<Object, TreeNode> entry = it.next();

                selectedNodeChildren.add(entry.getValue().getData().toString());
            }
        }
    }

    public void processExpansion(NodeExpandedEvent evt) {

        Object source = evt.getSource();
        if (source instanceof HtmlTreeNode) {
            UITree tree = ((HtmlTreeNode) source).getUITree();
            if (tree == null) {
                return;
            }
            // get the row key i.e. id of the given node.
            Object rowKey = tree.getRowKey();
            // get the model node of this node.
            TreeRowKey key = (TreeRowKey) tree.getRowKey();

            TreeState state = (TreeState) tree.getComponentState();
            if (state.isExpanded(key)) {
                System.out.println(rowKey + " - expanded");
            } else {
                System.out.println(rowKey + " - collapsed");
            }
        }
    }

    public String toFolder() {
        getTreeNode();
        Iterator it = rootNode.getChildren();
        while (it.hasNext()) {
            java.util.Map.Entry<java.lang.Object, TreeNode<String>> entry = (Entry<Object, TreeNode<String>>) it.next();
            if ((entry.getValue().getData().equalsIgnoreCase(defaultFolder))) {
                currentNode = entry.getValue();
                selFolder = mapTree.get(currentNode);
                nodeTitle = currentNode.getData().toString();
                break;
            }
        }
        return "flmngr";
    }

    public TreeNode getTreeNode() {
        if (rootNode == null) {
            loadTree();
        }
        return rootNode;
    }

    public String getNodeTitle() {
        getTreeNode();
        return nodeTitle;
    }

    public void setNodeTitle(String nodeTitle) {
        this.nodeTitle = nodeTitle;
    }

    public List getFileItems() {
        fileItems = null;
        if (selFolder != null && selFolder.getId() != 0 && getDateStart() != null && getDateEnd() != null && currentStore != null) {
            Date dateBegin = DateUtils.addDays(getDateStart(), -6);
            try {
                String sql = "from ReportFile where folder= " + String.valueOf(selFolder.getId()) + " and dateReport between '" + formatter.valueToString(dateBegin) + "' and '" + formatter.valueToString(getDateEnd().getTime()) + "'";

                fileItems = dao.get(sql);
            } catch (Exception e) {
                throw new FacesException(e.getMessage(), e);
            }
        }

        return fileItems;
    }

    public void startCreateFolder() {

        if (selFolder == null || !selFolder.isUploaded()) {
            return;
        }

        folderCreated = false;

    }

    public String createFolder() {
        if (newFolderName.trim().isEmpty() || selFolder == null) {
            folderCreated = true;
            return null;
        }

        Folder newFolder = new Folder();
        newFolder.setName(newFolderName);
        newFolder.setParent(selFolder);
        newFolder.setUploaded(true);
        newFolder.setStore(currentStore);
        dao.saveOrUpdate(newFolder);
        TreeNode nodeFldr = new TreeNodeImpl();
        nodeFldr.setData(newFolder.getName());
        currentNode.addChild(newFolder, nodeFldr);
        mapTree.put(nodeFldr, newFolder);
        folderCreated = true;
        newFolderName = "";
        return thisPage;
    }

    public void deleteDataFolder(TreeNode node) {
        Iterator it = node.getChildren();
        while (it.hasNext()) {
            java.util.Map.Entry<java.lang.Object, TreeNode<String>> entry = (Entry<Object, TreeNode<String>>) it.next();
            deleteDataFolder(entry.getValue());

        }
        try {
            Folder f = mapTree.get(node);
            String sql = "from ReportFile where folder=" + String.valueOf(f.getId());
            List<ReportFile> rf = dao.get(sql);
            dao.deleteAll(rf);
            dao.delete(f);
        } catch (Exception e) {
            throw new FacesException(e.getMessage(), e);
        }

    }

    public String deleteFolder() {
        //don't delete the Root Node
        if (currentNode != null && selFolder.getParent() != null) {

            deleteDataFolder(currentNode);
            TreeNode prntNode = currentNode.getParent();
            selFolder = mapTree.get(prntNode);
            nodeTitle = prntNode.getData().toString();
            Iterator iter = prntNode.getChildren();
            while (iter.hasNext()) {
                java.util.Map.Entry<java.lang.Object, TreeNode<String>> entry = (Entry<Object, TreeNode<String>>) iter.next();
                if (entry.getValue().equals(currentNode)) {
                    prntNode.removeChild(entry.getKey());
                    break;
                }
            }

            mapTree.remove(currentNode);
            currentNode.setParent(null);
            currentNode = prntNode;


        }

        return thisPage;
    }

    public void fillFiles() {
        getFileItems();
    }

    private Folder findFolder(String folderName) {

        String sql = "from Folder where name like '" + folderName + "%' and store=" + String.valueOf(currentStore.getId()); //
        List<Folder> items = dao.get(sql);
        if (items.size() > 0) {
            return items.iterator().next();
        } else {
            return null;
        }
    }

    public String submitReports() {
        String curReport = selectedReport;
        for (int i = 0; i < arReportCaption.length; i++) {
            selectedReport = arReportCaption[i];
            String nameReport = arReport[i];
            if (arReport[i].equalsIgnoreCase("inventoryView")) {
                nameReport = "inventory";
            }
            try {

                Calendar clndr = Calendar.getInstance();
                clndr.setTime(Week.getWeekBegin(Week.nullTime(selectedDate).getTime()));//first day of week with null-time
                SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
                String sDate = sdf.format(selectedDate.getTime());

                Folder fldr = findFolder(selectedReport);
                if (fldr == null) {
                    return null;
                }

                ParentReport rprt = preparedReport();
                rprt.addParameter("rPrint", true);
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                rprt.exportToPDF(os);

                ReportFile dbFile = new ReportFile();
                dbFile.setReport(os.toByteArray());
                dbFile.setSizeFile(os.size());
                dbFile.setName(nameReport + sDate + ".pdf");
                dbFile.setDateFile(new java.util.Date());
                dbFile.setFolder(fldr);
                dbFile.setDateReport(clndr.getTime());
                dbFile.setPerson(currentUser);

                dao.saveOrUpdate(dbFile);
                os.flush();
                os.close();


                //Excel file
                ParentReport rprtXls = preparedReport();
                rprtXls.addParameter("rPrint", false);
                ReportFile dbFileXls = new ReportFile();

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                rprtXls.exportToExcel(bos);
                ByteArrayInputStream input = new ByteArrayInputStream(((ByteArrayOutputStream) bos).toByteArray());
                HSSFWorkbook wb = new HSSFWorkbook(input);
                HSSFSheet sheet = wb.getSheetAt(0);
                sheet.setAutobreaks(true);
                HSSFPrintSetup ps = sheet.getPrintSetup();
                ps.setFitWidth((short) 1);
                ps.setFitHeight((short) 9999);
                sheet.setGridsPrinted(true);
                sheet.setHorizontallyCenter(true);
                ps.setPaperSize(HSSFPrintSetup.LETTER_PAPERSIZE);
                ps.setHeaderMargin((double) .35);
                ps.setFooterMargin((double) .35);
                sheet.setMargin(HSSFSheet.TopMargin, (double) .50);
                sheet.setMargin(HSSFSheet.BottomMargin, (double) .50);
                sheet.setMargin(HSSFSheet.LeftMargin, (double) .50);
                sheet.setMargin(HSSFSheet.RightMargin, (double) .50);

                bos.flush();
                bos.close();

                bos = new ByteArrayOutputStream();
                wb.write(bos);

                dbFileXls.setReport(bos.toByteArray());
                dbFileXls.setSizeFile(bos.size());
                dbFileXls.setName(nameReport + sDate + ".xls");
                dbFileXls.setDateFile(new java.util.Date());
                dbFileXls.setFolder(fldr);
                dbFileXls.setDateReport(clndr.getTime());
                dbFileXls.setPerson(currentUser);

                dao.saveOrUpdate(dbFileXls);
                bos.flush();
                bos.close();

            } catch (Exception e) {

                Logger.getLogger(FileSystemBean.class.getName()).log(Level.SEVERE, null, e);
            }


        }
        selectedReport = curReport;
        userManagedBean.refreshReportingWeeks();
        fillMapMonths();

        return thisPage;
    }

    public String printBlankInventory() throws JRException {

        setSelectedReport("InventoryBlank");
        Calendar cldnr = Calendar.getInstance();
        cldnr.setTime(userManagedBean.getActiveWeekDay());
        setSelectedDate(cldnr);
        return viewReportPdf();
    }

    public String viewReportPdf() throws JRException {

        try {
            ParentReport rprt = preparedReport();
            if (rprt == null) {
                return null;
            }
            
            
            rprt.addParameter("rPrint", true);
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response =
                    (HttpServletResponse) context.getExternalContext().getResponse();
            response.setHeader("Pragma", "public");
            response.setHeader("Cache-Control", "max-age=0");
            response.setContentType(MimeUtility.manageType("pdf"));
            response.setHeader("Content-disposition", "attachment; filename=\"preview.pdf\"");
            OutputStream os;

            os = response.getOutputStream();

            rprt.exportToPDF(os);
            os.flush();
            os.close();
            context.responseComplete();
        } catch (ParseException ex) {
            Logger.getLogger(FileSystemBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileSystemBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    public ParentReport preparedReport() throws JRException, ParseException {


        Date userDate = userManagedBean.getActiveWeekDay();
        userManagedBean.setActiveWeekDay(Week.getWeekBegin(selectedDate.getTime()));
        ParentReport rprt = null;
        try {
            if (selectedReport.equalsIgnoreCase("Daily Summaries")) {
                summaryController.loadData();
                DataSet dataSet = summaryController.getDataSet();
                try {
                    dataSet.recalculate();
                } catch (Exception ex) {
                    Logger.getLogger(SummaryController.class.getName()).log(Level.INFO, "Dataset recalculation exception.");
                }
                rprt = new ReportSummary(getParamReport(), summaryController.getDataSet());
            } else if (selectedReport.equalsIgnoreCase("Purchase Journal")) {
                invoiceController.reInitDataList();
                List<InvoiceDetail> purchases = new ArrayList(invoiceController.getInvoiceDetails());
                rprt = new ReportPurchase(getParamReport(), purchases);
            } else if (selectedReport.equalsIgnoreCase("Waste")) {
                wasteController.loadData();
                DataSet dataSet = wasteController.getDataSet();
                try {
                    dataSet.recalculate();
                } catch (Exception ex) {
                    Logger.getLogger(SummaryController.class.getName()).log(Level.INFO, "Dataset recalculation exception.");
                }
                rprt = new ReportWaste(getParamReport(), wasteController.getDataSet());
            } else if (selectedReport.equalsIgnoreCase("Inventory") ||
                    selectedReport.equalsIgnoreCase("InventoryBlank")) {
                wasteController.loadData();
                DataSet dataSet = wasteController.getDataSet();
                try {
                    dataSet.recalculate();
                } catch (Exception ex) {
                    Logger.getLogger(SummaryController.class.getName()).log(Level.INFO, "Dataset recalculation exception.");
                }
                if (selectedReport.equalsIgnoreCase("Inventory")) {
                    rprt = new ReportInventory(getParamReport(), wasteController.getDataSet());
                } else {
                    rprt = new ReportInventoryBlank(getParamReport(), wasteController.getDataSet());
                }
            } else {
                return null;
            }
        } finally {
            userManagedBean.setActiveWeekDay(userDate);
        }
        return rprt;
    }

    public String viewReportExcel() throws JRException {
        try {
            ParentReport rprt = preparedReport();

            if (rprt == null) {
                return null;
            }
            
            String filename = formateFileName();
            
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.setContentType(MimeUtility.manageType("xls"));
            response.setHeader("Content-disposition", "attachment; filename=\""+filename+".xls\"");

            OutputStream bos = new ByteArrayOutputStream();
            rprt.exportToExcel(bos);

            ByteArrayInputStream input = new ByteArrayInputStream(((ByteArrayOutputStream) bos).toByteArray());
            HSSFWorkbook wb = new HSSFWorkbook(input);            
            wb.setSheetName(0, filename);
            
            HSSFSheet sheet = wb.getSheetAt(0);
            
            if (rprt.getClass().equals(ReportSummary.class)) {
                //Map<String, HSSFCellStyle>stylesMap= new HashMap();
                HSSFFormulaEvaluator evaluator = new HSSFFormulaEvaluator(wb);
                HSSFFont fontBold = wb.createFont();
                fontBold.setFontHeightInPoints((short) 8);
                fontBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                HSSFFont fontNrml = wb.createFont();
                fontNrml.setFontHeightInPoints((short) 8);
                HSSFCellStyle styleParent = wb.createCellStyle();
                styleParent.cloneStyleFrom(sheet.getRow(1).getCell(1).getCellStyle());
                styleParent.setFont(fontNrml);
                styleParent.setAlignment(HSSFCellStyle.ALIGN_RIGHT);

                HSSFCellStyle styleCurr = wb.createCellStyle();
                styleCurr.cloneStyleFrom(styleParent);
                HSSFDataFormat dfc = wb.createDataFormat();
                styleCurr.setDataFormat(dfc.getFormat("$#,##0.00"));
                HSSFCellStyle styleCurrBold = wb.createCellStyle();
                styleCurrBold.cloneStyleFrom(styleCurr);
                styleCurrBold.setFont(fontBold);


                HSSFCellStyle styleInt = wb.createCellStyle();
                styleInt.cloneStyleFrom(styleParent);
                styleInt.setDataFormat((short) 1);
                HSSFCellStyle styleIntBold = wb.createCellStyle();
                styleIntBold.cloneStyleFrom(styleInt);
                styleIntBold.setDataFormat((short) 1);
                styleIntBold.setFont(fontBold);

                HSSFCellStyle styleFloat = wb.createCellStyle();
                styleFloat.cloneStyleFrom(styleParent);
                styleFloat.setDataFormat((short) 2);
                HSSFCellStyle styleFloatBold = wb.createCellStyle();
                styleFloatBold.cloneStyleFrom(styleFloat);
                styleFloatBold.setDataFormat((short) 2);
                styleFloatBold.setFont(fontBold);


                HSSFCellStyle stylePrcnt = wb.createCellStyle();
                stylePrcnt.cloneStyleFrom(styleParent);
                stylePrcnt.setDataFormat((short) 0xa);
                HSSFCellStyle stylePrcntBold = wb.createCellStyle();
                stylePrcntBold.cloneStyleFrom(stylePrcnt);
                stylePrcntBold.setFont(fontBold);

                HSSFRow row;
                HSSFCell cell;
                short rowNum = 999;
                short colNum = 10;
                for (int i = 3; i < rowNum; i++) {
                    row = sheet.getRow(i);
                    if (row == null) {
                        break;
                    }
                    for (int j = 2; j < colNum; j++) {

                        cell = row.getCell(j);
                        if (cell != null) {
                            CellValue cellValue = evaluator.evaluate(cell);
                            if (cellValue != null) {
                                boolean isFontBold =
                                        (cell.getCellStyle().getFont(wb).getBoldweight() ==
                                        HSSFFont.BOLDWEIGHT_BOLD) ? true : false;

                                String strValue = cellValue.getStringValue();
                                if (strValue.contains("$")) {
                                    try {
                                        DecimalFormat df = new DecimalFormat("$#,##0.00");
                                        df.setParseBigDecimal(true);
                                        BigDecimal bdValue = (BigDecimal) df.parse(strValue);
                                        cell.setCellValue(bdValue.doubleValue());
                                        if (isFontBold) {
                                            cell.setCellStyle(styleCurrBold);
                                        } else {
                                            cell.setCellStyle(styleCurr);
                                        }

                                        continue;
                                    } catch (ParseException ex) {
                                        Logger.getLogger(FileSystemBean.class.getName()).log(Level.INFO, null, ex);
                                    }
                                } else if (strValue.contains(".")) {
                                    try {
                                        DecimalFormat df = new DecimalFormat("0.00");
                                        df.setParseBigDecimal(true);
                                        BigDecimal bdValue = (BigDecimal) df.parse(strValue);
                                        if (!strValue.contains("%")) {
                                            cell.setCellValue(bdValue.doubleValue());
                                            if (isFontBold) {
                                                cell.setCellStyle(styleFloatBold);
                                            } else {
                                                cell.setCellStyle(styleFloat);
                                            }


                                        } else {
                                            cell.setCellValue(bdValue.doubleValue() / 100);
                                            if (isFontBold) {
                                                cell.setCellStyle(stylePrcntBold);
                                            } else {
                                                cell.setCellStyle(stylePrcnt);
                                            }
                                        }
                                        continue;
                                    } catch (ParseException ex) {
                                        Logger.getLogger(FileSystemBean.class.getName()).log(Level.INFO, null, ex);
                                    }
                                } else {
                                    if (strValue.contains("NA")) {
                                        continue;
                                    }
                                    try {
                                        Integer intValue = Integer.parseInt(strValue);
                                        cell.setCellValue((double) intValue);
                                        if (isFontBold) {
                                            cell.setCellStyle(styleIntBold);
                                        } else {
                                            cell.setCellStyle(styleInt);
                                        }
                                        continue;
                                    } catch (NumberFormatException ex) {
                                        Logger.getLogger(FileSystemBean.class.getName()).log(Level.INFO, null, ex);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            sheet.setMargin(HSSFSheet.TopMargin, (double) 0.5);
            sheet.setMargin(HSSFSheet.BottomMargin, (double) 0.5);
            sheet.setMargin(HSSFSheet.LeftMargin, (double) 0.05);
            sheet.setMargin(HSSFSheet.RightMargin, (double) 0.05);

            sheet.setAutobreaks(true);
            sheet.createFreezePane(0, 3);
            wb.setRepeatingRowsAndColumns(0, 0, 0, 0, 3);
            
            HSSFPrintSetup ps = sheet.getPrintSetup();
            ps.setFitWidth((short) 1);
            ps.setFitHeight((short) 9999);
            sheet.setGridsPrinted(false);
            sheet.setHorizontallyCenter(true);
            ps.setPaperSize(HSSFPrintSetup.LETTER_PAPERSIZE);
            ps.setHeaderMargin((double) .35);
            ps.setFooterMargin((double) .35);

            OutputStream os;
            os = response.getOutputStream();
            wb.write(os);
            bos.flush();
            bos.close();
            input.close();
            os.flush();
            os.close();
            context.responseComplete();
        } catch (IOException ex) {
            Logger.getLogger(FileSystemBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(FileSystemBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;

    }

    public String deleteFile() {
        String toReturn = null;
        if (selectedFile == null) {
            return toReturn;
        }
        if (!selectedFile.getFolder().isUploaded()) {

            List<ReportFile> lstFiles = new ArrayList<ReportFile>();
            try {
                lstFiles = dao.get("from ReportFile where folder.uploaded=false and dateReport='" + formatter.valueToString(selectedFile.getDateReport()) + "'");
            } catch (ParseException ex) {
                Logger.getLogger(FileSystemBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            dao.deleteAll(lstFiles);
            userManagedBean.refreshReportingWeeks();
            fillMapMonths();
            toReturn = thisPage;
        } else {
            dao.delete(selectedFile);
        }
        return toReturn;
    }

    private String formateFileName() {
        SimpleDateFormat fmt = new SimpleDateFormat("mmddyy");
        String filename = "DS"+fmt.format(new Date());
        return filename;
    }
}
