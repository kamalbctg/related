package com.zuma.web.filemanager;

import com.zuma.db.core.domain.Folder;
import com.zuma.db.core.domain.ReportFile;
import com.zuma.db.dao.UniversalDAO;

import com.zuma.db.service.util.Week;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.OutputStream;
import org.richfaces.event.UploadEvent;
import org.richfaces.model.UploadItem;

import java.io.FileInputStream;
import java.io.Serializable;
import java.util.Calendar;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class FileUploadBean implements Serializable {

    @Autowired
    protected UniversalDAO dao;
    @Autowired
    protected FileSystemBean fileSystemBean;
    private int uploadsAvailable = 5;
    private boolean autoUpload = false;
    private boolean useFlash = false;
    private String reportName;

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getReportName() {
        return reportName;
    }
    private Folder uploadFolder;

    public Folder getUploadFolder() {
        return uploadFolder;
    }

    public FileUploadBean() {
    }

    public void listener(UploadEvent event) throws Exception {
         if (!fileSystemBean.getSelFolder().isUploaded() || fileSystemBean.getSelFolder()==null) return;

        UploadItem item = event.getUploadItem();
     
        if (item.isTempFile()) {
            java.io.File file = item.getFile();
           // System.out.println("createTempFiles=true");
        } else {

           // System.out.println("createTempFiles=false");
                String fileName=item.getFileName();
                if (fileName.lastIndexOf(File.separator)>0)
                    fileName=fileName.substring(fileName.lastIndexOf(File.separator)+1);
                if (fileName.length()>39)
                fileName=fileName.substring(0,35)+fileName.substring(fileName.lastIndexOf(".")).substring(0, 4);

                ReportFile dbFile = new ReportFile();
                dbFile.setName(fileName);
                dbFile.setSizeFile(item.getData().length);

                dbFile.setDateFile(new java.util.Date());
                dbFile.setReport(item.getData());                
                dbFile.setFolder(fileSystemBean.getSelFolder());

                Calendar clndr=Calendar.getInstance();
                clndr.setTime(Week.getWeekBegin(fileSystemBean.getCurrentWeek().getWeekDate()));
                clndr=Week.nullTime(clndr);
                dbFile.setDateReport(clndr.getTime());                
                dbFile.setPerson(fileSystemBean.currentUser);

                dao.saveOrUpdate(dbFile);

                uploadsAvailable--;        
            
        }
    }

 
    public long getTimeStamp() {
        return System.currentTimeMillis();
    }

    public int getUploadsAvailable() {
        return uploadsAvailable;
    }

    public void setUploadsAvailable(int uploadsAvailable) {
        this.uploadsAvailable = uploadsAvailable;
    }

    public boolean isAutoUpload() {
        return autoUpload;
    }

    public void setAutoUpload(boolean autoUpload) {
        this.autoUpload = autoUpload;
    }

    public boolean isUseFlash() {
        return useFlash;
    }

    public void setUseFlash(boolean useFlash) {
        this.useFlash = useFlash;
    }


    public String downloadDocument() {
        ReportFile rfItem = fileSystemBean.getSelectedFile();


        FacesContext context = FacesContext.getCurrentInstance();

//Then we have to get the Response where to write our file
        HttpServletResponse response =
                (HttpServletResponse) context.getExternalContext().getResponse();

//Now we create some variables we will use for writting the file to the response
       //  fileSystemBean.getSeletedFile();

//Be sure to retrieve the absolute path to the file with the required method


//Now set the content type for our response, be sure to use the best suitable content type depending on your file
//the content type presented here is ok for, lets say, text files and others (like  CSVs, PDFs)
        String fileName = rfItem.getName();
        String fileType = rfItem.getFileType();
        response.setContentType(MimeUtility.manageType(fileType));
        response.setHeader("Content-disposition", "attachment; filename=\"" + rfItem.getName() + "\"");
        //response.setHeader("Pragma", "no-cache");


//This is another important attribute for the header of the response
//Here fileName, is a String with the name that you will suggest as a name to save as
//I use the same name as it is stored in the file system of the server.
//
     /* response.setHeader("Content-Disposition", "attachment;filename=\"" +
        fileName + "\"");
         */

      

//Streams we will use to read, write the file bytes to our response
        FileInputStream fis = null;

        OutputStream os = null;

//First we load the file in our InputStream
        try {
            //  fis = new FileInputStream(new java.io.File(filePath,fileName));
            os = response.getOutputStream();

//While there are still bytes in the file, read them and write them to our OutputStream

            int read = 0;
            byte[] buff = rfItem.getReport();

            ByteArrayInputStream bas = new ByteArrayInputStream(buff);
            

            while ((read = bas.read(buff)) != -1) {
                os.write(buff, 0, read);
            }

//Clean resources
            os.flush();
            os.close();
        } catch (Exception e) {
            Logger log = Logger.getRootLogger();
                    log.info("I/O Stream Exception", e);
        }
//This option isn't quite necessary, It worked for me with or without it

        context.responseComplete();
        return null;
    }

}
