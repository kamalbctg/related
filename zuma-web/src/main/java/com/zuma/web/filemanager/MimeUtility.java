
package com.zuma.web.filemanager;

public class MimeUtility {
	public static final String EXCEL_CONTENT_TYPE = "application/vnd.ms-excel";

	public static final String WORD_CONTENT_TYPE = "application/vnd.ms-word";

	public static final String HTML_CONTENT_TYPE = "text/html";

	public static final String PDF_CONTENT_TYPE = "application/pdf";

	public static final String ZIP_CONTENT_TYPE = "application/x-zip";

	public static final String RTF_CONTENT_TYPE = "application/rtf";

	public static final String PPT_CONTENT_TYPE = "application/vnd.ms-powerpoint";

	public static final String PPS_CONTENT_TYPE = "application/vnd.ms-powerpoint";

	public static final String EML_CONTENT_TYPE = "application/vnd.ms-outlook";

	public static final String SXW_CONTENT_TYPE = "application/vnd.sun.xml.writer";

	public static final String STW_CONTENT_TYPE = "application/vnd.sun.xml.writer.template";

	public static final String SXG_CONTENT_TYPE = "application/vnd.sun.xml.writer.global";

	public static final String SXC_CONTENT_TYPE = "application/vnd.sun.xml.calc";

	public static final String STC_CONTENT_TYPE = "application/vnd.sun.xml.calc.template";

	public static final String SXI_CONTENT_TYPE = "application/vnd.sun.xml.impress";

	public static final String STI_CONTENT_TYPE = "application/vnd.sun.xml.impress.template";

	public static final String SXD_CONTENT_TYPE = "application/vnd.sun.xml.draw";

	public static final String STD_CONTENT_TYPE = "application/vnd.sun.xml.draw.template";

	public static final String SXM_CONTENT_TYPE = "application/vnd.sun.xml.math";

	public static final String EXCEL2007_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

	public static final String WORD2007_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

	public static final String PPT2007_CONTENT_TYPE = "application/vnd.openxmlformats-officedocument.presentationml.presentation";


	protected static String manageType(String type) {
		String result;
		if (type.equalsIgnoreCase("html"))
			result = HTML_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("pdf"))
			result = PDF_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("xls"))
			result = EXCEL_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("doc"))
			result = WORD_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("rtf"))
			result = RTF_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("sxw"))
			result = SXW_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("stw"))
			result = STW_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("sxg"))
			result = SXG_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("sxc"))
			result = SXC_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("stc"))
			result = STC_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("sxi"))
			result = SXI_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("sti"))
			result = STI_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("sxd"))
			result = SXD_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("std"))
			result = STD_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("sxm"))
			result = SXM_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("msg"))
			result = EML_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("ppt"))
			result = PPT_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("pps"))
			result = PPS_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("zip"))
			result = ZIP_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("eml"))
			result = EML_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("txt"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("ico"))
			result = "image/x-icon";
		else if (type.equalsIgnoreCase("xlc"))
			result = EXCEL_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("xlt"))
			result = EXCEL_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("xla"))
			result = EXCEL_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("text"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("bib"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("err"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("faq"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("log"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("csv"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("sql"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("xml"))
			result = "application/xml";
		else if (type.equalsIgnoreCase("xsl"))
			result = "application/xml";
		else if (type.equalsIgnoreCase("xslt"))
			result = "application/xml";
		else if (type.equalsIgnoreCase("js"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("jsp"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("jspx"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("css"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("php"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("php3"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("php4"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("php5"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("asp"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("aspx"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("cfm"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("htx"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("java"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("properties"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("c"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("cpp"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("h"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("cgi"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("dtd"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("pl"))
			result = "text/plain";
		else if (type.equalsIgnoreCase("rar"))
			result = ZIP_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("gz"))
			result = ZIP_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("bz2"))
			result = ZIP_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("pst"))
			result = EML_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("tiff"))
			result = "image/tiff";
		else if (type.equalsIgnoreCase("tif"))
			result = "image/tiff";
		else if (type.equalsIgnoreCase("g3f"))
			result = "image/g3fax";
		else if (type.equalsIgnoreCase("g3"))
			result = "image/g3fax";
		else if (type.equalsIgnoreCase("xlsx"))
			result = EXCEL2007_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("docx"))
			result = WORD2007_CONTENT_TYPE;
		else if (type.equalsIgnoreCase("pptx"))
			result = PPT2007_CONTENT_TYPE;
		else
			result = "";
		return result;
	}
}
