package com.zuma.web.filemanager;

import org.richfaces.component.UITree;
import org.richfaces.component.state.TreeStateAdvisor;
import org.richfaces.model.TreeNode;
import org.richfaces.model.TreeRowKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class TreeFolderStateAdvisor  implements TreeStateAdvisor {
   @Autowired
    protected FileSystemBean fileSystemBean;

    @Override
    public Boolean adviseNodeOpened(UITree tree) {
        if (!PostbackPhaseListener.isPostback()) {
            Object key = tree.getRowKey();
            TreeRowKey treeRowKey = (TreeRowKey) key;
            if (treeRowKey == null || treeRowKey.depth() <= 5) {
                return Boolean.TRUE;
            }
        }

        return null;
    }

    @Override
    public Boolean adviseNodeSelected(UITree tree) {
        TreeNode node = tree.getModelTreeNode(tree.getRowKey());
        if (node != null && node == fileSystemBean.getCurrentNode()) {
            return Boolean.TRUE;
        }
        else
            return null;
    }
}
