package com.zuma.web.filters;

import java.io.IOException;

import javax.faces.FactoryFinder;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.lifecycle.Lifecycle;
import javax.faces.lifecycle.LifecycleFactory;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.zuma.db.core.domain.Page;
import com.zuma.web.controllers.UserManagedBean;

public class LoginFilter implements Filter {

    private FilterConfig config;
    private ServletContext servletContext;
    static private String loginPageName = "welcome";
    private String prefix = "/";
    static private String suffixFaces = ".faces";
    static private String suffixJSP = ".jsp";
    static private Logger log = Logger.getLogger(LoginFilter.class.getName());

    // You need an inner class to be able to call
    // FacesContext.setCurrentInstance
    // since it's a protected method
    private abstract static class InnerFacesContext extends FacesContext {

        protected static void setFacesContextAsCurrentInstance(
                FacesContext facesContext) {
            FacesContext.setCurrentInstance(facesContext);
        }
    }

    private FacesContext getFacesContext(ServletRequest request,
            ServletResponse response) {
        // Try to get it first
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if (facesContext != null) {
            return facesContext;
        }

        FacesContextFactory contextFactory = (FacesContextFactory) FactoryFinder.getFactory(FactoryFinder.FACES_CONTEXT_FACTORY);
        LifecycleFactory lifecycleFactory = (LifecycleFactory) FactoryFinder.getFactory(FactoryFinder.LIFECYCLE_FACTORY);
        Lifecycle lifecycle = lifecycleFactory.getLifecycle(LifecycleFactory.DEFAULT_LIFECYCLE);

        // Doesn't set this instance as the current instance of
        // FacesContext.getCurrentInstance
        facesContext = contextFactory.getFacesContext(servletContext, request,
                response, lifecycle);

        // Set using our inner class
        InnerFacesContext.setFacesContextAsCurrentInstance(facesContext);

        // set a new viewRoot, otherwise context.getViewRoot returns null
        UIViewRoot view = facesContext.getApplication().getViewHandler().createView(facesContext, "LoginFilterID");
        facesContext.setViewRoot(view);

        return facesContext;
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        this.config = config;
        servletContext = config.getServletContext();
    }

    @Override
    public void destroy() {
        config = null;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        prefix = getUrlPrefix((HttpServletRequest) request);
        FacesContext facesContext = getFacesContext(request, response);
        UserManagedBean userManagedBean = null;
        //System.out.println(":::Created doFilter"); // DEBUG
        try {
            if (facesContext != null) {
                userManagedBean = (UserManagedBean) facesContext.getApplication().getELResolver().getValue(
                        facesContext.getELContext(), null, "userManagedBean");
            } else {
                log.debug("Error to access UserManagedBean. facesContext is null.");
            }
        } catch (RuntimeException e) {
            log.debug(e.getMessage());
            ((HttpServletResponse) response).sendRedirect(getRedirectPage());
            return;
        }

        // Check session:
        if (facesContext.getExternalContext().getSession(false) == null) {
            // System.out.println(":::Session time out - redirect from Filter");
            throw new com.zuma.web.exceptions.SecurityResponseException("Session time out.");
        }

        boolean access = false;
        Page securePage = null;

        String url = "";
        if (request instanceof HttpServletRequest && (response instanceof HttpServletResponse)) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpServletResponse httpResponse = (HttpServletResponse) response;

            url = getPageName(httpRequest.getRequestURI()); // refresh prefix;

            // is session invalid?
            if (isSessionInvalid(httpRequest)) {
                // userManagedBean.logout();
                // is session expire control required for this request?
                if (isSessionControlRequiredForThisResource(httpRequest)) {
                    log.info("Session is invalid! redirecting to: " + getRedirectPage());
                    httpResponse.sendRedirect(getRedirectPage());
                    return;
                }
            }

            // if no secure - welcome for all!
            boolean secure = true;
            securePage = userManagedBean.getSecurePage(url);
            if (securePage == null) {
                access = true;
                secure = false;
            }

            if (secure) {
                access = userManagedBean.isAccessToPage(url, access);
            }

            if (!access) {
                boolean canNotRedirect;
                canNotRedirect = (httpResponse == null);
                if (canNotRedirect) {
                    //System.out.println("=== response==null");
                    HttpServletResponse newResponse;
                    newResponse = ((HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse());
                    if (newResponse != null) {
                        newResponse.sendRedirect(getRedirectPage());
                        //System.out.println("=== newresponse!=null");
                    } else {
                        //System.out.println("=== newresponse==null");
                        throw new com.zuma.web.exceptions.SecurityResponseException("Page " + url + " was expired.");
                    }
                    // if (context!=null) context.redirect(getRedirectPage());
                    /*
                    if(servletContext!=null)
                    {
                    servletContext.getRequestDispatcher(getRedirectPage()).forward(request, response);
                    }
                    else
                    throw new com.zuma.web.exceptions.SecurityResponseException("Page "+url+" was expired.");
                     */

                } else {
                    httpResponse.sendRedirect(getRedirectPage());
                    //System.out.println("== response != null");
                    //System.out.println(httpResponse.toString());
                }


                return;
            }
        } // request
        if (access) {
            chain.doFilter(request, response);
        }
        // debug info: ==========================================
        /*
        log.debug("Pass Login Filter: " + url);
        log.debug("Access: " + (access ? "permit" : "denied"));
        Person user = userManagedBean.getActiveUser();
        if (user != null) {
        log.debug("User: " + user.getFirstName() + " " + user.getLastName());
        } else {
        log.debug("No user loggined.");
        }
         */
        // =======================================================
    }

    private boolean isSessionControlRequiredForThisResource(HttpServletRequest httpServletRequest) {
        String requestPath = httpServletRequest.getRequestURI();
        return !requestPath.equals(prefix + loginPageName + suffixJSP);
    }

    // timeout session part
    private boolean isSessionInvalid(HttpServletRequest httpServletRequest) {
        boolean sessionInValid = (httpServletRequest.getRequestedSessionId() != null) && !httpServletRequest.isRequestedSessionIdValid();
        return sessionInValid;
    }

    public String getUrlPrefix(HttpServletRequest request) {
        StringBuffer buf = new StringBuffer();
        buf.append(request.getScheme());
        buf.append("://");
        if (!request.getServerName().isEmpty()) {
            buf.append(request.getServerName());
        }
        if (request.getLocalPort() > 0) {
            buf.append(":");
            buf.append(request.getLocalPort());
        }
        if (!request.getContextPath().isEmpty()) {
            buf.append(request.getContextPath());
        }
        buf.append("/");
        String result = buf.toString();
        result = result.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
        result = result.replaceAll("eval\\((.*)\\)", "");
        result = result.replaceAll("[\\\"\\\'][\\s]*((?i)javascript):(.*)[\\\"\\\']", "\"\"");
        result = result.replaceAll("((?i)script)", "");

        return result;
    }

    private String getPageName(String url) {
        String result = "";
        int delim1 = url.lastIndexOf(suffixFaces);
        int delim2 = url.lastIndexOf(suffixJSP);
        int dotDelim = (delim1 >= 0) ? delim1 : delim2;

        if (dotDelim == -1) {
            return result;
        }
        int lineDelim = url.lastIndexOf("/", dotDelim);
        try {
            result = url.substring(lineDelim + 1, dotDelim);
        } catch (IndexOutOfBoundsException e) {
            log.debug(e.getMessage());
        }

        prefix = url.substring(0, lineDelim + 1);
        // System.out.println("::: url:"+url);
        // System.out.println("::: prefix:"+prefix);
        // System.out.println("::: name:"+result);
        return result;
    }

    private String getRedirectPage() {
        String result = prefix + loginPageName + suffixFaces;
        // System.out.println("::: redirectPage:"+result);
        return result;
    }
}
