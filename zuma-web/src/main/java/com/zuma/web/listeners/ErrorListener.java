/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class ErrorListener implements PhaseListener {

    // Actions ------------------------------------------------------------------------------------
    /**
     * @see javax.faces.event.PhaseListener#getPhaseId()
     */
    @Override
    public PhaseId getPhaseId() {

        // Listen on render response phase.
        return PhaseId.RENDER_RESPONSE;
    }

    /**
     * @see javax.faces.event.PhaseListener#beforePhase(javax.faces.event.PhaseEvent)
     */
    @Override
    public void beforePhase(PhaseEvent event) {

        // Init.
        FacesContext facesContext = event.getFacesContext();
        String focus = null;
        StringBuilder highlight = new StringBuilder();

        // Iterate over all client ID's with messages.
        Iterator<String> clientIdsWithMessages = facesContext.getClientIdsWithMessages();
        while (clientIdsWithMessages.hasNext()) {
            String clientIdWithMessages = clientIdsWithMessages.next();
            if (focus == null) {
                focus = clientIdWithMessages;
            }
            highlight.append(clientIdWithMessages);
            if (clientIdsWithMessages.hasNext()) {
                highlight.append(",");
            }
        }
        int count = 0;
        Collection<FacesMessage> messages = new ArrayList<FacesMessage>();
        Iterator it = facesContext.getMessages();
        while (it.hasNext()) {
            FacesMessage mes = (FacesMessage) it.next();
            if (exist(messages, mes)) {
                mes.setSummary(null);
            } else {
                messages.add(mes);
            }
            count++;
        }


        // Set ${focus} and ${highlight} in JSP.
        facesContext.getExternalContext().getRequestMap().put("focus", focus);
        facesContext.getExternalContext().getRequestMap().put("highlight", highlight.toString());
        facesContext.getExternalContext().getRequestMap().put("ErrorCount", String.valueOf(count));

        // facesContext.getExternalContext().getRequest().

    }

    /**
     * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
     */
    @Override
    public void afterPhase(PhaseEvent event) {
        // Do nothing.
    }

    boolean exist(Collection<FacesMessage> messages, FacesMessage mes) {
        for (FacesMessage tmp : messages) {
            if (tmp.getSummary().equals(mes.getSummary())) {
                return true;
            }
        }
        return false;
    }
}
