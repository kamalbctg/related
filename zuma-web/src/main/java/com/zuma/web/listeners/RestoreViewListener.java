package com.zuma.web.listeners;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class RestoreViewListener implements PhaseListener {
    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }

    @Override
    public void afterPhase(PhaseEvent event) {
    }
    
    // http://www.jsfcentral.com/listings/A92000?link
    // http://forums.java.net/jive/thread.jspa?messageID=347643
    // http://devgrok.blogspot.com/2009/07/access-control-using-phaselistener.html
    @Override
    public void beforePhase(PhaseEvent event) {
        FacesContext fc = event.getFacesContext();
        if (fc.getExternalContext().getSession(false) == null) {
             String viewId = fc.getViewRoot().getViewId();
             System.out.println(":::expire - "+getPageName(viewId));
             fc.getApplication().getNavigationHandler().handleNavigation(fc, null, "viewexpire");
        }
    }

    private String getPageName(String url) {
        String result = "";
        int delim1 = url.lastIndexOf(".faces");
        int delim2 = url.lastIndexOf(".jsp");
        int dotDelim = (delim1 >= 0) ? delim1 : delim2;

        if (dotDelim == -1) {
            return result;
        }
        int lineDelim = url.lastIndexOf("/", dotDelim);
        result = url.substring(lineDelim + 1, dotDelim);

        return result;
    }
}
