package com.zuma.web.listeners;

import com.zuma.web.controllers.UserManagedBean;
import java.util.Date;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import org.apache.log4j.Logger;

/**
 * When the user session timedout, ({@link sessionDestroyed(HttpSessionEvent)}
 * )method will be invoked. This method will make necessary cleanups (logging
 * out user, updating db and audit logs, etc...) As a result; after this method,
 * we will be in a clear and stable state. So nothing left to think about
 * because session expired, user can do nothing after this point.
 * 
 * Thanks to hturksoy
 **/
public class TimeOutSessionListener implements HttpSessionListener {

	static private Logger log = Logger.getLogger(HttpSessionListener.class
			.getName());

	public TimeOutSessionListener() {
	}

	@Override
	public void sessionCreated(HttpSessionEvent event) {
		log.debug("Current Session created : " + event.getSession().getId()
				+ " at " + new Date());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		// get the destroying session...
		HttpSession session = event.getSession();
		log.debug("Current Session destroyed :" + session.getId()
				+ " Logging out user...");
		/*
		 * nobody can reach user data after this point because session is
		 * invalidated already. So, get the user data from session and save its
		 * logout information before losing it. User's redirection to the
		 * timeout page will be handled by the SessionTimeoutFilter.
		 */

		// Only if needed
		try {
			prepareLogoutInfoAndLogoutActiveUser(session);
		} catch (Exception e) {
			log.debug("Error while logging out at session destroyed : "
					+ e.getMessage());
		}
	}

	/**
	 * Clean your logout operations.
	 */
	private void prepareLogoutInfoAndLogoutActiveUser(HttpSession httpSession) {
		// Need to call UsermangedBean.LoOut();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		if (facesContext != null) {
			UserManagedBean userManagedBean = (UserManagedBean) facesContext
					.getApplication().getELResolver().getValue(
							facesContext.getELContext(), null,
							"userManagedBean");
			if (userManagedBean != null) // TODO getNotNull context liki
											// LoginFilter
				userManagedBean.logout();
		} else
			log
					.debug("Error to access UserManagedBean.logout. facesContext is null.");
	}
}
