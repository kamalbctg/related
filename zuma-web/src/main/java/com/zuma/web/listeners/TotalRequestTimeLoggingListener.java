package com.zuma.web.listeners;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;

public class TotalRequestTimeLoggingListener implements ServletRequestListener {
	static private Logger logger = Logger
			.getLogger(TotalRequestTimeLoggingListener.class);
	private static final String TOKEN = "token";
	private static final String BEGINING_TIME = "beginTime";
	public static String FACES_EXTENTION = "faces";
	public static String A4J_PATH = "a4j";

	public void requestInitialized(ServletRequestEvent event) {
		String requestURI = getRequestString(event);

		if (isFacesPage(event, requestURI)) {
			long currentTimeMilis = System.currentTimeMillis();
			String randomToken = RandomStringUtils.randomAlphabetic(15);

			event.getServletRequest().setAttribute(BEGINING_TIME,
					currentTimeMilis);
			event.getServletRequest().setAttribute(TOKEN, randomToken);

			logger.info("Started request to " + requestURI + " (id="
					+ randomToken + ")...");
		}
	}

	public void requestDestroyed(ServletRequestEvent event) {

		if (isFacesPage(event)) {
			String requestURI = getRequestString(event);

			long currentTimeMilis = System.currentTimeMillis();
			String token = (String) event.getServletRequest().getAttribute(
					TOKEN);
			long totalMilisTime = currentTimeMilis
					- (Long) event.getServletRequest().getAttribute(
							BEGINING_TIME);
			double totalSecondsTime = totalMilisTime / 1000.0;
			logger.info("Finished request to " + requestURI + " (id=" + token
					+ ") total time = " + totalSecondsTime + " s");

		}
	}

	private String getRequestString(ServletRequestEvent event) {
		return ((HttpServletRequest) event.getServletRequest()).getRequestURI();
	}

	private boolean isFacesPage(ServletRequestEvent event) {
		String requestURI = getRequestString(event);
		return isFacesPage(event, requestURI);
	}

	private boolean isFacesPage(ServletRequestEvent event, String requestURI) {
		boolean facesPage = true;

		String requestExtension = FilenameUtils.getExtension(requestURI);
		facesPage &= requestExtension.equals(FACES_EXTENTION);
		facesPage &= !requestURI.contains(A4J_PATH);

		return facesPage;
	}
}