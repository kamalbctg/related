package com.zuma.web.menu;

public abstract class MapProcessor {
        public boolean isEqual(Object key,Object property) {
            return key.equals(property);
        }
        abstract public Object run(Object key,Object property);
}
