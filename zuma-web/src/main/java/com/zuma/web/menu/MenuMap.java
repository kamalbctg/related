package com.zuma.web.menu;

import java.util.HashMap;

public class MenuMap extends HashMap {
    private Object property;
    private MapProcessor processor;

    public MenuMap() {
    }

    public MenuMap(Object property, MapProcessor processor) {
        this.property  = property;
        this.processor = processor;
    }

    @Override
    public Object get(Object o) {
        return (processor != null) ? (String) processor.run((String) o, property) : "";
    }

    public Object getProperty() {
        return property;
    }

    public void setProperty(Object property) {
        this.property = property;
    }

    public MapProcessor getProcessor() {
        return processor;
    }

    public void setProcessor(MapProcessor processor) {
        this.processor = processor;
    }
}