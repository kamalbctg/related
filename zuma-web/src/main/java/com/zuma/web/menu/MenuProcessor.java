package com.zuma.web.menu;

public class MenuProcessor extends MapProcessor {

    public String trueString = "true";
    public String falseString = "false";

    public MenuProcessor() {
    }

    public MenuProcessor(String trueString, String falseString) {
        this.trueString = trueString;
        this.falseString = falseString;
    }

    @Override
    public Object run(Object key, Object property) {
        return isEqual(key, property) ? trueString : falseString;
    }

    public void setTrueString(String trueString) {
        this.trueString = trueString;
    }

    public void setFalseString(String falseString) {
        this.falseString = falseString;
    }
}
