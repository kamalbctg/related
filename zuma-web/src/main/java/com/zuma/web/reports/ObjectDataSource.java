package com.zuma.web.reports;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JRRewindableDataSource;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class ObjectDataSource implements JRRewindableDataSource {
    private Collection list;
    private Iterator iterator;
    private Object currentValue;
    private static Map<String, String> fieldNameMap = new HashMap<String, String>();

    public ObjectDataSource(Collection list) {        
        this.list = list;
        this.iterator = list.iterator();
    }
    // end ObjectDataSource

    @Override
    public Object getFieldValue(JRField field) throws JRException {
        Object o = null;
        try {
            
             System.out.println(""+field.getName());
            String nameField = getFieldName(field.getName());
            System.out.println(""+nameField);
            o = PropertyUtils.getProperty(currentValue, nameField);
            
        } catch (IllegalAccessException ex) {
            Logger.getLogger(this.getClass()).log(Level.INFO, null, ex);
        } catch (InvocationTargetException ex) {
            Logger.getLogger(this.getClass()).log(Level.INFO, null, ex);
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(this.getClass()).log(Level.INFO, null, ex);
        }
        return o;
    }
    // end getFieldValue

    @Override
    public boolean next() throws JRException {
        currentValue = iterator.hasNext() ? iterator.next() : null;
        return (currentValue != null);
    }

    @Override
    public void moveFirst() throws JRException {
        this.iterator = list.iterator();
        currentValue = null;
    }

    private String getFieldName(String fieldName) {
        String filteredFieldName = (String) fieldNameMap.get(fieldName);
        if (filteredFieldName == null) {
            filteredFieldName = fieldName.replace('_', '.');
            fieldNameMap.put(fieldName, filteredFieldName);
        }
        return filteredFieldName;
    }
}
