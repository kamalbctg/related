/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.reports;

import com.zuma.web.controllers.UserManagedBean;
import com.zuma.web.util.FileUtil;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;


public class ParentReport {

    private String templatePath;
    private JRDataSource dataSource;
    protected Map<String, Object> parameters= new HashMap<String, Object>();

     public ParentReport(Map<String, Object> paramReport) {
        parameters.putAll(paramReport);
    }

    public ParentReport(Map<String, Object> paramReport, JRDataSource dataSource) {
        this(paramReport);
        this.dataSource = dataSource;
    }

    public void addParameter(String name, Object o) {
        parameters.put(name, o);
    }

    protected void compileReport(String source, String dest) throws JRException {
        JasperCompileManager.compileReportToFile(source, dest);
    }

    protected JasperReport getCompiledReport(String path) throws JRException {
        String compiled = ".jasper";
        String compiledPath = FileUtil.changeExtension(path, compiled);
        File fl1 = new File(compiledPath);
        if (!fl1.exists()) {
            compileReport(path, compiledPath);
        }
        return (JasperReport) JRLoader.loadObjectFromLocation(compiledPath);
    }

    protected void prepareJasperReports() throws JRException {
    }

    protected InputStream getTemplate(String path) throws FileNotFoundException {
        //FacesContext context = FacesContext.getCurrentInstance();
        //InputStream is = context.getExternalContext().getResourceAsStream(path);
        FileInputStream is = new FileInputStream(path);
        return is;
    }

    protected InputStream getTemplate() throws Exception {
        return getTemplate(getTemplatePath());
    }

    protected String getTemplatePath() {
        return templatePath;
    }

    protected void setTemplatePath(String templatePath) {
        this.templatePath = templatePath;
    }

    public void exportToPDF(OutputStream os) throws JRException {
        JasperExportManager.exportReportToPdfStream(getJasperPrint(), os);
    }

    public void exportToExcel(OutputStream os) throws JRException {
        JasperPrint jasperPrint = getJasperPrint();
        JRXlsExporter exporter = new JRXlsExporter();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
        exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
        exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
        exporter.setParameter(JRXlsExporterParameter.IGNORE_PAGE_MARGINS, Boolean.TRUE);
        exporter.setParameter(JRXlsExporterParameter.IS_IGNORE_CELL_BORDER,Boolean.TRUE);
        exporter.setParameter(JRXlsExporterParameter.IS_FONT_SIZE_FIX_ENABLED, Boolean.TRUE);
        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, byteArrayOutputStream);
        exporter.exportReport();

        try {
            os.write(byteArrayOutputStream.toByteArray());
        } catch (IOException ex) {
            Logger.getLogger(ParentReport.class.getName()).log(Level.SEVERE, "Error writing to output stream", ex);
        } finally {
            if (byteArrayOutputStream != null) {
                try {
                    byteArrayOutputStream.close();
                    byteArrayOutputStream.flush();
                } catch (IOException e) {
                    Logger.getLogger(ParentReport.class.getName()).log(Level.SEVERE, "Error writing to output stream", e);
                }
            }
        }
    }

    public JRDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(JRDataSource dataSource) {
        this.dataSource = dataSource;
    }

    protected JasperPrint getJasperPrint() throws JRException {
        prepareJasperReports();
        JasperReport jasperReport = getCompiledReport(getTemplatePath());
        JasperPrint jasperPrint = JasperFillManager.fillReport(
                jasperReport, parameters, dataSource);
        return jasperPrint;
    }
}
