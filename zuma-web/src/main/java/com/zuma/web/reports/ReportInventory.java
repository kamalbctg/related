/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.reports;

import com.zuma.web.controllers.helper.waste.WasteDataSet;
import java.io.File;

import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JRException;

public class ReportInventory extends ParentReport {

    public ReportInventory(Map<String, Object> paramReport, WasteDataSet data)  {
        super(paramReport);
        parameters.put("rName", "Inventory");

        FacesContext fcontext = FacesContext.getCurrentInstance();
        ServletContext scontext = (ServletContext) fcontext.getExternalContext().getContext();
        String path = scontext.getRealPath(File.separator);
        parameters.put("SUBREPORT_DIR", path + "reports" + File.separator);

        setTemplatePath(path + "reports" + File.separator + "reportInventory.jrxml");
        setDataSource(data.getDataSource());
    }

    @Override
    protected void prepareJasperReports()throws JRException {
        String file1 = "reportInventory.jrxml";
        String file2 = "subcatInv.jrxml";
        String file3 = "itemsInv.jrxml";

        FacesContext fcontext = FacesContext.getCurrentInstance();
        ServletContext scontext = (ServletContext) fcontext.getExternalContext().getContext();
        String path = scontext.getRealPath(File.separator);
        path += "reports" + File.separator;

        getCompiledReport(path + file3);
        getCompiledReport(path + file2);
        getCompiledReport(path + file1);


    }
}
