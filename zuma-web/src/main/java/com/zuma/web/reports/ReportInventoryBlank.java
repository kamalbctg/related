/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.reports;


import com.zuma.web.controllers.helper.waste.WasteDataSet;
import java.io.File;


import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JRException;

public class ReportInventoryBlank extends ParentReport {

    public ReportInventoryBlank(Map<String, Object> paramReport,WasteDataSet data)  {
        super(paramReport);
        parameters.put("rName", "Blank Inventory Form");

        FacesContext fcontext = FacesContext.getCurrentInstance();
        ServletContext scontext = (ServletContext) fcontext.getExternalContext().getContext();
        String path = scontext.getRealPath(File.separator);
        parameters.put("SUBREPORT_DIR", path + "reports" + File.separator);

        setTemplatePath(path + "reports" + File.separator + "reportInventoryBlank.jrxml");
        setDataSource(data.getDataSource());
    }

    @Override
    protected void prepareJasperReports()throws JRException {
        String file1 = "reportInventoryBlank.jrxml";
        String file2 = "subcatInvBlank.jrxml";
        String file3 = "itemsInvBlank.jrxml";

        FacesContext fcontext = FacesContext.getCurrentInstance();
        ServletContext scontext = (ServletContext) fcontext.getExternalContext().getContext();
        String path = scontext.getRealPath(File.separator);
        path += "reports" + File.separator;

        getCompiledReport(path + file3);
        getCompiledReport(path + file2);
        getCompiledReport(path + file1);


    }
}
