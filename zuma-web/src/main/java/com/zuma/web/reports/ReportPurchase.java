package com.zuma.web.reports;

import com.zuma.db.core.domain.InvoiceDetail;
import java.util.List;
import java.io.File;

import java.math.BigDecimal;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JRException;

public class ReportPurchase extends ParentReport {

    public ReportPurchase(Map<String, Object> paramReport, List<InvoiceDetail> data) {
        super(paramReport);
        parameters.put("rName", "Purchase Journal");

        FacesContext fcontext = FacesContext.getCurrentInstance();
        ServletContext scontext = (ServletContext) fcontext.getExternalContext().getContext();
        String path = scontext.getRealPath(File.separator);
        parameters.put("SUBREPORT_DIR", path + "reports" + File.separator);

        setTemplatePath(path + "reports" + File.separator + "reportInvoice.jrxml");
        BigDecimal result = BigDecimal.ZERO;

        for (InvoiceDetail invoice : data) {
            result = result.add(invoice.getPrice());

        }
        parameters.put("rSumTotal", result);
        setDataSource(new ObjectDataSource(data));
    }

    @Override
    protected void prepareJasperReports() throws JRException {
        String file1 = "reportInvoice.jrxml";

        FacesContext fcontext = FacesContext.getCurrentInstance();
        ServletContext scontext = (ServletContext) fcontext.getExternalContext().getContext();
        String path = scontext.getRealPath(File.separator);
        path += "reports" + File.separator;

        getCompiledReport(path + file1);
    }
}
