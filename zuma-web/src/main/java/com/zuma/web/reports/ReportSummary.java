/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.reports;

import com.zuma.web.controllers.helper.summary.SummaryDataSet;
import java.io.File;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JRException;

public class ReportSummary extends ParentReport {

    public ReportSummary(Map<String, Object> paramReport, SummaryDataSet data) {
        super(paramReport);
        parameters.put("rName", "Daily Summary");
        FacesContext fcontext = FacesContext.getCurrentInstance();
        ServletContext scontext = (ServletContext) fcontext.getExternalContext().getContext();
        String path = scontext.getRealPath(File.separator);
        parameters.put("SUBREPORT_DIR", path + "reports"+File.separator);
        setTemplatePath(path+"reports"+File.separator+"reportSummary.jrxml");
        setDataSource(data.getDataSource());
    }

    @Override
    protected void prepareJasperReports()throws JRException {
        String file1 = "reportSummary.jrxml";
        String file2 = "subcatRpt.jrxml";
        String file3 = "summaryItems.jrxml";

        FacesContext fcontext = FacesContext.getCurrentInstance();
        ServletContext scontext = (ServletContext) fcontext.getExternalContext().getContext();
        String path = scontext.getRealPath(File.separator);
        path+="reports"+File.separator;
        
        getCompiledReport(path+file3);
        getCompiledReport(path+file2);
        getCompiledReport(path+file1);
        
        
    }
}
