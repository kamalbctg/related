/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.reports;

import com.zuma.web.controllers.helper.waste.WasteDataSet;
import java.io.File;
import java.math.BigDecimal;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import net.sf.jasperreports.engine.JRException;

public class ReportWaste extends ParentReport {


    public ReportWaste(Map<String, Object> paramReport, WasteDataSet data) {
        super(paramReport);
        parameters.put("rName", "Waste");
        parameters.put("dsRetail", data.getRetailData().getDataSource());
        parameters.put("pSum1",data.getRetailData().getTotalCost() );
        if (data.getSalesPercentage().getValue()!=null)
            parameters.put("pSum2",data.getSalesPercentage().getValue().divide(new BigDecimal(100)) );
        else
            parameters.put("pSum2",data.getSalesPercentage().getValue());
        parameters.put("pSum3", data.getNetRetailWaste());
        parameters.put("pSum4", data.getTotalWaste());
        FacesContext fcontext = FacesContext.getCurrentInstance();
        ServletContext scontext = (ServletContext) fcontext.getExternalContext().getContext();
        String path = scontext.getRealPath(File.separator);
        parameters.put("SUBREPORT_DIR", path + "reports"+File.separator);
        setTemplatePath(path+"reports"+File.separator+"reportWaste.jrxml");
        setDataSource(data.getDataSource());
    }

    @Override
    protected void prepareJasperReports()throws JRException {
        String file1 = "reportWaste.jrxml";
        String file2 = "subcatWaste.jrxml";
        String file3 = "retailSub.jrxml";
        String file4 = "itemsWaste.jrxml";


        FacesContext fcontext = FacesContext.getCurrentInstance();
        ServletContext scontext = (ServletContext) fcontext.getExternalContext().getContext();
        String path = scontext.getRealPath(File.separator);
        path+="reports"+File.separator;
        
        getCompiledReport(path+file4);
        getCompiledReport(path+file3);
        getCompiledReport(path+file2);
        getCompiledReport(path+file1);
        
        
    }
}
