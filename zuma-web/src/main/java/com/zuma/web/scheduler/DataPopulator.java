/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zuma.web.scheduler;

import com.zuma.db.core.domain.InventoryWeeklyRecord;
import com.zuma.db.core.domain.ReportingWeek;
import com.zuma.db.dao.UniversalDAO;
import com.zuma.web.controllers.ParentController;
import com.zuma.db.service.util.Week;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.DateFormatter;
import org.springframework.beans.factory.annotation.Autowired;

public class DataPopulator implements Runnable {
   
    @Autowired
    UniversalDAO dao;
    protected int interval = 60; //one minute inteval (60 second)

    protected void populate() throws ParseException {

       String sql = "SELECT max(weekDate) FROM ReportingWeek";
       Date weekStart = dao.getObject(sql, Date.class,false);
       Calendar currWeek = Calendar.getInstance();
       currWeek.setTime(Week.getCurrentWeekBegin());

       if(!currWeek.getTime().equals(weekStart))
       {
           ReportingWeek newWeek = new ReportingWeek(Week.getCurrentWeekBegin());
           dao.save(newWeek);
           DateFormatter formatter = new DateFormatter(new SimpleDateFormat(ParentController.DATE_FORMAT));
           sql = "FROM InventoryWeeklyRecord WHERE inventoryItem.deleted=false AND reportingWeek.weekDate='"+
                   formatter.valueToString(weekStart)+"'";
           List<InventoryWeeklyRecord> records = dao.get(sql);
           List<InventoryWeeklyRecord> newRecords = new ArrayList<InventoryWeeklyRecord>();
           for(InventoryWeeklyRecord record:records)
           {
              InventoryWeeklyRecord newRecord = record.clone();
              newRecord.setReportingWeek(newWeek);
              newRecord.setQuantity(BigDecimal.ZERO);
              newRecords.add(newRecord);
           }
           dao.saveOrUpdateAll(newRecords);
       }
    }

    protected boolean isPopulatingNeeded() {
        Calendar curr = Calendar.getInstance();
        int hour = curr.get(Calendar.HOUR_OF_DAY);
        int minute = curr.get(Calendar.MINUTE);
        int second = curr.get(Calendar.SECOND);

        if (hour == 0 && minute == 0 && second < interval) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void run() {
      /*  if (isPopulatingNeeded()) */{
            try {
                populate();
            } catch (ParseException ex) {
                Logger.getLogger(DataPopulator.class.getName()).log(Level.INFO, null, ex);
            }
        }
    }
}
