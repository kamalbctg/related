package com.zuma.web.util;

import java.util.Collection;

import org.apache.commons.lang.SystemUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class AbbStyle extends ToStringStyle {
	private final static ToStringStyle instance = new AbbStyle();

	public AbbStyle() {
		setArrayContentDetail(true);
		setUseShortClassName(true);
		setUseClassName(false);
		setUseIdentityHashCode(false);
		setFieldSeparator(", " + SystemUtils.LINE_SEPARATOR + "  ");
	}

	public static ToStringStyle getInstance() {
		return instance;
	};

	@Override
	public void appendDetail(StringBuffer buffer, String fieldName, Object value) {
		if (!value.getClass().getName().startsWith("java")) {
			buffer.append(ReflectionToStringBuilder.toString(value, instance));
		} else {
			super.appendDetail(buffer, fieldName, value);
		}
	}

	@Override
	public void appendDetail(StringBuffer buffer, String fieldName,
			Collection value) {
		appendDetail(buffer, fieldName, value.toArray());
	}
}