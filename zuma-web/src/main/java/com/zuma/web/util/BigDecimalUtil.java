package com.zuma.web.util;

import java.math.BigDecimal;

public class BigDecimalUtil {
	public static String roundAndToStringMoney(BigDecimal amount) {
		return amount.setScale(2, BigDecimal.ROUND_HALF_EVEN).toPlainString();
	}

}
