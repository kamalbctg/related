package com.zuma.web.util;

public class Email {

    static public boolean validate(String str) {
        String at = "@";
        String dot = ".";
        String email = str.trim();
        if (email.isEmpty()) {
            return false;
        }

        int lat = email.indexOf(at);
        int ldot = email.indexOf(dot);
        int last = email.length() - 1;

        if (lat == -1 || lat == 0 || lat == last) {
            return false;
        }

        if (ldot == -1 || ldot == 0 || ldot == last) {
            return false;
        }

        if (email.indexOf(at, (lat + 1)) != -1) {
            return false;
        }

        if (email.indexOf(dot, (lat + 2)) == -1) {
            return false;
        }

        if (email.substring(lat - 1, lat).equals(dot) ||
            email.substring(lat + 1, lat + 2).equals(dot)) {
            return false;
        }

        return true;
    }
}
