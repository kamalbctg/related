/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.zuma.web.util;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map.Entry;


public class NormalizedMap extends HashMap<String,BigDecimal>{
    {
        put("target",BigDecimal.ZERO);
        put("current",BigDecimal.ZERO);
        put("mtd",BigDecimal.ZERO);
        put("ytd",BigDecimal.ZERO);
    }
    @Override
    public BigDecimal get(Object key)
    {
        return super.get((String)key);
    }
    public void normalize()
    {
        BigDecimal max = BigDecimal.ZERO;
        if(entrySet().isEmpty())
            return;
        else
        {
            max = entrySet().iterator().next().getValue();
        }
        for(Entry<String,BigDecimal> entry:entrySet())
        {
            if(entry.getValue().compareTo(max)>0)
                max = entry.getValue();
        }
        for(Entry<String,BigDecimal> entry:entrySet())
        {
            entry.setValue(entry.getValue().divide(max).multiply(BigDecimal.valueOf(100)));
        }
    }
}
