package com.zuma.web.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.log4j.Logger;




public class Util {
	
	static final Logger log = Logger.getLogger(Util.class);

	
	


	public static void print(Object o) {
		System.out.println(ReflectionToStringBuilder.toString(o,
				AbbStyle.getInstance()));
		log.info(ReflectionToStringBuilder.toString(o,
				AbbStyle.getInstance()));
	}


}
