
package com.zuma.web.validators;

import java.util.Date;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class InvoiceDateValidator implements Validator {

    Date minDate, maxDate;
    public final static String minValidDate = "minValidDate";
    public final static String maxValidDate = "maxValidDate";

    @Override
    public void validate(FacesContext context, UIComponent component, Object value)
            throws FacesException {
        initDates(component);

        Date newValue = (Date) value;
        if (!verifyDate(newValue)) {
            throw new ValidatorException(new FacesMessage());
        }
    }

    protected void initDates(UIComponent component) {
        minDate = (Date) component.getAttributes().get(minValidDate);
        maxDate = (Date) component.getAttributes().get(maxValidDate);
    }

    public boolean verifyDate(Date day) {
        if (day == null) {
            return false;
        }
        if (day.compareTo(minDate) < 0
                || day.compareTo(maxDate) > 0) {
            return false;
        }
        return true;
    }
}
