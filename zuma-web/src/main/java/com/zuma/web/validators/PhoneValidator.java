package com.zuma.web.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class PhoneValidator implements Validator {
    /** Creates a new instance of PhoneNumberValidator */
    public PhoneValidator() {
    }

    /** phone number*/
    private static final String PHONE_NUM= "[0-9]{3}[-]{1}[0-9]{3}[-]{1}[0-9]{4}";
    
    @Override
    public void validate(FacesContext context, UIComponent component, Object value){
        /* create a mask */
        Pattern mask = Pattern.compile(PHONE_NUM);

        /* retrieve the string value of the field*/
        String phoneNumber = (String)value;

        /* ensure value is a phone number*/
        Matcher matcher = mask.matcher(phoneNumber);

        if(!matcher.matches()){
            FacesMessage message = new FacesMessage();
            message.setDetail("Invalid phone number format. Please enter numerical characters only.");
            message.setSummary("Invalid phone number format.");
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }
}
