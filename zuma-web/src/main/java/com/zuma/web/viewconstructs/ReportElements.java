package com.zuma.web.viewconstructs;

public enum ReportElements {

	CATEGORY, SUB_CATEGORY, ITEM_ID, ITEM {
		@Override
		public String toString() {
			return "ID";
		}
	},
	ITEM_INDEX {
		@Override
		public String toString() {
			return "";
		}
	},
	ITEM_NAME {
		@Override
		public String toString() {
			return "Item Name";
		}
	},
	ITEM_MEASURE {
		@Override
		public String toString() {
			return "Measure";
		}
	},
	ITEM_UNIT_COST {
		@Override
		public String toString() {
			return "Unit Cost";
		}
	},
	ITEM_COUNT {
		@Override
		public String toString() {
			return "Count";
		}
	},
	ITEM_AMOUNT {
		@Override
		public String toString() {
			return "Amount";
		}
	}
}
