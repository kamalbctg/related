<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ page isErrorPage="true"%>
<%response.setStatus(HttpServletResponse.SC_OK);

String exceptionMsg = exception.toString();
if (exceptionMsg.indexOf("javax.faces.application.ViewExpiredException")>=0)
    response.sendRedirect(request.getContextPath()+"/welcome.faces");
else if(exceptionMsg.indexOf("org.ajax4jsf.resource.ResourceNotFoundException")>=0)
    response.sendRedirect(request.getContextPath()+"/errorpages/notfound.jsp");
/*
    String strTrace = "";
    StackTraceElement elements[] = exception.getStackTrace();
    for (int i = 0, n = elements.length; i < n; i++) {
        strTrace += elements[i].getFileName() + ":" +
                    elements[i].getLineNumber() + ">> " +
                    elements[i].getMethodName() + "()<br/>";
    }
*/
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Error has occurred!</title>
        <style type="text/css">
            root {
                display: block;
            }
            * {
                margin: 0px;
                padding: 0px;
            }
            html {
                width: 100%;
                height: 100%;
                background-color: #FFFFFF;
            }
            body {
                width:100%;
            }
            p {
                font-family: Arial,Helvetica,sans-serif;
                font-size: 12px;
                font-weight: bold;
            }
            .client {
                height:100px;
                padding-top:20px;
                padding-left:20px;
                color:#666666;
            }
            a, a:link, a:hover, a:active, a:visited {
                text-decoration:underline;
                color: black;
            }
            a:hover, a:active
            {
                color: red;
            }
            .ex {
                color: red;
            }
        </style>
    </head>
    <body>
        <div class="client">
            <p>Sorry, the page has expired or an error has occurred.<br/>
                <span class="ex"><%=exception.toString()%></span><br/>
                You can continue working from our <a href="/welcome.faces">Welcome Page</a></p>
            <p>You can also go back to the page that caused this error by going
                <a onclick="history.go(-1)">here</a></p>
        </div>
    </body>
</html>