<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>File Manager</title>
        <link type="text/css" rel="stylesheet" href="css/zuma.css"/>
        <style  type="text/css">
            .rich-table
            {
                border:none;
                border-collapse:separate;
            }
            .rich-panel-body
            {
                vertical-align:middle;
                border:0px;
                margin:0px;
                padding:0px;
            }
            .rich-table-headercell{
                background-image: url('images/bg_cap.jpg'); /**/
                background-repeat: repeat-x;
                font-size:12px;
                border:none;
                vertical-align:middle;
            }
            .rich-table-cell, .rich-subtable-cell {
                line-height:100%;
                padding:0px;
                text-align:left;
                vertical-align:middle;
                margin:5px;
                font-size:12px;
                border-right:none;
            }
            .rich-tree-node-selected
            {
                background:#ffe6bb; /*transparent url(images/bg_cap.jpg) repeat-x;*/
            }
            input.buttonClear {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 12px;
                font-weight: bold;
                text-align: center;
                vertical-align:middle;
                height: 20px;
                width: 120px;
                padding: 1px;
                padding-bottom: 3px;
                background-image: url('images/bg_cap.jpg');
                background-repeat: repeat-x;
                border: 1px solid #cccccc;
                color: #ff0000;
            }
            .rich-mpnl-header
            {
                background-image: none;
            }
            .buttons {
                background:transparent url(images/bg_cap.jpg) repeat-x;
                border:1px solid #cccccc;
                color:#B61E1E;
                font-size:12px;
                font-weight:bold;
                height:25px;
                width:83px;
                font-size:12px !important;
                vertical-align:middle;
            }
            .td-header
            {
                -moz-background-clip:border;
                -moz-background-inline-policy:continuous;
                -moz-background-origin:padding;
                background:transparent url(images/bg_cap.jpg) repeat-x;

                color:#666666;
                font-size:12px;
                font-weight:bold;
                height:25px;
                margin-right:5px;
                padding-bottom:6px;                
                font-size:12px !important;
                border:none;
            }
            .rich-subtable-footercell, .td-cell
            {
                border:none;
                margin:0px;
                padding: 0px;
            }
            .btnAdd
            {
                font-size:12px !important;
                margin:0px;
                border:none;
            }
            .rich-fileupload-button-lignt
            {
                font-weight:normal;

            }
            .rich-fileupload-list-decor,
            .rich-fileupload-toolbar-decor
            {               
                border-bottom:none;
                border-left:none;
                border-right:none;
                border-top:none;
                background-color: #F1EEE9;
            }
            .button-light
            {
                border:none;
                margin-bottom:3px;
                padding-right:1px;
                padding-bottom:2px;
                cursor:pointer;
            }
            .rich-fileupload-button-light
            {
                border-color:transparent;
                font-weight:normal;               
            }
            .rich-button-disabled
            {
                width:24px;
            }
            .rich-fileupload-ico-add-dis
            {
                width:24px;
                overflow:hidden;
                /* width:0px;
                 margin:0px;
                 border:none;
                 visibility:hidden;
                 display:none;*/
            }

        </style>
        <script type="text/javascript">
            var deleteMessage = "Deleted items cannot be retrieved.  Are you sure you wish to delete this item?";
            var submitMessage = "This action cannot be undone.  Weeks should be closed only after all materials for the week have been completed.  Do you wish to continue?";
        </script>
    </head>
    <body>
    <f:view  beforePhase="#{fileSystemBean.reload}">
        <f:subview id="menuView">
            <jsp:include page="includes/menu.jsp" />
        </f:subview>
        <f:subview id="topClientView">
            <jsp:include page="includes/topclient.jsp" />
        </f:subview>
        <rich:modalPanel
            id="waitModalPanel"
            autosized="true"
            width="200"
            height="120"
            moveable="false"
            resizeable="false">
            <f:facet name="header">
                <h:outputText value="Processing"/>
            </f:facet>
            <h:graphicImage  value="images/transmit.gif"/>
            <div style="width:100%; height:45px; text-align:center; vertical-align:middle;">
                <h:outputText value="Please wait..."/>
            </div>
        </rich:modalPanel>
        <rich:modalPanel  id="mpUpload" minHeight="320" minWidth="300"
                          height="360" width="340" style="background-image:none;"
                          onshow="javascript:document.getElementById('f_uplPayroll:uplFile:flashContainer').click()" >
            <f:facet name="header">
                <h:panelGroup >
                    <h:outputText value="Upload"></h:outputText>
                </h:panelGroup>
            </f:facet>
            <f:facet name="controls">
                <h:panelGroup>
                    <h:graphicImage value="images/close.png" styleClass="hidelink" id="hidelinkPayroll"/>
                    <rich:componentControl for="mpUpload" attachTo="hidelinkPayroll" operation="hide" event="onclick"/>
                </h:panelGroup>
            </f:facet>
            <h:form id="f_uplFile">
                <rich:fileUpload fileUploadListener="#{fileUploadBean.listener}" autoclear="true"
                                 maxFilesQuantity="5"
                                 id="uplFile" allowFlash="true"
                                 immediateUpload="true"
                                 listHeight="250px" listWidth="320px" >
                    <a4j:support event="onuploadcomplete" reRender="f_filemngr:itemlist"
                                 oncomplete="Richfaces.hideModalPanel('mpUpload');" />
                </rich:fileUpload>
            </h:form>
            <br/>
            <p align="center">
            <h:outputLink value="#" id="linkPayroll"> Close
                <rich:componentControl for="mpUpload" attachTo="linkPayroll" operation="hide" event="onclick"/>
            </h:outputLink>
            </p>
        </rich:modalPanel>
        <rich:modalPanel  id="mpFolder" minHeight="100" minWidth="200"
                          height="120" width="250" style="background-image:none;" >
            <f:facet name="header">
                <h:panelGroup >
                    <h:outputText value="New Folder"/>
                </h:panelGroup>
            </f:facet>
            <f:facet name="controls">
                <h:panelGroup>
                    <h:graphicImage value="images/close.png" styleClass="hidelink" id="hidelinkFolder"/>
                    <rich:componentControl for="mpFolder" attachTo="hidelinkFolder" operation="hide" event="onclick"/>
                </h:panelGroup>
            </f:facet>
            <a4j:form id="f_Folder" style="top:100;">
                <table cellpadding="0" cellspacing="0" border="0" width="100%">
                    <tr>
                        <td colspan="2" align="center">
                            <span style="font-size:14px;">Name&nbsp;</span>
                    <h:inputText id="itFldr" style="font-size:12px;height:20px;width:150px;maxlength:20;"
                                 value="#{fileSystemBean.newFolderName}" />
                    <br/><br/>
                    </td>
                    </tr>
                    <tr><td height="30" width="50%" align="center">
                    <h:commandButton id="btnApply" action="#{fileSystemBean.createFolder}" styleClass="buttons"  value="Create">
                        <a4j:support event="onclick" reRender="f_filemngr:treeFolder" />
                    </h:commandButton>
                    </td>
                    <td align="center">
                    <h:commandButton value="Close" id="linkFolder" styleClass="buttons" type="reset">
                        <rich:componentControl for="mpFolder" attachTo="linkFolder" operation="hide" event="onclick"/>
                    </h:commandButton>
                    </td>
                    </tr>
                </table>
            </a4j:form>
            <br/>
        </rich:modalPanel>
        <rich:modalPanel id="mpDelete" autosized="false" width="300" height="200">
            <f:facet name="header">
                <h:outputText value="Warning!"
                              style="padding-right:15px;" />
            </f:facet>
            <f:facet name="controls">
                <h:panelGroup>
                    <h:graphicImage value="/images/close.png"
                                    styleClass="hidelink" id="hidelink3" />
                    <rich:componentControl for="mpDelete" attachTo="hidelink3"
                                           operation="hide" event="onclick" />
                </h:panelGroup>
            </f:facet>
            <a4j:form>
                <a4j:region id="intr">
                    <p align="center">
                        <span class="buttons" style="border:none;color:red; text-align:center"> This action cannot be undone. </span></p>
                    <p align="left">
                        <br/> <span style="text-align:left">Weeks should be closed only after all materials for the week have been completed. </span></p>
                    <p align="center">
                        <br/> <span style="text-align:center"> Do you wish to continue?</span></p>
                    <div style="height:35px" align="center">
                        <a4j:status>
                            <f:facet name="start">
                                <h:graphicImage  value="images/transmit.gif"/>
                            </f:facet>
                        </a4j:status>
                    </div>
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tbody>
                            <tr>
                                <td align="center" width="50%">
                        <a4j:commandButton value="Yes" ajaxSingle="false" reRender="f_filemngr:itemlist" styleClass="buttons" >

                            <a4j:support event="onclick" action="#{fileSystemBean.submitReports}" reRender="dateList"
                                         oncomplete="Richfaces.hideModalPanel('mpDelete');"/>

                        </a4j:commandButton>
                        </td>
                        <td align="center" width="50%">
                        <a4j:commandButton styleClass="buttons" value="Cancel" onclick="Richfaces.hideModalPanel('mpDelete')" />
                        </td>
                        </tr>
                        </tbody>
                    </table>
                </a4j:region>
            </a4j:form>
        </rich:modalPanel>
        <div style="height:14px;"><h1 class="pageCaption" style="float:left;">File Manager</h1></div><br/>
        <div align="center">
            <table id="tblItems" border="0" width="938px" class="table_border" style="text-align:left">
                <tr>
                    <td colspan="4" height="18" bgcolor="#868686">
                        <span id="otItems1" style="color:#FFFFFF; font-size:14px;font-weight: 700;">&nbsp;Open Items</span>
                    </td>
                </tr>
                <tr>
                    <td>
                <rich:dataTable id="dateList"  width="100%"
                                value="#{fileSystemBean.monthsNames}" var="recMonth"
                                rowKeyVar="monthIndex" styleClass="maintable" >
                    <rich:columnGroup>
                        <rich:column styleClass="td-header" style="width:350px;height:16px;margin:0px;padding:0;border:0;">
                            <h:outputText value="#{recMonth}">
                                <f:convertDateTime type="date" pattern="MMMMM"/>
                            </h:outputText>
                            <h:outputText value=" Open Items" />
                        </rich:column>
                        <rich:column styleClass="td-header" style="width:200px;height:16px;margin:0px;padding:0;border:0;">
                            <h:outputText value="Materials" />
                        </rich:column>
                        <rich:column styleClass="td-header" style="width:75px;height:16px;">
                            <h:outputText value="Preview" />
                        </rich:column>
                        <rich:column styleClass="td-header" style="width:301px;height:16px;" >
                            <h:outputText value="Action" rendered="#{fn:length(fileSystemBean.mapMonths[recMonth])>1 || recMonth!=fileSystemBean.lastDate}" />
                        </rich:column>
                    </rich:columnGroup>
                    <rich:subTable var="recWeek" value="#{fileSystemBean.mapMonths[recMonth]}">
                        <rich:column styleClass="td-cell" colspan="4" style="border:0px" width="100%">
                            <a4j:form id="frmOpenItems" ajaxSingle="true">
                                <table width="100%"><tr>
                                        <td class="td-cell" style="width:350px;height:16px">
                                    <h:outputText value="Week Ending [" />
                                    <h:outputText value="#{recWeek.time}">
                                        <f:convertDateTime type="date" pattern="MMM dd, yyyy"/>
                                    </h:outputText>
                                    <h:outputText value="] Materials" />
                                    <f:facet name="footer">
                                        <rich:spacer />
                                    </f:facet>
                                    </td>
                                    <td class="td-cell" style="width:200px">
                                    <h:selectOneListbox value="#{fileSystemBean.selectedReport}" id="cbPrvw" size="1"
                                                        style="width:135px;font-size:12px;font-family:Arial,Verdana,sans-serif" >

                                        <f:selectItems value="#{fileSystemBean.userReports}" />
                                    </h:selectOneListbox>
                                    </td>
                                    <td class="td-cell" style="width:75px">
                                    <a4j:htmlCommandLink action="#{fileSystemBean.viewReportPdf}" >
                                        <h:graphicImage  alt="preview PDF" value="images/pdf.png" style="height:16px" />
                                        <f:setPropertyActionListener     target="#{fileSystemBean.selectedDate}" value="#{recWeek}" />
                                    </a4j:htmlCommandLink>&nbsp;
                                    <a4j:htmlCommandLink action="#{fileSystemBean.viewReportExcel}">
                                        <h:graphicImage  alt="preview Excel" value="images/xls.png" style="height:16px" />
                                        <f:setPropertyActionListener     target="#{fileSystemBean.selectedDate}" value="#{recWeek}" />
                                    </a4j:htmlCommandLink>
                                    </td>
                                    <td class="td-cell">
                                    <a4j:commandLink id="clSbmt" styleClass="red" value="Close Week" onclick="Richfaces.showModalPanel('mpDelete');" ajaxSingle="true"
                                                     rendered="#{recWeek.time!=fileSystemBean.lastDate}" >
                                        <f:setPropertyActionListener     target="#{fileSystemBean.selectedDate}" value="#{recWeek}" />
                                    </a4j:commandLink>&nbsp;
                                    </td>
                                    </tr></table>
                            </a4j:form >
                        </rich:column>
                    </rich:subTable>
                </rich:dataTable>
                </td>
                </tr>
            </table>
        </div>
        <br/>
        <h:form id="f_filemngr">
            <a name="folder"></a>
            <table border="0" cellpadding="0" cellspacing="0" class="table_border" width="938">
                <tr style="border:#cccccc 1px solid;">
                    <td colspan="2" height="18" bgcolor="gray" style="vertical-align:middle;width:100%">
                <h:outputText  value="Archived Files" style="color:#FFFFFF; font-size:14px;font-weight: 700; margin-left:3px;" />
                </td></tr>
                <tr><td colspan="2" height="25" class="rich-table-headercell" style="vertical-align:middle; text-align:left;margin-left:3px;">
                <h:outputText  value="View Archived Files From:"/>&nbsp;&nbsp;

                <rich:calendar id="cld_strt" value="#{fileSystemBean.dateStart}" enableManualInput="true" datePattern="MM/dd/yyyy"
                               converterMessage="Dates must be entered using mm/dd/yyyy formatting. Please try again or use the date selection tool to the right of the date fields."/>
                &nbsp;&nbsp;-&nbsp;&nbsp;
                <rich:calendar id="cld_end" value="#{fileSystemBean.dateEnd}" enableManualInput="true"  datePattern="MM/dd/yyyy"
                               converterMessage="Dates must be entered using mm/dd/yyyy formatting. Please try again or use the date selection tool to the right of the date fields."/>&nbsp;
                <a4j:commandButton id="btnCldApply" reRender="itemlist" action="#{fileSystemBean.fillFiles}"
                                   image="images/accept.png" style="vertical-align: middle;" />
                </td>
                </tr>
                <tr><td colspan="2" height="25" class="rich-table-headercell" style="background:#FFE6BB;text-align:left;margin-left:3px;">
                <h:outputText escape="false" style="font-size:14px;font-weight: 700;" value=" Active Folder:  "/>
                <h:outputText escape="false" style="font-size:14px;font-weight: 700;color:red;" value="  #{fileSystemBean.nodeTitle}" id="selectedNode" />
                </td>
                </tr>
                <tr>
                    <td colspan="2" align="left" style="width:100%;height:18px;vertical-align:middle;border:#cccccc 1px solid;background-color:#F1EEE9;"class="rich-table-cell">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="width:160px;height:28px;vertical-align:middle;">
                            <a4j:outputPanel id="clUpload" style="height:100%; width:100%; padding:0px; border:0px;">
                                <rich:fileUpload fileUploadListener="#{fileUploadBean.listener}" autoclear="true" rendered="#{fileSystemBean.uploaded}"
                                                 addButtonClass="btnAdd" addControlLabel="Add File to Active Folder"
                                                 stopControlLabel="Please wait..."
                                                 id="uplFile1" allowFlash="false" immediateUpload="true"  sizeErrorLabel="File size is too large, the maximum upload size is 20 MB"
                                                 listHeight="0" listWidth="180"  style="float:center;"
                                                 onsizerejected="alert('File size is too large, the maximum upload size is 20 MB.');  window.location.reload( false ); return false;">
                                    <a4j:support event="onadd" oncomplete="Richfaces.showModalPanel('waitModalPanel');" />
                                    <a4j:support event="onerror" oncomplete="alert('Error while uploading file! Try again.');Richfaces.hideModalPanel('waitModalPanel');" />
                                    <a4j:support event="onuploadcomplete" reRender="f_filemngr:itemlist, f_filemngr:uplFile1"
                                                 oncomplete="Richfaces.hideModalPanel('waitModalPanel');" />
                                </rich:fileUpload>
                            </a4j:outputPanel>
                    </td>
                    <td style="width:750px;height:34px;vertical-align:middle; background-color:#F1EEE9;">
                <a4j:outputPanel id="clFolder" style="height:100%; width:100%; padding:0px; border:0px" >
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr><td style="height:34px;vertical-align:middle;width:205px; padding-top:1px;">
                        <a4j:outputPanel id="pnlSubfolder" rendered="#{fileSystemBean.uploaded}" >
                            <div class="rich-fileupload-button-border rich-fileupload-button"
                                 style="height:17px;width:190px;vertical-align:middle;clear:both;margin:0px;padding:0px;padding-top:2px;">
                                <a4j:commandLink id="clnkFolder" ajaxSingle="true"
                                                 oncomplete="Richfaces.showModalPanel('mpFolder');"
                                                 styleClass="rich-fileupload-button rich-fileupload-font"
                                                 onmouseout="this.className='rich-fileupload-button button-light  rich-fileupload-font'"
                                                 onmouseover="this.className='rich-fileupload-button-light  rich-fileupload-font'"
                                                 style="color:black;font-weight:normal;font-size:12px;height:100%;width:100%; padding-top:0">
                                    <img src="images/ico_add.gif" alt ="Upload" style="vertical-align:middle;" >
                                    <span style="vertical-align:middle;">Add Subfolder to Active Folder</span></a4j:commandLink></div>
                        </a4j:outputPanel>
                        </td>
                        <td style="vertical-align:middle; width:137px;">
                        <a4j:outputPanel id="pnlDelFolder" rendered="#{fileSystemBean.allowDelete}" >
                            <div class="rich-fileupload-button-border rich-fileupload-button"
                                 style="height:17px;width:137px;vertical-align:middle;clear:both;margin:0px;padding:0px;padding-top:3px;">
                                <h:commandLink id="clnkDel" onclick="if (! window.confirm(deleteMessage) ) {return false}"
                                               action="#{fileSystemBean.deleteFolder}"
                                               styleClass="rich-fileupload-button rich-fileupload-font "
                                               onmouseout="this.className='rich-fileupload-button   rich-fileupload-font'"
                                               onmouseover="this.className='rich-fileupload-button-light button-light rich-fileupload-font'"
                                               style="color:black;font-weight:normal;font-size:12px;height:100%;width:100%;">
                                    <img src="images/del.gif" alt ="Delete" style="vertical-align:middle;width:16px;">
                                    <span>Delete Active Folder&nbsp;</span>
                                </h:commandLink></div>
                        </a4j:outputPanel>
                        </td>
                        <td>&nbsp;</td>
                        </tr>
                    </table>
                </a4j:outputPanel>
                </td>
                </tr>
            </table>
            </td>
            </tr>
            <tr>
                <td style="height:300px;width:250px;border:#cccccc 1px solid; vertical-align:top; "  >
            <rich:tree style="width:300px valign:top"  nodeSelectListener="#{fileSystemBean.processSelection}"
                       reRender="selectedNode,itemlist, clUpload, clFolder" ajaxSubmitSelection="true"  switchType="client"
                       value="#{fileSystemBean.treeNode}" var="item" ajaxKeys="#{null }"
                       treeNodeVar="treeNode" iconLeaf="images/folder.gif" id="treeFolder"  stateAdvisor="#{treeFolderStateAdvisor}" >

            </rich:tree>
            </td>
            <td style="height:300px;border:#cccccc 1px solid; vertical-align:top;" >
                <table cellpadding="0" cellspacing="0" border="0">
                    <tr><td valign="top" height="100%">
                    <rich:dataTable id="itemlist" value="#{fileSystemBean.fileItems}"
                                    var="files" rowKeyVar="index" styleClass="itemtable"  >
                        <f:facet name="header">
                            <rich:columnGroup>
                                <rich:column width="230" style="text-align:left;">
                                    <h:outputText value="Name" />
                                </rich:column>
                                <rich:column width="80" style="text-align:left" >
                                    Week<br/>Ending
                                </rich:column>
                                <rich:column width="90" style="text-align:left">
                                    Created<br/>By
                                </rich:column>
                                <rich:column width="80" style="text-align:left">
                                    Date<br/> Submitted
                                </rich:column>
                                <rich:column width="51"  style="text-align:right">
                                    <h:outputText value="Size" />
                                </rich:column>
                                <rich:column width="50">
                                    <h:outputText value="View" />
                                </rich:column>
                                <rich:column width="39">
                                    <h:outputText value="Delete" />
                                </rich:column>
                            </rich:columnGroup>
                        </f:facet>
                        <rich:column id="itamnamecolumn" headerClass="itemheader">
                            <h:outputText id="itemnametext" value="#{files.name}" styleClass="nowrap" />
                        </rich:column>
                        <rich:column id="itemDR">
                            <h:outputText id="itemDRtext"
                                          value="#{files.dateWeekReport}"  styleClass="nowrap"  >
                                <f:convertDateTime type="date" pattern="MMM dd,yyyy"/>
                            </h:outputText>
                        </rich:column>
                        <rich:column id="itemUsr">
                            <h:outputText id="itemUsrtext"
                                          value="#{files.person.login}"  styleClass="nowrap"  />
                        </rich:column>
                        <rich:column id="itemDF">
                            <h:outputText id="itemDFtext"
                                          value="#{files.dateFile}"  styleClass="nowrap"  >
                                <f:convertDateTime type="date" pattern="MMM dd,yyyy"/>
                            </h:outputText>
                        </rich:column>
                        <rich:column id="itemSF" style="text-align:right">
                            <h:outputText id="itemSFtext"
                                          value="#{files.sizeFileKb}"  styleClass="nowrap"  >
                                <f:convertNumber pattern="#,###,##0"/>
                            </h:outputText>&nbsp;
                            <h:outputText id="kb" value="Kb"/>
                        </rich:column>
                        <rich:column id="itemxls" style="text-align: center">
                            <a4j:htmlCommandLink action="#{fileUploadBean.downloadDocument}" >
                                <h:graphicImage id="giimg" alt="download" value="images/#{files.fileType}.png" style="height:16px" />
                                <f:setPropertyActionListener     target="#{fileSystemBean.selectedFile}" value="#{files}"/>
                            </a4j:htmlCommandLink>
                        </rich:column>
                        <rich:column id="itemDel" style="text-align: center">
                            <a4j:htmlCommandLink action="#{fileSystemBean.deleteFile}"  onclick="javascript:if(!confirm(deleteMessage)) return false;">
                                <h:graphicImage  alt="delete" value="images/ico_clear.gif" style="height:16px" rendered="#{fileSystemBean.uploaded ||userManagedBean.admin}" />
                                <f:setPropertyActionListener     target="#{fileSystemBean.selectedFile}" value="#{files}"/>
                                <a4j:support  rendered="#{!fileSystemBean.uploaded &&userManagedBean.admin}" reRender="dateList"/>
                            </a4j:htmlCommandLink>
                        </rich:column>
                    </rich:dataTable>
            </td><tr/>
            </table>
            </td>
            </tr>
            </table>
        </h:form>
        <f:subview id="bottomView">
            <jsp:include page="includes/bottom.jsp" />
        </f:subview>
    </f:view>
</body>
</html>
