<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Forgot Password</title>
        <link type="text/css" rel="stylesheet" href="css/zuma.css"/>
        <link type="text/css" rel="stylesheet" href="css/forgot.css"/>
        <script type="text/javascript" src="js/prototype.js"></script>
        <script type="text/javascript" src="js/hotkeys.js"></script>
        <script type="text/javascript" src="js/table.js"></script>
        <script type="text/javascript" src="js/effects.js"></script>
        <script type="text/javascript" src="js/messages.js"></script>
        <script type="text/javascript">
            function save() {
                $("buttonSave").click();
                saveSign = true;
            }
            Hotkeys.bind("ctrl+s", save);         
        </script>
    </head>
    <body>
        <f:view>
            <f:subview id="mnu" >
                <jsp:include page="includes/menu.jsp" />
            </f:subview>
            <f:subview id="tc">
                <jsp:include page="includes/topclient.jsp" />
            </f:subview>

            <h:panelGroup id ="pnlMain" layout="block" styleClass="clientArea" rendered="#{!passwordManagerController.showSuccessPage}" >
                <div id="content">
                    <div id="paddedContent" style="padding-left:20px; ">
                        <h1 style="font-size:1.86em;color:#4B465A;padding-bottom:20px;padding-top:30px;">Forgotten your password?</h1>
                        <p style="font-size:1.3em;padding-bottom:20px;">Change your password in three easy steps. This helps to keep your new password secure.</p>
                        <ul class="steps" style="font-size:1.3em;">
                            <li class="step1">Fill in your email address below.</li>
                            <li class="step2">We'll email you a temporary code.</li>
                            <li class="step3">Use the code to change your password on our secure website.</li>
                        </ul>
                        <h:form  id="frm">
                            <f:subview id="flying">
                                <c:import url="includes/flyingpanels.jsp" />
                            </f:subview>
                            <a4j:status id="status" startStyleClass="save-sign"
                                        onstart="showSavingSign()"
                                        onstop="hideSavingSign();">
                            </a4j:status>
                            <h2 style="font-size:1.4em;;padding-bottom:5px;padding-top:15px;">Enter your email address</h2>
                            <div class="fieldSet" style="padding-left:20px;padding-bottom:5px;">
                                <h:inputText id="email" maxlength="256" required="true" requiredMessage="Email is required"  value="#{passwordManagerController.email}">
                                    <f:validator validatorId="emailValidator" />
                                </h:inputText>
                                <span style="color:red"><h:message for="email" /></span>
                            </div>
                            <p  style="padding-bottom:20px;">Type in the email address you used when you registered your account.</p>
                            <p id="getToken" class="forceLayout">
                                <h:commandButton styleClass="buttonSave" type="submit" id="getTokenSubmit" value="Submit" action="#{passwordManagerController.renewPassword}" />
                                <!--onclick='saveSign=true; setApproveText("Please check your email in a few minutes"); showSaveApprove();'-->
                                <h:outputLink styleClass="buttonSave" value="welcome.faces" id="linkPayroll">&nbsp;&nbsp;&nbsp;
                                    <h:commandButton styleClass="buttonSave"  id="getTokenReset" value="Cancel" onclick="this.disabled=true;document.getElementById('frm:linkPayroll').click();">
                                    </h:commandButton>
                                </h:outputLink>
                            <p style="padding-top:25px;"><a class="redlink" id="forgottenEmailLink" href="mailto:ClientService@QSRfinancial.com?subject=Forgot%20Password">Still having trouble? Contact Us.</a></p>
                        </h:form>
                    </div>
                </div>
            </h:panelGroup>
            <f:subview id="svSccss" rendered="#{passwordManagerController.showSuccessPage}">
                <a4j:form>
                    <h:panelGroup  id="pnlSccss" layout="block" styleClass="clientArea"  rendered="#{passwordManagerController.showSuccessPage}" >
                        <h3 align="center">
                            Within a few minutes, you will receive an email with a temporary password.<br/>
                            You may change it at any time in the My Account section.<br/>
                            Once you receive your temporary password, you may 
                            <h:commandLink id="lnkWlcm" value="Sign In." action="#{passwordManagerController.redirectPage}" styleClass="links2"/>
                        </h3>
                        <br/>
                        <p align="center">If you are not redirected automatically within a few seconds, then please click on the link above.</p>

                        <a4j:poll interval="5000" oncomplete="window.location.href='signin.faces'" >
                            <f:setPropertyActionListener target="#{passwordManagerController.showSuccessPage}" value="false"/>
                        </a4j:poll>
                    </h:panelGroup>
                </a4j:form>
            </f:subview>
            <f:subview id="btm">
                <jsp:include page="includes/bottom.jsp" />
            </f:subview>
        </f:view>
    </body>
</html>