<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<!-- start footer disclaimer code -->
<table border="0" cellspacing="0" cellpadding="0" align="center" class="copyWidth">
    <tbody>
        <tr>
            <td class="subText" valign="top" height="18"><span class="subText" style="padding-left:10px;">By using this site, you agree to our <a href="terms.faces" class="red">Terms of Use</a>. Use of Sign In information by anyone other than the authorized person is strictly prohibited.</span></td>
            <td rowspan="3" width="135" align="right" valign="top">
                <table width="135" border="0" cellpadding="2" cellspacing="0" title="Click to Verify - This site chose VeriSign SSL for secure e-commerce and confidential communications.">
                    <tr>
                        <td width="135" align="center" valign="top"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.zumaplaceholder.com&amp;size=S&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=en"></script><br />
                            <a href="http://www.verisign.com/ssl-certificate/" target="_blank"  style="color:#000000; text-decoration:none; font:bold 7px verdana,sans-serif; letter-spacing:.5px; text-align:center; margin:0px; padding:0px;">ABOUT SSL CERTIFICATES</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="subText" valign="top" height="18"><span class="subText" style="padding-left:10px;">Zuma, Inc. Copyright &copy; 2010-2013</span>
                <img src="images/delim.gif" height="9" width="1" alt="|"/> <a href="about.faces" class="red">About Us</a>
                <img src="images/delim.gif" height="9" width="1" alt="|"/> <a href="terms.faces" class="red">Terms of Use</a>
                <img src="images/delim.gif" height="9" width="1" alt="|"/> <a href="policy.faces" class="red">Privacy Policy</a>
                <img src="images/delim.gif" height="9" width="1" alt="|"/>
            </td>
        </tr>
        <tr>
            <td valign="top">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- end footer disclaimer code -->
