<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<h:form id="mFrm">
    <div id="topLayer">
    <h:outputText value="Kamal Hossaib"/>
        <table border="0" cellspacing="0" cellpadding="0" align="center" class="menuWidth">
            <tbody>
               <tr>
                    <td>
                        <div id="topMenu" class="menu" style="margin-top:5px;height:42px;display:block;">
                            <div id="logoBox"><img src="images/logo5.png" alt="ZUMA"/></div>
                            <ul id="maintab" class="basictab">
                                <!--<li rel="empty"	class="<h:outputText value="#{menuManagedBean.style['welcome']}"/>"
                                    style='<h:outputText value="#{menuManagedBean.beforeSignIn}"/>'><a href="welcome.faces">Home</a></li>
                                <li rel="empty"	  class="<h:outputText value="#{menuManagedBean.style['qlinks']}"/>"
                                    style='<h:outputText value="#{menuManagedBean.afterSignIn}"/>'><a href="qlinks.faces">Home</a></li>-->
                                <!--<li rel="empty"	  class="<h:outputText value="#{menuManagedBean.style['services']}"/>"
                                    style='<h:outputText value="#{menuManagedBean.beforeSignIn}"/>'><a href="services.faces">What is QSRfinancial?</a></li>
                                <li rel="empty"	  class="<h:outputText value="#{menuManagedBean.style['getstarted']}"/>"
                                    style='<h:outputText value="#{menuManagedBean.beforeSignIn}"/>'><a href="openaccount.faces">Get Started</a></li>
                                <li rel="empty"	  class="<h:outputText value="#{menuManagedBean.style['signin']}"/>"
                                    style='<h:outputText value="#{menuManagedBean.beforeSignIn}"/>'><a href="signin.faces">Sign In</a></li>-->
                                <li rel="data"    class='<h:outputText value="#{menuManagedBean.dataStyle}"/>'
                                    style='<h:outputText value="#{menuManagedBean.dataRendered}"/>'><a href="#" <h:outputText value="#{menuManagedBean.dataClass}"/>>Data Management <span style="color:#d7d7d7;">&#9660;</span></a></li>
                                <li rel="reports" class='<h:outputText value="#{menuManagedBean.reportsStyle}"/>'
                                    style='<h:outputText value="#{menuManagedBean.reportsRendered}"/>'><a href="#" <h:outputText value="#{menuManagedBean.reportsClass}"/>>Reports and Analysis <span style="color:#d7d7d7;">&#9660;</span></a></li>
                                <!--<li rel="admin"   class='<h:outputText value="#{menuManagedBean.adminStyle}"/>'
                                    style='<h:outputText value="#{menuManagedBean.adminRendered}"/>'><a href="#">Administration <span style="color:#d7d7d7;">&#9660;</span></a></li>
                                <li rel="empty"	  class='<h:outputText value="#{menuManagedBean.style['myaccount']}"/>'
                                    style='<h:outputText value="#{menuManagedBean.myAccountRendered}"/>'><a href='<h:outputText value="#{(menuManagedBean.myAccount)?'myaccount.faces':'#'}"/>'>My Account</a></li>
                                <li rel="empty"	  class="<h:outputText value="#{menuManagedBean.style['faq']}"/>"><a href="faq.faces">FAQs</a></li>
                                <li rel="empty"	  class='<h:outputText value="#{menuManagedBean.style['contact']}"/>'><a href="contact.faces">Contact Us</a></li>-->
                            </ul>
                            <div id="empty" class="submenu" style="display:block;"><span style="padding-left:0px;"><img src="images/spacer.gif" width="1" height="1" border="0" alt=""/></span></div>
                            <div id="home" class="submenu">
                                <span style="padding-left:0px;"></span>
                                <h:commandLink rel="def" id="welcome" value="Welcome"
                                               rendered="#{!userManagedBean.logon}"
                                               styleClass="#{menuManagedBean.style['welcome']}" action="#{startedController.refreshData}">
                                    <f:setPropertyActionListener
                                        target="#{startedController.targetPage}" value="home"/>
                                </h:commandLink>
                                <h:outputLink id="services" value="services.faces"
                                              styleClass="#{menuManagedBean.style['services']}">
                                    <h:outputText value="What is QSRfinancial?"/>
                                </h:outputLink>
                                <h:outputLink rel="def" id="qlinks" value="qlinks.faces"
                                              rendered="#{userManagedBean.logon}"
                                              styleClass="#{menuManagedBean.style['qlinks']}">
                                    <h:outputText value="Quick Links"/>
                                </h:outputLink>
                                <h:outputLink id="getstarted" value="getstarted.faces"
                                              styleClass="#{menuManagedBean.style['getstarted']}"
                                              rendered="#{!userManagedBean.logon || userManagedBean.admin}">
                                    <h:outputText value="Get Started"/>
                                </h:outputLink>
                                <h:outputLink rel="def" id="signin" value="signin.faces"
                                              rendered="#{!userManagedBean.logon}"
                                              styleClass="#{menuManagedBean.style['signin']}">
                                    <h:outputText value="Sign In"/>
                                </h:outputLink>
                            </div>
                            <div id="data" class="submenu">
                                <span style="padding-left:59px;"><img src="images/spacer.gif" width="1" height="1" border="0" alt=""/></span>
                                <h:commandLink rel="def" accesskey="s"
                                               id="summary" value="Daily Summary"
                                               action="#{summaryController.setNeedRefreshFlag}"
                                               styleClass="#{menuManagedBean.style['summary']}"
                                               rendered="#{menuManagedBean.summary}">
                                    <f:setPropertyActionListener
                                        target="#{summaryController.targetPage}" value="summary" />
                                </h:commandLink>
                                <h:commandLink id="summaryconstruct" value="Daily Summary - Edit"
                                               action="#{summaryConstructController.setNeedRefreshFlag}"
                                               styleClass="#{menuManagedBean.style['summaryconstruct']}"
                                               rendered="#{menuManagedBean.summaryConstruct}">
                                    <f:setPropertyActionListener
                                        target="#{summaryConstructController.targetPage}" value="summaryconstruct" />
                                </h:commandLink>
                                <!--<h:outputLink id="purchase" value="purchase.faces"
                                              styleClass="#{menuManagedBean.style['purchase']}"
                                              rendered="#{menuManagedBean.purchase}">
                                    <h:outputText value="Purchase Journal"/>
                                </h:outputLink>
                                <h:commandLink id="inventoryview" value="Inventory"
                                               action="#{inventoryViewController.refreshPage}"
                                               styleClass="#{menuManagedBean.style['inventoryview']}"
                                               rendered="#{menuManagedBean.inventoryView}">
                                    <f:setPropertyActionListener
                                        target="#{inventoryViewController.targetPage}" value="inventoryview"/>
                                </h:commandLink>
                                <h:commandLink id="inventoryedit" value="Inventory - Edit"
                                               action="#{inventoryEditController.refreshPage}"
                                               styleClass="#{menuManagedBean.style['inventoryedit']}"
                                               rendered="#{menuManagedBean.inventoryEdit}">
                                    <f:setPropertyActionListener
                                        target="#{inventoryEditController.targetPage}" value="inventoryedit" />
                                </h:commandLink>
                                <h:commandLink id="waste" value="Waste"
                                               action="#{wasteController.setNeedRefreshFlag}"
                                               styleClass="#{menuManagedBean.style['waste']}"
                                               rendered="#{menuManagedBean.waste}">
                                    <f:setPropertyActionListener
                                        target="#{wasteController.targetPage}" value="waste" />
                                </h:commandLink>-->
                            </div>
                            <div id="reports" class="submenu">
                                <span style="padding-left:220px;"><img src="images/spacer.gif" width="1" height="1" border="0" alt=""/></span>
                                <h:outputLink rel="def" id="performance" value="performance.faces"
                                              styleClass="#{menuManagedBean.style['performance']}"
                                              rendered="#{menuManagedBean.performance}">
                                    <h:outputText value="Dashboard" />
                                </h:outputLink>
                                <!--<h:outputLink rel="def" id="reportCenter" value="reportcenter.faces"
                                              styleClass="#{menuManagedBean.style['reportcenter']}"
                                              rendered="#{menuManagedBean.reportCenter}">
                                    <h:outputText value="Report Center" />
                                </h:outputLink>
                                <h:outputLink id="profitability" value="profitability.faces"
                                              styleClass="#{menuManagedBean.style['profitability']}"
                                              rendered="#{menuManagedBean.profitability}">
                                    <h:outputText value="Profitability Tracker" />
                                </h:outputLink>-->
                                <h:commandLink id="filemanager" value="File Manager"
                                               styleClass="#{menuManagedBean.style['filemanager']}"
                                               rendered="#{menuManagedBean.fileManager}"
                                               action="#{fileSystemBean.refreshPage}">
                                    <f:setPropertyActionListener
                                        target="#{fileSystemBean.targetPage}" value="filemanager" />
                                </h:commandLink>
                            </div>
                            <div id="admin" class="submenu">
                                <span style="padding-left:400px;"><img src="images/spacer.gif" width="1" height="1" border="0" alt=""/></span>
                                <h:commandLink rel="def" id="accountmanagement" value="Account Management"
                                               action="#{accountManagementController.refreshPage}"
                                               styleClass="#{menuManagedBean.style['management']}"
                                               rendered="#{userManagedBean.admin}">
                                    <f:setPropertyActionListener
                                        target="#{accountManagementController.targetPage}" value="management" />
                                </h:commandLink>
                                <h:commandLink id="accountdetails" value="Account Details"
                                               action="#{accountManagedBean.refresh}"
                                               styleClass="#{menuManagedBean.style['accountdetails']}"
                                               rendered="#{userManagedBean.admin}">
                                    <f:setPropertyActionListener value="accountdetails"
                                                                 target="#{accountManagedBean.targetPage}"/>
                                </h:commandLink>
                                <h:commandLink id="createuser" value="Create User"
                                               action="createuser"
                                               styleClass="#{menuManagedBean.style['createuser']}"
                                               rendered="#{userManagedBean.admin}">
                                    <f:setPropertyActionListener
                                        target="#{createUserController.selectedUser}" value=""/>
                                </h:commandLink>
                            </div>
                        </div>
                    </td>
                    
                </tr>
                
            </tbody>
        </table>
         <div id="topBanner">
                <h:panelGroup layout="block" id="pAccont" styleClass="topInfoBox" rendered="#{userManagedBean.logon}">
                <div id="welcomeBox">
                    <table cellpadding="2" cellspacing="0" border="0">
                        <tr><td style="padding-left:3px;padding-right:10px;height:28px;max-height:28px;"><h:outputText value="#{userManagedBean.welcomeMessage}" styleClass="topCap"/></td>
                            <td><h:commandLink id="logout" value="Sign Off" styleClass="topLogout"
                                           action="#{userManagedBean.logout}" rendered="#{userManagedBean.logon}"/></td></tr>
                    </table>
                </div>
                <h:panelGroup layout="block" id="pWeek" styleClass="weekBox" rendered="#{menuManagedBean.menuWeekRendered}"><h:outputText value="Active Week:" styleClass="topText"/><br/>
                    <h:selectOneMenu id="weekCombo" styleClass="topCombo" style="width:160px;" value="#{userManagedBean.activeWeekDay}" onchange="submit()">
                        <f:convertDateTime type="date" pattern="MM/dd/yyyy"/>
                        <f:selectItems value="#{userManagedBean.reportingWeeks}"/>
                    </h:selectOneMenu>
                </h:panelGroup><h:panelGroup layout="block" id="pStore" styleClass="storeBox" rendered="#{menuManagedBean.menuStoreRendered}">
                    <h:outputText value="Active Store:" styleClass="topText"/><br/>
                    <h:selectOneMenu id="storeCombo" styleClass="topCombo" style="width:170px;" value="#{userManagedBean.activeStore}" onchange="submit()">
                        <f:selectItems value="#{userManagedBean.storeItems}" />
                        <f:converter converterId="selectMapConverter"/>
                    </h:selectOneMenu></h:panelGroup><h:panelGroup layout="block" id="pAccount" styleClass="accountBox" rendered="#{menuManagedBean.menuAccountRendered}">
                    <h:outputText value="Active Account:" styleClass="topText"/><br/>
                    <h:selectOneMenu id="accountCombo" styleClass="topCombo" value="#{userManagedBean.activeAccount}" rendered="#{userManagedBean.admin}" onchange="submit()">
                        <f:selectItems value="#{userManagedBean.accountItems}" />
                        <f:converter converterId="selectMapConverter"/>
                    </h:selectOneMenu><h:outputText styleClass="topCap" value="#{userManagedBean.activeUser.company.caption}" rendered="#{!userManagedBean.admin}"/>
                </h:panelGroup>
            </h:panelGroup>
        </div>

        <!-- don't use tag <script/> in xml standart-->
        <script type="text/javascript" src="js/menu.js"></script>
    </div>
</h:form>
