<%@ page language="java"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<style type="text/css">
    .tooltip {
        background-color:#F8FBFB;
        border-width:3px;
        padding:10px;
        width:500px;
        border-color:#DBE0E3;
    }
    .tooltip-text {
        width:200px;
        height:120px;
        cursor:arrow;
        border-width:2px;
        text-align:center;
        display: table-cell;
        vertical-align: middle;
    }
    .tooltipData {
        font-weight: bold;
    }
</style>
<rich:toolTip id="tt"  direction="auto"  styleClass="tooltip" layout="block"
              for="ttreferral" event="onclick">
    <span  class="header_gray">Share the benefits of QSRfinancial<sup>TM</sup> with someone you know...</span>
    <p>Share the benefits of QSRfinancial<sup>TM</sup> and get $100 for every referral.
        Eligible referrals must continue service for three consecutive months. Referral rewards will be credited against your service fee at that time.
        It's just our way of saying thank you for being a client and telling others about QSRfinancial<sup>TM</sup>.</p><br/>
    <span class="header_gray">Start referring now...</span>
    <ul class="leftnav1">
        <li>Tell someone you know about the benefits of QSRfinancial<sup>TM</sup>.</li>
        <li>Email us at <a href="mailto:ClientService@QSRfinancial.com" class="redlink">ClientService@QSRfinancial.com</a> (or have them contact us and mention your name).</li>
        <li>Enjoy a $100 credit to your account at the end of 90 days for each eligible referral.</li>
    </ul>
</rich:toolTip>
