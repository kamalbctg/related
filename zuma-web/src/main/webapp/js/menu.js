/***********************************************
* 2 level Horizontal Tab Menu- by JavaScript Kit (www.javascriptkit.com)
* This notice must stay intact for usage
* Visit JavaScript Kit at http://www.javascriptkit.com/ for full source code
***********************************************/
var mastertabvar = new Object()
mastertabvar.selecteditem = "empty"
mastertabvar.prev = mastertabvar.selecteditem
mastertabvar.refreshcount = 0;

var counter = 0
var callCount = 0

function showsubmenu(masterid, id) {
    if (mastertabvar.prev == id && mastertabvar.refreshcount > 0) return

    // hide blank spase under menu
    var empty = document.getElementById("empty")
    if(id!="empty" && empty.style.display == "block") {
        empty.style.display = "none";
    }

    mastertabvar.refreshcount = 1
    var submenuobject = document.getElementById(id)
    hidesubmenus(mastertabvar[masterid])
    submenuobject.style.display = "block"
    mastertabvar.prev = id
}

function showlink(id) {
    var linkitems = document.getElementById(id).getElementsByTagName("a");
    for (var i=0; i<linkitems.length; i++) {
        if(linkitems[i].getAttribute("rel")) {
            var callClick = linkitems[i].onclick
            if (callClick != null) {
                callClick() // generate on click
            }
            else {
                document.location = linkitems[i].getAttribute("href")
            }
            break;
        }
    }
}

function hidesubmenus(submenuarray) {
    for (var i = 0; i < submenuarray.length; i++)
        document.getElementById(submenuarray[i]).style.display="none"
}

function initializetab(tabid) {
    counter = 0;
    var main_menu = document.getElementById("topMenu");
    main_menu.onmouseover = overMenu;
    main_menu.onmouseout = outMenu;

    mastertabvar[tabid] = new Array()
    var menuitems = document.getElementById(tabid).getElementsByTagName("li")
    if (menuitems == null) return;
    showsubmenu(tabid, "empty");
    for (var i=0; i<menuitems.length; i++) {
        if (menuitems[i].getAttribute("rel")) {
            menuitems[i].setAttribute("rev", tabid) //associate this submenu with main tab
            mastertabvar[tabid][mastertabvar[tabid].length] = menuitems[i].getAttribute("rel") //store ids of submenus of tab menu
            var relItem = menuitems[i].getAttribute("rel")
            if (menuitems[i].className == "selected") {
                showsubmenu(tabid, relItem)
                mastertabvar.selecteditem = relItem
                mastertabvar.prev = mastertabvar.selecteditem
            }
            if(menuitems[i].getElementsByTagName("a")[0] != null) {
                menuitems[i].getElementsByTagName("a")[0].onclick = function() {
                    if (this.className == "disabled") return;
                    if (this.className == "locked") {
                        alert("You do not have access to this section.\nPlease contact your administrator if you wish to change your access rights.")
                        return
                    }
                    showsubmenu(this.parentNode.getAttribute("rev"),
                                this.parentNode.getAttribute("rel"))
                    showlink(this.parentNode.getAttribute("rel"));
                }
                menuitems[i].getElementsByTagName("a")[0].onmouseover = function() {
                    if (this.className == "disabled") return;
                    if (this.className == "locked") return;
                    showsubmenu(this.parentNode.getAttribute("rev"),
                                this.parentNode.getAttribute("rel"))
                }
            }
        }
    }
}

function overMenu()
{
    counter++
}

function outMenu()
{
    counter--
    refreshMenu()
}

function  refreshMenu()
{
    callCount++
    if (typeof intervalHide != "undefined")
        window.clearInterval(intervalHide)
    intervalHide = window.setInterval('delayedRefresh()',500)
}

function delayedRefresh()
{
 if (callCount == 0) return // if no new calls - return
 if (counter == 0) {
    showsubmenu('maintab', mastertabvar.selecteditem)
 }
 callCount = 0; // all calls during delay are nulled
}

initializetab("maintab");