<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Privacy Policy</title>
        <link type="text/css" rel="stylesheet" href="css/zuma.css"/>
        <link type="text/css" rel="stylesheet" href="css/simple.css"/>
    </head>
    <body>
        <f:view>
            <f:subview id="mnu">
                <jsp:include page="includes/menu.jsp" />
            </f:subview>
            <f:subview id="tc">
                <jsp:include page="includes/topclient.jsp" />
            </f:subview>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tbody><tr>
                        <td class="column_div" valign="top" width="200"><h1 class="pageCaption">Privacy Policy</h1></td>
                        <td valign="top" style="padding-left:5px;padding-top:5px;">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td valign="top">
                                            <p>Maintaining a high degree of confidence from you regarding the protection and secure transmission of all personal, company and financial information obtained through our site is of the utmost importance to us. This Privacy and Security Statement discloses our privacy policy and security practices.</p>
                                            <h1>Security and Protection</h1>
                                            <p>We are committed to maintaining the security of personal information obtained through our site.  We take significant precautions to protect your personal information using industry standard physical, electronic and procedural safeguards.  Our systems are configured with data encryption technologies and firewalls.  When you send or receive personal information using our site, your data is protected by 128-bit Secure Socket Layer (SSL) technology to ensure safe transmission.  We also have a digital certificate from VeriSign, Inc., a top maker of hardware and software used to protect and manage computer network access, certifying the secure nature of our site.  All of our servers storing personal information are kept in a secure environment and may be accessed only by a limited number of authorized persons, all of whom are under signed confidentiality agreements specifically prohibiting their use or dissemination of such information in any unauthorized manner.</p>
                                            <h1>Information We Collect</h1>
                                            <p>We collect personal and company information that you explicitly provide to us such as your name, company name, company and store addresses, e-mail addresses, phone numbers and credit card and other payment information.  We collect and store on our servers finance and accounting information explicitly provided by you in connection with agreed upon finance and accounting services.  We may collect information about your computer (IP address, operating system and Web browser software) for the purpose of assessing the effectiveness of site content and traffic.  This data allows us to improve the quality of your visit by streamlining your ability to navigate the site.</p>
                                            <h1>Use of Your Personal Information</h1>
                                            <p>We value your confidence and want you to feel confident when using our services.</p>
                                            <ul class="leftnav1" style="color:#464646;">
                                                <li>We do not sell, share or rent your personal information to anyone for any reason.</li>
                                                <li>We use your personal information to provide you with agreed upon information and services and may use your personal information to tell you about other services and products that we offer.</li>
                                                <li>We may aggregate information about you and your use of this site with other information collected on this site or other sites in ways that do not personally identify you. This type of aggregated or statistical information may be used by us to improve the quality of our site or for other purposes that we may deem appropriate.</li>
                                            </ul><br/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                </tbody>
            </table>
            <f:subview id="btm">
                <jsp:include page="includes/bottom.jsp" />
            </f:subview>
        </f:view>
    </body>
</html>