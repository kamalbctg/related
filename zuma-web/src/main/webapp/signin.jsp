<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        
        <title>Sign In to Zuma</title>
        <link type="text/css" rel="stylesheet" href="css/zuma.css"/>
        <style type="text/css">
        </style>
    </head>
    <body>
        <f:view>
            <f:subview id="flying">
                <c:import url="includes/flyingpanels.jsp" />
            </f:subview>
            <f:subview id="mnu">
                <jsp:include page="includes/menu.jsp" />
            </f:subview>
            <f:subview id="tc">
                <jsp:include page="includes/topclient.jsp" />
            </f:subview>
            <!-- start content code -->
            <h:panelGroup layout="block" id="badLogin"
                          rendered="#{!userManagedBean.logon && userManagedBean.loginAttempt}"
                          style="width:936px;text-align:center;padding-top:5px;">
                <h:outputText value="Invalid Email Address or Password. Please try again."
                              styleClass="failed"/>
            </h:panelGroup>
            <h:panelGroup  layout="block" style="float:left;width:100%;padding-top:20px;padding-left:20px;border-left:40px;align:center" rendered="#{userManagedBean.logon}" >
                <h1 class="pageCaption" style="float:left;">You Are Successfully Signed Into Your Account</h1>
                <br/><br/><br/>
                <div align="center">
                    <table align="center" width="350px;">
                        <tr><td>
                                <img src ="images/bgModuleWelcome2.jpg" style=" background:no-repeat 1px 0px; width:314px; height:235px;" alt="Welcome"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </h:panelGroup>
            <h:panelGroup  layout="block" style="float:left;width:100%;padding-top:20px;padding-left:20px;border-left:40px;align:center" rendered="#{!userManagedBean.logon}" >
                <h1 class="pageCaption" style="float:left;">Sign In to Zuma<sup>TM</sup></h1>
            </h:panelGroup>
            <h:panelGroup  layout="block" style="float:left;width:700px;padding-top:20px;padding-left:20px;border-left:40px; padding-bottom:20px;" >
                <table width="460" align="right" cellpadding="0" cellspacing="0" style=" border:1px solid #c4c0c9;" >
                    <tbody>
                        <tr>
                            <td style="" valign="top" >
                                <h:form id="loginForm" rendered="#{!userManagedBean.logon}">
                                    <table style="margin-left: 10px; margin-top: 15px;" width="460" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td colspan="3" valign="top" height="23"> &nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="110px" valign="top" align="right"  style="padding-right:10px;">Email Address:</td>
                                                <td valign="top" width="200px" height="20">
                                                    <h:inputText id="usertxt" styleClass="textfield" style="width: 175px; height:18px;"
                                                                 value="#{userManagedBean.name}" required="true" requiredMessage="Email is a required field."  />
                                                </td>
                                                <td valign="top" width="150">&nbsp;</td>
                                            </tr>
                                            <tr><td colspan="3">&nbsp;</td></tr>
                                            <tr>
                                                <td valign="top" align="right" style="padding-right:10px;">Password:</td>
                                                <td valign="top" valign="top" height="20" width="175">
                                                    <h:inputSecret id="passwordtxt" styleClass="textfield" value="#{userManagedBean.password}"
                                                                   style="width: 175px; height:18px;" redisplay="false"
                                                                   requiredMessage="Password is a required field."/>
                                                </td>
                                                <td valign="top" width="95"><a href="forgot.faces" class="redlink">Forgot Password?</a></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td valign="top">&nbsp;</td>
                                                <td colspan="2"><h:commandButton
                                                        id="loginbutton" value=""
                                                        style="width:100px;height:22px;border:0;"
                                                        image="images/btn-signin.png"
                                                        action="#{userManagedBean.verifyUser}"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </h:form>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </h:panelGroup>
            <f:subview id="btm">
                <jsp:include page="includes/bottom.jsp" />
            </f:subview>
        </f:view>
                    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16421277-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
    </body>
</html>