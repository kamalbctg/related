    <%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <link href="http://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet" type="text/css">
        <meta name="description" content="Zuma is a SaaS bookkeeping and business intelligence solution for restaurants."/>
        <meta name="keywords" content="Zuma, Bookkeeping, QSR Bookkeeping, Restaurant Bookkeeping, Franchise Bookkeeping, Outsourced Bookkeeping"/>
        <title>Sign In to Zuma</title>
        <link type="text/css" rel="stylesheet" href="css/zuma.css"/>
        <link rel="stylesheet" type="text/css" href="css/signin.css">
        <style type="text/css"></style>
    </head>

          <!-- subview and panel groups -->
      <f:view>
            <f:subview id="flying">
                <c:import url="includes/flyingpanels.jsp" />
            </f:subview>
            

        <h:panelGroup layout="block" id="badLogin"
                          rendered="#{!userManagedBean.logon && userManagedBean.loginAttempt}"
                          style="width:936px;text-align:center;padding-top:5px;">
                <h:outputText value="Invalid Email Address or Password. Please try again."
                              styleClass="failed"/>
            </h:panelGroup>
            <h:panelGroup  layout="block" style="float:left;width:100%;padding-top:20px;padding-left:20px;border-left:40px;align:center" rendered="#{userManagedBean.logon}" >
                
            </h:panelGroup>
             
           <!-- start content code -->
            <h:panelGroup>
               <body>

                <div id="header">
                  <div class="container">
                 <div id="logoBox2"><img src="images/logo5.png" alt="ZUMA"/></div>
                                 
                  <div class="login">
    <fieldset>
                    <ul id="control_gen_0">
                          <p class="textad"></p>

                <h:form id="loginForm" rendered="#{!userManagedBean.logon}">
                    
                    <li> <label>Email Address</label>
                        <div class="fieldgroup">
                    <h:inputText id="usertxt" styleClass="textfield" style="width: 170px; height:20px;"
                                 value="#{userManagedBean.name}" required="true" requiredMessage="Email is a required field." tabindex="1"  />
                        </div></li>
                    <li> <label>Password</label>
                         <a href="forgot.faces" class="forgot-pwd" tabindex="4">Forgot Password?</a>
                        <div class="fieldgroup">
                         <h:inputSecret id="passwordtxt" styleClass="textfield" value="#{userManagedBean.password}"
                                   style="width: 170px; height:20px;" redisplay="false"
                                   requiredMessage="Password is a required field." tabindex="2"/>
                        </div>
                   </li>
                       <li>
                    <h:commandButton id="signin" value="Sign In"
                                     
                                     styleClass="btn-secondary"
                                     tabindex="3"
                                     type="submit"
                                     
                                     action="#{userManagedBean.verifyUser}"/>
                      </li>
                 </h:form>
                       
                       </ul>
       </fieldset>
                   </div>
                </div>
                    </div> 
<div
 class="blockMain"
 id="blockMain">
<div class="container-12"><br>
<br>
<br>
<br>
<br>
<br>
<br>
<h2>We save restaurants time and money with powerful
data
integration, data analysis and financial reporting tools.</h2>
<div class="product-list product-grid-3 clearfix">
<div class="cta-box products-desktop clearfix"> <b
 style="color: rgb(0, 97, 156);" class="badge icon-laptop"><img
 style="width: 37px; height: 35px;" alt="int"
 src="images/int.png"></b>
<h3>Data Integration</h3>
<span class="tagline">SPEND LESS TIME<br>
COLLECTING DATA</span>
<p>POS, bank, credit card, payroll, inventory and accounting all in one place</p>
</div>
<div class="cta-box products-server clearfix"> <b
 style="color: rgb(0, 97, 156);" class="badge icon-people"><img
 style="width: 37px; height: 35px;" alt="ana"
 src="images/ana.png"></b>
<h3>Data Analysis</h3>
<span class="tagline">MAKE FASTER, MORE<br>
INFORMED DECISIONS</span>
<p>Robust reporting and data analytics tools available anytime, anywhere</p>
</div>
<div class="cta-box products-online clearfix"> <b
 style="color: rgb(0, 97, 156);" class="badge icon-cursor-cloud"><img
 style="width: 37px; height: 35px;" alt="rep"
 src="images/rep1.png"></b>
<h3>Financial Reporting</h3>
<span class="tagline">SPEND LESS MONEY<br>
FOR BETTER ACCOUNTING</span>
<p>People, processes and technology focused exclusively on restaurants</p>
</div>
</div>
</div>
</div>
<br>
<br>

             </h:panelGroup>
        </f:view>
                    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-16421277-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
    </body>
</html>