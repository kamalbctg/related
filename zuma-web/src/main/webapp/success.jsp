<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="refresh" content="5; URL=welcome.faces"/>
        <title>Congratulation</title>
        <link type="text/css" rel="stylesheet" href="css/zuma.css"/>
        <link type="text/css" rel="stylesheet" href="css/simple.css"/>
    </head>
    <body>
        <f:view>
            <f:subview id="mnu">
                <jsp:include page="includes/menu.jsp" />
            </f:subview>
            <f:subview id="tc">
                <jsp:include page="includes/topclient.jsp" />
            </f:subview>
            <h1>Successful registration!</h1><br/>
            <p>You can continue working from our <a href="welcome.faces" class="links2">Welcome Page</a></p>
            <p align="center">If you are not redirected automatically within a few
                seconds then please click on the link above.</p>
                <f:subview id="btm">
                    <jsp:include page="includes/bottom.jsp" />
                </f:subview>
            </f:view>
    </body>
</html>