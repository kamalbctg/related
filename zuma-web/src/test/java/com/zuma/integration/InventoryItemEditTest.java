package com.zuma.integration;

import java.io.IOException;
import java.net.MalformedURLException;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlButtonInput;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlImageInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;

@Ignore
public class InventoryItemEditTest {

	private static WebClient client;
	private HtmlPage page;
	private HtmlElement inventoryForm;
	private static final int TOTAL_ELEMENTS_TO_ADD = 10;
	private static final String BEGINNING_ELEMENT_NAME_ID = "inventoryeditbody:inventoryform:categorylist:0:subcategorylist:0:itemlist:";
	private static final String ENDING_ELEMENT_NAME_ID = ":itemnametext";
	private static final String[] NEW_ELEMENT_NAME_IDS = getenateNewElementIds();
	private static final int START_INDEX = 6;
	private static final String NEW_ELEMENT_DELETE_LINK_ID = "inventoryeditbody:inventoryform:categorylist:0:subcategorylist:0:itemlist:6:itemdeletebutton";
	private static final String FIRST_SUB_CATEGORY_ADD_ITEM_LINK_ID = "inventoryeditbody:inventoryform:categorylist:0:subcategorylist:0:itemaddlink";
	private static final String TEST_ELEMENT_NAME = "Test item";
	public static final String INVENTORY_EDIT_PAGE = "http://localhost:8081/zuma-web/inventoryedit.faces";
	public static final String INVENTORY_LOGIN_PAGE = "http://localhost:8081/zuma-web/welcome.faces";
	public static final String LOGIN_TEXT_ID = "loginForm:usernametext";
	public static final String LOGIN_FORM_ID = "loginForm";
	public static final String PASSWORD_TEXT_ID = "loginForm:passwordtext";
	public static final String LOGIN_BUTTON_ID = "loginForm:loginbutton";
	private static final String INVENTORYFORM_SAVE_HEADER_BUTTON = "inventoryeditbody:inventoryform:categorylist:0:savetopbutton";
	public static final String LOGIN_NAME = "admin";
	public static final String PASSWORD = "admin";
	public static final int JAVASCRIPT_TIMEOUT = 300000;

	@BeforeClass
	public static void init() throws IOException {
		client = new WebClient();
		client.setJavaScriptTimeout(JAVASCRIPT_TIMEOUT);

	}

	@Before
	public void loadInventoryEditPage() throws IOException,
			MalformedURLException {
		page = client.getPage(INVENTORY_EDIT_PAGE);
		// outputAll(page);
		inventoryForm = page.getElementById("inventoryeditbody:inventoryform");
	}

	@Test
	public void login() throws IOException {
		HtmlPage loginPage = client.getPage(INVENTORY_LOGIN_PAGE);
		HtmlElement loginForm = loginPage.getElementById(LOGIN_FORM_ID);

		HtmlTextInput userName = (HtmlTextInput) loginForm
				.getElementById(LOGIN_TEXT_ID);
		userName.setValueAttribute(LOGIN_NAME);

		HtmlPasswordInput password = (HtmlPasswordInput) loginForm
				.getElementById(PASSWORD_TEXT_ID);
		password.setValueAttribute(PASSWORD);
		HtmlSubmitInput loginButton = (HtmlSubmitInput) loginForm
				.getElementById(LOGIN_BUTTON_ID);
		loginButton.click();
		loadInventoryEditPage();
	}

	@Test
	public void testCreateReadElement() throws IOException {
		HtmlAnchor addItemAnchor = inventoryForm
				.getElementById(FIRST_SUB_CATEGORY_ADD_ITEM_LINK_ID);
		addItemAnchor.click();
		client.waitForBackgroundJavaScript(JAVASCRIPT_TIMEOUT);
		HtmlTextInput newItem = inventoryForm
				.getElementById(NEW_ELEMENT_NAME_IDS[0]);
		saveChanges();
	}

	@Test
	public void testUpdateElement() throws IOException {

		HtmlTextInput newItem = inventoryForm
				.getElementById(NEW_ELEMENT_NAME_IDS[0]);

		newItem.setValueAttribute(TEST_ELEMENT_NAME);
		saveChanges();

		HtmlTextInput newItemNameNode = inventoryForm
				.getElementById(NEW_ELEMENT_NAME_IDS[0]);
		TestCase.assertEquals(TEST_ELEMENT_NAME, inventoryForm.getElementById(
				NEW_ELEMENT_NAME_IDS[0]).getAttribute("value"));
	}

	@Test(expected = ElementNotFoundException.class)
	public void testDeleteElement() throws IOException {
		HtmlImageInput deleteItemElement = inventoryForm
				.getElementById(NEW_ELEMENT_DELETE_LINK_ID);
		deleteItemElement.click();
		client.waitForBackgroundJavaScript(JAVASCRIPT_TIMEOUT);
		inventoryForm.getElementById(NEW_ELEMENT_NAME_IDS[0]);
	}

	public void testAddNItems() {

	}

	@Ignore
	private void saveChanges() throws IOException {
		HtmlButtonInput saveButton = inventoryForm
				.getElementById(INVENTORYFORM_SAVE_HEADER_BUTTON);
		saveButton.click();
	}

	@Ignore
	private void outputAll(HtmlPage page) throws IOException {

		for (HtmlElement element : page.getAllHtmlChildElements()) {
			System.out.println(element);
		}
	}

	public static String[] getenateNewElementIds() {

		String[] ids = new String[TOTAL_ELEMENTS_TO_ADD];
		for (int i = 0; i < TOTAL_ELEMENTS_TO_ADD; i++) {
			ids[i] = BEGINNING_ELEMENT_NAME_ID + (i + START_INDEX)
					+ ENDING_ELEMENT_NAME_ID;
		}
		return ids;
	}
}