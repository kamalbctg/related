<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>---DEBUG PAGE---</title>
        <link type="text/css" rel="stylesheet" href="css/zuma.css" />
        <link rel="stylesheet" type="text/css" href="css/inventoryview.css" />
    </head>
    <body>
        <f:view>
            <div id="inventiryViewContent" align="center"><f:subview
                    id="menuView">
                    <jsp:include page="includes/menu.jsp" />
                </f:subview> <h:form id="debugform">
                    <h:inputText label="Enter sql here" id="sqltext"
                                 value="#{debugController.userEnteredSql}" />
                    <h:inputText label="Enter Execution count" id="execcount"
                                 value="#{debugController.sqlExecutionCount}" />

                    <h:commandButton id="executsql" value="Execute sql!"
                                     action="#{debugController.executeSQLAndMarkTime}"></h:commandButton>
                    <h:messages layout="table" errorClass="message" styleClass="message" />

                    <h:dataTable value="#{debugController.sqlResultStrings}" var="row">
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="RESULT STRINGS" />
                            </f:facet>
                            <h:outputText value="#{row}" />
                        </h:column>
                    </h:dataTable>

                    <h:dataTable value="#{debugController.debugInfKeyList}" var="key">
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="Key" />
                            </f:facet>
                            <h:outputText value="#{key}" />
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="Value" />
                            </f:facet>
                            <h:outputText value="#{debugController.debugInfoMap[key]}" />
                        </h:column>
                    </h:dataTable>

                </h:form> <f:subview id="btm">
                    <jsp:include page="includes/bottom.jsp" />
                </f:subview></div>
        </f:view>
    </body>
</html>