<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<% response.setStatus(HttpServletResponse.SC_OK); %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Page expired!</title>
        <style type="text/css">
            root {
                display: block;
            }
            * {
                margin: 0px;
                padding: 0px;
            }
            html {
                width: 100%;
                height: 100%;
                background-color: #FFFFFF;
            }
            body {
                width:100%;
            }
            p {
                font-family: Arial,Helvetica,sans-serif;
                font-size: 12px;
                font-weight: bold;
            }
            .client {
                height:100px;
                padding-top:20px;
                padding-left:20px;
                color:#666666;
            }
            a, a:link, a:hover, a:active, a:visited {
                text-decoration:underline;
                color: black;
            }
            a:hover, a:active
            {
                color: red;
            }
            .ex {
                color: red;
            }
        </style>
    </head>
    <body>
            <div class="client">
                <p>Sorry, the requested page was expired. You can continue working from
                    <a href="/welcome.faces">Welcome Page</a></p>
            </div>
    </body>
</html>