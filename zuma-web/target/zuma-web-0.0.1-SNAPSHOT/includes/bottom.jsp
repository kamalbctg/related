<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<!-- start footer disclaimer code -->

<td class="pgRightBackground"><img src="images/spacer.gif" height="3" width="9" border="0" alt=""/></td>

<tr>
    <td height="8" width="9"><img src="images/pgborder_btmleft.gif" height="8" width="9" border="0" alt=""/></td>
    <td class="pgBottomBackground"><img src="images/spacer.gif" height="8" border="0" alt=""/></td>
    <td width="9"><img src="images/pgborder_btmright.gif" height="8" width="9" border="0" alt=""/></td>
</tr>
</tbody>

<table border="0" cellspacing="0" cellpadding="0" align="center" class="copyWidth">
    <tbody>
        <tr>
            <td class="subText" valign="top" height="18"><span class="subText" style="padding-left:10px;">By using this site, you agree to our <a href="terms.faces" class="red">Terms of Use</a>. Use of Sign In information by anyone other than the authorized person is strictly prohibited.</span></td>
            <td rowspan="3" width="200" align="right" valign="top">
                <table width="200" border="0" cellpadding="0" cellspacing="0" title="Click to Verify - This site chose VeriSign SSL for secure e-commerce and confidential communications.">
                    <tr>
                        <td width="100" align="center" valign="top" style="padding-top:0;"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.qsrfinancial.com&amp;size=S&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=en"></script></td>
                        <td width="100" align="center" valign="top" style="padding-top:8px;">
			<!-- START SCANALERT CODE -->
				<a target="_blank" href="https://www.mcafeesecure.com/RatingVerify?ref=www.qsrfinancial.com"><img width="94" height="54" border="0" src="//images.scanalert.com/meter/www.qsrfinancial.com/23.gif" alt="McAfee SECURE sites help keep you safe from identity theft, credit card fraud, spyware, spam, viruses and online scams" oncontextmenu="alert('Copying Prohibited by Law - McAfee Secure is a Trademark of McAfee, Inc.'); return false;"></a>
			<!-- END SCANALERT CODE -->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="subText" valign="top" height="18"><span class="subText" style="padding-left:10px;">&copy; 2013 Zuma, Inc.  All rights reserved.</span>
                <!--<img src="images/delim.gif" height="9" width="1" alt="|"/> <a href="about.faces" class="red">About Us</a>-->
                <img src="images/delim.gif" height="9" width="1" alt="|"/> <a href="terms.faces" class="red">Terms of Use</a>
                <img src="images/delim.gif" height="9" width="1" alt="|"/> <a href="policy.faces" class="red">Privacy Policy</a>
                
            </td>
        </tr>
        <tr>
            <td valign="top" style="height:36px;">&nbsp;</td>
        </tr>
    </tbody>
</table>
<!-- end footer disclaimer code -->
