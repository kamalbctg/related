<%@ page language="java"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>

<style  type="text/css">
    .error-header {
        width:350px;
    }
    .rich-mpnl-header {
        background-image: url('images/bg_red.gif');
    }
    .rich-mpnl-body {
        padding-bottom:3px;
    }
    .hidelink {
        cursor:pointer;
        padding-right:1px;
        padding-top:1px;
    }
    .core-message {
        color:red;
    }
</style>

<div id="save_approve" style="display:none;">
    Thank you.  Your changes have been saved.
</div>
<rich:modalPanel id="modalPnl" width="350" autosized="true"
                 showWhenRendered="#{ErrorCount>0}" zindex="2000"
                 onshow='setHighlight("#{highlight}"); setFocus("#{focus}"); this.style.position="fixed"'
                 onhide='setHighlight("#{highlight}")'>
    <f:facet name="header">
        <h:panelGroup styleClass="error-header">
            <h:outputText value="Error Detected"></h:outputText>
        </h:panelGroup>
    </f:facet>
    <f:facet name="controls">
        <h:panelGroup>
            <h:graphicImage value="/images/close.png" styleClass="hidelink" id="hideBtn"/>
            <rich:componentControl for="modalPnl" attachTo="hideBtn" operation="hide" event="onclick"/>
        </h:panelGroup>
    </f:facet>
    <h:messages styleClass="core-message" layout="table" />
    <br/>
    <div style="text-align:center;">
        <h:outputLink value="#" id="closeBtn" >Close
            <rich:componentControl for="modalPnl" attachTo="closeBtn" operation="hide" event="onclick"/>
        </h:outputLink>
    </div>
</rich:modalPanel>

<script type="text/javascript">

    var yesCallBack = null;
    var cancelCallBack = null;
    function messageDlg(onYes,onCancel,message,caption)
    {
        if($("flying:confirmationContainer").style.display=="")
            return false;
        yesCallBack = onYes;
        cancelCallBack = onCancel;
        if(message)
            $("flying:mes").textContent = message;
        if(caption)
            $("flying:mesHead").textContent = caption;
        showDialog();
        return true;
    }
    function showDialog()
    {
        Richfaces.showModalPanel('confirmation') ;
    }
    function hideDialog()
    {
        Richfaces.hideModalPanel('confirmation') ;
    }
    function doYes()
    {
        if(yesCallBack)
            yesCallBack();
        hideDialog();
    }
    function doCancel()
    {
        if(cancelCallBack)
            cancelCallBack();
        hideDialog();
    }
</script>
<rich:modalPanel id="confirmation" autosized="true" width="350" rendered="true" showWhenRendered="false">
    <f:facet name="header">
        <h:panelGroup id="mesHead" styleClass="error-header" style="font-size:12px;">Confirmation
        </h:panelGroup>
    </f:facet>
    <f:facet name="controls">
        <h:panelGroup>
            <h:graphicImage value="/images/close.png" styleClass="hidelink" id="btnClose" onclick="doCancel();return false;"/>
        </h:panelGroup>
    </f:facet>
    <h:panelGrid>
        <h:panelGrid columns="2">
            <h:graphicImage value="images/alert.png" />
            <h:outputText id="mes" value="Are you sure?" style="font-size: 12px;"/>
        </h:panelGrid>
        <h:panelGroup layout="block" style="text-align:right;">
            <input type="button" value="OK" style="width:55px;"
                   onclick="doYes();return false;" />
            <input type="button" value="Cancel" style="width:55px;"
                   onclick="doCancel();return false;" />
        </h:panelGroup>
    </h:panelGrid>
</rich:modalPanel>
<script type="text/javascript">
    function showSavingSign()
    {
        Richfaces.showModalPanel('saving') ;
    }
    function hideSavingSign()
    {
        Richfaces.hideModalPanel('saving') ;
    }
</script>
        <rich:modalPanel id="saving" rendered="true" width="65" height="65"
                 showWhenRendered="false" onshow="this.style.position='fixed'"
                 style="background-color:none;background-image:url(images/saving.gif); background-repeat: no-repeat;">

</rich:modalPanel>
