<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<rich:dataTable id="tbl" value="#{summaryController.dataSet.dataValues}" var="categoryData" >
    <f:facet name="header">
        <rich:columnGroup>
            <rich:column id="hd_Title" width="272" style="text-align:left;">
                <h:outputText value="Day" />
            </rich:column>
            <rich:column id="hd_Mon" width="72">
                <h:outputText value="Mon" />
            </rich:column>
            <rich:column id="hd_Tue" width="72">
                <h:outputText value="Tue" />
            </rich:column>
            <rich:column id="hd_Wed" width="72">
                <h:outputText value="Wed" />
            </rich:column>
            <rich:column id="hd_Thu" width="72">
                <h:outputText value="Thu" />
            </rich:column>
            <rich:column id="hd_Fri" width="72">
                <h:outputText value="Fri" />
            </rich:column>
            <rich:column id="hd_Sat" width="72">
                <h:outputText value="Sat" />
            </rich:column>
            <rich:column id="hd_Sun" width="72">
                <h:outputText value="Sun" />
            </rich:column>
            <rich:column id="hd_Total" width="80">
                <h:outputText value="Total" />
            </rich:column>
            <rich:column id="hd_date" breakBefore="true" style="text-align:left;">
                <h:outputText value="Date"/>
            </rich:column>
            <rich:column id="hd_dt0" >
                <h:outputText value="#{summaryController.dataSet.dates[0]}" >
                    <f:convertDateTime pattern="MM/dd"/>
                </h:outputText>
            </rich:column>
            <rich:column id="hd_dt1" >
                <h:outputText value="#{summaryController.dataSet.dates[1]}" >
                    <f:convertDateTime pattern="MM/dd"/>
                </h:outputText>
            </rich:column>
            <rich:column id="hd_dt2" >
                <h:outputText value="#{summaryController.dataSet.dates[2]}" >
                    <f:convertDateTime pattern="MM/dd"/>
                </h:outputText>
            </rich:column>
            <rich:column id="hd_dt3" >
                <h:outputText value="#{summaryController.dataSet.dates[3]}" >
                    <f:convertDateTime pattern="MM/dd"/>
                </h:outputText>
            </rich:column>
            <rich:column id="hd_dt4" >
                <h:outputText value="#{summaryController.dataSet.dates[4]}" >
                    <f:convertDateTime pattern="MM/dd"/>
                </h:outputText>
            </rich:column>
            <rich:column id="hd_dt5" >
                <h:outputText value="#{summaryController.dataSet.dates[5]}" >
                    <f:convertDateTime pattern="MM/dd"/>
                </h:outputText>
            </rich:column>
            <rich:column id="hd_dt6" >
                <h:outputText value="#{summaryController.dataSet.dates[6].time}" >
                    <f:convertDateTime pattern="MM/dd"/>
                </h:outputText>
            </rich:column>
            <rich:column id="hd_dt_space" >&nbsp;
            </rich:column>
        </rich:columnGroup>
    </f:facet>
    <rich:column  id="Cat_Caption" colspan="9" styleClass="category-style">
        <h:outputText value="#{categoryData.parent.caption}" />
    </rich:column>
    <rich:subTable id="sbcat" var="subCategoryData" value="#{categoryData.dataValues}">
        <rich:column id="subCatSpace" colspan="9" styleClass="spacer" rendered="#{subCategoryData.parent.breakBefore}"><rich:spacer/></rich:column>
        <rich:columnGroup rendered="#{subCategoryData.parent.showCaption}" styleClass="subcategory-style">
            <rich:column id="SubCat_Caption">
                <h:outputText id="seq" value="#{subCategoryData.parent.editedName}"/>
            </rich:column>
            <rich:column colspan="9"><rich:spacer/></rich:column>
        </rich:columnGroup>
        <rich:subTable id="it" var="itemData" value="#{subCategoryData.dataValues}" 
                       onRowMouseOver="activateRow(this)"  onRowMouseOut="deactivateRow(this)">
            <rich:columnGroup rendered="#{!itemData.parent.calculated}">
                <rich:column id="item_edt_caption" styleClass="item-style">
                    <h:outputText value="#{itemData.parent.editedCaption}"/>
                </rich:column>
                <rich:column id="cell_mon" styleClass="rich-subtable-cell" style="border-right:none;">
                    <h:inputText id="mon" value="#{itemData.mondayData.value}"
                                 styleClass="editor"
                                 validatorMessage="Invalid monday data for item '#{itemData.parent.editedCaption}'!"
                                 converterMessage="Invalid monday data for item '#{itemData.parent.editedCaption}'!"
                                 valueChangeListener="#{summaryController.processValueChange}">
                        <f:converter converterId="uiConverter" />
                        <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                        <f:param value="#{itemData.mondayData}" />
                        <f:validateLength minimum="1" maximum="12" />
                        <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                    </h:inputText>
                </rich:column>
                <rich:column id="cell_tue"  style="border-right:none;">
                    <h:inputText id="tue" value="#{itemData.tuesdayData.value}"
                                 readonly="#{itemData.parent.calculated}"  styleClass="editor"
                                 validatorMessage="Invalid tuesday data for item '#{itemData.parent.editedCaption}'!"
                                 converterMessage="Invalid tuesday data for item '#{itemData.parent.editedCaption}'!"
                                 valueChangeListener="#{summaryController.processValueChange}">
                        <f:converter converterId="uiConverter" />
                        <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                        <f:param value="#{itemData.tuesdayData}" />
                        <f:validateLength minimum="1" maximum="12" />
                        <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                    </h:inputText>
                </rich:column>
                <rich:column id="cell_wed" style="border-right:none;">
                    <h:inputText id="wed" value="#{itemData.wednesdayData.value}"
                                 readonly="#{itemData.parent.calculated}"  styleClass="editor"
                                 validatorMessage="Invalid wednesday data for item '#{itemData.parent.editedCaption}'!"
                                 converterMessage="Invalid wednesday data for item '#{itemData.parent.editedCaption}'!"
                                 valueChangeListener="#{summaryController.processValueChange}">
                        <f:converter converterId="uiConverter" />
                        <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                        <f:param value="#{itemData.wednesdayData}" />
                        <f:validateLength minimum="1" maximum="12" />
                        <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                    </h:inputText>
                </rich:column>
                <rich:column id="cell_thu" style="border-right:none;">
                    <h:inputText id="thu" value="#{itemData.thursdayData.value}"
                                 readonly="#{itemData.parent.calculated}"  styleClass="editor"
                                 validatorMessage="Invalid thursday data for item '#{itemData.parent.editedCaption}'!"
                                 converterMessage="Invalid thursday data for item '#{itemData.parent.editedCaption}'!"
                                 valueChangeListener="#{summaryController.processValueChange}">
                        <f:converter converterId="uiConverter" />
                        <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                        <f:param value="#{itemData.thursdayData}" />
                        <f:validateLength minimum="1" maximum="12" />
                        <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                    </h:inputText>
                </rich:column>
                <rich:column id="cell_fri"style="border-right:none;">
                    <h:inputText id="fri" value="#{itemData.fridayData.value}"
                                 readonly="#{itemData.parent.calculated}"  styleClass="editor"
                                 validatorMessage="Invalid friday data for item '#{itemData.parent.editedCaption}'!"
                                 converterMessage="Invalid friday data for item '#{itemData.parent.editedCaption}'!"
                                 valueChangeListener="#{summaryController.processValueChange}">
                        <f:converter converterId="uiConverter" />
                        <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                        <f:param value="#{itemData.fridayData}" />
                        <f:validateLength minimum="1" maximum="12" />
                        <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                    </h:inputText>
                </rich:column>
                <rich:column id="cell_sat" style="border-right:none;">
                    <h:inputText id="sat" value="#{itemData.saturdayData.value}"
                                 readonly="#{itemData.parent.calculated}"  styleClass="editor"
                                 validatorMessage="Invalid saturday data for item '#{itemData.parent.editedCaption}'!"
                                 converterMessage="Invalid saturday data for item '#{itemData.parent.editedCaption}'!"
                                 valueChangeListener="#{summaryController.processValueChange}">
                        <f:converter converterId="uiConverter" />
                        <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                        <f:param value="#{itemData.saturdayData}" />
                        <f:validateLength minimum="1" maximum="12" />
                        <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                    </h:inputText>
                </rich:column>
                <rich:column id="cell_sun">
                    <h:inputText id="sun" value="#{itemData.sundayData.value}"
                                 readonly="#{itemData.parent.calculated}"  styleClass="editor"
                                 validatorMessage="Invalid sunday data for item '#{itemData.parent.editedCaption}'!"
                                 converterMessage="Invalid sunday data for item '#{itemData.parent.editedCaption}'!"
                                 valueChangeListener="#{summaryController.processValueChange}">
                        <f:converter converterId="uiConverter" />
                        <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                        <f:param value="#{itemData.sundayData}" />
                        <f:validateLength minimum="1" maximum="12" />
                        <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                    </h:inputText>
                </rich:column>
                <rich:column id="cell_total_1" styleClass="item-style-calculated">
                    <h:outputText id="totalValue" value="#{itemData.totalValueText}" />
                </rich:column>

            </rich:columnGroup>
            <rich:columnGroup rendered="#{itemData.parent.calculated}">
                <rich:column id="calcItemEdtCapt" styleClass="item-style" style="font-weight:bold;">
                    <h:outputText value="#{itemData.parent.editedCaption}"/>
                </rich:column>
                <rich:column id="cell_mon_calc"  styleClass="item-style-calculated">
                    <h:outputText value="#{itemData.mondayData.valueText}"/>
                </rich:column>
                <rich:column id="cell_tue_calc" styleClass="item-style-calculated">
                    <h:outputText value="#{itemData.tuesdayData.valueText}" />
                </rich:column>
                <rich:column id="cell_wed_calc"  styleClass="item-style-calculated">
                    <h:outputText value="#{itemData.wednesdayData.valueText}" />
                </rich:column>
                <rich:column id="cell_thu_calc"  styleClass="item-style-calculated">
                    <h:outputText value="#{itemData.thursdayData.valueText}" />
                </rich:column>
                <rich:column id="cell_fri_calc" styleClass="item-style-calculated">
                    <h:outputText value="#{itemData.fridayData.valueText}" />
                </rich:column>
                <rich:column id="cell_sat_calc"  styleClass="item-style-calculated">
                    <h:outputText value="#{itemData.saturdayData.valueText}" />
                </rich:column>
                <rich:column id="cell_sun_calc"  styleClass="item-style-calculated">
                    <h:outputText value="#{itemData.sundayData.valueText}" />
                </rich:column>
                <rich:column id="cell_total_2" styleClass="item-style-calculated">
                    <h:outputText id="totalValue_2" value="#{itemData.totalValueText}" />
                </rich:column>

            </rich:columnGroup>

        </rich:subTable>
    </rich:subTable>
    <rich:column colspan="9" styleClass="spacer"><rich:spacer/></rich:column>
</rich:dataTable>
