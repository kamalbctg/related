<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
.message {
	position: absolute;
	left: 536px;
}
</style>
<title>List of dictionaries</title>
</head>

<body>
<f:view>
	<h:form id="ItemList">
		<h:dataTable id="items" value="#{dictionaryController.dataModel}"
			var="item" border="1">
			<h:column>
				<f:facet name="header">
					<h:outputText value="ItemId" />

				</f:facet>
				<h:message styleClass="message" showSummary="true"
					showDetail="false" id="errors1" for="itemId" />
				<h:inputText id="itemId" value="#{item.itemId}"
					validatorMessage="itemId is too long">
					<f:validateLength maximum="20" minimum="2" />
				</h:inputText>
			</h:column>
			<h:column>
				<f:facet name="header">
					<h:outputText value="Code" />
				</f:facet>
				<h:message styleClass="message" showSummary="true"
					showDetail="false" id="errors2" for="code" />
				<h:inputText id="code" value="#{item.code}"
					validatorMessage="code is too long">
					<f:validateLength maximum="20" minimum="2" />
				</h:inputText>
			</h:column>

			<h:column>
				<f:facet name="header">
					<h:outputText value="Description" />
				</f:facet>
				<h:message styleClass="message" showSummary="true"
					showDetail="false" id="errors3" for="description" />
				<h:inputText id="description" value="#{item.description}"
					validatorMessage="description is too long">
					<f:validateLength maximum="20" minimum="2" />
				</h:inputText>

			</h:column>
			<h:column>
				<f:facet name="header">
					<h:outputText value="Delete" />
				</f:facet>
				<h:commandLink id="Delete"
					action="#{dictionaryController.deleteDictionary}">
					<h:outputText value="Delete" />
					<f:setPropertyActionListener
						target="#{dictionaryController.selectedDictionary}"
						value="#{item}" />
				</h:commandLink>
			</h:column>
		</h:dataTable>

		<h:commandButton immediate="true" id="Add"
			action="#{dictionaryController.addNewDictionary}" value="Add an item" />
		<h:commandButton id="update"
			action="#{dictionaryController.performUpdate}" value="Update" />
		<h:commandButton id="add100" value="Add100"
			action="#{dictionaryController.addDictionarys}">
			<f:setPropertyActionListener
				target="#{dictionaryController.amountToAdd}" value="100" />
		</h:commandButton>
	</h:form>
</f:view>
<a href="index.faces">Index</a>
</body>
</html>
