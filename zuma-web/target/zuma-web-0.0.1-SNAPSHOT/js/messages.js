var deleteMessage = "Deleted items cannot be retrieved. Are you sure you wish to delete this item?";


var saveSign = false;
function showAnimation()
{
    var result = true;
    
    var IE = (navigator.appName == "Microsoft Internet Explorer") ? true : false;
    if(IE )
    {
        var version = parseFloat(navigator.appVersion.split("MSIE")[1]);
        if(version<8)
            result = false;
    }
    return result;
}
function showSaveApprove()
{
    if(saveSign)
    {
        saveSign = false;
        window.setTimeout('hiveSaveApprove()', 3000);
        if(showAnimation())
        {
            Effect.BlindDown('save_approve', {
                duration:0.5
            });
            $('save_approve').style.display = "none";
        }else
            $('save_approve').style.display = "";
    }
    return false;
}
function hiveSaveApprove()
{
    if(showAnimation())
    {
        Effect.BlindUp('save_approve', {
            duration:0.8
        });
        $('save_approve').style.display = "";
    }else
        $('save_approve').style.display = "none";
}
function setFocus(id) {
    var element = document.getElementById(id);
    if (element && element.focus) {
        element.focus();
    }
}

function setHighlight(ids) {
    var idsArray = ids.split(",");
    if(idsArray.length>0)
        saveSign = false;
    for (var i = 0; i < idsArray.length; i++) {
        var element = document.getElementById(idsArray[i]);
        if (element) {
            element.style.backgroundColor = '#fcc';
        }
    }
}
function setApproveText(text)
{
    $('save_approve').innerHTML = text;
}
