var phoneMessage = "Invalid phone number format. Please enter numerical characters only.";
var isNN = (navigator.appName.indexOf("Netscape")!=-1);

function autoFieldChange(input,len,e)
{
    var keyCode = (isNN) ? e.which : e.keyCode;
    var filter  = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
    if(input.value.length >= len && !containsElement(filter,keyCode)) {
        input.value = input.value.slice(0, len);
        input.form[(getIndex(input)+1) % input.form.length].focus();
    }

    function containsElement(arr, ele)
    {
        var found = false;
        var index = 0;

        while(!found && index < arr.length)
            if(arr[index] == ele)
                found = true;
            else
                index++;
        
        return found;
    }

    function getIndex(input)
    {
        var index = -1;
        var i = 0;

        while (i < input.form.length && index == -1)
            if (input.form[i] == input)
                index = i;
            else
                i++;

        return index;
    }

    return true;
}
//------------------------------------------------------------------------------
function IsNumerical(charObj) {
    var ValidChars = "0123456789";
    var IsNumber=true;

    for (i = 0; i < charObj.length && IsNumber; i++)
    {
        var Char = charObj.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
            IsNumber = false;
        }
    }

    return IsNumber;
}

//return FullPhoneNumber
function makePhone(formObj,prefix,suffix) {
    var full = formObj[prefix+"pac"+suffix].value + "-" +
               formObj[prefix+"pex"+suffix].value + "-" +
               formObj[prefix+"pnum"+suffix].value;
    if (full == "--")
        full = "";
    formObj[prefix+"phoneFull"+suffix].value = full;
}

function setToken(name,token) {
    (document.getElementById(name)).value = (token != null && token != "undefined")?token:"";
}

//return PartsPhoneNumber
function breakePhone(prefix,suffix) {
    if (document.getElementById(prefix+"phoneFull"+suffix)==null)
        return;
    var phone = (document.getElementById(prefix+"phoneFull"+suffix)).value;
    var tokens = phone.toString().split("-");

    //TODO verify tokens
    setToken(prefix+"pac"+suffix,tokens[0])
    setToken(prefix+"pex"+suffix,tokens[1])
    setToken(prefix+"pnum"+suffix,tokens[2])
}

function IsNumerical(charObj) {
    var ValidChars = "0123456789";
    var IsNumber   = true;

    for (i = 0; i < charObj.length && IsNumber; i++) {
        var Char = charObj.charAt(i);
        if (ValidChars.indexOf(Char) == -1) {
            IsNumber = false;
        }
    }

    return IsNumber;
}

//-------  Phone Number Validation -----------
function phoneNumberValaidation(formObj,suffix) {
    var numberTest = true;

    var pacs  = "pac" + suffix;
    var pexs  = "pex" + suffix;
    var pnums = "pnum"+ suffix;

    for (var x=0; x < formObj.length && numberTest; x++ )
    {
        var name = formObj[x].name
        switch(name)
        {
            case pacs:
            case pexs:
            case pnums:
                for(var y=0; y < formObj[x].value.length && numberTest; y++)
                {
                    numberTest = IsNumerical(formObj[name].value);
                }
                break;

            default:
                break;
        }

    }

    if(!numberTest)
        alert(phoneMessage);
    
    return numberTest;
}
