function combinePhones(formObj,prefix) {
    //COMPANY
    makePhone(formObj,prefix,"_c01");
    makePhone(formObj,prefix,"_c0fax");

    makePhone(formObj,prefix,"_c11");
    makePhone(formObj,prefix,"_c12");
    makePhone(formObj,prefix,"_c1fax");

    makePhone(formObj,prefix,"_c21");
    makePhone(formObj,prefix,"_c22");
    makePhone(formObj,prefix,"_c2fax");

    //STORE
    makePhone(formObj,prefix,"_s01");
    makePhone(formObj,prefix,"_s0fax");

    makePhone(formObj,prefix,"_s11");
    makePhone(formObj,prefix,"_s12");
    makePhone(formObj,prefix,"_s1fax");

    makePhone(formObj,prefix,"_s21");
    makePhone(formObj,prefix,"_s22");
    makePhone(formObj,prefix,"_s2fax");
}

function parsePhones(prefix) {
    //COMPANY
    breakePhone(prefix,"_c01");
    breakePhone(prefix,"_c0fax");

    breakePhone(prefix,"_c11");
    breakePhone(prefix,"_c12");
    breakePhone(prefix,"_c1fax");

    breakePhone(prefix,"_c21");
    breakePhone(prefix,"_c22");
    breakePhone(prefix,"_c2fax");

    //STORE
    breakePhone(prefix,"_s01");
    breakePhone(prefix,"_s0fax");

    breakePhone(prefix,"_s11");
    breakePhone(prefix,"_s12");
    breakePhone(prefix,"_s1fax");

    breakePhone(prefix,"_s21");
    breakePhone(prefix,"_s22");
    breakePhone(prefix,"_s2fax");
}


