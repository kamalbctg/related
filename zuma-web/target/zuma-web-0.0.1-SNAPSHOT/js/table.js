
function Cell()
{
    this.input = null;
    this.subTable = null;
    this.parentRow = null;

    this.colNumber = -1;
    this.hPrev = null;
    this.hNext = null;
    this.vPrev = null;
    this.vNext = null;

    this.realCell = null;

    this.setVPrev = function(cell)
    {
        this.vPrev = cell;
        if(cell)
            cell.vNext = this;
    }
    this.setHPrev = function(cell)
    {
        this.hPrev = cell;
        if(cell)
        {
            cell.hNext = this;
            this.colNumber = cell.colNumber + 1;
        }
        else
            this.colNumber=0;
    }
    this.getHPrev = function()
    {
        if(this.hPrev)
            return this.hPrev;

        if(this.parentRow.parentTable.parentCell)
        {
            return this.parentRow.parentTable.parentCell.hPrev;
        }
        return null;
    }
    this.getHNext = function()
    {
        if(this.hNext)
            return this.hNext;

        if(this.parentRow.parentTable.parentCell)
        {
            return this.parentRow.parentTable.parentCell.hNext;
        }
        return null;
    }
    this.getVPrev = function()
    {
        if(this.vPrev)
            return this.vPrev;
        if(this.parentRow.parentTable.parentCell &&
            this.parentRow.parentTable.parentCell.vPrev)
            {
            var nextCell = this.parentRow.parentTable.parentCell.vPrev;
            if(nextCell.input)
            {
                if(nextCell.input.disabled)
                {
                    return nextCell.getVNext();
                }
                else
                    return nextCell;
            }
            else if(nextCell.subTable)
            {
                var rowCount = nextCell.subTable.rows.length;
                var tmpCell = nextCell.subTable.getCell(rowCount-1,this.colNumber);
                return tmpCell;
            }
        }
        return null;
    }
    this.getVNext = function()
    {
        if(this.vNext)
            return this.vNext;
        if(this.parentRow.parentTable.parentCell &&
            this.parentRow.parentTable.parentCell.vNext)
            {
            var nextCell = this.parentRow.parentTable.parentCell.vNext;
            if(nextCell.input)
            {
                if(nextCell.input.disabled)
                {
                    return nextCell.getVNext();
                }
                else
                    return nextCell;
            }
            else if(nextCell.subTable)
            {
                var tmpCell = nextCell.subTable.getCell(0,this.colNumber);
                return tmpCell;
            }
        }
        return null;
    }
    this.init = function(tableCell)
    {
        this.realCell = tableCell;
        tableCell.virtualCell = this;
        
        var elem = tableCell.firstChild;
        if(elem && elem.tagName)
        {
            elem.parentCell = this;
            if(elem.tagName.toUpperCase()=="TABLE")
            {
                var subTable = new Table();
                subTable.addTable(elem);
                subTable.parentCell = this;
                this.subTable = subTable;
            }
            else if ((elem.tagName.toUpperCase()=="INPUT" && elem.type.toUpperCase()=="TEXT" ))
            {
                this.input = elem;
                Event.observe(elem, 'keydown', cellKeyPress);
            }
            else if(elem.tagName.toUpperCase()=="SELECT")
            {
                this.input = elem;
                Event.observe(elem, 'keydown', cellKeyPress);
            }
        }
    }
}
function isCellAceptable(tableCell)
{
    var elem = tableCell.firstChild;
    if(elem && elem.tagName)
    {

        if(elem.tagName.toUpperCase()=="TABLE" ||
            (elem.tagName.toUpperCase()=="INPUT" && elem.type.toUpperCase()=="TEXT" ) ||
            elem.tagName.toUpperCase()=="SELECT")
            {
            return true;
        }
    }
    return false;
}
function Row()
{
    this.parentTable = null;
    this.cells = [];
    this.prevRow = null;
    this.nextRow = null;
    this.realRow = null;
    this.setPrevRow = function(row)
    {
        this.prevRow = row;
        if(row)
            row.nextRow = this;
    }
    this.findVPrev = function(cell)
    {
        if(!this.prevRow)
            return null;
        if(this.prevRow.cells.length-1<cell.colNumber)
            return this.prevRow.cells[this.prevRow.cells.length-1];
        else
            return this.prevRow.cells[cell.colNumber];
       
    }
    this.getCellsWithColNumber = function(value)
    {
        var result = new Array;
        for(var i=0;i<this.cells.length;i++)
        {
            var cell = this.cells[i];
            if(cell.colNumber==value)
            {
                result[result.length] = cell;
            }
        }
        return result;
    }
    this.addCell = function(tableCell)
    {
        if(isCellAceptable(tableCell))
        {
            var cell = new Cell();
            cell.parentRow = this;
            var prevCell = this.cells[this.cells.length-1];
            cell.init(tableCell);
            cell.setHPrev(prevCell);
            cell.setVPrev(this.findVPrev(cell));
            this.cells[this.cells.length] = cell;
        }

    }
    this.init = function(tableRow)
    {
        this.realRow = tableRow;
        var newCells = tableRow.cells;
        for(var rw=0; rw<newCells.length;rw++)
        {
            this.addCell(newCells[rw]);
        }
    }
}
function Table()
{
    this.parentCell = null;
    this.rows = [];
    this.realTables = [];
    this.addRow = function(tableRow)
    {
        var inputsCount = tableRow.getElementsByTagName("input").length;
        var selectsCount = tableRow.getElementsByTagName("select").length;
        var tablesCount = tableRow.getElementsByTagName("table").length;
        if((inputsCount+selectsCount+tablesCount)==0)
            return null;
        var newRow = new Row();
        newRow.parentTable = this;
        var prevRow = this.rows[this.rows.length-1];
        newRow.setPrevRow(prevRow);
        newRow.rowNumber = this.rows.length;
        this.rows[this.rows.length]=newRow;
        newRow.init(tableRow);
        return newRow;
    }
    
    this.addCell = function(input)
    {
        var curRow = this.rows[this.rows.length-1];
        curRow.addCell(input);
    }
    this.addTable= function(obj)
    {
        if(!obj)
            return;
        this.realTables[this.realTables.length] = obj;
        var newRows = obj.rows;

        for(var rw=0; rw<newRows.length;rw++)
        {
            this.addRow(newRows[rw]);

        }
    }
    this.clear = function()
    {
        this.rows.length = 0;
        this.realTables.length = 0;
    }
    this.getCell = function(rowN,colN)
    {
        var row = this.rows[rowN];
        if(row)
        {
            return row.cells[colN];
        }
        return null;
    }
    this.correct = function()
    {
        var validRows = new Array;
        for(var i=0;i<this.rows.length;i++)
        {
            var tmpRow = this.rows[i];
            tmpRow.parentTable = this;
            for(var j=0;j<tmpRow.cells.length;j++)
            {
                if(tmpRow.cells[j].subTable)
                    tmpRow.cells[j].subTable.correct();
            }
            if(tmpRow.cells.length>0)
                validRows[validRows.length] = tmpRow;
            else
            {
                if(tmpRow.nextRow)
                {
                    tmpRow.nextRow.setPrevRow(validRows[validRows.length-1]);
                }
            }
        }
        this.rows = validRows;
    }
}

var table = new Table();

function doGetCaretPosition (oField) {

    // Initialize
    var iCaretPos = 0;

    // IE Support
    if (document.selection) {

        // Set focus on the element
        oField.focus ();

        // To get cursor position, get empty selection range
        var oSel = document.selection.createRange ();

        // Move selection start to 0 position
        oSel.moveStart ('character', -oField.value.length);

        // The caret position is selection length
        iCaretPos = oSel.text.length;
    }

    // Firefox support
    else if (oField.selectionStart || oField.selectionStart == '0')
        iCaretPos = oField.selectionStart;

    // Return results
    return (iCaretPos);
}

function cellKeyPress(e)
{
    var key;
    var sourceElem = Event.element(e);
    if(window.event)
    {
        key = window.event.keyCode; //IE
    }
    else
    {
        key = e.keyCode; //firefox
    }
    var result = true;
    switch(key)
    {
        case Event.KEY_LEFT:
            result = moveLeft(sourceElem);
            break;
        case Event.KEY_UP:
            result = moveUp(sourceElem);
            break;
        case Event.KEY_RIGHT:
            result =  moveRight(sourceElem);
            break;
        case Event.KEY_DOWN:
            result = moveDown(sourceElem);
            break;
    }
    if(!result)
        Event.stop(e);
    return result;
}

function moveLeft(sourceElem)
{

    if(isToLeftAllowed(sourceElem))
    {
        var nextFocused = sourceElem.parentCell.getHPrev();
        while(nextFocused)
        {
            if(nextFocused.input && nextFocused.input.disabled == false)
            {
                nextFocused.input.focus();
                nextFocused.input.select();
                return false;
            }
            nextFocused = nextFocused.getHPrev();
        }      
    }
    return true;
 
}
function isToLeftAllowed(sourceElem)
{
    if(sourceElem.tagName.toUpperCase()=="INPUT")
        return doGetCaretPosition(sourceElem)==0;
    else if(sourceElem.tagName.toUpperCase()=="SELECT")
    {
        return sourceElem.selectedIndex==0;
    }
    else return false;
}
function moveRight(sourceElem)
{
    if(isToRightAllowed(sourceElem))
    {
        var nextFocused = sourceElem.parentCell.getHNext();
        while(nextFocused)
        {
            if(nextFocused.input && nextFocused.input.disabled == false)
            {
                nextFocused.input.focus();
                nextFocused.input.select();
                return false;
            }
            if(nextFocused.subTable)
            {
                var firstRow = nextFocused.subTable.rows[0];
                if(firstRow)
                {
                    nextFocused = firstRow.cells[0];
                    if(nextFocused)
                    {
                        nextFocused.input.focus();
                        nextFocused.input.select();
                    }
                    return false;

                }
            }
            nextFocused = nextFocused.getHNext();
        }
    }
    return true;

}
function isToRightAllowed(sourceElem)
{
    if(sourceElem.tagName.toUpperCase()=="INPUT")
        return doGetCaretPosition(sourceElem)>=sourceElem.value.length;
    else if(sourceElem.tagName.toUpperCase()=="SELECT")
    {
        return sourceElem.selectedIndex>=sourceElem.options.length-1;
    }
    else return false;
}

function moveUp(sourceElem)
{
    if(isToUpAllowed(sourceElem))
    {
        var nextFocused = sourceElem.parentCell.getVPrev();
        if(nextFocused)
        {
            nextFocused.input.focus();
            nextFocused.input.select();
        }
        return false;
    }
    return true;
}
function isToUpAllowed(sourceElem)
{
    if(sourceElem.rowNumber<=0)
        return false;

    if(sourceElem.tagName.toUpperCase()=="INPUT")
        return true;
    else if(sourceElem.tagName.toUpperCase()=="SELECT")
    {
        return sourceElem.selectedIndex==0;
    }
    else return false;
}
function moveDown(sourceElem)
{
    if(isToDownAllowed(sourceElem))
    {
        var nextFocused = sourceElem.parentCell.getVNext();
        if(nextFocused)
        {
            nextFocused.input.focus();
            nextFocused.input.select();
        }
        return false;
    }
    return true;
}
function isToDownAllowed(sourceElem)
{
    if(sourceElem.rowNumber>=table.rows.length-1)
        return false;

    if(sourceElem.tagName.toUpperCase()=="INPUT")
        return true;
    else if(sourceElem.tagName.toUpperCase()=="SELECT")
    {
        return sourceElem.selectedIndex>=sourceElem.options.length-1;
    }
    else return false;
}
function updateTable(tblID)
{
    table.clear();
    table.addTable($(tblID));
    table.correct();
}

function activateRow(row)
{
    var activeColor = "#AAC2F4";
    row.oldBackground = row.style.backgroundColor;
    row.style.backgroundColor = activeColor;

    var cellsTD = row.getElementsByTagName("td");
    var cellLenTD = cellsTD.length;
    for(var cellNTD=0;cellNTD<cellLenTD;cellNTD++)
    {
        var elemTD = cellsTD[cellNTD];
        var inputs = elemTD.getElementsByTagName("input");
        if(inputs.length>0)
        {
            for(var inputN=0;inputN<inputs.length;inputN++)
            {
                var input = inputs[inputN];
                input.oldBackground = input.style.backgroundColor;
                input.style.backgroundColor = activeColor;

            }
        }
        else
        {
            elemTD.oldBackground = elemTD.style.backgroundColor;
            elemTD.style.backgroundColor = activeColor;
        }
    }
}
function deactivateRow(row)
{
    row.style.backgroundColor = row.oldBackground;

    var cellsTD = row.getElementsByTagName("td");
    var cellLenTD = cellsTD.length;
    for(var cellNTD=0;cellNTD<cellLenTD;cellNTD++)
    {
        var elemTD = cellsTD[cellNTD];
        var inputs = elemTD.getElementsByTagName("input");
        if(inputs.length>0)
        {
            for(var inputN=0;inputN<inputs.length;inputN++)
            {
                var input = inputs[inputN];
                input.style.backgroundColor = input.oldBackground;
            }
        }
        else
        {
            elemTD.style.backgroundColor = elemTD.oldBackground;
        }
    }
}
