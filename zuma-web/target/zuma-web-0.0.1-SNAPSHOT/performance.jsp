<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Performance Dashboard</title>
        <link type="text/css" rel="stylesheet" href="css/zuma.css"/>
        <style type="text/css">
            .container{
                position:relative;
                width:50%;
                height:50%;
                float:left;
            }
            .header{
                background-color: gray;
                border-left:1px white solid;
                color:white;
                font-weight:normal;
                font-size:16px;
                width:99%;
                height:20px;
                padding-left:3px;
            }
            .detail{
                width:49%;
                float:left;
            }
            .diagram{
                width:50%;
                float:right;
                border-left:solid black 1px;
                text-align: right;
            }
            td.inf, td.inf-value {height:20px;
                                  color:#666666;
                                  font-weight:normal;
            }
            td.inf-value{
                text-align:right;
            }
            .target_lnk{
                padding-right: 15px;
            }
            .invent_lnk{
                color: #666666;
            }
            input.buttonSave{
                width: 100px;
            }
            .rich-mpnl-header {
                background-image: url('images/bg_cap.jpg');
            }

        </style>
        <script type="text/javascript" src="js/prototype.js"></script>
        <script type="text/javascript" src="js/messages.js"></script>
        <script type="text/javascript">
            function updateDialog()
            {
                $('dialogHeader').style.backgroundImage = "url('images/bg_cap.jpg')";
            }
        </script>
    </head>
    <body>
        <f:view beforePhase="#{performanceController.recalculate}">
            <f:subview id="flying">
                <c:import url="includes/flyingpanels.jsp" />
            </f:subview>
            <f:subview id="mnu">
                <jsp:include page="includes/menu.jsp" />
            </f:subview>
            <f:subview id="tc">
                <jsp:include page="includes/topclient.jsp" />
            </f:subview>

            <rich:modalPanel id="dialog" width="350" autosized="true"
                             showWhenRendered="#{performanceController.neededStartDataDialog}"
                             onshow='this.style.position="fixed";updateDialog();'>
                <f:facet name="header">
                        <h:panelGroup styleClass="error-header">
                            <h:outputText value="New User Information Request"></h:outputText>
                        </h:panelGroup>
                    </f:facet>
                    <f:facet name="controls">
                        <h:panelGroup>
                            <h:graphicImage value="/images/close.png" styleClass="hidelink" id="hideBtn"/>
                            <rich:componentControl for="dialog" attachTo="hideBtn" operation="hide" event="onclick"/>
                        </h:panelGroup>
                    </f:facet>
                <div>
                    <b>Since this is your first week using QSRfinancial, we need your beginning inventory balances.</b>
                </div>
                <h:form>
                    <table style="margin-top: 5px;margin-bottom: 5px;width:100%">
                        <tr>
                            <td align="right">Food & Beverage Inventory</td>
                            <td>
                                <h:inputText id="fbInv" value="#{performanceController.inventoryStartData.foodValue}"
                                             styleClass="editor"
                                             validatorMessage="Invalid Food & Beverage Inventory data!"
                                             converterMessage="Invalid Food & Beverage Inventory data!">
                                    <f:converter converterId="uiConverter" />
                                    <f:attribute name="converterPattern" value="$#,###,##0.00;#,###,##0.00"/>
                                    <f:validateLength minimum="1" maximum="12" />
                                    <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                                </h:inputText>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">Paper Inventory</td>
                            <td>
                                <h:inputText id="paperInv" value="#{performanceController.inventoryStartData.paperValue}"
                                             styleClass="editor"
                                             validatorMessage="Invalid Paper Inventory data!"
                                             converterMessage="Invalid Paper Inventory data!">
                                    <f:converter converterId="uiConverter" />
                                    <f:attribute name="converterPattern" value="$#,###,##0.00;#,###,##0.00"/>
                                    <f:validateLength minimum="1" maximum="12" />
                                    <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                                </h:inputText>
                            </td>
                        </tr>
                        <tr >
                            <td style="height: 40px; padding-right: 2px; text-align: right; vertical-align: bottom;">
                                <h:commandButton id="save"
                                                 styleClass="buttonSave"
                                                 action="#{performanceController.saveInventoryStartData}"
                                                 title="Submit"
                                                 value="Submit">
                                </h:commandButton></td>
                            <td style="height: 40px; padding-left: 2px; text-align: left; vertical-align: bottom;">
                                <h:commandButton value="Ask Me Later" id="closeBtn" styleClass="buttonSave" onclick="return false;">
                                    <rich:componentControl for="dialog" attachTo="closeBtn" operation="hide" event="onclick"/>
                                </h:commandButton>
                            </td>
                        </tr>
                    </table>
                </h:form>

            </rich:modalPanel>
            <h1 class="pageCaption">Performance Dashboard</h1>
            <div>
                <div id="fb" class="container">
                    <div class="header">Food and Beverage</div>
                    <div class="detail">
                        <table border="0" width="100%" style="height:100%" class="tabInfo">
                            <tr>
                                <td class="inf" colspan="2">Active Week Detail</td>
                            </tr>
                            <tr>
                                <td class="inf">Beg. Inventory</td>
                                <td class="inf-value">
                                    <h:outputText value="#{performanceController.begInventory}" rendered="#{!performanceController.prevWeekAvaible || !menuManagedBean.inventoryView}">
                                        <f:convertNumber pattern="$###,###,##0.00"/>
                                    </h:outputText>
                                    <h:form rendered="#{performanceController.prevWeekAvaible && menuManagedBean.inventoryView}">
                                        <h:commandLink id="inventoryview"
                                                       action="#{inventoryViewController.refreshPage}"
                                                       style="color: #666666;">
                                            <h:outputText value="#{performanceController.begInventory}">
                                                <f:convertNumber pattern="$###,###,##0.00"/>
                                            </h:outputText>
                                            <f:setPropertyActionListener target="#{performanceController.newWeek}" value="-1"/>
                                            <f:setPropertyActionListener
                                                target="#{inventoryViewController.targetPage}" value="inventoryview"/>
                                        </h:commandLink>
                                    </h:form>
                                </td>
                            </tr>
                            <tr>
                                <td class="inf">Purchases</td>
                                <td class="inf-value">
                                    <h:outputText value="#{performanceController.purchase}">
                                        <f:convertNumber pattern="$###,###,##0.00"/>
                                    </h:outputText>
                                </td>
                            </tr>
                            <tr>
                                <td class="inf">Waste</td>
                                <td class="inf-value">
                                    <h:outputText value="#{performanceController.waste}">
                                        <f:convertNumber pattern="($###,###,##0.00)"/>
                                    </h:outputText>
                                </td>
                            </tr>
                            <tr>
                                <td class="inf">End. Inventory</td>
                                <td class="inf-value">
                                    <h:outputText value="#{performanceController.endInventory}" rendered="#{!menuManagedBean.inventoryView}">
                                        <f:convertNumber pattern="($###,###,##0.00)"/>
                                    </h:outputText>
                                    <h:form rendered="#{menuManagedBean.inventoryView}">
                                        <h:commandLink id="inventoryview"
                                                       action="#{inventoryViewController.refreshPage}"
                                                       style="color: #666666;">
                                            <f:setPropertyActionListener
                                                target="#{inventoryViewController.targetPage}" value="inventoryview"/>
                                            <h:outputText value="#{performanceController.endInventory}">
                                                <f:convertNumber pattern="($###,###,##0.00)"/>
                                            </h:outputText>
                                        </h:commandLink>
                                    </h:form>
                                </td>
                            </tr>
                            <tr>
                                <td class="inf">F&amp;B Cost</td>
                                <td class="inf-value">
                                    <h:outputText value="#{performanceController.dataTargetMap['Food and Beverage'].actualCurrent}">
                                        <f:convertNumber pattern="$###,###,##0.00"/>
                                    </h:outputText>
                                </td>
                            </tr>
                            <tr>
                                <td class="inf">F&amp;B Cost/Sales</td>
                                <td class="inf-value">
                                    <h:outputText value="#{performanceController.dataTargetMap['Food and Beverage'].percentCurrent}">
                                        <f:convertNumber pattern="##0.00%"/>
                                    </h:outputText>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <div class="diagram">
                        <a4j:mediaOutput id="fb" element="img"
                                         cacheable="false"
                                         createContent="#{performanceController.generateChart}"
                                         value="#{performanceController.dataTargetMap['Food and Beverage']}"
                                         mimeType="image/jpeg" style="width:230px;height:175px;"/>
                        <br/>
                        <h:outputLink id="profitability" value="profitability.faces"
                                      styleClass="target_lnk"
                                      rendered="#{menuManagedBean.profitability}">
                            <h:outputText value="Update Target" />
                        </h:outputLink>
                    </div>
                </div>
                <div id="labor" class="container">
                    <f:subview id="labor_diagram">
                        <div class="header">Labor</div>
                        <div class="detail">
                            <table border="0" width="100%" style="height:100%" class="tabInfo">
                                <tr>
                                    <td class="inf" colspan="2">Active Week Detail</td>
                                </tr>
                                <tr>
                                    <td class="inf">Total Labor Hours</td>
                                    <td class="inf-value">
                                        <h:outputText value="#{performanceController.laborHours}">
                                            <f:convertNumber pattern="###,###,##0.00"/>
                                        </h:outputText>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="inf">Total Labor Cost</td>
                                    <td class="inf-value">
                                        <h:outputText value="#{performanceController.dataTargetMap['Labor'].actualCurrent}">
                                            <f:convertNumber pattern="$###,###,##0.00"/>
                                        </h:outputText>
                                    </td>
                                </tr>
                                <tr><td class="inf" colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td class="inf">Average/Hour</td>
                                    <td class="inf-value">
                                        <h:outputText value="#{performanceController.averageLaborHours}">
                                            <f:converter converterId="uiConverter" />
                                            <f:attribute name="converterPattern" value="$###,###,##0.00;#,###,##0.00"/>
                                        </h:outputText>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inf">Total Labor/Sales</td>
                                    <td class="inf-value">
                                        <h:outputText value="#{performanceController.dataTargetMap['Labor'].percentCurrent}">
                                            <f:convertNumber pattern="##0.00%"/>
                                        </h:outputText>
                                    </td>
                                </tr>
                            </table>
                        </div><div class="diagram">
                            <a4j:mediaOutput id="fb" element="img"
                                             cacheable="false"
                                             createContent="#{performanceController.generateChart}"
                                             value="#{performanceController.dataTargetMap['Labor']}"
                                             mimeType="image/jpeg" style="width:230px;height:175px;"/>
                            <br/>
                            <h:outputLink id="profitability" value="profitability.faces"
                                          styleClass="target_lnk"
                                          rendered="#{menuManagedBean.profitability}">
                                <h:outputText value="Update Target" />
                            </h:outputLink>
                        </div>
                    </f:subview>
                </div></div>
            <div>
                <div id="parer" class="container"><f:subview id="parer_diagram">
                        <div class="header">Paper</div>
                        <div class="detail">
                            <table border="0" width="100%" style="height:100%" class="tabInfo">
                                <tr>
                                    <td class="inf" colspan="2">Active Week Detail</td>
                                </tr>
                                <tr>
                                    <td class="inf">Beg. Inventory</td>
                                    <td class="inf-value">
                                        <h:outputText value="#{performanceController.paperBegInventory}" rendered="#{!(performanceController.prevWeekAvaible && menuManagedBean.inventoryView)}">
                                            <f:convertNumber pattern="$###,###,##0.00"/>
                                        </h:outputText>
                                        <h:form rendered="#{performanceController.prevWeekAvaible && menuManagedBean.inventoryView}">
                                            <h:commandLink id="inventoryview"
                                                           action="#{inventoryViewController.refreshPage}"
                                                           style="color: #666666;">
                                                <h:outputText value="#{performanceController.paperBegInventory}">
                                                    <f:convertNumber pattern="$###,###,##0.00"/>
                                                </h:outputText>
                                                <f:setPropertyActionListener target="#{performanceController.newWeek}" value="-1"/>
                                                <f:setPropertyActionListener
                                                    target="#{inventoryViewController.targetPage}" value="inventoryview"/>
                                            </h:commandLink>
                                        </h:form>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inf">Purchases</td>
                                    <td class="inf-value">
                                        <h:outputText value="#{performanceController.paperPurchase}">
                                            <f:convertNumber pattern="$###,###,##0.00"/>
                                        </h:outputText>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inf">End. Inventory</td>
                                    <td class="inf-value">
                                        <h:outputText value="#{performanceController.paperEndInventory}" rendered="#{!menuManagedBean.inventoryView}">
                                            <f:convertNumber pattern="($###,###,##0.00)"/>
                                        </h:outputText>
                                        <h:form rendered="#{menuManagedBean.inventoryView}">
                                            <h:commandLink id="inventoryview"
                                                           action="#{inventoryViewController.refreshPage}"
                                                           style="color: #666666;">
                                                <f:setPropertyActionListener
                                                    target="#{inventoryViewController.targetPage}" value="inventoryview"/>
                                                <h:outputText value="#{performanceController.paperEndInventory}">
                                                    <f:convertNumber pattern="($###,###,##0.00)"/>
                                                </h:outputText>
                                            </h:commandLink>
                                        </h:form>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inf">Paper Cost</td>
                                    <td class="inf-value">
                                        <h:outputText value="#{performanceController.dataTargetMap['Paper'].actualCurrent}">
                                            <f:convertNumber pattern="$###,###,##0.00"/>
                                        </h:outputText>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inf">Paper Cost/Sales</td>
                                    <td class="inf-value">
                                        <h:outputText value="#{performanceController.dataTargetMap['Paper'].percentCurrent}">
                                            <f:convertNumber pattern="##0.00%"/>
                                        </h:outputText>
                                    </td>
                                </tr>
                            </table>
                        </div><div class="diagram">
                            <a4j:mediaOutput id="fb" element="img"
                                             cacheable="false"
                                             createContent="#{performanceController.generateChart}"
                                             value="#{performanceController.dataTargetMap['Paper']}"
                                             mimeType="image/jpeg" style="width:230px;height:175px;"/>
                            <br/>
                            <h:outputLink id="profitability" value="profitability.faces"
                                          styleClass="target_lnk"
                                          rendered="#{menuManagedBean.profitability}">
                                <h:outputText value="Update Target" />
                            </h:outputLink>
                        </div>
                    </f:subview>
                </div>
                <div id="discounts" class="container">
                    <f:subview id="discounts_diagram">
                        <div class="header">Discounts</div>
                        <div class="detail">
                            <table border="0" width="100%" style="height:100%" class="tabInfo">
                                <tr>
                                    <td class="inf" colspan="2">Active Week Detail</td>
                                </tr>
                                <tr>
                                    <td class="inf">Sales</td>
                                    <td class="inf-value">
                                        <h:outputText value="#{performanceController.sales}">
                                            <f:convertNumber pattern="$###,###,##0.00"/>
                                        </h:outputText>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="inf">Discounts</td>
                                    <td class="inf-value">
                                        <h:outputText value="#{performanceController.discounts}">
                                            <f:convertNumber pattern="$###,###,##0.00"/>
                                        </h:outputText>
                                    </td>
                                </tr>
                                <tr><td class="inf" colspan="2">&nbsp;</td></tr>
                                <tr>
                                    <td class="inf">Discounts/Sales</td>
                                    <td class="inf-value">
                                        <h:outputText value="#{performanceController.percentDiscounts}">
                                            <f:convertNumber pattern="##0.00%"/>
                                        </h:outputText>
                                    </td>
                                </tr>
                            </table>
                        </div><div class="diagram">
                            <a4j:mediaOutput id="fb" element="img"
                                             cacheable="false"
                                             createContent="#{performanceController.generateChart}"
                                             value="#{performanceController.dataTargetMap['Discounts']}"
                                             mimeType="image/jpeg" style="width:230px;height:175px;"/>
                            <br/>
                            <h:outputLink id="profitability" value="profitability.faces"
                                          styleClass="target_lnk"
                                          rendered="#{menuManagedBean.profitability}">
                                <h:outputText value="Update Target" />
                            </h:outputLink>
                        </div>
                    </f:subview>
                </div></div>
                <f:subview id="btm">
                    <jsp:include page="includes/bottom.jsp" />
                </f:subview>
            </f:view>
    </body>
</html>