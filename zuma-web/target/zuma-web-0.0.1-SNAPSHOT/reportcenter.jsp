<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Report Center</title>
        <link type="text/css" rel="stylesheet" href="css/zuma.css"/>
        <style type="text/css">
            .container{
                position:relative;
                width:50%;
                height:50%;
                float:left;
            }
            .header{
                background-color: gray;
                border-left:1px white solid;
                color:white;
                font-weight:normal;
                font-size:16px;
                width:99%;
                height:20px;
                padding-left:3px;
            }
            .detail{
                width:49%;
                float:left;
            }
            .diagram{
                width:50%;
                float:right;
                border-left:solid black 1px;
                text-align: right;
            }
            td.inf, td.inf-value {height:20px;
                                  color:#666666;
                                  font-weight:normal;
            }
            td.inf-value{
                text-align:right;
            }
            .target_lnk{
                padding-right: 15px;
            }
            .invent_lnk{
                color: #666666;
            }
            input.buttonSave{
                width: 100px;
            }
            .rich-mpnl-header {
                background-image: url('images/bg_cap.jpg');
            }
            .rich-table {
                border:none;
                border-collapse:separate;
            }
            .rich-table-headercell {
                background-image: url('images/bg_cap.jpg');
                background-repeat: repeat-x;
            }
            .rich-table-cell, .rich-subtable-cell {
                padding:0px;
                text-align:left;
                vertical-align:middle;
                border:1px solid #C4C0C9;
                font-size:12px;
            }
            .editor {
                border:0px;
                width:80px;
                text-align:right;
            }
            .category-style {
                font-size:14px;
                font-weight:normal;
                color:white;
                text-align:left;
                vertical-align:middle;
                background-color:gray;
            }
            .subcategory-style {
                font-size:medium;
                font-weight:normal;
                text-align:left;
                vertical-align:middle;
                background-color:#C0C0C0;
            }
            .item-style {
                text-align:left;
                border-width:1px;
                border:0px;
            }
            .item-style-calculated {
                text-align:right;
                background-color:#F6FCF7;
                font-size:13px;
                font-weight:bold;
                border-width:1px;
                border:0px;
            }
            .message {
                position: absolute;
                right: 15%;
            }
            .spacer {
                background-color:white;
                border:none;
                height:14px;
            }

        </style>
        <script type="text/javascript" src="js/prototype.js"></script>
        <script type="text/javascript" src="js/hotkeys.js"></script>
        <script type="text/javascript" src="js/table.js"></script>
        <script type="text/javascript" src="js/effects.js"></script>
        <script type="text/javascript" src="js/messages.js"></script>
        <script type="text/javascript">
            function updateDialog()
            {
                $('dialogHeader').style.backgroundImage = "url('images/bg_cap.jpg')";
            }
        </script>
    </head>
    <body>
        <f:view beforePhase="#{performanceController.recalculate}">
            <f:subview id="flying">
                <c:import url="includes/flyingpanels.jsp" />
            </f:subview>
            <f:subview id="mnu">
                <jsp:include page="includes/menu.jsp" />
            </f:subview>
            <f:subview id="tc">
                <jsp:include page="includes/topclient.jsp" />
            </f:subview>

            <rich:modalPanel id="dialog" width="350" autosized="true"
                             showWhenRendered="#{performanceController.neededStartDataDialog}"
                             onshow='this.style.position="fixed";updateDialog();'>
                <f:facet name="header">
                    <h:panelGroup styleClass="error-header">
                        <h:outputText value="New User Information Request"></h:outputText>
                    </h:panelGroup>
                </f:facet>
                <f:facet name="controls">
                    <h:panelGroup>
                        <h:graphicImage value="/images/close.png" styleClass="hidelink" id="hideBtn"/>
                        <rich:componentControl for="dialog" attachTo="hideBtn" operation="hide" event="onclick"/>
                    </h:panelGroup>
                </f:facet>

            </rich:modalPanel>
            <h1 class="pageCaption">Report Center</h1>
            <a4j:form id="smr" ajaxSubmit="true">
                <a4j:status startStyleClass="save-sign" 
                            onstart="showSavingSign()"
                            onstop="hideSavingSign();">
                </a4j:status>

                <h:outputText style="float:left;" value="Dates" />
                <h:selectOneMenu id="datesTypeCombo" styleClass="topCombo" style="width:160px;" value="#{summaryController.dateType}" >
                    <f:selectItems value="#{summaryController.dateTypes}"/>
                    <a4j:support event="onchange" reRender="fromDateCalendar,toDateCalendar"  />
                </h:selectOneMenu>
                <h:outputText value="From" rendered="#{summaryController.fromEnabled}" />
                <rich:calendar value="#{summaryController.fromDate}" id="fromDateCalendar" locale="en" popup="true" datePattern="MM/dd/yyyy"
                               disabled="#{summaryController.dateType != 'Custom'}"       showApplyButton="false" cellWidth="24px" cellHeight="22px" style="width:200px">
                </rich:calendar>
                <h:outputText  value="To" rendered="#{summaryController.toEnabled}" />
                <rich:calendar value="#{summaryController.toDate}" id="toDateCalendar" locale="en" popup="true" datePattern="MM/dd/yyyy"
                               disabled="#{summaryController.dateType != 'Custom'}" showApplyButton="false" cellWidth="24px" cellHeight="22px" style="width:200px" rendered="#{summaryController.toEnabled}">
                </rich:calendar>
                <h:outputText value="Columns" />
                <h:selectOneMenu id="columnFrequency" styleClass="topCombo" style="width:160px;" value="#{summaryController.columnFrequency}" >
                    <f:selectItems value="#{summaryController.columnFrequencies}"/>
                </h:selectOneMenu>
                <a4j:commandButton id="save" 
                                   styleClass="buttonDisplayData"
                                   title="Display Data"
                                   reRender="smr"
                                   action="#{summaryController.displayData}" value="Display Data"
                                   >
                </a4j:commandButton>


                <rich:panel id="reports" >
                    <f:subview rendered="#{summaryController.dateType eq 'Week-to-date'}" id="weektodatereport">
                        <jsp:include page="includes/weektodatereport.jsp" />
                    </f:subview>
                    <f:subview rendered="#{summaryController.dateType eq 'Custom'}" id="customdatereport">
                        <jsp:include page="includes/customdatereport.jsp" />
                    </f:subview>
                </rich:panel>
            </a4j:form>

            <f:subview id="btm">
                <jsp:include page="includes/bottom.jsp" />
            </f:subview>
        </f:view>
    </body>
</html>