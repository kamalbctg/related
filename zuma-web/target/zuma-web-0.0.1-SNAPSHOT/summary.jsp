<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<f:view beforePhase="#{summaryController.recalculate}" >
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
            <title>Daily Summary</title>
            <link type="text/css" rel="stylesheet" href="css/zuma.css"/>
            <style  type="text/css">
                .rich-table {
                    border:none;
                    border-collapse:separate;
                }
                .rich-table-headercell {
                    background-image: url('images/bg_cap.jpg');
                    background-repeat: repeat-x;
                }
                .rich-table-cell, .rich-subtable-cell {
                    padding:0px;
                    text-align:left;
                    vertical-align:middle;
                    border:1px solid #C4C0C9;
                    font-size:12px;
                }
                .editor {
                    border:0px;
                    width:80px;
                    text-align:right;
                }
                .category-style {
                    font-size:14px;
                    font-weight:normal;
                    color:white;
                    text-align:left;
                    vertical-align:middle;
                    background-color:gray;
                }
                .subcategory-style {
                    font-size:medium;
                    font-weight:normal;
                    text-align:left;
                    vertical-align:middle;
                    background-color:#C0C0C0;
                }
                .item-style {
                    text-align:left;
                    border-width:1px;
                    border:0px;
                }
                .item-style-calculated {
                    text-align:right;
                    background-color:#F6FCF7;
                    font-size:13px;
                    font-weight:bold;
                    border-width:1px;
                    border:0px;
                }
                .message {
                    position: absolute;
                    right: 15%;
                }
                .spacer {
                    background-color:white;
                    border:none;
                    height:14px;
                }
            </style>
            <script type="text/javascript" src="js/prototype.js"></script>
            <script type="text/javascript" src="js/hotkeys.js"></script>
            <script type="text/javascript" src="js/table.js"></script>
            <script type="text/javascript" src="js/effects.js"></script>
            <script type="text/javascript" src="js/messages.js"></script>
            <script type="text/javascript">
                function save()
                {
                    $("smr:save").click();
                    saveSign = true;
                }
                Hotkeys.bind("ctrl+s", save);
            </script>
        </head>
        <body>

            <f:subview id="flying">
                <c:import url="includes/flyingpanels.jsp" />
            </f:subview>
            <f:subview id="mnu">
                <jsp:include page="includes/menu.jsp"/>
            </f:subview>
            <f:subview id="tc">
                <jsp:include page="includes/topclient.jsp" />
            </f:subview>
            <h1 class="pageCaption" style="float:left;">Daily Summary</h1>
            <div align="center">
                <a4j:form id="smr" ajaxSubmit="true">
                    <a4j:status startStyleClass="save-sign" 
                                onstart="showSavingSign()"
                                onstop="hideSavingSign();updateTable('smr:tbl');showSaveApprove();">
                    </a4j:status>
                    <div class="save"><a4j:commandButton id="save"  reRender="smr,flying:modalPnl"
                                       styleClass="buttonSave"
                                       title="Save [Ctrl+S]"
                                       action="#{summaryController.save}" value="Save"
                                       onclick="saveSign=true;">
                        </a4j:commandButton>
                    </div>

                    <rich:dataTable id="tbl" value="#{summaryController.dataSet.dataValues}" var="categoryData" >
                        <f:facet name="header">
                            <rich:columnGroup>
                                <rich:column id="hd_Title" width="272" style="text-align:left;">
                                    <h:outputText value="Day" />
                                </rich:column>
                                <rich:column id="hd_Mon" width="72">
                                    <h:outputText value="Mon" />
                                </rich:column>
                                <rich:column id="hd_Tue" width="72">
                                    <h:outputText value="Tue" />
                                </rich:column>
                                <rich:column id="hd_Wed" width="72">
                                    <h:outputText value="Wed" />
                                </rich:column>
                                <rich:column id="hd_Thu" width="72">
                                    <h:outputText value="Thu" />
                                </rich:column>
                                <rich:column id="hd_Fri" width="72">
                                    <h:outputText value="Fri" />
                                </rich:column>
                                <rich:column id="hd_Sat" width="72">
                                    <h:outputText value="Sat" />
                                </rich:column>
                                <rich:column id="hd_Sun" width="72">
                                    <h:outputText value="Sun" />
                                </rich:column>
                                <rich:column id="hd_Total" width="80">
                                    <h:outputText value="Total" />
                                </rich:column>
                                <rich:column id="hd_date" breakBefore="true" style="text-align:left;">
                                    <h:outputText value="Date"/>
                                </rich:column>
                                <rich:column id="hd_dt0" >
                                    <h:outputText value="#{summaryController.dataSet.dates[0]}" >
                                        <f:convertDateTime pattern="MM/dd"/>
                                    </h:outputText>
                                </rich:column>
                                <rich:column id="hd_dt1" >
                                    <h:outputText value="#{summaryController.dataSet.dates[1]}" >
                                        <f:convertDateTime pattern="MM/dd"/>
                                    </h:outputText>
                                </rich:column>
                                <rich:column id="hd_dt2" >
                                    <h:outputText value="#{summaryController.dataSet.dates[2]}" >
                                        <f:convertDateTime pattern="MM/dd"/>
                                    </h:outputText>
                                </rich:column>
                                <rich:column id="hd_dt3" >
                                    <h:outputText value="#{summaryController.dataSet.dates[3]}" >
                                        <f:convertDateTime pattern="MM/dd"/>
                                    </h:outputText>
                                </rich:column>
                                <rich:column id="hd_dt4" >
                                    <h:outputText value="#{summaryController.dataSet.dates[4]}" >
                                        <f:convertDateTime pattern="MM/dd"/>
                                    </h:outputText>
                                </rich:column>
                                <rich:column id="hd_dt5" >
                                    <h:outputText value="#{summaryController.dataSet.dates[5]}" >
                                        <f:convertDateTime pattern="MM/dd"/>
                                    </h:outputText>
                                </rich:column>
                                <rich:column id="hd_dt6" >
                                    <h:outputText value="#{summaryController.dataSet.dates[6].time}" >
                                        <f:convertDateTime pattern="MM/dd"/>
                                    </h:outputText>
                                </rich:column>
                                <rich:column id="hd_dt_space" >&nbsp;
                                </rich:column>
                            </rich:columnGroup>
                        </f:facet>
                        <rich:column  id="Cat_Caption" colspan="9" styleClass="category-style">
                            <h:outputText value="#{categoryData.parent.caption}" />
                        </rich:column>
                        <rich:subTable id="sbcat" var="subCategoryData" value="#{categoryData.dataValues}">
                            <rich:column id="subCatSpace" colspan="9" styleClass="spacer" rendered="#{subCategoryData.parent.breakBefore}"><rich:spacer/></rich:column>
                            <rich:columnGroup rendered="#{subCategoryData.parent.showCaption}" styleClass="subcategory-style">
                                <rich:column id="SubCat_Caption">
                                    <h:outputText id="seq" value="#{subCategoryData.parent.editedName}"/>
                                </rich:column>
                                <rich:column colspan="9"><rich:spacer/></rich:column>
                            </rich:columnGroup>
                            <rich:subTable id="it" var="itemData" value="#{subCategoryData.dataValues}" 
                                           onRowMouseOver="activateRow(this)"  onRowMouseOut="deactivateRow(this)">
                                <rich:columnGroup rendered="#{!itemData.parent.calculated}">
                                    <rich:column id="item_edt_caption" styleClass="item-style">
                                        <h:outputText value="#{itemData.parent.editedCaption}"/>
                                    </rich:column>
                                    <rich:column id="cell_mon" styleClass="rich-subtable-cell" style="border-right:none;">
                                        <h:inputText id="mon" value="#{itemData.mondayData.value}"
                                                     styleClass="editor"
                                                     validatorMessage="Invalid monday data for item '#{itemData.parent.editedCaption}'!"
                                                     converterMessage="Invalid monday data for item '#{itemData.parent.editedCaption}'!"
                                                     valueChangeListener="#{summaryController.processValueChange}">
                                            <f:converter converterId="uiConverter" />
                                            <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                                            <f:param value="#{itemData.mondayData}" />
                                            <f:validateLength minimum="1" maximum="12" />
                                            <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                                        </h:inputText>
                                    </rich:column>
                                    <rich:column id="cell_tue"  style="border-right:none;">
                                        <h:inputText id="tue" value="#{itemData.tuesdayData.value}"
                                                     readonly="#{itemData.parent.calculated}"  styleClass="editor"
                                                     validatorMessage="Invalid tuesday data for item '#{itemData.parent.editedCaption}'!"
                                                     converterMessage="Invalid tuesday data for item '#{itemData.parent.editedCaption}'!"
                                                     valueChangeListener="#{summaryController.processValueChange}">
                                            <f:converter converterId="uiConverter" />
                                            <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                                            <f:param value="#{itemData.tuesdayData}" />
                                            <f:validateLength minimum="1" maximum="12" />
                                            <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                                        </h:inputText>
                                    </rich:column>
                                    <rich:column id="cell_wed" style="border-right:none;">
                                        <h:inputText id="wed" value="#{itemData.wednesdayData.value}"
                                                     readonly="#{itemData.parent.calculated}"  styleClass="editor"
                                                     validatorMessage="Invalid wednesday data for item '#{itemData.parent.editedCaption}'!"
                                                     converterMessage="Invalid wednesday data for item '#{itemData.parent.editedCaption}'!"
                                                     valueChangeListener="#{summaryController.processValueChange}">
                                            <f:converter converterId="uiConverter" />
                                            <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                                            <f:param value="#{itemData.wednesdayData}" />
                                            <f:validateLength minimum="1" maximum="12" />
                                            <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                                        </h:inputText>
                                    </rich:column>
                                    <rich:column id="cell_thu" style="border-right:none;">
                                        <h:inputText id="thu" value="#{itemData.thursdayData.value}"
                                                     readonly="#{itemData.parent.calculated}"  styleClass="editor"
                                                     validatorMessage="Invalid thursday data for item '#{itemData.parent.editedCaption}'!"
                                                     converterMessage="Invalid thursday data for item '#{itemData.parent.editedCaption}'!"
                                                     valueChangeListener="#{summaryController.processValueChange}">
                                            <f:converter converterId="uiConverter" />
                                            <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                                            <f:param value="#{itemData.thursdayData}" />
                                            <f:validateLength minimum="1" maximum="12" />
                                            <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                                        </h:inputText>
                                    </rich:column>
                                    <rich:column id="cell_fri"style="border-right:none;">
                                        <h:inputText id="fri" value="#{itemData.fridayData.value}"
                                                     readonly="#{itemData.parent.calculated}"  styleClass="editor"
                                                     validatorMessage="Invalid friday data for item '#{itemData.parent.editedCaption}'!"
                                                     converterMessage="Invalid friday data for item '#{itemData.parent.editedCaption}'!"
                                                     valueChangeListener="#{summaryController.processValueChange}">
                                            <f:converter converterId="uiConverter" />
                                            <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                                            <f:param value="#{itemData.fridayData}" />
                                            <f:validateLength minimum="1" maximum="12" />
                                            <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                                        </h:inputText>
                                    </rich:column>
                                    <rich:column id="cell_sat" style="border-right:none;">
                                        <h:inputText id="sat" value="#{itemData.saturdayData.value}"
                                                     readonly="#{itemData.parent.calculated}"  styleClass="editor"
                                                     validatorMessage="Invalid saturday data for item '#{itemData.parent.editedCaption}'!"
                                                     converterMessage="Invalid saturday data for item '#{itemData.parent.editedCaption}'!"
                                                     valueChangeListener="#{summaryController.processValueChange}">
                                            <f:converter converterId="uiConverter" />
                                            <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                                            <f:param value="#{itemData.saturdayData}" />
                                            <f:validateLength minimum="1" maximum="12" />
                                            <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                                        </h:inputText>
                                    </rich:column>
                                    <rich:column id="cell_sun">
                                        <h:inputText id="sun" value="#{itemData.sundayData.value}"
                                                     readonly="#{itemData.parent.calculated}"  styleClass="editor"
                                                     validatorMessage="Invalid sunday data for item '#{itemData.parent.editedCaption}'!"
                                                     converterMessage="Invalid sunday data for item '#{itemData.parent.editedCaption}'!"
                                                     valueChangeListener="#{summaryController.processValueChange}">
                                            <f:converter converterId="uiConverter" />
                                            <f:attribute name="converterPattern" value="#{itemData.parent.pattern};#,###,##0.00"/>
                                            <f:param value="#{itemData.sundayData}" />
                                            <f:validateLength minimum="1" maximum="12" />
                                            <f:validateDoubleRange maximum="999999.9999" minimum="-999999.9999"/>
                                        </h:inputText>
                                    </rich:column>
                                    <rich:column id="cell_total_1" styleClass="item-style-calculated">
                                        <h:outputText id="totalValue" value="#{itemData.totalValueText}" />
                                    </rich:column>

                                </rich:columnGroup>
                                <rich:columnGroup rendered="#{itemData.parent.calculated}">
                                    <rich:column id="calcItemEdtCapt" styleClass="item-style" style="font-weight:bold;">
                                        <h:outputText value="#{itemData.parent.editedCaption}"/>
                                    </rich:column>
                                    <rich:column id="cell_mon_calc"  styleClass="item-style-calculated">
                                        <h:outputText value="#{itemData.mondayData.valueText}"/>
                                    </rich:column>
                                    <rich:column id="cell_tue_calc" styleClass="item-style-calculated">
                                        <h:outputText value="#{itemData.tuesdayData.valueText}" />
                                    </rich:column>
                                    <rich:column id="cell_wed_calc"  styleClass="item-style-calculated">
                                        <h:outputText value="#{itemData.wednesdayData.valueText}" />
                                    </rich:column>
                                    <rich:column id="cell_thu_calc"  styleClass="item-style-calculated">
                                        <h:outputText value="#{itemData.thursdayData.valueText}" />
                                    </rich:column>
                                    <rich:column id="cell_fri_calc" styleClass="item-style-calculated">
                                        <h:outputText value="#{itemData.fridayData.valueText}" />
                                    </rich:column>
                                    <rich:column id="cell_sat_calc"  styleClass="item-style-calculated">
                                        <h:outputText value="#{itemData.saturdayData.valueText}" />
                                    </rich:column>
                                    <rich:column id="cell_sun_calc"  styleClass="item-style-calculated">
                                        <h:outputText value="#{itemData.sundayData.valueText}" />
                                    </rich:column>
                                    <rich:column id="cell_total_2" styleClass="item-style-calculated">
                                        <h:outputText id="totalValue_2" value="#{itemData.totalValueText}" />
                                    </rich:column>

                                </rich:columnGroup>

                            </rich:subTable>
                        </rich:subTable>
                        <rich:column colspan="9" styleClass="spacer"><rich:spacer/></rich:column>
                    </rich:dataTable>
                    <div class="save"><a4j:commandButton id="savebottom"  reRender="smr,flying:modalPnl"
                                       styleClass="buttonSave"
                                       title="Save [Ctrl+S]"
                                       action="#{summaryController.save}" value="Save"
                                       onclick="saveSign=true;">
                        </a4j:commandButton>
                    </div>
                </a4j:form>
            </div>
            <f:subview id="btm">
                <jsp:include page="includes/bottom.jsp" />
            </f:subview>
            <script type="text/javascript">updateTable("smr:tbl");</script>

        </body>
    </html>
</f:view>
