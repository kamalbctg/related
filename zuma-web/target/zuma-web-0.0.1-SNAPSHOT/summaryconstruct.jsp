<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://richfaces.org/a4j" prefix="a4j"%>
<%@ taglib uri="http://richfaces.org/rich" prefix="rich"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" type="text/css" href="css/inventoryedit.css" />

        <title>Daily Summary - Edit</title>
        <link type="text/css" rel="stylesheet" href="css/zuma.css"/>
        <style type="text/css">
            .dr-table-cell {
                padding:0px;
                text-align:center;
                vertical-align:middle;
            }
            .del_style {
                border:0px;
            }
            /* ---- new style ----- */
            input {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 11px;
                color: #666666;
                border: 1px #868686 solid;
                text-align: left;
                font-weight: normal;
                padding: 1px;
                height: 14px;
                width: 122px;
            }
            input.box
            {
                width: 14px;
            }
            input.pattern
            {
                width: 65px;
            }
            .rich-table-headercell
            {
                padding: 1px;
            }
            input.catCaption{
                color: #666666;
                font-weight: bold;
                width: 171px;
            }
            .categoryCaption {
                text-align: left;
                padding-left: 15px;
                color: #666666;
                border-top: 1px #C4C0C9 solid;
            }
            .delCaption {
                text-align: center;
                vertical-align: middle;
                padding: 1px;
                border-top: 1px #C4C0C9 solid;
            }
            .memo {
                border:0px;
                padding:0px;
                border-bottom:1px #666666 dashed;
                width:318px;
                resize:none; /* Chrome */
                word-wrap: break-word; /* IE>=5.5 */
                white-space: normal;
            }
        </style>
        <script type="text/javascript" src="js/prototype.js"></script>
        <script type="text/javascript" src="js/hotkeys.js"></script>
        <script type="text/javascript" src="js/effects.js"></script>
        <script type="text/javascript" src="js/messages.js"></script>
        <script type="text/javascript">
            function save()
            {
                $("frmCnstr:savetop").click();
                saveSign=true;
            }
            Hotkeys.bind("ctrl+s", save);
        </script>

    </head>
    <body>
        <f:view beforePhase="#{summaryConstructController.recalculate}" >
            <f:subview id="flying">
                <c:import url="includes/flyingpanels.jsp" />
            </f:subview>
            <f:subview id="mnu">
                <jsp:include page="includes/menu.jsp"/>
            </f:subview>
            <f:subview id="tc">
                <jsp:include page="includes/topclient.jsp" />
            </f:subview>
            <h1 class="pageCaption" style="float:left;">Daily Summary - Edit</h1><br/>
            <div align="center">

                <a4j:form ajaxSubmit="true" rendered="#{userManagedBean.admin}">
                    <div style="float:right; text-align:left;vertical-align:middle;">
                        <table>
                            <tr>
                                <td><h:outputText value="Copy report from:"/>&nbsp;</td>
                                <td>
                                    <h:selectOneMenu id="accountCombo" styleClass="topCombo"
                                                     value="#{summaryConstructController.companyToCopyFrom}">
                                        <f:selectItems value="#{userManagedBean.accountItems}" />
                                        <f:converter converterId="selectMapConverter"/>
                                        <a4j:support event="onchange" reRender="storeCombo" ajaxSingle="true"/>
                                    </h:selectOneMenu>&nbsp;
                                </td>
                                <td>
                                    <h:selectOneMenu id="storeCombo" styleClass="topCombo" style="width:140px;"
                                                     value="#{summaryConstructController.storeToCopyFrom}">
                                        <f:selectItems value="#{summaryConstructController.storesToCopyFrom}" />
                                        <f:converter converterId="selectMapConverter"/>
                                    </h:selectOneMenu>&nbsp;
                                </td>
                                <td>
                                    <a4j:commandButton id="copy"
                                                       reRender="frmCnstr, flying:modalPnl"
                                                       value="Copy" styleClass="buttonSave"
                                                       action="#{summaryConstructController.processDataCopying}"/>
                                </td>
                            </tr>
                        </table>
                    </div><br/><br/>
                </a4j:form>

                <a4j:form id="frmCnstr" ajaxSubmit="true" >
                    <a4j:status startStyleClass="save-sign"
                                onstart="showSavingSign()"
                                onstop="hideSavingSign();showSaveApprove();">
                    </a4j:status>
                    <div class="save"><a4j:commandButton id="savetop"
                                       reRender="frmCnstr, flying:modalPnl"
                                       action="#{summaryConstructController.save}"
                                       title="Save [Ctrl+S]"
                                       value="Save" styleClass="buttonSave"
                                       onclick="saveSign=true;"/>
                    </div>
                    <rich:dataTable id="cttbl"
                                    value="#{summaryConstructController.dataSet.dataValues}" var="categoryData" width="938">
                        <rich:column>
                            <rich:dataTable id="subctbl" value="#{categoryData.dataValues}" var="subCategoryData" width="100%" border="0" rowKeyVar="subCategoryIndex">
                                <f:facet name="header">
                                    <rich:columnGroup>
                                        <rich:column id="clctcptn" styleClass="categoryCaption" width="906" ><h:outputText value="Category caption: " />
                                            <h:inputText id = "ctcptn" value="#{categoryData.parent.caption}"
                                                         styleClass="catCaption"
                                                         valueChangeListener="#{summaryConstructController.processValueChange}">
                                                <f:param value="#{categoryData.parent}" />
                                            </h:inputText>
                                            <rich:spacer/>
                                        </rich:column>
                                        <rich:column id="clctdlt" styleClass="delCaption" width="32">
                                            <a4j:commandLink id="dltCat" reRender="frmCnstr"
                                                             action="#{summaryConstructController.deleteCategory}"
                                                             onclick="javascript:if(!confirm(deleteMessage))return false;">
                                                <h:graphicImage alt="" url="images/delete.gif" styleClass="del_style"/>
                                                <f:setPropertyActionListener
                                                    target="#{summaryConstructController.selectedCategory}"
                                                    value="#{categoryData.parent}" />
                                            </a4j:commandLink></rich:column>
                                    </rich:columnGroup>
                                </f:facet>

                                <rich:column colspan="2">
                                    <rich:dataTable id="tblSbCt" value="#{subCategoryData.dataValues}" var="itemData" width="100%" rowKeyVar="index" style="padding:0px;">
                                        <f:facet name="header">
                                            <rich:columnGroup >
                                                <rich:column id="clSbCtId" width="30">
                                                    <h:outputText value="#{subCategoryData.parent.id}" />
                                                </rich:column>
                                                <rich:column id="clSbCtCptn" width="130" style="vertical-align:bottom;">
                                                    <h:outputText value="Subcategory caption"
                                                                  rendered="#{subCategoryIndex==0}"/>
                                                    <h:inputText id="sbCtCptn" value="#{subCategoryData.parent.caption}"
                                                                 valueChangeListener="#{summaryConstructController.processValueChange}">
                                                        <f:param value="#{subCategoryData.parent}" />
                                                    </h:inputText>
                                                </rich:column>
                                                <rich:column id="clSbCtCptn1" width="130">
                                                    <h:outputText value="Customized Subcategory Name"
                                                                  rendered="#{subCategoryIndex==0}"/>
                                                    <h:inputText id="sbCtCptn1" value="#{subCategoryData.parent.editedName}"
                                                                 valueChangeListener="#{summaryConstructController.processValueChange}">
                                                        <f:param value="#{subCategoryData.parent}" />
                                                    </h:inputText>
                                                </rich:column>
                                                <rich:column id="clShwCptn" width="320">
                                                    <h:outputText value="Show caption "/>
                                                    <h:selectBooleanCheckbox id="shwCptn" value="#{subCategoryData.parent.showCaption}"
                                                                             styleClass="box"
                                                                             valueChangeListener="#{summaryConstructController.processValueChange}">
                                                        <f:param value="#{subCategoryData.parent}" />
                                                    </h:selectBooleanCheckbox>
                                                </rich:column>
                                                <rich:column id="clBr" colspan="3">
                                                    <h:outputText value="Break before " />
                                                    <h:selectBooleanCheckbox id="br" value="#{subCategoryData.parent.breakBefore}"
                                                                             styleClass="box"
                                                                             valueChangeListener="#{summaryConstructController.processValueChange}">
                                                        <f:param value="#{subCategoryData.parent}" />
                                                    </h:selectBooleanCheckbox>
                                                </rich:column>

                                                <rich:column id="clSbDlt" width="30">
                                                    <a4j:commandLink id="dltSub" reRender="frmCnstr"
                                                                     action="#{summaryConstructController.deleteSubCategory}"
                                                                     onclick="javascript:if(!confirm(deleteMessage))return false;"
                                                                     style="text-align: center;vertical-align: middle; padding:1px;">
                                                        <h:graphicImage alt="" url="images/delete.gif" styleClass="del_style"/>
                                                        <f:setPropertyActionListener
                                                            target="#{summaryConstructController.selectedSubCategory}"
                                                            value="#{subCategoryData.parent}" />
                                                        <f:setPropertyActionListener
                                                            target="#{summaryConstructController.selectedCategory}"
                                                            value="#{categoryData.parent}" />
                                                    </a4j:commandLink>
                                                </rich:column>
                                                <rich:column id="clCtUp" width="30">
                                                    <rich:spacer rendered="#{subCategoryIndex==0}"/>
                                                    <a4j:commandLink id="ctUp" reRender="frmCnstr"
                                                                     action="#{summaryConstructController.putSubCategoryUp}" value="Up"
                                                                     rendered="#{subCategoryIndex>0}">
                                                        <f:setPropertyActionListener
                                                            target="#{summaryConstructController.selectedSubCategory}"
                                                            value="#{subCategoryData.parent}" />
                                                    </a4j:commandLink>
                                                </rich:column>
                                                <rich:column id="clCtDwn" width="32">
                                                    <rich:spacer rendered="#{subCategoryIndex==(fn:length(categoryData.dataValues)-1)}"/>
                                                    <a4j:commandLink id="ctDwn" reRender="frmCnstr"
                                                                     rendered="#{subCategoryIndex<(fn:length(categoryData.dataValues)-1)}"
                                                                     action="#{summaryConstructController.putSubCategoryDown}" value="Down">
                                                        <f:setPropertyActionListener
                                                            target="#{summaryConstructController.selectedSubCategory}"
                                                            value="#{subCategoryData.parent}" />
                                                    </a4j:commandLink>
                                                </rich:column>
                                                <rich:columnGroup rendered="#{fn:length(subCategoryData.dataValues)>0}">
                                                    <rich:column id="clIdHd" breakBefore="true"><h:outputText value = "ID"/>
                                                    </rich:column>
                                                    <rich:column id="clNmHd"><h:outputText value="Item Name" style="color:#868686;"/>
                                                    </rich:column>
                                                    <rich:column id="clNmHd1"><h:outputText value="Customized Row Name" style="color:#868686;"/>
                                                    </rich:column>
                                                    <rich:column id="clfrmlHd"><h:outputText value="Item formula" />
                                                    </rich:column>
                                                    <rich:column id="clPtrnHd"><h:outputText value="Pattern" />
                                                    </rich:column>
                                                    <rich:column id="clExcptnHd"><h:outputText value="Exception" />
                                                    </rich:column>
                                                    <rich:column id="clAcmltHd"><h:outputText value="Accumulated" />
                                                    </rich:column>
                                                    <rich:column id="clDltHd"><h:outputText value="Delete"/>
                                                    </rich:column>
                                                    <rich:column id="clSpcHd"><rich:spacer />
                                                    </rich:column>
                                                    <rich:column id="clSpcHd1"><rich:spacer/>
                                                    </rich:column>
                                                </rich:columnGroup>
                                            </rich:columnGroup>
                                        </f:facet>

                                        <rich:column id="clId" style="text-align: center;vertical-align: middle;">
                                            <h:outputText value="#{itemData.parent.id}" />
                                        </rich:column>

                                        <rich:column id="clCptn" style="text-align: center;vertical-align: middle;">
                                            <h:inputText id="cptn" value="#{itemData.parent.caption}"
                                                         validatorMessage="Text out of range!"
                                                         valueChangeListener="#{summaryConstructController.processValueChange}">
                                                <f:param value="#{itemData.parent}" />
                                                <f:validateLength minimum="1" maximum="50" />
                                            </h:inputText>
                                        </rich:column>
                                        <rich:column id="clCptn1" style="text-align: center;vertical-align: middle;">
                                            <h:inputText id="cptn1" value="#{itemData.parent.editedCaption}"
                                                         validatorMessage="Text out of range!"
                                                         valueChangeListener="#{summaryConstructController.processValueChange}">
                                                <f:param value="#{itemData.parent}" />
                                                <f:validateLength minimum="1" maximum="100" />
                                            </h:inputText>
                                        </rich:column>
                                        <rich:column id="clFrml" width="320">
                                            <h:inputTextarea id="frml" value="#{itemData.parent.formula}"
                                                             rows="2"
                                                             styleClass="memo"
                                                             validatorMessage="Text out of range!"
                                                             valueChangeListener="#{summaryConstructController.processValueChange}">
                                                <f:param value="#{itemData.parent}" />
                                                <f:validateLength minimum="1" maximum="500" />
                                            </h:inputTextarea>
                                        </rich:column>
                                        <rich:column id="clPtrn">
                                            <h:inputText id="ptrn" value="#{itemData.parent.pattern}"
                                                         styleClass="pattern"
                                                         validatorMessage="Text out of range!"
                                                         valueChangeListener="#{summaryConstructController.processValueChange}">
                                                <f:param value="#{itemData.parent}" />
                                                <f:validateLength minimum="1" maximum="20" />
                                            </h:inputText>
                                        </rich:column>
                                        <rich:column id="clExcptn" style="text-align: center;vertical-align: middle;">
                                            <h:selectBooleanCheckbox id="excptn" value="#{itemData.parent.allowsException}"
                                                                     styleClass="box"
                                                                     valueChangeListener="#{summaryConstructController.processValueChange}">
                                                <f:param value="#{itemData.parent}" />
                                            </h:selectBooleanCheckbox>
                                        </rich:column>

                                        <rich:column id="clPrv" style="text-align: center;vertical-align: middle;">
                                            <h:selectBooleanCheckbox id="prv" value="#{itemData.parent.prevDaysDepended}"
                                                                     styleClass="box"
                                                                     valueChangeListener="#{summaryConstructController.processValueChange}">
                                                <f:param value="#{itemData.parent}" />
                                            </h:selectBooleanCheckbox>
                                        </rich:column>
                                        <rich:column id="clDltIt" style="text-align: center;vertical-align: middle;">
                                            <a4j:commandLink id="dltIt" reRender="frmCnstr"
                                                             action="#{summaryConstructController.deleteItem}"
                                                             onclick="javascript:if(!confirm(deleteMessage))return false;">
                                                <h:graphicImage alt="" url="images/delete.gif" styleClass="del_style"/>
                                                <f:setPropertyActionListener
                                                    target="#{summaryConstructController.selectedSubCategory}"
                                                    value="#{subCategoryData.parent}" />
                                                <f:setPropertyActionListener
                                                    target="#{summaryConstructController.selectedItem}"
                                                    value="#{itemData.parent}" />
                                            </a4j:commandLink>
                                        </rich:column>
                                        <rich:column id="clItUp" width="30"  style="text-align:center;vertical-allign:middle;">
                                            <a4j:commandLink id="Up" reRender="frmCnstr"
                                                             action="#{summaryConstructController.putItemUp}" value="Up"
                                                             rendered="#{index!=0}">
                                                <f:setPropertyActionListener
                                                    target="#{summaryConstructController.selectedSubCategory}"
                                                    value="#{subCategoryData.parent}" />
                                                <f:setPropertyActionListener
                                                    target="#{summaryConstructController.selectedItem}"
                                                    value="#{itemData.parent}" />
                                            </a4j:commandLink>
                                        </rich:column>
                                        <rich:column id="clItDwn" width="32">
                                            <a4j:commandLink id="dwn" reRender="frmCnstr"
                                                             rendered="#{index<(fn:length(subCategoryData.dataValues)-1)}"
                                                             action="#{summaryConstructController.putItemDown}" value="Down">
                                                <f:setPropertyActionListener
                                                    target="#{summaryConstructController.selectedSubCategory}"
                                                    value="#{subCategoryData.parent}" />
                                                <f:setPropertyActionListener
                                                    target="#{summaryConstructController.selectedItem}"
                                                    value="#{itemData.parent}" />
                                            </a4j:commandLink>
                                        </rich:column>

                                        <f:facet name="footer">
                                            <a4j:commandLink id="Add" reRender="frmCnstr"
                                                             action="#{summaryConstructController.insertOneItem}"
                                                             value="Add an Item">
                                                <f:setPropertyActionListener
                                                    target="#{summaryConstructController.selectedSubCategory}"
                                                    value="#{subCategoryData.parent}" />
                                            </a4j:commandLink>
                                        </f:facet>

                                    </rich:dataTable>
                                </rich:column>
                                <f:facet name="footer">
                                    <a4j:commandLink id="Add" reRender="frmCnstr"
                                                     action="#{summaryConstructController.insertOneSubCategory}"
                                                     value="Add a SubCategory">
                                        <f:setPropertyActionListener
                                            target="#{summaryConstructController.selectedCategory}"
                                            value="#{categoryData.parent}" />
                                    </a4j:commandLink>
                                </f:facet>

                            </rich:dataTable>
                        </rich:column>
                        <f:facet name="footer">
                            <a4j:commandLink id="Add" reRender="frmCnstr"
                                             action="#{summaryConstructController.insertOneCategory}"
                                             value="Add a Category" />
                        </f:facet>
                    </rich:dataTable>
                    <div class="save"><a4j:commandButton id="savebottom"
                                       reRender="frmCnstr, flying:modalPnl"
                                       action="#{summaryConstructController.save}"
                                       styleClass="buttonSave"
                                       title="Save [Ctrl+S]"
                                       value="Save"
                                       onclick="saveSign=true;"/></div>
                    </a4j:form>
            </div>
            <f:subview id="btm">
                <jsp:include page="includes/bottom.jsp" />
            </f:subview>
        </f:view>
    </body>
</html>