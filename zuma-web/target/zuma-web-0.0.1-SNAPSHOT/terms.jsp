<%@ page language="java"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>Terms of Use</title>
        <link type="text/css" rel="stylesheet" href="css/zuma.css"/>
        <link type="text/css" rel="stylesheet" href="css/simple.css"/>
    </head>
    <body>
        <f:view>
            <f:subview id="mnu">
                <jsp:include page="includes/menu.jsp" />
            </f:subview>
            <f:subview id="tc">
                <jsp:include page="includes/topclient.jsp" />
            </f:subview>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tbody><tr>
                        <td class="column_div" valign="top" width="200"><h1 class="pageCaption">Terms of Use</h1></td>
                        <td valign="top" style="padding-left:5px;padding-top:5px;">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td valign="top">
                                            <p>The use of Zuma<sup>TM</sup> and other services (collectively, the "Service") offered by Zuma, Inc. ("Zuma", "We", "Us", "Our") require your acceptance of the terms, conditions and notices contained herein.
                                                Your use of the Service constitutes your agreement to all such terms, conditions and notices. You warrant that you will not use the Service for any purpose that is unlawful or prohibited by these terms, conditions and notices.
                                                Please review the terms of this agreement carefully. If you do not wish to agree to be bound by the terms of this agreement, do not complete the registration process and do not use the Service.</p>
                                            <h1>Your Representations</h1>
                                            <p>You represent and warrant that you are at least 18 years of age. If you are entering into this agreement on behalf of a corporation, partnership, or other organization having its own legal existence,
                                                you represent and warrant, on its behalf, that the organization has been duly formed and is in good standing with all required jurisdictions. You represent that you are duly authorized to enter into this agreement on its behalf.
                                                You certify that all registration information you have provided to us is complete and accurate in all material respects. If we ever have grounds to suspect otherwise, we may at our discretion suspend your access to the Service.
                                            </p>
                                            <h1>User Restrictions and Limitations</h1>
                                            <p>This Service is for your personal and noncommercial use. You may not modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer or sell any information, software,
                                                products or services obtained through your use of the Service.
                                            </p>
                                            <p>You are the only person authorized to use your Sign In information and for maintaining the confidentiality of your Sign In information.
                                                You shall not permit or allow other persons to have access to or use your Sign In information. You expressly agree not to provide access to any third parties, particularly those who may be in competition with Us,
                                                and assume all responsibility for acts of such parties in connection with any unauthorized use of your Sign In information. You will not impersonate anyone else or pretend to represent another individual or entity in connection with the use of the Service.
                                            </p>
                                            <p>You agree that non-permitted use, access to the Service or disclosure of confidential information in a manner inconsistent with the provisions of this agreement may cause irreparable harm and damage for which adequate remedy at law may not be available.
                                                Accordingly, we will be entitled to obtain temporary or permanent injunctive relief against such breach or violation from any court of competent jurisdiction immediately upon request.
                                                Our right to obtain injunctive relief shall not limit in any manner our right to seek additional remedies, at law or in equity, including, without limitation, specific performance.</p>
                                            <h1>Copyright &amp; Trademark Notice</h1>
                                            <p>All content of the Service is copyright protected by Zuma, Inc.  All rights reserved. Other product and company names mentioned herein may be the trademarks of their respective owners.
                                            </p>
                                            <h1>Security and Privacy Notice</h1>
                                            <p>Maintaining a high degree of confidence from you regarding the protection and secure transmission of all personal, company and financial information obtained through the Service is of the utmost importance to us.
                                                Our Privacy Policy discloses our privacy policy and security practices, which may be amended periodically.
                                            </p>
                                            <h1>Services</h1>
                                            <p class="blue">Use of Zuma<sup>TM</sup></p>
                                            <p>We will provide you with limited, non-exclusive access to license and use the Service in accordance with the relevant service plan you choose. The license grant is expressly conditioned on (i) your agreement to be bound by this agreement,
                                                (ii) your payment of any agreed upon service fees, and (iii) your compliance with this agreement and the terms and conditions of the relevant service plan. Your license to use the Service is not transferable.</p>
                                            <p class="blue">Integration Services</p>
                                            <p>Certain service plans include features that allow you to import information, as applicable, from your point of sale (POS) system, business bank accounts, payroll processors and other financial software application such as QuickBooks.
                                                You are ultimately responsible for verifying the accuracy of the information that is imported from these various parties.</p>
                                            <h1>Pricing and Payment</h1>
                                            <p>Prices vary by plan and are determined at the time you begin service. Prices are subject to change with 30 days notice. Payments will be billed to you in U.S. dollars, and your account will be debited in accordance with the terms outlined when you subscribe and provide your payment information.</p>
                                            <p>You must pay using (i) a valid credit card; (ii) a valid debit card or (iii) electronic debit (ACH). We will automatically renew your monthly or annual service plan at the then current rates, unless this agreement is terminated 30 days prior to your next payment date.</p>
                                            <p>If your payment and registration information is not accurate, current, and complete and you do not notify us promptly when such information changes, we may suspend or terminate your account and refuse any use of the Services. </p>
                                            <h1>Term and Termination</h1>
                                            <p>This agreement will continue in perpetuity unless terminated by one of the parties. Either party may terminate the agreement with or without cause at any time with 30 days written notice and without liability or continuing obligation to the other party,
                                                except as it relates to confidentiality, intellectual property right protections and any fees earned by us through the date of termination.</p>
                                            <h1>Warranty Disclaimer</h1>
                                            <p>ZUMA DOES NOT MAKE ANY REPRESENTATIONS ABOUT THE SUITABILITY OF THE SERVICE FOR ANY PURPOSE. THE SERVICE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND.
                                               WE HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH REGARD TO THE SERVICE, INCLUDING, WITHOUT LIMITATION, ALL IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NONINFRINGEMENT.
                                               IN NO EVENT SHALL WE BE LIABLE FOR ANY DIRECT, INDIRECT, PUNITIVE, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE SERVICE OR WITH ANY DELAY OR INABILITY TO USE THE SERVICE, OR FOR ANY INFORMATION, SOFTWARE, PRODUCTS OR SERVICES ARISING OUT OF THE USE OF THE SERVICE,
                                               WHETHER BASED ON CONTRACT, TORT, STRICT LIABILITY OR OTHERWISE, EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF DAMAGES.
                                               BECAUSE SOME STATES/JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU.</p>
                                            <h1>Dispute Resolution</h1>
                                            <p>If any dispute arises among the parties hereto, the parties agree first to try in good faith to settle the dispute through arbitration.  The costs of any arbitration proceeding shall be shared equally by all parties. If disputes cannot be resolved through arbitration, then all disputes arising out of or relating to this agreement, or its interpretation, validity, or enforcement, are to be decided in accordance with the laws of the United States of America and the State of New York, without reference or giving effect to any conflicts of law principles of any state or other jurisdiction.</p>
                                            <h1>General</h1>
                                            <p>You agree that no joint venture, partnership, employment or agency relationship exists between you and us as a result of this agreement or use of the Service. Nothing contained in the Service shall be construed to prevent us from complying with law enforcement requests or requirements relating to your use of the Service or information provided to or gathered by us with respect to such use.
                                               If any part of this agreement is determined to be invalid or unenforceable pursuant to applicable law including, but not limited to, the warranty disclaimers and liability limitations set forth above, then the invalid or unenforceable provision will be deemed superseded by a valid, enforceable provision that most closely matches the intent of the original provision, and the remainder of the agreement shall continue in effect.
                                               A printed version of this agreement and of any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. We may assign or transfer this agreement without your consent to (i) a parent company or direct or indirect subsidiary,
                                               (ii) another company through a sale of stock or assets by Us or (iii) a successor by merger. Any rights not expressly granted herein are reserved. This Terms of Use represents the entire agreement between the parties. We may change the Terms of Use periodically, and the changes will be effective when posted to our site or when we notify you by other means. Please review the Terms of Use periodically on our site for changes.
                                               Your continued use of the Service after changes are posted to our site constitutes your acceptance of such changes.</p>
                                            
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                </tbody>
            </table>
            <f:subview id="btm">
                <jsp:include page="includes/bottom.jsp" />
            </f:subview>
        </f:view>
    </body>
</html>